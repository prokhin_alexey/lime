/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "jabberprotocol.h"
#include "jabberaccount.h"
#include "jabberaccountcreator.h"
#include "jabberbookmarks.h"
#include "jabberactivity.h"
#include "jabbermood.h"
#include "jabbertune.h"
#include "jabbercontact.h"
#include "jabbervcards.h"
#include <lime/config.h>
#include <lime/icon.h>

namespace Lime
{

static JabberProtocol *proto = 0;

Jreen::Presence::Type statusToPresence(Status status)
{
	if (status == Status::Offline)
		return Jreen::Presence::Unavailable;
	else if (status == Status::NA)
		return Jreen::Presence::XA;
	else if (status == Status::DND)
		return Jreen::Presence::DND;
	else if (status == Status::Away)
		return Jreen::Presence::Away;
	else if (status == Status::FreeChat)
		return Jreen::Presence::Chat;
	else if (status == Status::Invisible)
		return Jreen::Presence::XA;
	else if (status & Status::Online)
		return Jreen::Presence::Available;
	else
		return Jreen::Presence::Invalid;
}

Status presenceToStatus(Jreen::Presence::Type presence)
{
	switch (presence) {
	case Jreen::Presence::XA:
		return Status::NA;
	case Jreen::Presence::DND:
		return Status::DND;
	case Jreen::Presence::Away:
		return Status::Away;
	case Jreen::Presence::Chat:
		return Status::FreeChat;
	case Jreen::Presence::Available:
		return Status::Online;
	default:
		return Status::Offline;
	}
}

QString errorString(const QSharedPointer<Jreen::Error> &error)
{
	switch (error->condition()) {
	case Jreen::Error::BadRequest:
		return QT_TR_NOOP("The sender has sent XML that is malformed or that cannot be processed.");
	case Jreen::Error::Conflict:
		return QT_TR_NOOP("Access cannot be granted because an existing resource or session exists with the same name or address.");
	case Jreen::Error::FeatureNotImplemented:
		return QT_TR_NOOP("The feature requested is not implemented by the recipient or server and therefore cannot be processed.");
	case Jreen::Error::Forbidden:
		return QT_TR_NOOP("The requesting entity does not possess the required permissions to perform the action.");
	case Jreen::Error::Gone:
		return QT_TR_NOOP("The recipient or server can no longer be contacted at this address.");
	case Jreen::Error::InternalServerError:
		return QT_TR_NOOP("The server could not process the stanza because of a misconfiguration or an otherwise-undefined internal server error.");
	case Jreen::Error::ItemNotFound:
		return QT_TR_NOOP("The addressed JID or item requested cannot be found.");
	case Jreen::Error::JidMalformed:
		return QT_TR_NOOP("The sending entity has provided or communicated an XMPP address or aspect thereof that does not adhere to the syntax defined in Addressing Scheme.");
	case Jreen::Error::NotAcceptable:
		return QT_TR_NOOP("The recipient or server understands the request but is refusing to process it because it does not meet criteria defined by the recipient or server.");
	case Jreen::Error::NotAllowed:
		return QT_TR_NOOP("The recipient or server does not allow any entity to perform the action.");
	case Jreen::Error::NotAuthorized:
		return QT_TR_NOOP("The sender must provide proper credentials before being allowed to perform the action, or has provided improper credentials.");
	case Jreen::Error::NotModified:
		return QT_TR_NOOP("The item requested has not changed since it was last requested.");
	case Jreen::Error::PaymentRequired:
		return QT_TR_NOOP("The requesting entity is not authorized to access the requested service because payment is required.");
	case Jreen::Error::RecipientUnavailable:
		return QT_TR_NOOP("The intended recipient is temporarily unavailable.");
	case Jreen::Error::Redirect:
		return QT_TR_NOOP("The recipient or server is redirecting requests for this information to another entity, usually temporarily.");
	case Jreen::Error::RegistrationRequired:
		return QT_TR_NOOP("The requesting entity is not authorized to access the requested service because registration is required.");
	case Jreen::Error::RemoteServerNotFound:
		return QT_TR_NOOP("A remote server or service specified as part or all of the JID of the intended recipient does not exist.");
	case Jreen::Error::RemoteServerTimeout:
		return QT_TR_NOOP("A remote server or service specified as part or all of the JID of the intended recipient could not be contacted within a reasonable amount of time.");
	case Jreen::Error::ResourceConstraint:
		return QT_TR_NOOP("The server or recipient lacks the system resources necessary to service the request.");
	case Jreen::Error::ServiceUnavailable:
		return QT_TR_NOOP("The server or recipient does not currently provide the requested service.");
	case Jreen::Error::SubscriptionRequired:
		return QT_TR_NOOP("The requesting entity is not authorized to access the requested service because a subscription is required.");
	case Jreen::Error::UndefinedCondition:
		return QT_TR_NOOP("The unknown error condition.");
	case Jreen::Error::UnexpectedRequest:
		return QT_TR_NOOP("The recipient or server understood the request but was not expecting it at this time.");
	case Jreen::Error::UnknownSender:
		return QT_TR_NOOP("The stanza 'from' address specified by a connected client is not valid for the stream.");
	case Jreen::Error::Undefined:
		return QT_TR_NOOP("No stanza error occurred. You're just sleeping.");
	}
	return QString();
}

JabberPep::~JabberPep()
{
	delete m_action;
}

void JabberPep::setAction(const QIcon &icon, const QString &title, const function<void(JabberAccount*)> &callback)
{
	m_action = new Action(icon, title, JabberProtocol::instance());
	m_action->setPriority(Action::LowPriority);
	m_action->setEnabled(false, 0);
	ActionContainer::addAction<JabberAccount>(m_action);
	lconnect(m_action, SIGNAL(triggered(QObject*,bool)), m_action, callback);
}

JabberProtocol::JabberProtocol() :
	Protocol(QLatin1String("jabber")), m_joinAction(0)
{
	Q_ASSERT(!proto);
	proto = this;

	auto addAction = [](Status status) {
		Account::addStatusAction<JabberAccount>(defaultStatusName(status), status);
	};

	addAction(Status::Online);
	addAction(Status::Offline);
	addAction(Status::Away);
	addAction(Status::DND);
	addAction(Status::NA);
	addAction(Status::FreeChat);
	addAction(Status::Invisible);

	JabberContact::supportedExtendedInfoHook.connect(this, [=](ExtendedInfoDescriptions &descriptions) {
		foreach (auto pep, m_peps)
			descriptions.insert(pep->name(), pep->description());
		descriptions.insert("client", tr("Show client icon"));
	});
}

JabberProtocol::~JabberProtocol()
{
	qDeleteAll(m_peps);
}

bool JabberProtocol::enableImpl()
{
	AccountManager::addWizard(new JabberAccountCreator(Icon("jabber"), tr("Jabber")));

	addPep<JabberActivityPep>();
	addPep<JabberMoodPep>();
	addPep<JabberTunePep>();

	auto cfg = config("general");
	foreach (auto &jid, cfg.value("accounts").toStringList())
		addAccount(new JabberAccount(jid));

	auto action = new Action(Icon("utilities-terminal"), tr("Open XML console"), this);
	auto additionMenu = ActionContainer::submenuAction<JabberAccount>("additional", tr("Additional"));
	additionMenu->setPriority(Action::LowestPriority - 5000);
	additionMenu->addSubAction<JabberAccount>(action);
	lconnect(action, SIGNAL(triggered(QObject*,bool)), this, [](JabberAccount *acc) {
		acc->openXmlConsole();
	});

	m_joinAction = new Action(Icon("im-chat"), tr("Join conference..."), this);
	m_joinAction->setPriority(Action::LowestPriority + 3000);
	m_joinAction->setEnabled(false, 0);
	lconnect(m_joinAction, SIGNAL(triggered(QObject*,bool)), this, [](JabberAccount *acc) {
		acc->bookmarksManager()->showJoinConferenceWidget();
	});
	ActionContainer::addAction<JabberAccount>(m_joinAction);

	m_serviceBrowserAction = new Action(Icon("defaultservice"), tr("Service browser..."), this);
	additionMenu->addSubAction<JabberAccount>(m_serviceBrowserAction);
	lconnect(m_serviceBrowserAction, SIGNAL(triggered(QObject*,bool)), this, [](JabberAccount *acc) {
		acc->showServiceBrowser();
	});

	action = new Action(Icon("dialog-information-user"), tr("Show information..."), this);
	action->setType(Action::ContextMenuAction | Action::MainToolbarAction);
	action->setPriority(Action::HighPriority - 3000);
	action->setEnabled(false, 0);
	action->setProxyContainerGetter([](ActionContainer *container) {
		if (auto contact = qobject_cast<JabberContact*>(container->toQObject()))
			container = contact->account();
		return container;
	});
	lconnect(action, SIGNAL(triggered(QObject*,bool)), this, [](JabberContact *contact) {
		auto infoWidget = new JabberVCardWindow(contact);
		infoWidget->setAttribute(Qt::WA_DeleteOnClose);
		infoWidget->show();
		centerizeWidget(infoWidget);
	});
	ActionContainer::addAction<JabberContact>(action);
	m_onlineActions << action;

	action = new Action(Icon("dialog-information-user"), tr("Edit information..."), this);
	action->setPriority(Action::LowestPriority + 2000);
	action->setEnabled(false, 0);
	lconnect(action, SIGNAL(triggered(QObject*,bool)), this, [](JabberAccount *account) {
		auto infoWidget = new JabberVCardWindow(account);
		infoWidget->setAttribute(Qt::WA_DeleteOnClose);
		infoWidget->show();
		centerizeWidget(infoWidget);
	});
	ActionContainer::addAction<JabberAccount>(action);
	m_onlineActions << action;

	return true;
}

bool JabberProtocol::disableImpl()
{
	return false;
}

bool JabberProtocol::hasAccount(const QString &jid) const
{
	foreach (auto acc, accounts()) {
		if (acc->id() == jid)
			return true;
	}
	return false;
}

void JabberProtocol::addAccount(const QString &jid, const QString &password)
{
	auto cfg = config("general");
	QStringList accounts = cfg.value("accounts", QStringList());
	accounts << jid;
	cfg.setValue("accounts", accounts);

	auto acc = new JabberAccount(jid);
	if (!password.isEmpty())
		acc->config("general").setValue("passwd", password);
	addAccount(acc);
}

JabberProtocol* JabberProtocol::instance()
{
	Q_ASSERT(proto);
	return proto;
}

} // Lime
