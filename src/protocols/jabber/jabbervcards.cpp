/*
	 This file is part of Lime.
	 Copyright (C) 2011-2012 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "jabbervcards.h"
#include "jabbercontact.h"
#include "jabberconference.h"
#include <QCryptographicHash>
#include <lime/config.h>
#include <jreen/vcardupdate.h>

#include <QVBoxLayout>
#include <QFormLayout>
#include <QTextEdit>
#include <QLineEdit>
#include <QDateEdit>
#include <QGroupBox>
#include <QUrl>
#include <QFrame>

namespace Lime
{

JabberVCardManager::JabberVCardManager(JabberAccount *account) :
	QObject(account), m_account(account)
{
	connect(account->client(), SIGNAL(iqReceived(Jreen::IQ)), SLOT(handleIQ(Jreen::IQ)));

	lconnect(account, SIGNAL(statusChanged(Lime::Status,Lime::Status)), this, [=](Lime::Status status) {
		if (status == Offline) {
			Jreen::VCard::Ptr vcard;
			foreach (auto &request, m_requests) {
				if (request.guard || !request.isGuarded)
					request.callback(vcard, true);
			}
		}
	});
}

JabberVCardManager::~JabberVCardManager()
{
}

void JabberVCardManager::request(const QString &jid, QObject *guard, const RequestCallback &callback)
{
	if (m_account->status() == Offline) {
		callback(Jreen::VCard::Ptr(), true);
		return;
	}

	bool contained = m_requests.contains(jid);
	m_requests.insert(jid, { callback, guard, guard != 0 });
	if (contained)
		return;

	Jreen::IQ iq(Jreen::IQ::Get, jid);
	iq.addExtension(new Jreen::VCard());
	account()->client()->send(iq, this, SLOT(handleIQ(Jreen::IQ,int)), 0);
}

void JabberVCardManager::storeVCard(const Jreen::VCard::Ptr &vcard, QObject *receiver, const char *slot)
{
	Jreen::IQ iq(Jreen::IQ::Set, Jreen::JID());
	iq.addExtension(vcard);
	account()->client()->send(iq, receiver, slot, 0);
}

void JabberVCardManager::handleVCard(const Jreen::VCard::Ptr vcard, const QString &id)
{
	const auto &photo = vcard->photo();
	auto account = this->account();
	QString avatarHash;
	QString avatarPath;
	if (!photo.data().isEmpty()) {
		avatarHash = QCryptographicHash::hash(photo.data(), QCryptographicHash::Sha1).toHex();
		avatarPath = account->storeAvatar(photo.data(), avatarHash);
	}

	if (account->id() == id) {
		auto presence = account->client()->presence();
		auto update = presence.payload<Jreen::VCardUpdate>();
		if (update->photoHash() != avatarHash) {
			account->config("general").setValue("photoHash", avatarHash);
			update->setPhotoHash(avatarHash);
			account->client()->send(presence);
		}
	} else if (auto contact = account->contact(id, false)) {
		contact->updateAvatar(avatarPath);
	} else {
		Jreen::JID jid(id);
		if (auto conf = account->conference(jid.bare(), false)) {
			if (auto mucUser = conf->participant(jid.resource())) {
				mucUser->updateAvatar(avatarPath);
			}
		}
	}
}

void JabberVCardManager::handleIQ(const Jreen::IQ &iq)
{
	if(!iq.containsPayload<Jreen::VCard>())
		return;

	iq.accept();
	auto id = iq.from().full();
	auto vcard = iq.payload<Jreen::VCard>();
	handleVCard(vcard, id);

	foreach (auto &request, m_requests.values(id)) {
		if (request.guard || !request.isGuarded)
			request.callback(vcard, iq.subtype() == Jreen::IQ::Error ? true : false);
	}
	m_requests.remove(id);
}

JabberVCardWindow::JabberVCardWindow(JabberContact *contact) :
	ContactInformationWindow(contact, true),
	m_jid(contact->id()),
	m_manager(contact->account()->vcardManager()),
	m_requesting(false)
{
	initialize();
}

JabberVCardWindow::JabberVCardWindow(JabberAccount *account) :
	ContactInformationWindow(account, false),
	m_jid(account->id()),
	m_manager(account->vcardManager()),
	m_requesting(false)
{
	initialize();
}

void JabberVCardWindow::initialize()
{
	m_vcardHandler = [=](Jreen::VCard::Ptr, bool) {};

	lconnect(m_manager->account(), SIGNAL(statusChanged(Lime::Status,Lime::Status)), this,
		[=](Lime::Status status)
	{
		setOfflineMode(status == Offline);
	});
}

void JabberVCardWindow::initializeFullInfo()
{
	auto createField = [=] {
		auto w = new QLineEdit(this);
		w->setReadOnly(isReadOnly());
		return w;
	};

	// General page
	auto generalPage   = new QFrame(this);
	auto generalLayout = new QFormLayout(generalPage);
	auto firstName     = createField();
	auto middleName    = createField();
	auto lastName      = createField();
	auto nick          = createField();
	auto birthday      = new QDateEdit(this);
	auto homePageAddr  = createField();
	generalLayout->addRow(tr("First name:"),  firstName);
	generalLayout->addRow(tr("Middle name:"), middleName);
	generalLayout->addRow(tr("Last name:"),   lastName);
	generalLayout->addRow(tr("Nickname:"),    nick);
	generalLayout->addRow(tr("Birthday:"),    birthday);
	generalLayout->addRow(tr("Home page:"),   homePageAddr);
	addPage(tr("General"), generalPage);
	generalPage->setFrameShape(QFrame::Panel);
	birthday->setReadOnly(isReadOnly());
	birthday->setDisplayFormat("dd.MM.yyyy");

	// Home
	auto homePage           = new QWidget(this);
	auto homeLayout         = new QVBoxLayout(homePage);
	auto homeAddressGroup   = new QGroupBox(tr("Address"), this);
	auto homeAddressLayout  = new QFormLayout(homeAddressGroup);
	auto homeContactsGroup  = new QGroupBox(tr("Contact information"), this);
	auto homeContactsLayout = new QFormLayout(homeContactsGroup);
	auto homeStreet         = createField();
	auto homeExtendedAddr   = createField();
	auto homeCountry        = createField();
	auto homeRegion         = createField();
	auto homeCity           = createField();
	auto homePostcode       = createField();
	auto homePostbox        = createField();
	auto homePhones         = createField();
	auto homeCells          = createField();
	auto homeEmails         = createField();
	homeLayout->addWidget(homeAddressGroup);
	homeLayout->addWidget(homeContactsGroup);
	homeAddressLayout->addRow(tr("Country:"),   homeCountry);
	homeAddressLayout->addRow(tr("State:"),     homeRegion);
	homeAddressLayout->addRow(tr("City:"),      homeCity);
	homeAddressLayout->addRow(tr("Post code:"), homePostcode);
	homeAddressLayout->addRow(tr("Post box:"),  homePostbox);
	homeAddressLayout->addRow(tr("Street:"),    homeStreet);
	homeAddressLayout->addRow(tr(""),           homeExtendedAddr);
	homeContactsLayout->addRow(tr("Phones:"),   homePhones);
	homeContactsLayout->addRow(tr("Cells:"),    homeCells);
	homeContactsLayout->addRow(tr("Emails:"),   homeEmails);
	addPage(tr("Home"), homePage);

	// Work
	auto workPage             = new QWidget(this);
	auto workLayout           = new QVBoxLayout(workPage);
	auto workAddressGroup     = new QGroupBox(tr("Address"), this);
	auto workAddressLayout    = new QFormLayout(workAddressGroup);
	auto workContactsGroup    = new QGroupBox(tr("Contact information"), this);
	auto workContactsLayout   = new QFormLayout(workContactsGroup);
	auto workOccupationGroup  = new QGroupBox(tr("Occupation"), this);
	auto workOccupationLayout = new QFormLayout(workOccupationGroup);
	auto workStreet           = createField();
	auto workExtendedAddr     = createField();
	auto workCountry          = createField();
	auto workRegion           = createField();
	auto workCity             = createField();
	auto workPostcode         = createField();
	auto workPostbox          = createField();
	auto workPhones           = createField();
	auto workEmails           = createField();
	auto workCompany          = createField();
	auto workDepartment       = createField();
	auto workPosition         = createField();
	auto workRole             = createField();
	workLayout->addWidget(workAddressGroup);
	workLayout->addWidget(workOccupationGroup);
	workLayout->addWidget(workContactsGroup);
	workAddressLayout->addRow(tr("Country:"),        workCountry);
	workAddressLayout->addRow(tr("State:"),          workRegion);
	workAddressLayout->addRow(tr("City:"),           workCity);
	workAddressLayout->addRow(tr("Post code:"),      workPostcode);
	workAddressLayout->addRow(tr("Post box:"),       workPostbox);
	workAddressLayout->addRow(tr("Street:"),         workStreet);
	workAddressLayout->addRow(tr(""),                workExtendedAddr);
	workContactsLayout->addRow(tr("Phones:"),        workPhones);
	workContactsLayout->addRow(tr("Emails:"),        workEmails);
	workOccupationLayout->addRow(tr("Company:"),     workCompany);
	workOccupationLayout->addRow(tr("Departments:"), workDepartment);
	workOccupationLayout->addRow(tr("Position:"),    workPosition);
	workOccupationLayout->addRow(tr("Role:"),        workRole);
	addPage(tr("Work"), workPage);

	// Notes
	auto notesPage = new QTextEdit(this);
	addPage(tr("Notes"), notesPage);

	m_vcardHandler = [=](Jreen::VCard::Ptr vcard, bool error) {
		Q_UNUSED(error);
		if (!vcard)
			return;

		Jreen::VCard::Address homeAddr, workAddr;
		findAddresses(homeAddr, workAddr);

		QString homePhonesStr, workPhonesStr, cellsStr;
		foreach (auto &phone, vcard->telephones()) {
			if (phone.testType(Jreen::VCard::Telephone::Home))
				appendString(homePhonesStr, phone.number());
			if (phone.testType(Jreen::VCard::Telephone::Work))
				appendString(workPhonesStr, phone.number());
			if (phone.testType(Jreen::VCard::Telephone::Cell))
				appendString(cellsStr, phone.number());
		}

		QString homeEmailsStr, workEmailsStr;
		foreach (auto &email, vcard->emails()) {
			if (email.testType(Jreen::VCard::EMail::Home))
				appendString(homeEmailsStr, email.userId());
			if (email.testType(Jreen::VCard::EMail::Work))
				appendString(workEmailsStr, email.userId());
		}

		// General
		auto name = vcard->name();
		firstName->setText(name.given());
		middleName->setText(name.middle());
		lastName->setText(name.family());
		nick->setText(vcard->nickname());
		birthday->setDate(vcard->birthday().date());
		homePageAddr->setText(vcard->url().toString());

		// Home
		homeStreet->setText(homeAddr.street());
		homeExtendedAddr->setText(homeAddr.extendedAddress());
		homeCountry->setText(homeAddr.country());
		homeRegion->setText(homeAddr.region());
		homeCity->setText(homeAddr.locality());
		homePostcode->setText(homeAddr.postCode());
		homePostbox->setText(homeAddr.postBox());
		homePhones->setText(homePhonesStr);
		homeCells->setText(cellsStr);
		homeEmails->setText(homeEmailsStr);

		// Work
		auto org = vcard->organization();
		workStreet->setText(workAddr.street());
		workExtendedAddr->setText(workAddr.extendedAddress());
		workCountry->setText(workAddr.country());
		workRegion->setText(workAddr.region());
		workCity->setText(workAddr.locality());
		workPostcode->setText(workAddr.postCode());
		workPostbox->setText(workAddr.postBox());
		workPhones->setText(workPhonesStr);
		workEmails->setText(workEmailsStr);
		workCompany->setText(org.name());
		workDepartment->setText(org.units().join(", "));
		workPosition->setText(vcard->title());
		workRole->setText(vcard->role());

		// Notes
		notesPage->setPlainText(vcard->desc());

		// Avatar
		if (auto contact = object<JabberContact>())
			setAvatar(contact->avatar());
		else if (auto account = object<JabberAccount>())
			setAvatar(account->avatar());
	};

	if (isReadOnly())
		return;

	m_vcardSaver = [=] {
		Jreen::VCard::Ptr vcard(new Jreen::VCard());

		auto addPhones = [=, &vcard](QLineEdit *edit, Jreen::VCard::Telephone::Type type) {
			foreach (auto &phoneStr, edit->text().split(',', QString::SkipEmptyParts)) {
				Jreen::VCard::Telephone phone;
				phone.setNumber(phoneStr.trimmed());
				phone.setType(type, true);
				vcard->addTelephone(phone);
			}
		};

		auto addEmails = [=, &vcard](QLineEdit *edit, Jreen::VCard::EMail::Type type) {
			foreach (auto &emailStr, edit->text().split(',', QString::SkipEmptyParts)) {
				Jreen::VCard::EMail email;
				email.setUserId(emailStr.trimmed());
				email.setType(type, true);
				vcard->addEmail(email);
			}
		};

		// General
		auto name = vcard->name();
		name.setGiven(firstName->text());
		name.setMiddle(middleName->text());
		name.setFamily(lastName->text());
		vcard->setNickname(nick->text());
		vcard->setBirthday(birthday->date());
		vcard->setUrl(QUrl(homePageAddr->text()));

		// Home
		Jreen::VCard::Address addr;
		addr.setStreet(homeStreet->text());
		addr.setExtendedAddress(homeExtendedAddr->text());
		addr.setCountry(homeCountry->text());
		addr.setRegion(homeRegion->text());
		addr.setLocality(homeCity->text());
		addr.setPostCode(homePostcode->text());
		addr.setPostBox(homePostbox->text());
		addr.setType(Jreen::VCard::Address::Home, true);
		vcard->addAdress(addr);
		addPhones(homePhones, Jreen::VCard::Telephone::Home);
		addPhones(homeCells,  Jreen::VCard::Telephone::Cell);
		addEmails(homeEmails, Jreen::VCard::EMail::Home);

		// Work
		addr = Jreen::VCard::Address();
		addr.setStreet(workStreet->text());
		addr.setExtendedAddress(workExtendedAddr->text());
		addr.setCountry(workCountry->text());
		addr.setRegion(workRegion->text());
		addr.setLocality(workCity->text());
		addr.setPostCode(workPostcode->text());
		addr.setPostBox(workPostbox->text());
		addr.setType(Jreen::VCard::Address::Work, true);
		vcard->addAdress(addr);
		addPhones(workPhones, Jreen::VCard::Telephone::Work);
		addEmails(workEmails, Jreen::VCard::EMail::Work);

		Jreen::VCard::Organization org;
		org.setName(workCompany->text());
		foreach (auto &unit, workDepartment->text().split(',', QString::SkipEmptyParts))
			org.addUnit(unit.trimmed());
		vcard->setOrganization(org);
		vcard->setTitle(workPosition->text());
		vcard->setRole(vcard->role());

		// Notes
		vcard->setDesc(notesPage->toPlainText());

		// Avatar
		auto newAvatar = avatar();
		if (newAvatar != getCurrentAvatar())
			vcard->setPhoto(newAvatar);
		else
			vcard->setPhoto(m_vcard->photo());

		m_vcard = vcard;
		return vcard;
	};
}

void JabberVCardWindow::findAddresses(Jreen::VCard::Address &homeAddr, Jreen::VCard::Address &workAddr)
{
	foreach (auto &addr, m_vcard->addresses()) {
		if (addr.testType(Jreen::VCard::Address::Home))
			homeAddr = addr;
		if (addr.testType(Jreen::VCard::Address::Work))
			workAddr = addr;
	}
}

void JabberVCardWindow::appendString(QString &target, const QString &str)
{
	if (!target.isEmpty())
		target += ", ";
	target += str;
}

QString JabberVCardWindow::getCurrentAvatar() const
{
	if (auto contact = object<JabberContact>())
		return contact->avatar();
	else if (auto account = object<JabberAccount>())
		return account->avatar();
	return QString();
}

void JabberVCardWindow::initializeShortInfo()
{
	if (!m_vcard)
		return;

	auto addAddress = [=](const Jreen::VCard::Address &addr) {
		QString street = addr.street();
		QString extended = addr.extendedAddress();
		if (!street.isEmpty() && !extended.isEmpty())
			street += "\n" + extended;

		addEntry(tr("Country"),   addr.country());
		addEntry(tr("State"),     addr.region());
		addEntry(tr("City"),      addr.locality());
		addEntry(tr("Post code"), addr.postCode());
		addEntry(tr("Post box"),  addr.postBox());
		addEntry(tr("Street"),    street);
	};

	Jreen::VCard::Address homeAddr, workAddr;
	findAddresses(homeAddr, workAddr);

	beginGroup(tr("General"));
	auto name = m_vcard->name();
	auto birthday = m_vcard->birthday();
	addEntry(tr("First name"),   name.given());
	addEntry(tr("Middle name"),  name.middle());
	addEntry(tr("Last name"),    name.family());
	addEntry(tr("Nickname"),     m_vcard->nickname());
	if (birthday.isValid())
		addEntry(tr("Birthday"), m_vcard->birthday().toString("dd.MM.yyyy"));
	addEntry(tr("Home page"),    m_vcard->url().toString());
	endGroup();

	beginGroup(tr("Home address"));
	addAddress(homeAddr);
	endGroup();

	beginGroup(tr("Work address"));
	addAddress(workAddr);
	endGroup();

	beginGroup(tr("Work"));
	auto org = m_vcard->organization();
	addEntry(tr("Company"),     org.name());
	addEntry(tr("Departments"), org.units().join(", "));
	addEntry(tr("Position"),    m_vcard->title());
	addEntry(tr("Role"),        m_vcard->role());
	endGroup();

	beginGroup(tr("Phones"));
	foreach (auto &phone, m_vcard->telephones()) {
		QString number = phone.number();
		QString type;
		if (phone.testType(Jreen::VCard::Telephone::Home))
			appendString(type, tr("home"));
		if (phone.testType(Jreen::VCard::Telephone::Work))
			appendString(type, tr("work"));
		if (phone.testType(Jreen::VCard::Telephone::Cell))
			appendString(type, tr("cell"));
		if (!type.isEmpty())
			number = QString("%1 (%2)").arg(number).arg(type);
		addEntry(number);
	}
	endGroup();

	beginGroup(tr("Emails"));
	foreach (auto &email, m_vcard->emails()) {
		QString userId = email.userId();
		QString type;
		if (email.testType(Jreen::VCard::EMail::Home))
			appendString(type, tr("home"));
		if (email.testType(Jreen::VCard::EMail::Work))
			appendString(type, tr("work"));
		if (!type.isEmpty())
			userId = QString("%1 (%2)").arg(userId).arg(type);
		addEntry(userId);
	}
	endGroup();

	beginGroup(tr("Notes"));
	addEntry(m_vcard->desc());
	endGroup();
}

void JabberVCardWindow::requestInfoImpl()
{
	if (m_manager->account()->status() == Offline) {
		infoReceived(AccountIsOfflineError);
		return;
	}
	if (m_requesting)
		return;
	m_requesting = true;
	m_manager->request(m_jid, this, [=](Jreen::VCard::Ptr vcard, bool error) {
		m_vcard = vcard;
		m_vcardHandler(vcard, error);
		infoReceived(!error ? NoErrors : ProtocolError);
		m_requesting = false;
	});
}

void JabberVCardWindow::saveInfoImpl()
{
	if (m_manager->account()->status() == Offline) {
		infoUpdated(AccountIsOfflineError);
		return;
	}
	m_manager->storeVCard(m_vcardSaver(), this, SLOT(onIqReceived(Jreen::IQ,int)));
}

void JabberVCardWindow::onIqReceived(const Jreen::IQ &iq, int)
{
	//if (!iq.containsPayload<Jreen::VCard>())
	//	return;
	auto acc = m_manager->account();
	auto id = iq.from().full();
	if (acc->id() != id)
		return;
	if (iq.subtype() == Jreen::IQ::Result) {
		infoUpdated(NoErrors);
		m_manager->handleVCard(m_vcard, m_jid);
	} else if (iq.subtype() == Jreen::IQ::Error){
		infoUpdated(ProtocolError);
	}
}

} // Lime
