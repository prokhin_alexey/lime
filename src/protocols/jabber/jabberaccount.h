/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_JABBERACCOUNT_H
#define LIME_JABBERACCOUNT_H

#include <lime/account.h>
#include <jreen/client.h>
#include <jreen/privatexml.h>
#include <jreen/privacymanager.h>
#include <jreen/pubsubmanager.h>

namespace Lime {

class JabberContact;
class JabberRoster;
class JabberMessageManager;
class JabberBookmarks;
class JabberConference;
class JabberServiceDiscovery;
class JabberServiceBrowser;
class JabberVCardManager;
class JabberResourceInterface;
class JabberActivityDialog;
class JabberMoodDialog;
class JabberProtocol;
class ChatUnit;
class JabberResource;
class XmlConsole;

class JabberAccount : public TagAccount
{
	Q_OBJECT
public:
    JabberAccount(const QString &id);
    virtual ~JabberAccount();
	Jreen::Client *client() const { return m_client; }
	JabberRoster *roster() const { return m_roster; }
	Jreen::PrivateXml *privateXml() const { return m_privateXml; }
	Jreen::PubSub::Manager *pubSubManager() const { return m_pubsub; }
	JabberMessageManager *messageManager() const { return m_messageManager; }
	JabberBookmarks *bookmarksManager() const { return m_bookmarksManager; }
	JabberServiceDiscovery *serviceDiscovery() const { return m_serviceDiscovery; }
	JabberVCardManager *vcardManager() const { return m_vcardManager; }
	JabberContact *contact(const QString &id, bool create = false);
	JabberConference *conference(const QString &id, bool create = false);
	ChatUnit *unit(const Jreen::JID &jid);
	JabberResourceInterface *resource(const Jreen::JID &jid);
	QList<JabberContact*> contacts();
	JabberProtocol* protocol() const { return reinterpret_cast<JabberProtocol*>(Account::protocol()); }
	void showServiceBrowser();
	void showActivityDialog();
	void showMoodDialog();
	Jreen::JID jid() const { return m_client->jid(); }
	bool avatarsAutoLoad() const { return m_avatarsAutoLoad; }
	QSet<QString> features() const;
	bool checkFeature(const QString &feature) const;
	bool checkIdentity(const QString &category, const QString &type) const;
	QString identity(const QString &category, const QString &type) const;
public slots:
	void openXmlConsole();
protected:
    virtual bool enableImpl();
    virtual bool disableImpl();
    virtual Contact* createContact(const QString &id);
    virtual JabberContact* createContactImpl(const QString &id);
	virtual void setStatusImpl(Status status, const QString &statusText);
	virtual bool setExtendedInfoImpl(const QString &name, const QVariantMap &info);
private slots:
	void onDisconnected(Jreen::Client::DisconnectReason reason);
private:
	void onConnected();
	void sendPresence(Status status, const QString &text);
private:
	Jreen::Client          *m_client;
	JabberRoster           *m_roster;
	JabberMessageManager   *m_messageManager;
	Jreen::PrivateXml      *m_privateXml;
	Jreen::PrivacyManager  *m_privacyManager;
	XmlConsole             *m_xmlConsole;
	Jreen::PubSub::Manager *m_pubsub;
	JabberBookmarks        *m_bookmarksManager;
	JabberServiceDiscovery *m_serviceDiscovery;
	JabberVCardManager     *m_vcardManager;

	int m_priority;
	bool m_keepStatus;
	bool m_avatarsAutoLoad;
	QString m_nick;

	Pointer<JabberServiceBrowser> m_serviceBrowser;
	Pointer<JabberActivityDialog> m_activityDialog;
	Pointer<JabberMoodDialog>     m_moodDialog;

	enum LoadFlags
	{
		RosterLoaded      = 0x01,
		PrivacyListLoaded = 0x02,
		EveryModuleLoaded = RosterLoaded | PrivacyListLoaded
	};
	unsigned char m_loadFlags;
};

inline JabberContact *JabberAccount::contact(const QString &id, bool create)
{
	return reinterpret_cast<JabberContact*>(Account::contact(id, create));
}

inline QList<JabberContact*> JabberAccount::contacts()
{
	auto contacts = Account::contacts();
	return *reinterpret_cast<QList<JabberContact*>*>(&contacts);
}

}

#endif // LIME_JABBERACCOUNT_H
