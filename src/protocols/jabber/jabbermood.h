/*
	 This file is part of Lime.
	 Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIME_JABBERMOOD_H_
#define LIME_JABBERMOOD_H_

#include <lime/extendedstatusdialog.h>
#include <jreen/mood.h>
#include "jabberprotocol.h"

namespace Lime
{

class JabberAccount;

class JabberMoodDialog : public ExtendedStatusDialog
{
public:
	JabberMoodDialog(JabberAccount *account);
	virtual ~JabberMoodDialog();
};

class JabberMoodPep : public JabberPep
{
public:
	JabberMoodPep();
	virtual QVariantMap toMap(const Jreen::Payload::Ptr &payload);
	virtual Jreen::Payload::Ptr fromMap(const QVariantMap &info);
};

} // Lime
#endif // LIME_JABBERMOOD_H_
