/*
	 This file is part of Lime.
	 Copyright (C) 2011-2012 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
 	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIME_JABBERVCARDS_H_
#define LIME_JABBERVCARDS_H_

#include <jreen/vcard.h>
#include "jabberaccount.h"
#include <lime/contactinformationwindow.h>

class QLineEdit;

namespace Lime
{

class JabberAccount;
class JabberContact;

class JabberVCardManager : public QObject
{
	Q_OBJECT
public:
	typedef std::function<void(Jreen::VCard::Ptr, bool error)> RequestCallback;
	JabberVCardManager(JabberAccount *account);
	virtual ~JabberVCardManager();
	void request(const QString &jid, QObject *guard = 0, const RequestCallback &callback = [](Jreen::VCard::Ptr,bool){});
	void storeVCard(const Jreen::VCard::Ptr &vcard, QObject *receiver = 0, const char *slot = 0);
	inline JabberAccount *account() { return sender_cast<JabberAccount*>(parent()); }
	void handleVCard(const Jreen::VCard::Ptr vcard, const QString &id);
private slots:
	void handleIQ(const Jreen::IQ &iq);
	void handleIQ(const Jreen::IQ &iq, int) { handleIQ(iq); }
private:
	struct Request
	{
		RequestCallback callback;
		Pointer<QObject> guard;
		bool isGuarded;
	};
	QMultiMap<QString, Request> m_requests;
	JabberAccount *m_account;
};

class JabberVCardWindow : public ContactInformationWindow
{
	Q_OBJECT
public:
	JabberVCardWindow(JabberContact *contact);
	JabberVCardWindow(JabberAccount *account);
protected:
	virtual void initializeFullInfo();
	virtual void initializeShortInfo();
	virtual void requestInfoImpl();
	virtual void saveInfoImpl();
private:
	void findAddresses(Jreen::VCard::Address &homeAddr, Jreen::VCard::Address &workAddr);
	void appendString(QString &target, const QString &str);
	QString getCurrentAvatar() const;
	void initialize();
private slots:
	void onIqReceived(const Jreen::IQ &iq, int);
private:
	QString m_jid;
	JabberVCardManager *m_manager;
	JabberVCardManager::RequestCallback m_vcardHandler;
	std::function<Jreen::VCard::Ptr()> m_vcardSaver;
	bool m_fullInfoInitialized;
	Jreen::VCard::Ptr m_vcard;
	bool m_requesting;
};

} // Lime
#endif // LIME_JABBERVCARDS_H_
