/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_JABBERRESOURCE_H
#define LIME_JABBERRESOURCE_H

#include <lime/buddy.h>
#include <jreen/presence.h>

namespace Lime {

class JabberContact;
class JabberAccount;

class JabberResourceInterface
{
public:
	void setFeatures(const QSet<QString> &features) { m_features = features; }
	QSet<QString> features() { return m_features; }
	virtual Buddy *toBuddy() = 0;
private:
	QSet<QString> m_features;
};

class JabberResource : public Buddy, public JabberResourceInterface
{
public:
	JabberResource(JabberContact *contact, const QString &name, JabberAccount *account);
	void handlePresence(const Jreen::Presence &presence);
	void handleChatState(ChatState state) { ChatUnit::handleChatState(state); }
	JabberAccount *account() const { return reinterpret_cast<JabberAccount*>(Buddy::account()); }
protected:
	Buddy *toBuddy() { return this; }
	virtual SendMessageError sendMessageImpl(const Lime::Message &message);
	virtual void sendChatStateImpl(ChatUnit::ChatState state);
};

} // Lime

#endif // LIME_JABBERRESOURCE_H
