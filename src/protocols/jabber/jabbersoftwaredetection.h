/****************************************************************************
 *  jsoftwaredetection.h
 *
 *  Copyright (c) 2009 by Nigmatullin Ruslan <euroelessar@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This library is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef JSOFTWAREDETECTION_H
#define JSOFTWAREDETECTION_H

#include <QObject>
#include <QSet>
#include <QBasicTimer>
#include <QStringList>


namespace Jreen
{
class Presence;
class IQ;
class Disco;
}

namespace Lime
{

class Buddy;
class ChatUnit;
class JabberAccount;
class JabberResourceInterface;

class JabberSoftwareDetection : public QObject
{
	enum { RequestDisco, RequestSoftware };
	Q_OBJECT
public:
	struct SoftwareInfo
	{
		SoftwareInfo() : finished(false) {}
		QSet<QString> features;
		QString name;
		QString version;
		QString os;
		QString icon;
		QString description;
		bool finished;
	};
	typedef QHash<QString, SoftwareInfo> SoftwareInfoHash;

	JabberSoftwareDetection(JabberAccount *account);
	~JabberSoftwareDetection();
	
protected:
	void timerEvent(QTimerEvent *ev);
protected slots:
	void handlePresence(const Jreen::Presence &presence);
	void handleIQ(const Jreen::IQ &iq, int context);
private:
	void updateCache(const QString &node, const SoftwareInfo &info, bool fixed = false);
	void updateClientData(Buddy *resource, const QString &client,
						  const QString &software, const QString &softwareVersion,
						  const QString &os, const QString &clientIcon);
	void setClientInfo(Buddy *resource, const QString &client, const QString &clientIcon);
	QString getClientDescription(const QString &software, const QString &softwareVersion, const QString &os);
	QString getClientIcon(const QString &software);
private:
	JabberAccount *m_account;
	QHash<QString, SoftwareInfo> m_hash;
	QStringList m_recent;
	QBasicTimer m_timer;
};
}

#endif // JSOFTWAREDETECTION_H
