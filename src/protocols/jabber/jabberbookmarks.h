/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_JABBERBOOKMARKS_H
#define LIME_JABBERBOOKMARKS_H

#include <lime/joinconferencewidget.h>
#include <jreen/bookmarkstorage.h>
#include <lime/utils.h>

class QLineEdit;
class QCheckBox;

namespace Lime {

using Jreen::Bookmark;

class JabberAccount;
class JabberJoinConfWidget;

class JabberBookmarks : public QObject
{
public:
	explicit JabberBookmarks(JabberAccount *account);
	void sync();
	void clear();
	void setBookmarks(const QList<Jreen::Bookmark::Conference> &bookmarks);
	void showJoinConferenceWidget();
	QList<Bookmark::Conference> bookmarks() const { return m_bookmarks; }
	JabberAccount *account() const { return m_account; }
private:
	void updateConferences(bool allowJoin = false);
	JabberAccount *m_account;
	QList<Bookmark::Conference> m_bookmarks;
	Jreen::BookmarkStorage *m_storage;
	Pointer<JabberJoinConfWidget> m_joinWidget;
	bool m_isInited;
};

class JabberJoinConfWidget : public JoinConferenceWidget
{
public:
	explicit JabberJoinConfWidget(JabberBookmarks *manager);
	virtual ~JabberJoinConfWidget();
protected:
	virtual void createBookmark();
	virtual void join(const QVariant& userData);
	virtual void removeBookmark(const QVariant &userData);
	virtual void save();
	virtual void setBookmark(const QVariant& userData);
	void updateCurrent();
private:
	JabberBookmarks *m_manager;
	QList<Bookmark::Conference*> m_bookmarks;
	Bookmark::Conference *m_current;
	QLineEdit *m_name;
	QLineEdit *m_nick;
	QLineEdit *m_conference;
	QLineEdit *m_password;
	QCheckBox *m_autoJoin;
};

} // Lime

#endif // LIME_JABBERBOOKMARKS_H
