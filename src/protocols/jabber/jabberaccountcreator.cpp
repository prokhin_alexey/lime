/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "jabberaccountcreator.h"
#include "jabberprotocol.h"
#include <QFormLayout>
#include <QLineEdit>

namespace Lime
{

class CreatorPage : public QWizardPage
{
public:
    CreatorPage();
    virtual bool isComplete() const;
    virtual bool validatePage();
private:
	QLineEdit *m_jidEdit;
	QLineEdit *m_passwordEdit;
};

CreatorPage::CreatorPage()
{
    setTitle(tr("Please type chosen protocol login data"));
    setSubTitle(tr("Please fill all fields."));

	auto layout = new QFormLayout(this);
	m_jidEdit = new QLineEdit(this);
	m_passwordEdit = new QLineEdit(this);
	m_passwordEdit->setEchoMode(QLineEdit::Password);

	layout->addRow(tr("JID:"), m_jidEdit);
	layout->addRow(tr("Password"), m_passwordEdit);

	connect(m_jidEdit, SIGNAL(textChanged(QString)), SIGNAL(completeChanged()));
}

bool CreatorPage::isComplete() const
{
	auto jid = m_jidEdit->text();
    return !jid.isEmpty() &&
		   !JabberProtocol::instance()->hasAccount(jid);
}

bool CreatorPage::validatePage()
{
    if (isComplete()) {
		JabberProtocol::instance()->addAccount(m_jidEdit->text(), m_passwordEdit->text());
		return true;
	}
	return false;
}

void JabberAccountCreator::initializePages()
{
	addWizardPage(new CreatorPage);
}

} // Lime

