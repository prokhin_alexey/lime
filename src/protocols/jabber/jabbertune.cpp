/*
	 This file is part of Lime.
	 Copyright (c) 2010 by Nigmatullin Ruslan <euroelessar@gmail.com>
	 Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "jabbertune.h"
#include <lime/icon.h>
#include <QUrl>
#include "jreen/tune.h"

namespace Lime
{

JabberTunePep::JabberTunePep() :
	JabberPep("tune", Jreen::Tune::staticPayloadType(), QT_TRANSLATE_NOOP("Tune", "Show tune icon"))
{
}

JabberTunePep::~JabberTunePep()
{
}

#define ADD_TAG(Tag, IsValid, ValueMethod)\
		if(tune->Tag() IsValid) { \
			QString value = ValueMethod(tune->Tag()); \
			info.insert(QLatin1String(#Tag), tune->Tag()); \
			if (!description.isEmpty()) \
				description += QLatin1String(" - "); \
			description += value; \
	   }
#define ADD_TEXT_TAG(Tag) \
		ADD_TAG(Tag, .length() > 0, )

QString uriToStringHelper(const QUrl &uri)
{
	return uri.toString();
}

QVariantMap JabberTunePep::toMap(const Jreen::Payload::Ptr &payload)
{
	auto tune = Jreen::se_cast<Jreen::Tune*>(payload.data());
	Q_ASSERT(tune);
	QVariantMap info;
	QString description;

	ADD_TEXT_TAG(artist);
	ADD_TAG(length, > -1, QString::number);
	ADD_TAG(rating, > -1, QString::number);
	ADD_TEXT_TAG(source);
	ADD_TEXT_TAG(title);
	ADD_TEXT_TAG(track);
	ADD_TAG(uri, .isValid(), uriToStringHelper);

	if (!info.isEmpty()) {
		info.insert(QLatin1String("title"), QT_TRANSLATE_NOOP("Tune", "Now listening"));
		info.insert(QLatin1String("description"), description);
		info.insert(QLatin1String("icon"), qVariantFromValue<QIcon>(Icon("user-status-listening_to_music")));
		info.insert("contactList.priority", 800);
		info.insert("keepOffline", false);
		info.insert("chat.showIcon", true);
		info.insert("chat.priority", 800);
	}
	return info;
}

Jreen::Payload::Ptr JabberTunePep::fromMap(const QVariantMap &info)
{
	auto tune = new Jreen::Tune();
	bool ok = true;
	tune->setArtist(info.value(QLatin1String("artist")).toString());
	tune->setLength(info.value(QLatin1String("length")).toInt(&ok) * ok + ok - 1);
	tune->setRating(info.value(QLatin1String("rating")).toInt(&ok) * ok + ok - 1);
	tune->setSource(info.value(QLatin1String("source")).toString());
	tune->setTitle(info.value(QLatin1String("title")).toString());
	tune->setTrack(info.value(QLatin1String("track")).toString());
	tune->setUri(info.value(QLatin1String("uri")).toUrl());
	return Jreen::Payload::Ptr(tune);
}

} // Lime
