/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "jabbercontact.h"
#include <jreen/messagesession.h>
#include <jreen/delayeddelivery.h>
#include <jreen/chatstate.h>
#include <jreen/vcardupdate.h>
#include "jabberaccount.h"
#include "jabberprotocol.h"
#include "jabberresource.h"
#include "jabbermessagemanager.h"
#include "jabbervcards.h"

namespace Lime
{

JabberContact::JabberContact(const QString &id, Account *account) :
	MetaContact(id, account, SupportsOutgoingMessages |
							 SupportsIncomingMessages |
							 SupportsMultiGroups |
							 SupportDefaultGroup |
							 NameMayBeChanged    |
							 GroupsMayBeChanged  |
							 SupportsChatStates  |
							 SupportsMessageReceipts)
{
}

JabberContact::~JabberContact()
{

}

void JabberContact::handlePresence(const Jreen::Presence &presence)
{
	auto error = presence.error();
	auto type = presence.subtype();
	auto resName = presence.from().resource();

	if (error || type == Jreen::Presence::Unavailable) {
		if (auto resource = m_resourceMap.take(resName)) {
			removeResource(resource);
			resource->deleteLater();
		}
		return;
	}

	switch (type)
	{
	case Jreen::Presence::Subscribe:
	case Jreen::Presence::Unsubscribe:
	case Jreen::Presence::Unsubscribed:
	case Jreen::Presence::Subscribed:
		//handleSubscription(presence);
		break;
	default:
		break;
	}

	if (resName.isEmpty() && resources().isEmpty()) {
		updateStatus(presenceToStatus(type), presence.status());
		updatePriority(presence.priority());
	} else if (!resName.isEmpty()) {
		auto res = resource(resName);
		Q_ASSERT(res);
		res->handlePresence(presence);
	}

	if (auto vcard = presence.payload<Jreen::VCardUpdate>()) {
		if (vcard->hasPhotoInfo()) {
			auto avatar = account()->getAvatar(vcard->photoHash());
			if (!avatar.isEmpty())
				updateAvatar(avatar);
			else if (account()->avatarsAutoLoad())
				account()->vcardManager()->request(id());
		} else {
			updateAvatar(QString());
		}
	}
}

void JabberContact::handleIncomingMessage(const Jreen::Message &msg)
{
	if(msg.body().isEmpty())
		return;
	auto d = msg.when();
	auto time = d ? d->dateTime() : QDateTime::currentDateTime();
	Message message(msg.body(), Message::Incoming, time);
	message.setSubject(message.subject());
	message.setUnit(resource(msg.from().resource(), false));
	if (!message.unit())
		message.setUnit(this);
	Contact::handleIncomingMessage(message);
}

ChatUnit::SendMessageError JabberContact::sendMessageImpl(const Message &message)
{
	return JabberRoster::sendMessage(message);
}

void JabberContact::sendChatStateImpl(ChatState state_)
{
	auto state = static_cast<Jreen::ChatState::State>(state_);
	Jreen::Message msg(Jreen::Message::Chat, id());
	msg.addExtension(new Jreen::ChatState(state));
	account()->messageManager()->send(msg);
}

void JabberContact::setGroupsImpl(const QStringList &groups)
{
	auto roster = account()->roster();
	if (auto item = roster->item(id())) {
		item->setGroups(groups);
		roster->synchronize();
	}
}

void JabberContact::setNameImpl(const QString &name)
{
	auto roster = account()->roster();
	if (auto item = roster->item(id())) {
		item->setName(name);
		roster->synchronize();
	}
}

void JabberContact::setInListImpl(bool inList)
{
	auto roster = account()->roster();
	if (inList)
		roster->addContact(this);
	else
		roster->removeContact(this);
	// TODO: update subscription
}

void JabberContact::updateData(QSharedPointer<Jreen::RosterItem> item)
{
	updateName(item->name());
	updateGroups(item->groups());
	//setContactSubscription(item->subscription());
}

JabberResource *JabberContact::resource(const QString &name, bool create)
{
	if (name.isEmpty())
		return 0;
	auto res = m_resourceMap.value(name);
	if (!res && create) {
		res = new JabberResource(this, name, account());
		m_resourceMap.insert(name, res);
		lconnect(res, SIGNAL(destroyed()), this, [name, this]() {
			m_resourceMap.remove(name);
		});
		addResource(res);
	}
	return res;
}

} // Lime

