/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "jabbermessagemanager.h"
#include "jabberaccount.h"
#include "jabbercontact.h"
#include "jabberresource.h"
#include "jreen/chatstate.h"
#include "jreen/receipt.h"

namespace Lime
{

JabberMessageManager::JabberMessageManager(JabberAccount *account) :
	Jreen::MessageSessionManager(account->client()), m_account(account)
{
	QList<Jreen::Message::Type> types;
	types.append(Jreen::Message::Chat);
	types.append(Jreen::Message::Headline);
	registerMessageSessionHandler(this, types);
}

void JabberMessageManager::handleMessageSession(Jreen::MessageSession *session)
{
	auto contact = m_account->contact(session->jid().bare(), true);
	session->registerMessageFilter(new JabberMessageFilter(m_account, session));
	connect(contact, SIGNAL(chatClosed()), session, SLOT(deleteLater()));
	lconnect(session, SIGNAL(messageReceived(Jreen::Message)), this, [this](const Jreen::Message &msg) {
		JabberContact *contact = m_account->contact(msg.from().bare(), true);
		contact->handleIncomingMessage(msg);
	});
}

JabberMessageFilter::JabberMessageFilter(JabberAccount *account, Jreen::MessageSession *session)	:
	Jreen::MessageFilter(session),
	m_account(account)
{

}

JabberMessageFilter::~JabberMessageFilter()
{
}

void JabberMessageFilter::filter(Jreen::Message &message)
{
	auto contact = m_account->contact(message.from().bare(), false);
	if (!contact || message.containsPayload<Jreen::Error>())
		return;

	if(auto *receipt = message.payload<Jreen::Receipt>().data()) {
		if (receipt->type() == Jreen::Receipt::Received) {
			QString id = receipt->id();
			if(id.isEmpty())
				id = message.id(); //for slowpoke client such as Miranda
			contact->handleMessageReceipt(id.toUInt(), ChatUnit::MessageDelivered);
		} else {
			//TODO send this request only when message marked as read
			Jreen::Message request(Jreen::Message::Chat, message.from());
			//for slowpoke clients
			request.setID(message.id());
			//correct behaviour
			request.addExtension(new Jreen::Receipt(Jreen::Receipt::Received, message.id()));
			m_account->client()->send(request);
		}
	}

	if (auto jstate = message.payload<Jreen::ChatState>().data()) {
		auto state = static_cast<ChatUnit::ChatState>(jstate->state());
		auto resource = message.from().isFull() ? contact->resource(message.from().resource(), false) : 0;

		if (resource)
			resource->handleChatState(state);
		else
			contact->handleChatState(state);
	}
}

void JabberMessageFilter::decorate(Jreen::Message &message)
{
	auto receipt = new Jreen::Receipt(Jreen::Receipt::Request);
	message.addExtension(receipt);
}

void JabberMessageFilter::reset()
{
}

int JabberMessageFilter::filterType() const
{
	return Jreen::Message::Chat;
}

} // Lime

