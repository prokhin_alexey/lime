/*
	 This file is part of Lime.
	 Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "jabbermood.h"
#include "jabberaccount.h"
#include <lime/icon.h>
#include <QScopedPointer>

namespace Lime
{

using Jreen::Mood;

static QString moodTitles[] = {
	QT_TRANSLATE_NOOP("Mood", "Afraid"),
	QT_TRANSLATE_NOOP("Mood", "Amazed"),
	QT_TRANSLATE_NOOP("Mood", "Amorous"),
	QT_TRANSLATE_NOOP("Mood", "Angry"),
	QT_TRANSLATE_NOOP("Mood", "Annoyed"),
	QT_TRANSLATE_NOOP("Mood", "Anxious"),
	QT_TRANSLATE_NOOP("Mood", "Aroused"),
	QT_TRANSLATE_NOOP("Mood", "Ashamed"),
	QT_TRANSLATE_NOOP("Mood", "Bored"),
	QT_TRANSLATE_NOOP("Mood", "Brave"),
	QT_TRANSLATE_NOOP("Mood", "Calm"),
	QT_TRANSLATE_NOOP("Mood", "Cautious"),
	QT_TRANSLATE_NOOP("Mood", "Cold"),
	QT_TRANSLATE_NOOP("Mood", "Confident"),
	QT_TRANSLATE_NOOP("Mood", "Confused"),
	QT_TRANSLATE_NOOP("Mood", "Contemplative"),
	QT_TRANSLATE_NOOP("Mood", "Contented"),
	QT_TRANSLATE_NOOP("Mood", "Cranky"),
	QT_TRANSLATE_NOOP("Mood", "Crazy"),
	QT_TRANSLATE_NOOP("Mood", "Creative"),
	QT_TRANSLATE_NOOP("Mood", "Curious"),
	QT_TRANSLATE_NOOP("Mood", "Dejected"),
	QT_TRANSLATE_NOOP("Mood", "Depressed"),
	QT_TRANSLATE_NOOP("Mood", "Disappointed"),
	QT_TRANSLATE_NOOP("Mood", "Disgusted"),
	QT_TRANSLATE_NOOP("Mood", "Dismayed"),
	QT_TRANSLATE_NOOP("Mood", "Distracted"),
	QT_TRANSLATE_NOOP("Mood", "Embarrassed"),
	QT_TRANSLATE_NOOP("Mood", "Envious"),
	QT_TRANSLATE_NOOP("Mood", "Excited"),
	QT_TRANSLATE_NOOP("Mood", "Flirtatious"),
	QT_TRANSLATE_NOOP("Mood", "Frustrated"),
	QT_TRANSLATE_NOOP("Mood", "Grateful"),
	QT_TRANSLATE_NOOP("Mood", "Grieving"),
	QT_TRANSLATE_NOOP("Mood", "Grumpy"),
	QT_TRANSLATE_NOOP("Mood", "Guilty"),
	QT_TRANSLATE_NOOP("Mood", "Happy"),
	QT_TRANSLATE_NOOP("Mood", "Hopeful"),
	QT_TRANSLATE_NOOP("Mood", "Hot"),
	QT_TRANSLATE_NOOP("Mood", "Humbled"),
	QT_TRANSLATE_NOOP("Mood", "Humiliated"),
	QT_TRANSLATE_NOOP("Mood", "Hungry"),
	QT_TRANSLATE_NOOP("Mood", "Hurt"),
	QT_TRANSLATE_NOOP("Mood", "Impressed"),
	QT_TRANSLATE_NOOP("Mood", "In awe"),
	QT_TRANSLATE_NOOP("Mood", "In love"),
	QT_TRANSLATE_NOOP("Mood", "Indignant"),
	QT_TRANSLATE_NOOP("Mood", "Interested"),
	QT_TRANSLATE_NOOP("Mood", "Intoxicated"),
	QT_TRANSLATE_NOOP("Mood", "Invincible"),
	QT_TRANSLATE_NOOP("Mood", "Jealous"),
	QT_TRANSLATE_NOOP("Mood", "Lonely"),
	QT_TRANSLATE_NOOP("Mood", "Lost"),
	QT_TRANSLATE_NOOP("Mood", "Lucky"),
	QT_TRANSLATE_NOOP("Mood", "Mean"),
	QT_TRANSLATE_NOOP("Mood", "Moody"),
	QT_TRANSLATE_NOOP("Mood", "Nervous"),
	QT_TRANSLATE_NOOP("Mood", "Neutral"),
	QT_TRANSLATE_NOOP("Mood", "Offended"),
	QT_TRANSLATE_NOOP("Mood", "Outraged"),
	QT_TRANSLATE_NOOP("Mood", "Playful"),
	QT_TRANSLATE_NOOP("Mood", "Proud"),
	QT_TRANSLATE_NOOP("Mood", "Relaxed"),
	QT_TRANSLATE_NOOP("Mood", "Relieved"),
	QT_TRANSLATE_NOOP("Mood", "Remorseful"),
	QT_TRANSLATE_NOOP("Mood", "Restless"),
	QT_TRANSLATE_NOOP("Mood", "Sad"),
	QT_TRANSLATE_NOOP("Mood", "Sarcastic"),
	QT_TRANSLATE_NOOP("Mood", "Satisfied"),
	QT_TRANSLATE_NOOP("Mood", "Serious"),
	QT_TRANSLATE_NOOP("Mood", "Shocked"),
	QT_TRANSLATE_NOOP("Mood", "Shy"),
	QT_TRANSLATE_NOOP("Mood", "Sick"),
	QT_TRANSLATE_NOOP("Mood", "Sleepy"),
	QT_TRANSLATE_NOOP("Mood", "Spontaneous"),
	QT_TRANSLATE_NOOP("Mood", "Stressed"),
	QT_TRANSLATE_NOOP("Mood", "Strong"),
	QT_TRANSLATE_NOOP("Mood", "Surprised"),
	QT_TRANSLATE_NOOP("Mood", "Thankful"),
	QT_TRANSLATE_NOOP("Mood", "Thirsty"),
	QT_TRANSLATE_NOOP("Mood", "Tired"),
	QT_TRANSLATE_NOOP("Mood", "Undefined"),
	QT_TRANSLATE_NOOP("Mood", "Weak"),
	QT_TRANSLATE_NOOP("Mood", "Worried")
};
static int moodCount = sizeof(moodTitles) / sizeof(moodTitles[0]);

static QString moodTitle(Mood::Type type)
{
	return type >= 0 && type < moodCount ? moodTitles[type] : QString();
}

static Mood *toMood(const QVariantMap &info)
{
	QString mood = info.value("mood").toString();
	QString desc = info.value("description").toString();
	return new Mood(mood, desc);
}

static QVariantMap fromMood(Mood *mood)
{
	QString name = mood->typeName();
	QVariantMap info;
	info.insert("description", mood->text());
	info.insert("mood", name);
	info.insert("icon", Icon("user-status-" + name));
	info.insert("contactList.priority", 900);
	info.insert("keepOffline", false);
	info.insert("chat.showIcon", true);
	info.insert("chat.priority", 900);
	return info;
}

JabberMoodDialog::JabberMoodDialog(JabberAccount *account) :
	ExtendedStatusDialog(HasDescrition)
{
	setAttribute(Qt::WA_DeleteOnClose, true);
	setWindowTitle(tr("Set mood"));

	auto currentMood = Mood::Ptr(toMood(account->extendedInfo("mood")));
	Item current = 0;

	Jreen::Mood tmp;
	for (int i = 0; i < moodCount; i++) {
		auto type = static_cast<Jreen::Mood::Type>(i);
		tmp.setType(type);
		auto item = addStatus(Icon("user-status-" + tmp.typeName()), moodTitle(type), i);
		if (!current && tmp.type() == currentMood->type())
			current = item;
	}

	setDescription(currentMood->text());
	finish(Icon("edit-delete-status"), tr("No mood"));
	setCurrentItem(current);

	lconnect(this, SIGNAL(accepted()), this, [=] {
		auto typeVar = currentUserData();
		Mood::Type type = Mood::Empty;
		if (!typeVar.isNull())
			type = Mood::Type(typeVar.toInt());
		auto mood = new Mood(type, description());
		auto item = Jreen::Payload::Ptr(mood);
		account->pubSubManager()->publishItems(QList<Jreen::Payload::Ptr>() << item, Jreen::JID());
	});
}

JabberMoodDialog::~JabberMoodDialog()
{
}

JabberMoodPep::JabberMoodPep() :
	JabberPep("mood", Mood::staticPayloadType(), QT_TRANSLATE_NOOP("Mood", "Show mood icon"))
{
	setAction(QIcon(), QT_TRANSLATE_NOOP("Mood", "Set mood..."), [=](JabberAccount *acc) {
		acc->showMoodDialog();
	});
}

QVariantMap JabberMoodPep::toMap(const Jreen::Payload::Ptr &payload)
{
	auto map = Jreen::se_cast<Mood*>(payload.data());
	Q_ASSERT(map);
	return fromMood(map);
}

Jreen::Payload::Ptr JabberMoodPep::fromMap(const QVariantMap &info)
{
	return Jreen::Payload::Ptr(toMood(info));
}

} // Lime
