/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_JABBERPROTOCOL_H
#define LIME_JABBERPROTOCOL_H

#include <lime/protocol.h>
#include <lime/status.h>
#include <jreen/presence.h>
#include <QVariantMap>

namespace Lime {

class Action;
class JabberAccount;

Jreen::Presence::Type statusToPresence(Status status);
Status presenceToStatus(Jreen::Presence::Type presence);
QString errorString(const QSharedPointer<Jreen::Error> &error);

class JabberPep
{
public:
	JabberPep(const QString &name, int payloadType, const QString &description) :
		m_action(0), m_name(name), m_payloadType(payloadType), m_description(description)
	{}
	~JabberPep();
	virtual QVariantMap toMap(const Jreen::Payload::Ptr &payload) = 0;
	virtual Jreen::Payload::Ptr fromMap(const QVariantMap &info) = 0;
	Action *action() const { return m_action; }
	QString name() const { return m_name; }
	QString description() const { return m_description; }
	int payloadType() const { return m_payloadType; }
protected:
	void setAction(const QIcon &icon, const QString &title, const function<void(JabberAccount*)> &callback);
private:
	Action *m_action;
	QString m_name;
	QString m_description;
	int m_payloadType;
};

class JabberProtocol : public Protocol, public SignalGuard
{
	Q_OBJECT
public:
	JabberProtocol();
	virtual ~JabberProtocol();
	bool hasAccount(const QString &jid) const;
	void addAccount(const QString &jid, const QString &password);
	Action *joinAction() const { return m_joinAction; }
	QMap<int, JabberPep*> peps() { return m_peps; }
	template <typename T>
	void addPep();
	QList<Action*> onlineActions() const { return m_onlineActions; }
	static JabberProtocol *instance();
protected:
	using Protocol::addAccount;
    virtual bool enableImpl();
	virtual bool disableImpl();
private:
	Action *m_joinAction;
	Action *m_serviceBrowserAction;
	QList<Action*> m_onlineActions;
	QMap<int, JabberPep*> m_peps;
};

template <typename T>
void JabberProtocol::addPep()
{
	JabberPep *pep = new T;
	m_peps.insert(pep->payloadType(), pep);
}

}

#endif // LIME_JABBERPROTOCOL_H
