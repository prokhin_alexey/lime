/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "jabberresource.h"
#include "jabberaccount.h"
#include "jabberprotocol.h"
#include "jabberroster.h"
#include "jabbercontact.h"
#include "jabbermessagemanager.h"
#include <jreen/message.h>
#include <jreen/chatstate.h>

namespace Lime
{

JabberResource::JabberResource(JabberContact *contact, const QString &name, JabberAccount *account) :
	Buddy(contact->id() + QLatin1Char('/') + name, account, BuddyDefaultFlags | SupportsChatStates)
{
	updateName(name);
	updateTitle(contact->name() + QLatin1Char('/') + name);
	lconnect(contact, SIGNAL(nameChanged(QString,QString)), this, [=](const QString &contactName) {
		updateTitle(QString("%1 (%2)").arg(contactName).arg(this->name()));
	});
}

void JabberResource::handlePresence(const Jreen::Presence &presence)
{
	switch(presence.subtype())
	{
	case Jreen::Presence::Subscribe:
	case Jreen::Presence::Unsubscribe:
	case Jreen::Presence::Unsubscribed:
	case Jreen::Presence::Subscribed:
		Q_ASSERT(!"The presence must have been handled in JabberContact");
		break;
	case Jreen::Presence::Error:
	case Jreen::Presence::Probe:
		return;
	default:
		break;
	}
	updateStatus(presenceToStatus(presence.subtype()), presence.status());
	updatePriority(presence.priority());
}

JabberResource::SendMessageError JabberResource::sendMessageImpl(const Lime::Message &message)
{
	return JabberRoster::sendMessage(message);
}


void JabberResource::sendChatStateImpl(ChatUnit::ChatState state_)
{
	auto state = static_cast<Jreen::ChatState::State>(state_);
	Jreen::Message msg(Jreen::Message::Chat, id());
	msg.addExtension(new Jreen::ChatState(state));
	account()->messageManager()->send(msg);
}

} // Lime

