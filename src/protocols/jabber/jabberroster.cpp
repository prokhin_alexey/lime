/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "jabberroster.h"
#include "jabberaccount.h"
#include "jabbercontact.h"
#include "jabbermessagemanager.h"
#include "jreen/messagesession.h"

namespace Lime
{

JabberRoster::JabberRoster(JabberAccount *account) :
	AbstractRoster(account->client()), m_account(account)
{
	lconnect(m_account->client(), SIGNAL(presenceReceived(Jreen::Presence)), this,
		[this](const Jreen::Presence &presence)
	{
		qDebug() << presence.from();
		auto contact = m_account->contact(presence.from().bare(), false);
		if (contact)
			contact->handlePresence(presence);
	});
}

ChatUnit::SendMessageError JabberRoster::sendMessage(const Message &message)
{
	if (message.type() == Message::TextMessage) {
		Q_ASSERT(message.unit());
		auto unit = message.unit();
		auto account = reinterpret_cast<JabberAccount*>(unit->account());
		Jreen::JID jid = unit->id();
		Jreen::Message msg(Jreen::Message::Chat,
						   jid,
						   message.text(),
						   message.subject());
		msg.setID(QString::number(message.id()));
		if (Jreen::MessageSession *s = account->messageManager()->session(jid, Jreen::Message::Chat)) {
			s->sendMessage(msg);
		} else {
			account->client()->send(msg);
		}
		return ChatUnit::MessageSent;
	}
	return ChatUnit::UnsupportedTypeError;
}

void JabberRoster::addContact(JabberContact *contact)
{
	add(contact->id(), contact->name(), contact->groups());
}

void JabberRoster::removeContact(JabberContact *contact)
{
	remove(contact->id());
}

void JabberRoster::onItemAdded(QSharedPointer<Jreen::RosterItem> item)
{
	auto contact = m_account->contact(item->jid(), true);
	contact->updateData(item);
	contact->updateInList(true);
}

void JabberRoster::onItemUpdated(QSharedPointer<Jreen::RosterItem> item)
{
	auto contact = m_account->contact(item->jid(), false);
	if (contact) {
		contact->updateData(item);
		contact->updateInList(true);
	}
}

void JabberRoster::onItemRemoved(const QString &jid)
{
	auto contact = m_account->contact(jid, false);
	if (contact)
		contact->updateInList(false);
}

} // Lime

