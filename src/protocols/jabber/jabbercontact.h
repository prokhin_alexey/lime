/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_JABBERCONTACT_H
#define LIME_JABBERCONTACT_H

#include <lime/metacontact.h>
#include "jabberroster.h"
#include <jreen/message.h>

namespace Lime {

class JabberResource;

class JabberContact : public MetaContact
{
	Q_OBJECT
public:
	JabberContact(const QString &id, Account *account);
	virtual ~JabberContact();
	JabberAccount *account() const { return reinterpret_cast<JabberAccount*>(Contact::account()); }
	void handlePresence(const Jreen::Presence &presence);
	void handleIncomingMessage(const Jreen::Message &msg);
	void handleChatState(ChatState state) { ChatUnit::handleChatState(state); }
	void handleMessageReceipt(int id, MessageReceipt success) { ChatUnit::handleMessageReceipt(id, success); }
	void updateAvatar(const QString &avatar) { MetaContact::updateAvatar(avatar); }
	JabberResource *resource(const QString &name, bool create = true);
protected:
	virtual SendMessageError sendMessageImpl(const Lime::Message& message);
	void sendChatStateImpl(ChatState state);
	virtual void setGroupsImpl(const QStringList &groups);
	virtual void setNameImpl(const QString &name);
	virtual void setInListImpl(bool inList);
private:
	friend class JabberRoster;
	friend class JabberMessageFilter;
	void updateData(QSharedPointer<Jreen::RosterItem> item);
private:
	QMap<QString, JabberResource*> m_resourceMap;
};

} // Lime

#endif // LIME_JABBERCONTACT_H
