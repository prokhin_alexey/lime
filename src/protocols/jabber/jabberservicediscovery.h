/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_JABBERSERVICEDISCOVERY_H
#define LIME_JABBERSERVICEDISCOVERY_H

#include <jreen/disco.h>
#include <jreen/jid.h>
#include <jreen/error.h>
#include <lime/signal.h>

namespace Lime {

class JabberAccount;
class JabberServiceDiscovery;

class JabberDiscoItem
{
public:
	JabberDiscoItem(JabberServiceDiscovery *discovery, const Jreen::JID &jid = Jreen::JID());
	JabberDiscoItem(const JabberDiscoItem &other) : d(other.d) {}
	~JabberDiscoItem();
	JabberDiscoItem &operator=(const JabberDiscoItem &other) { d = other.d; return *this; }
	Jreen::JID jid() const { return d->jid; }
	QString node() const { return d->node; }
	QString name() const { return d->name; }
	QString error() const { return d->error; }
	QList<Jreen::Disco::Identity> identities() const { return d->identities; }
	bool hasIdentity(const QString &category, const QString &type = QString()) const;
	QSet<QString> features() const { return d->features; }
	bool hasFeature(const QString &feature) const { return d->features.contains(feature); }
	void requestInfo();
	void requestItems();
// signals:
	Signal<void(QSharedPointer<Jreen::Error>)> errorOccurred;
	Signal<void()> infoReceived;
	Signal<void(QList<JabberDiscoItem>)> itemsReceived;
	Signal<void()> destroyed;
private:
	friend class JabberServiceDiscovery;
	struct JabberDiscoItemPrivate : public QSharedData
	{
		JabberServiceDiscovery *discovery;
		Jreen::JID jid;
		QString node;
		QString name;
		QString error;
		QList<Jreen::Disco::Identity> identities;
		QSet<QString> features;
	};
	QSharedDataPointer<JabberDiscoItemPrivate> d;
};

class JabberServiceDiscovery : public QObject
{
	Q_OBJECT
public:
	JabberServiceDiscovery(JabberAccount *account);
	~JabberServiceDiscovery();
private slots:
	void handleIQ(const Jreen::IQ &iq, int context);
private:
	friend class JabberDiscoItem;
	template <typename ExtensionType>
	void request(JabberDiscoItem *item);
	void requestInfo(JabberDiscoItem *item);
	void requestItems(JabberDiscoItem *item);
	void handleDiscoInfo(const Jreen::JID &from, QSharedPointer<Jreen::Disco::Info> info, JabberDiscoItem *item);
	void handleDiscoItems(const Jreen::JID &from, QSharedPointer<Jreen::Disco::Items> items, JabberDiscoItem *item);
	void handleDiscoError(const Jreen::JID &from, QSharedPointer<Jreen::Error> error, JabberDiscoItem *item);
private:
	int m_lastContext;
	QMap<int, JabberDiscoItem*> m_requests;
	JabberAccount *m_account;
};

} // Lime

#endif // LIME_JABBERSERVICEDISCOVERY_H
