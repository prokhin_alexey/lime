/*
	 This file is part of Lime.
	 Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "jabberactivity.h"
#include "jabberaccount.h"
#include <lime/icon.h>
#include <QScopedPointer>

namespace Lime
{

using Jreen::Activity;

struct ActivityGroup
{
	ActivityGroup(Activity::General general_) :
		general(general_)
	{}
	Activity::General general;
	QList<Activity::Specific> items;
};
typedef QList<ActivityGroup> ActivityGroups;
typedef QStringList ActivityNames;

static void init_activity_groups(ActivityGroups &activities)
{
	{
		ActivityGroup group(Activity::DoingChores);
		group.items.push_back(Activity::BuyingGroceries);
		group.items.push_back(Activity::Cleaning);
		group.items.push_back(Activity::Cooking);
		group.items.push_back(Activity::DoingMaintenance);
		group.items.push_back(Activity::DoingTheDishes);
		group.items.push_back(Activity::DoingTheLaundry);
		group.items.push_back(Activity::Gardening);
		group.items.push_back(Activity::RunningAnErrand);
		group.items.push_back(Activity::WalkingTheDog);
		activities.push_back(group);
	}
	{
		ActivityGroup group(Activity::Drinking);
		group.items.push_back(Activity::HavingABeer);
		group.items.push_back(Activity::HavingCoffee);
		group.items.push_back(Activity::HavingTea);
		activities.push_back(group);
	}
	{
		ActivityGroup group(Activity::Eating);
		group.items.push_back(Activity::HavingASnack);
		group.items.push_back(Activity::HavingBreakfast);
		group.items.push_back(Activity::HavingDinner);
		group.items.push_back(Activity::HavingLunch);
		activities.push_back(group);
	}
	{
		ActivityGroup group(Activity::Exercising);
		group.items.push_back(Activity::Cycling);
		group.items.push_back(Activity::Dancing);
		group.items.push_back(Activity::Hiking);
		group.items.push_back(Activity::Jogging);
		group.items.push_back(Activity::PlayingSports);
		group.items.push_back(Activity::Running);
		group.items.push_back(Activity::Skiing);
		group.items.push_back(Activity::Swimming);
		group.items.push_back(Activity::WorkingOut);
		activities.push_back(group);
	}
	{
		ActivityGroup group(Activity::Grooming);
		group.items.push_back(Activity::AtTheSpa);
		group.items.push_back(Activity::BrushingTeeth);
		group.items.push_back(Activity::GettingAHaircut);
		group.items.push_back(Activity::Shaving);
		group.items.push_back(Activity::TakingABath);
		group.items.push_back(Activity::TakingAShower);
		activities.push_back(group);
	}
	{
		ActivityGroup group(Activity::HavingAppointment);
		activities.push_back(group);
	}
	{
		ActivityGroup group(Activity::Inactive);
		group.items.push_back(Activity::DayOff);
		group.items.push_back(Activity::HangingOut);
		group.items.push_back(Activity::Hiding);
		group.items.push_back(Activity::OnVacation);
		group.items.push_back(Activity::Praying);
		group.items.push_back(Activity::ScheduledHoliday);
		group.items.push_back(Activity::Sleeping);
		group.items.push_back(Activity::Thinking);
		activities.push_back(group);
	}
	{
		ActivityGroup group(Activity::Relaxing);
		group.items.push_back(Activity::Fishing);
		group.items.push_back(Activity::Gaming);
		group.items.push_back(Activity::GoingOut);
		group.items.push_back(Activity::Partying);
		group.items.push_back(Activity::Reading);
		group.items.push_back(Activity::Rehearsing);
		group.items.push_back(Activity::Shopping);
		group.items.push_back(Activity::Smoking);
		group.items.push_back(Activity::Socializing);
		group.items.push_back(Activity::Sunbathing);
		group.items.push_back(Activity::WatchingTv);
		group.items.push_back(Activity::WatchingAMovie);
		activities.push_back(group);
	}
	{
		ActivityGroup group(Activity::Talking);
		group.items.push_back(Activity::InRealLife);
		group.items.push_back(Activity::OnThePhone);
		group.items.push_back(Activity::OnVideoPhone);
		activities.push_back(group);
	}
	{
		ActivityGroup group(Activity::Traveling);
		group.items.push_back(Activity::Commuting);
		group.items.push_back(Activity::Cycling);
		group.items.push_back(Activity::Driving);
		group.items.push_back(Activity::InACar);
		group.items.push_back(Activity::OnABus);
		group.items.push_back(Activity::OnAPlane);
		group.items.push_back(Activity::OnATrain);
		group.items.push_back(Activity::OnATrip);
		group.items.push_back(Activity::Walking);
		activities.push_back(group);
	}
	{
		ActivityGroup group(Activity::Working);
		group.items.push_back(Activity::Coding);
		group.items.push_back(Activity::InAMeeting);
		group.items.push_back(Activity::Studying);
		group.items.push_back(Activity::Writing);
		activities.push_back(group);
	}
}
Q_GLOBAL_STATIC_WITH_INITIALIZER(ActivityGroups, allActivityGroups, init_activity_groups(*x));

QString generalTitle(Activity::General general)
{
	static QString names[] = {
		QT_TRANSLATE_NOOP("Activity", "Doing chores"),
		QT_TRANSLATE_NOOP("Activity", "Drinking"),
		QT_TRANSLATE_NOOP("Activity", "Eating"),
		QT_TRANSLATE_NOOP("Activity", "Exercising"),
		QT_TRANSLATE_NOOP("Activity", "Grooming"),
		QT_TRANSLATE_NOOP("Activity", "Having appointment"),
		QT_TRANSLATE_NOOP("Activity", "Inactive"),
		QT_TRANSLATE_NOOP("Activity", "Relaxing"),
		QT_TRANSLATE_NOOP("Activity", "Talking"),
		QT_TRANSLATE_NOOP("Activity", "Traveling"),
		QT_TRANSLATE_NOOP("Activity", "Undefined"),
		QT_TRANSLATE_NOOP("Activity", "Working")
	};
	static int size = sizeof(names) / sizeof(names[0]);
	return general >= 0 && general < size ? names[general] : QString();
}

QString specificTitle(Activity::Specific specific)
{
	static QString names[] = {
		QT_TRANSLATE_NOOP("Activity", "At the spa"),
		QT_TRANSLATE_NOOP("Activity", "Brushing teeth"),
		QT_TRANSLATE_NOOP("Activity", "Buying groceries"),
		QT_TRANSLATE_NOOP("Activity", "Cleaning"),
		QT_TRANSLATE_NOOP("Activity", "Coding"),
		QT_TRANSLATE_NOOP("Activity", "Commuting"),
		QT_TRANSLATE_NOOP("Activity", "Cooking"),
		QT_TRANSLATE_NOOP("Activity", "Cycling"),
		QT_TRANSLATE_NOOP("Activity", "Dancing"),
		QT_TRANSLATE_NOOP("Activity", "Day off"),
		QT_TRANSLATE_NOOP("Activity", "Doing maintenance"),
		QT_TRANSLATE_NOOP("Activity", "Doing the dishes"),
		QT_TRANSLATE_NOOP("Activity", "Doing the laundry"),
		QT_TRANSLATE_NOOP("Activity", "Driving"),
		QT_TRANSLATE_NOOP("Activity", "Fishing"),
		QT_TRANSLATE_NOOP("Activity", "Gaming"),
		QT_TRANSLATE_NOOP("Activity", "Gardening"),
		QT_TRANSLATE_NOOP("Activity", "Getting a haircut"),
		QT_TRANSLATE_NOOP("Activity", "Going out"),
		QT_TRANSLATE_NOOP("Activity", "Hanging out"),
		QT_TRANSLATE_NOOP("Activity", "Having a beer"),
		QT_TRANSLATE_NOOP("Activity", "Having a snack"),
		QT_TRANSLATE_NOOP("Activity", "Having breakfast"),
		QT_TRANSLATE_NOOP("Activity", "Having coffee"),
		QT_TRANSLATE_NOOP("Activity", "Having dinner"),
		QT_TRANSLATE_NOOP("Activity", "Having lunch"),
		QT_TRANSLATE_NOOP("Activity", "Having tea"),
		QT_TRANSLATE_NOOP("Activity", "Hiding"),
		QT_TRANSLATE_NOOP("Activity", "Hiking"),
		QT_TRANSLATE_NOOP("Activity", "In a car"),
		QT_TRANSLATE_NOOP("Activity", "In a meeting"),
		QT_TRANSLATE_NOOP("Activity", "In real life"),
		QT_TRANSLATE_NOOP("Activity", "Jogging"),
		QT_TRANSLATE_NOOP("Activity", "On a bus"),
		QT_TRANSLATE_NOOP("Activity", "On a plane"),
		QT_TRANSLATE_NOOP("Activity", "On a train"),
		QT_TRANSLATE_NOOP("Activity", "On a trip"),
		QT_TRANSLATE_NOOP("Activity", "On the phone"),
		QT_TRANSLATE_NOOP("Activity", "On vacation"),
		QT_TRANSLATE_NOOP("Activity", "On video phone"),
		QT_TRANSLATE_NOOP("Activity", "Other"),
		QT_TRANSLATE_NOOP("Activity", "Partying"),
		QT_TRANSLATE_NOOP("Activity", "Playing sports"),
		QT_TRANSLATE_NOOP("Activity", "Praying"),
		QT_TRANSLATE_NOOP("Activity", "Reading"),
		QT_TRANSLATE_NOOP("Activity", "Rehearsing"),
		QT_TRANSLATE_NOOP("Activity", "Running"),
		QT_TRANSLATE_NOOP("Activity", "Running an errand"),
		QT_TRANSLATE_NOOP("Activity", "Scheduled holiday"),
		QT_TRANSLATE_NOOP("Activity", "Shaving"),
		QT_TRANSLATE_NOOP("Activity", "Shopping"),
		QT_TRANSLATE_NOOP("Activity", "Skiing"),
		QT_TRANSLATE_NOOP("Activity", "Sleeping"),
		QT_TRANSLATE_NOOP("Activity", "Smoking"),
		QT_TRANSLATE_NOOP("Activity", "Socializing"),
		QT_TRANSLATE_NOOP("Activity", "Studying"),
		QT_TRANSLATE_NOOP("Activity", "Sunbathing"),
		QT_TRANSLATE_NOOP("Activity", "Swimming"),
		QT_TRANSLATE_NOOP("Activity", "Taking a bath"),
		QT_TRANSLATE_NOOP("Activity", "Taking a shower"),
		QT_TRANSLATE_NOOP("Activity", "Thinking"),
		QT_TRANSLATE_NOOP("Activity", "Walking"),
		QT_TRANSLATE_NOOP("Activity", "Walking the dog"),
		QT_TRANSLATE_NOOP("Activity", "Watching a movie"),
		QT_TRANSLATE_NOOP("Activity", "Watching TV"),
		QT_TRANSLATE_NOOP("Activity", "Working out"),
		QT_TRANSLATE_NOOP("Activity", "Writing")
	};
	static int size = sizeof(names) / sizeof(names[0]);
	return specific >= 0 && specific < size ? names[specific] : QString();
}

static Activity *toActivity(const QVariantMap &info)
{
	QString general  = info.value("general").toString();
	QString specific = info.value("specific").toString();
	QString desc     = info.value("description").toString();
	return new Activity(general, specific, desc);
}

static QVariantMap fromActivity(Activity *activity)
{
	QString general  = activity->generalName();
	QString specific = activity->specificName();

	QString iconName = "user-status-" + general;
	if (!specific.isEmpty())
		iconName += "-" + specific;

	QVariantMap info;
	info.insert("description", activity->text());
	info.insert("general", general);
	info.insert("specific", specific);
	info.insert("icon", Icon(iconName));
	info.insert("contactList.priority", 1000);
	info.insert("keepOffline", false);
	info.insert("chat.showIcon", true);
	info.insert("chat.priority", 1000);
	return info;
}

JabberActivityDialog::JabberActivityDialog(JabberAccount *account) :
	ExtendedStatusDialog(HasDescrition)
{
	setAttribute(Qt::WA_DeleteOnClose, true);
	setWindowTitle(tr("Set activity"));

	auto currentActivity = Activity::Ptr(toActivity(account->extendedInfo("activity")));
	Item current = 0;
	foreach (auto &activity, *allActivityGroups()) {
		QString generalIconName = "user-status-" + Activity::generalName(activity.general);
		QIcon icon = Icon(generalIconName);
		QString title = generalTitle(activity.general);
		int generalFlag = activity.general << 8;
		Item generalItem = beginGroup(icon, title, generalFlag);
		bool isGeneralCurrent = current == 0 && currentActivity->general() == activity.general;

		foreach (auto specific, activity.items) {
			icon = Icon(generalIconName + "-" + Activity::specificName(specific));
			title = specificTitle(specific);
			Item specificItem = addStatus(icon, title, generalFlag | specific);
			if (current == 0 && isGeneralCurrent && specific == currentActivity->specific())
				current = specificItem;
		}

		if (isGeneralCurrent && current == 0)
			current = generalItem;
		endGroup();
	}

	setDescription(currentActivity->text());
	finish(Icon("edit-delete-status"), tr("No activity"));
	setCurrentItem(current);

	lconnect(this, SIGNAL(accepted()), this, [=] {
		auto activityVar = currentUserData();
		Activity::General  general  = Activity::EmptyGeneral;
		Activity::Specific specific = Activity::EmptySpecific;
		if (!activityVar.isNull()) {
			int activityId = activityVar.toInt();
			general = Activity::General(activityId >> 8);
			specific = Activity::Specific(activityId & 63);
		}

		auto activity = new Activity(general, specific, description());
		auto item = Jreen::Payload::Ptr(activity);
		account->pubSubManager()->publishItems(QList<Jreen::Payload::Ptr>() << item, Jreen::JID());
	});
}

JabberActivityDialog::~JabberActivityDialog()
{
}

JabberActivityPep::JabberActivityPep() :
	JabberPep("activity", Activity::staticPayloadType(), QT_TRANSLATE_NOOP("Activity", "Show activity icon"))
{
	setAction(QIcon(), QT_TRANSLATE_NOOP("Activity", "Set activity..."), [=](JabberAccount *acc) {
		acc->showActivityDialog();
	});
}

QVariantMap JabberActivityPep::toMap(const Jreen::Payload::Ptr &payload)
{
	auto activity = Jreen::se_cast<Activity*>(payload.data());
	Q_ASSERT(activity);
	return fromActivity(activity);
}

Jreen::Payload::Ptr JabberActivityPep::fromMap(const QVariantMap &info)
{
	return Jreen::Payload::Ptr(toActivity(info));
}

} // Lime
