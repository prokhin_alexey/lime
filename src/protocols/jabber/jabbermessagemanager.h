/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_JABBERMESSAGEMANAGER_H
#define LIME_JABBERMESSAGEMANAGER_H

#include <jreen/messagesession.h>

namespace Lime {

class JabberAccount;

class JabberMessageManager : public Jreen::MessageSessionManager, public Jreen::MessageSessionHandler
{
	Q_OBJECT
public:
	JabberMessageManager(JabberAccount *account);
	virtual void handleMessageSession(Jreen::MessageSession *session);
private:
	JabberAccount *m_account;
};

class JabberMessageFilter : public Jreen::MessageFilter
{
public:
	JabberMessageFilter(JabberAccount *account, Jreen::MessageSession *session);
	virtual ~JabberMessageFilter();
	virtual void filter(Jreen::Message &message);
	virtual void decorate(Jreen::Message &message);
	virtual void reset();
	virtual int filterType() const;
private:
	JabberAccount *m_account;
};

} // Lime

#endif // LIME_JABBERMESSAGEMANAGER_H
