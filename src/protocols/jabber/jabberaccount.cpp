/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "jabberaccount.h"
#include "jabberprotocol.h"
#include "jabbercontact.h"
#include "jabberroster.h"
#include "jabbermessagemanager.h"
#include "jabberconference.h"
#include "jabberbookmarks.h"
#include "jabberservicediscovery.h"
#include "jabbervcards.h"
#include "jabbersoftwaredetection.h"
#include "jabberactivity.h"
#include "jabbermood.h"
#include "servicebrowser/servicebrowser.h"
#include "xmlconsole/xmlconsole.h"
#include <jreen/connectionbosh.h>
#include <jreen/vcardupdate.h>
#include <jreen/capabilities.h>
#include <jreen/vcardupdate.h>
#include <jreen/tcpconnection.h>
#include <lime/systeminfo.h>
#include <lime/config.h>
#include <lime/icon.h>
#include <QTimer>

Q_DECLARE_METATYPE(QSet<QString>);

namespace Lime
{


JabberAccount::JabberAccount(const QString &id) :
	TagAccount(id, JabberProtocol::instance(), AccountDefaultFlags),
	m_client(0),
	m_roster(0),
	m_privateXml(0),
	m_privacyManager(0),
	m_xmlConsole(0),
	m_pubsub(0),
	m_bookmarksManager(0),
	m_serviceDiscovery(0),
	m_vcardManager(0),
	m_loadFlags(0)
{
}

JabberAccount::~JabberAccount()
{
}

JabberConference *JabberAccount::conference(const QString &id, bool create)
{
	auto conf = sender_cast<JabberConference*>(Account::conference(id));
	if (!conf && create) {
		conf = new JabberConference(this, id);
		addConference(conf);
	}
	return conf;
}

ChatUnit *JabberAccount::unit(const Jreen::JID &jid)
{
	if (auto contact = this->contact(jid.bare(), false)) {
		if (jid.isFull())
			return contact->resource(jid.resource());
		return contact;
	} else if (auto conf = conference(jid.bare(), false)) {
		if (jid.isFull())
			return conf->participant(jid.resource());
		return conf;
	}
	return 0;
}

JabberResourceInterface *JabberAccount::resource(const Jreen::JID &jid)
{
	if (auto contact = this->contact(jid.bare(), false))
		return contact->resource(jid.resource(), false);
	else if (auto conf = conference(jid.bare(), false))
		return conf->participant(jid.resource());
	return 0;
}

void JabberAccount::openXmlConsole()
{
	if (m_xmlConsole) {
		m_xmlConsole->show();
		m_xmlConsole->raise();
	}
}

void JabberAccount::showServiceBrowser()
{
	if (!m_serviceBrowser) {
		m_serviceBrowser = new JabberServiceBrowser(this);
		m_serviceBrowser->setAttribute(Qt::WA_DeleteOnClose, true);
	}
	m_serviceBrowser->show();
	m_serviceBrowser->raise();
}

void JabberAccount::showActivityDialog()
{
	if (!m_activityDialog)
		m_activityDialog = new JabberActivityDialog(this);
	m_activityDialog->show();
	m_activityDialog->raise();
}

void JabberAccount::showMoodDialog()
{
	if (!m_moodDialog)
		m_moodDialog = new JabberMoodDialog(this);
	m_moodDialog->show();
	m_moodDialog->raise();
}

QSet<QString> JabberAccount::features() const
{
	return m_client->serverFeatures();
}

bool JabberAccount::checkFeature(const QString &feature) const
{
	return m_client->serverFeatures().contains(feature);
}

bool JabberAccount::checkIdentity(const QString &category, const QString &type) const
{
	auto identities = m_client->serverIdentities();
	foreach (auto &identity, m_client->serverIdentities()) {
		if (identity.category() == category && identity.type() == type)
			return true;
	}
	return false;
}

QString JabberAccount::identity(const QString &category, const QString &type) const
{
	foreach (auto &identity, m_client->serverIdentities()) {
		if (identity.category() == category && identity.type() == type)
			return identity.name();
	}
	return QString();
}

bool JabberAccount::enableImpl()
{
	m_client = new Jreen::Client;
	m_roster = new JabberRoster(this);
	m_messageManager = new JabberMessageManager(this);
	m_privacyManager = new Jreen::PrivacyManager(m_client);
	m_privateXml = new Jreen::PrivateXml(m_client);
	m_xmlConsole = new XmlConsole(this);
	m_pubsub = new Jreen::PubSub::Manager(m_client);
	m_bookmarksManager = new JabberBookmarks(this);
	m_serviceDiscovery = new JabberServiceDiscovery(this);
	m_vcardManager = new JabberVCardManager(this);
	Q_UNUSED(new JabberSoftwareDetection(this));
	m_client->presence().addExtension(new Jreen::VCardUpdate());

	auto caps = m_client->presence().payload<Jreen::Capabilities>();
	caps->setNode(QLatin1String("https://github.com/lime"));

	Jreen::Disco *disco = m_client->disco();
	disco->setSoftwareVersion(QLatin1String("lime"),
							  versionString(),
							  SystemInfo::getFullName());

	disco->addIdentity(Jreen::Disco::Identity(QLatin1String("client"),
											  QLatin1String("pc"),
											  QString("lime"),
											  QLatin1String("en")));
	QString lime = tr("lime", "Local qutIM's name");
	QString lang = tr("en", "Default language");
	if(lime != QLatin1String("lime") && lang != QLatin1String("en"))
		disco->addIdentity(Jreen::Disco::Identity(QLatin1String("client"), QLatin1String("pc"), lime, lang));

	foreach (auto pep, protocol()->peps())
		m_pubsub->addEntityType(pep->payloadType());

	connect(m_client, SIGNAL(connected()), m_privacyManager, SLOT(request()));
	connect(m_client, SIGNAL(disconnected(Jreen::Client::DisconnectReason)), this,
			SLOT(onDisconnected(Jreen::Client::DisconnectReason)));

	lconnect(m_client, SIGNAL(serverFeaturesReceived(QSet<QString>)), this, [this](const QSet<QString> &) {
		qDebug() << "Received server features";
		m_roster->load();
	});

	lconnect(m_roster, SIGNAL(loaded()), this, [this]() {
		m_loadFlags |= RosterLoaded;
		this->onConnected();
	});
	lconnect(m_privacyManager, SIGNAL(listsReceived()), this, [this]() {
		m_loadFlags |= PrivacyListLoaded;
		this->onConnected();
	});

	lconnect(m_pubsub, SIGNAL(eventReceived(Jreen::PubSub::Event::Ptr,Jreen::JID)), this,
		[=](const Jreen::PubSub::Event::Ptr &event, const Jreen::JID &from)
	{
		if (from.bare() == id()) {
			auto peps = protocol()->peps();
			foreach (auto &payload, event->items()) {
				auto pep = peps.value(payload->payloadType());
				if (pep)
					updateExtendedInfo(pep->name(), pep->toMap(payload));
			}
		} else if (auto buddy = qobject_cast<Buddy*>(unit(from))) {
			auto peps = protocol()->peps();
			foreach (auto &payload, event->items()) {
				auto pep = peps.value(payload->payloadType());
				if (pep)
                                        buddy->setExtendedInfo(pep->name(), pep->toMap(payload));
			}
		}
	});

	Config general = config();
	general.beginGroup("general");

	m_priority = general.value("priority", 15);
	m_keepStatus = general.value("keepstatus", true);
	m_avatarsAutoLoad = general.value("avatarsAutoLoad", true);
	m_nick = general.value("nick", id());
	if (general.hasChildKey("photoHash")) {
		auto update = m_client->presence().payload<Jreen::VCardUpdate>();
		update->setPhotoHash(general.value("photoHash", QString()));
	}

	Jreen::JID jid(id());
	jid.setResource(general.value("resource", "lime"));
	m_client->setJID(jid);
	m_client->setPassword(general.value("passwd", "" /*, Config::Crypted*/));
	if (!general.value("autoDetect", true)) {
		m_client->setPort(general.value("port", 5222));
		m_client->setServer(general.value("server", m_client->server()));
	}
	general.beginGroup("bosh");
	if (general.value("use", false)) {
		QString host = general.value("host", jid.domain());
		int port = general.value("port", 5280);
		m_client->setConnection(new Jreen::ConnectionBOSH(host, port));
	} else {
		m_client->setConnection(new Jreen::TcpConnection(m_client->server(), m_client->port()));
	}
	general.endGroup();

	general.endGroup();

	lconnect(m_client->connection(), SIGNAL(stateChanged(SocketState)), this,
		[=](Jreen::Connection::SocketState state)
	{
		if (state == Jreen::Connection::UnconnectedState)
			this->onDisconnected(Jreen::Client::Unknown);
	});
	return true;
}

bool JabberAccount::disableImpl()
{
	m_client->deleteLater(); m_client = 0;
	return true;
}

Contact* JabberAccount::createContact(const QString &id)
{
	return createContactImpl(id);
}

JabberContact* JabberAccount::createContactImpl(const QString &id)
{
	auto contact = new JabberContact(id, this);
	addContact(contact);
	return contact;
}

void JabberAccount::setStatusImpl(Status status, const QString &statusText)
{
	auto old = this->status();
	if ((old & Status::Connecting) && status != Status::Offline) {
		status |= Status::Connecting;
		updateStatus(status, statusText);
		return;
	} else if (old == Status::Offline && (status & Status::Online) && !(status & Status::Connecting)) {
		status |= Status::Connecting;
		m_client->connectToServer();
	} else if ((old & Status::Online) && (status & Status::Offline)) {
		m_loadFlags = 0;
		m_client->disconnectFromServer(true);
	}

	if (old != Status::Offline)
		sendPresence(status, statusText);

	updateStatus(status, statusText);
}

bool JabberAccount::setExtendedInfoImpl(const QString &name, const QVariantMap &info)
{
	JabberPep *pep = 0;
	foreach (auto itr, JabberProtocol::instance()->peps()) {
		if (itr->name() == name) {
			pep = itr;
			break;
		}
	}

	if (!pep)
		return false;

	auto payload = pep->fromMap(info);
	m_pubsub->publishItems(QList<Jreen::Payload::Ptr>() << payload, Jreen::JID());
	return true;
}


void JabberAccount::onConnected()
{
	if (m_loadFlags != EveryModuleLoaded)
		return;
	qDebug() << "Roster received";
	auto status = this->status() ^ Status::Connecting;
	sendPresence(status, statusText());
	updateStatus(status, statusText());
	m_vcardManager->request(id());
	m_bookmarksManager->sync();
	m_client->setPingInterval(this->config("general").value("pingInterval", 30000));

	bool supportPeps = checkIdentity("pubsub", "pep");
	if (supportPeps) {
		foreach (auto pep, JabberProtocol::instance()->peps()) {
			if (auto act = pep->action())
				act->setEnabled(true, this);
		}
	}

	foreach (auto act, JabberProtocol::instance()->onlineActions())
		act->setEnabled(true, this);
}

void JabberAccount::onDisconnected(Jreen::Client::DisconnectReason reason)
{
	updateStatus(Status::Offline);
	m_bookmarksManager->clear();
	foreach (auto contact, this->contacts()) {
		foreach (auto resource, contact->resources())
			delete resource;
	}

	foreach (auto conf, conferences())
		conf->leave();

	foreach (auto pep, JabberProtocol::instance()->peps()) {
		if (auto act = pep->action())
			act->setEnabled(false, this);
	}

	foreach (auto act, JabberProtocol::instance()->onlineActions())
		act->setEnabled(false, this);
}

void JabberAccount::sendPresence(Status status, const QString &text)
{
	QString invisible = QLatin1String("invisible");
	if (status == Status::Invisible) {
		if (m_privacyManager->activeList() != invisible) {
			if (!m_privacyManager->lists().contains(invisible)) {
				Jreen::PrivacyItem item;
				item.setAction(Jreen::PrivacyItem::Deny);
				item.setOrder(1);
				item.setStanzaTypes(Jreen::PrivacyItem::PresenceOut);
				m_privacyManager->setList(invisible, QList<Jreen::PrivacyItem>() << item);
			}
			m_privacyManager->setActiveList(invisible);
		}
	} else {
		if (m_privacyManager->activeList() == invisible)
			m_privacyManager->desetActiveList();
	}
	m_client->setPresence(statusToPresence(status), text, m_priority);
}

} // Lime

