/*
    This file is part of Lime.
    Copyright (c) 2009  Denis Daschenko <daschenko@gmail.com>
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "servicebrowser.h"
#include "../jabberaccount.h"
#include <algorithm>
#include "ui_jabberservicebrowser.h"
#include <lime/icon.h>
#include <lime/config.h>
#include <lime/iconmanager/iconmanager.h>
#include <QMovie>
#include <QPoint>
#include <QMenu>

namespace Lime
{

template <typename T>
QString getServiceIcon(const T &item)
{
	QString service_icon;
	if (item.hasIdentity("server")) {
		service_icon = "network-server";
	} else if (item.hasIdentity("conference", "text")) {
		if (Jreen::JID(item.jid()).node().isEmpty())
			service_icon = "conference-server";
		else if (Jreen::JID(item.jid()).resource().isEmpty())
			service_icon = "conference";
		else
			service_icon = "conference-user";
	} else if (item.hasIdentity("conference", "irc")) {
		service_icon = "im-irc-gateway";
	} else if (item.hasIdentity("gateway", "icq")) {
		service_icon = "im-icq-gateway";
	} else if (item.hasIdentity("gateway", "aim")) {
		service_icon = "im-aim-gateway";
	} else if (item.hasIdentity("gateway", "mrim")) {
		service_icon = "im-mrim-gateway";
	} else if (item.hasIdentity("gateway", "msn")) {
		service_icon = "im-msn-gateway";
	} else if (item.hasIdentity("gateway", "xmpp")) {
		service_icon = "im-jabber-gateway";
	} else if (item.hasIdentity("gateway")) {
		service_icon = "im-default-gateway";
	} else if (item.hasIdentity("directory")) {
		service_icon = "edit-find-user";
	} else if (item.hasIdentity("automation")) {
		service_icon = "utilities-terminal";
	} else {
		service_icon = "defaultservice";
	}

	return service_icon;
}

JabberServiceBrowserModelItem::JabberServiceBrowserModelItem(const JabberDiscoItem &item, bool expandable) :
	m_parent(0),
	m_item(new JabberDiscoItem(item)),
	m_isExpandable(expandable),
	m_isExpanded(false),
	m_isEnabled(false)
{
	m_item->requestInfo();
	m_item->infoReceived.connect([=] {
		m_isEnabled = true;
		if (!m_item->identities().isEmpty())
			m_icon = Icon(getServiceIcon(*m_item));
		this->updateActions();
		updated();
	});
	m_item->itemsReceived.connect([=](const QList<JabberDiscoItem> &items) {
		QList<JabberServiceBrowserModelItem*> subitems;
		foreach (auto &item, items) {
			auto modelItem = new JabberServiceBrowserModelItem(item);
			modelItem->m_parent = this;
			subitems << modelItem;
		}
		childrenReceived(subitems);
		m_subitems = subitems;
		childrenAdded();
	});
	m_item->errorOccurred.connect([=](const QSharedPointer<Jreen::Error> &) {
		m_isEnabled = false;
		updated();
	});
}

JabberServiceBrowserModelItem::~JabberServiceBrowserModelItem()
{
	delete m_item;
	foreach (auto subitem, m_subitems)
		delete subitem;
}

void JabberServiceBrowserModelItem::expand()
{
	if (m_isExpandable && !m_isExpanded) {
		m_item->requestItems();
		m_isExpanded = true;
	}
}

struct IdentityWrapper
{
public:
	IdentityWrapper(const Jreen::Disco::Identity &identity, const Jreen::JID &jid) :
		m_identity(identity), m_jid(jid)
	{}
	bool hasIdentity(const QString &category, const QString &type = QString()) const
	{
		return category == m_identity.category() && (type.isEmpty() || type == m_identity.type());
	}
	Jreen::JID jid() const { return m_jid; }
private:
	Jreen::Disco::Identity m_identity;
	Jreen::JID m_jid;
};

QString JabberServiceBrowserModelItem::tooltip()
{
	if (!m_tooltip.isEmpty() && !m_isEnabled)
		return m_tooltip;

	auto name = m_item->name();
	if (name.isEmpty())
		m_tooltip = "<b>" + m_item->jid() + "</b><br/>";
	else
		m_tooltip = "<b>" + name + "</b> (" + m_item->jid() + ")<br/>";

	m_tooltip += QT_TR_NOOP("<br/><b>Identities:</b><br/>");
	foreach (auto &identity, m_item->identities()) {
		QString img = IconManager::instance()->path(getServiceIcon(IdentityWrapper(identity, m_item->jid())));
		m_tooltip += "<img src='" + img + "'> " + identity.name() + " (" +
					 QT_TR_NOOP("category: ") + identity.category() + ", " +
					 QT_TR_NOOP("type: ") + identity.type() + ")<br/>";
	}

	if (!m_item->features().isEmpty()) {
		m_tooltip += QT_TR_NOOP("<br/><b>Features:</b><br/>");
		foreach(auto &feature, m_item->features())
			m_tooltip += feature + "<br/>";
	}

	return m_tooltip;
}

void JabberServiceBrowserModelItem::updateActions()
{
	Actions actions;
	bool isIRC = m_item->hasIdentity("conference", "irc");
	m_actions |= ActionAdd;
	if (m_item->hasFeature(QLatin1String("http://jabber.org/protocol/muc"))
		&& (!Jreen::JID(m_item->jid()).node().isEmpty() || isIRC))
	{
		m_actions |= ActionJoin;
	}
	if (m_item->hasFeature(QLatin1String("http://jabber.org/protocol/bytestreams")))
		m_actions |= ActionProxy;
	if(m_item->hasFeature("http://jabber.org/protocol/muc#register")
			|| m_item->hasFeature(QLatin1String("jabber:iq:register")))
		m_actions |= ActionRegister;
	if (m_item->hasFeature(QLatin1String("jabber:iq:search")))
		m_actions |= ActionSearch;
	if (m_item->hasFeature(QLatin1String("vcard-temp")))
		m_actions |= ActionVCard;
	if (m_item->hasFeature(QLatin1String("http://jabber.org/protocol/disco#items"))
			|| (m_item->hasFeature(QLatin1String("http://jabber.org/protocol/muc")) && !isIRC)
			|| (m_item->features().isEmpty() && m_item->identities().isEmpty()))
		m_isExpandable = true;
	if (m_item->hasIdentity("automation")) {
		bool expand = false;
		foreach (auto &identity, m_item->identities())
			if (identity.type() == "command-list")
				expand = true;
		if (expand)
			m_isExpandable = true;
		m_actions |= ActionExecute;
	} else if (m_item->hasFeature(QLatin1String("http://jabber.org/protocol/commands"))) {
		m_isExpandable = true;
		m_actions |= ActionExecute;
	}
}

JabberServiceBrowserModel::JabberServiceBrowserModel(JabberServiceDiscovery *discovery, QObject *parent) :
	QAbstractItemModel(parent), m_discovery(discovery), m_root(0), m_fetchCount(0)
{

}

JabberServiceBrowserModel::~JabberServiceBrowserModel()
{
	delete m_root;
}

void JabberServiceBrowserModel::setService(const QString &service)
{
	beginResetModel();
	delete m_root;
	m_root = new JabberServiceBrowserModelItem(JabberDiscoItem(m_discovery, Jreen::JID(service)), true);
	handleNewItem(m_root);
	endResetModel();
	m_root->expand();
	// Two requests has been already sent, one requesting disco info from the JabberDiscoItem constructor
	// and another one requesting disco items from expand()
	fetchStateChanged(true);
	m_fetchCount = 2;
}

void JabberServiceBrowserModel::handleNewItem(JabberServiceBrowserModelItem *item)
{
	item->updated += [=] {
		auto modelIndex = this->createIndex(item->index(), 0, item);
		emit this->dataChanged(modelIndex, modelIndex);
		// A disco info request has been received (or an error has occurred, but that does not really matter
		// in that case)
		if (--m_fetchCount == 0)
			fetchStateChanged(false);
		Q_ASSERT(m_fetchCount >= 0);
	};

	item->childrenReceived += [=](const QList<JabberServiceBrowserModelItem*> &subitems) {
		this->beginInsertRows(this->createIndex(item->index(), 0, item), 0, subitems.count()-1);
		// A disco items request has been finished, but, for every received item, a disco info request,
		// has been sent.
		m_fetchCount += subitems.count() - 1;
		foreach (auto subitem, subitems)
			this->handleNewItem(subitem);
	};

	item->childrenAdded += [=] {
		this->endInsertRows();
	};
}

JabberServiceBrowserModelItem *JabberServiceBrowserModel::getItem(const QModelIndex &index)
{
	if (!index.isValid())
		return 0;
	return reinterpret_cast<JabberServiceBrowserModelItem*>(index.internalPointer());
}

int JabberServiceBrowserModel::columnCount(const QModelIndex &parent) const
{
	return 2;
}

QVariant JabberServiceBrowserModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role != Qt::DisplayRole || orientation != Qt::Horizontal)
		return QVariant();
	if (section == 0)
		return tr("Name");
	else if (section == 1)
		return tr("JID");
	return QVariant();
}

QVariant JabberServiceBrowserModel::data(const QModelIndex &index, int role) const
{
	auto item = getItem(index);
	if (!item)
		return QVariant();
	int column = index.column();
	auto &disco = item->discoItem();

	if (column == 0) {
		if (role == Qt::DisplayRole) {
			auto name = disco.name();
			return name.isEmpty() ? disco.jid().full() : name;
		} else if (role == Qt::DecorationRole) {
			return item->icon();
		}
	} else if (column == 1 && role == Qt::DisplayRole) {
		return disco.jid().full();
	}

	if (role == Qt::ToolTipRole)
		return item->tooltip();

	return QVariant();
}

Qt::ItemFlags JabberServiceBrowserModel::flags(const QModelIndex &index) const
{
	auto flags = QAbstractItemModel::flags(index);
	auto item = getItem(index);
	return item && !item->isEnabled() ? flags & ~Qt::ItemIsEnabled : flags;
}

QModelIndex JabberServiceBrowserModel::index(int row, int column, const QModelIndex &parent) const
{
	if (column < 0 || column >= 2 || row < 0)
		return QModelIndex();
	auto item = getItem(parent);
	if (!item) {
		if (row == 0)
			return createIndex(row, column, m_root);
	} else {
		auto items = item->subitems();
		if (row < items.count())
			return createIndex(row, column, items.at(row));
	}
	return QModelIndex();
}

QModelIndex JabberServiceBrowserModel::parent(const QModelIndex &child) const
{
	auto item = getItem(child);
	if (!item || !item->parent())
		return QModelIndex();
	auto parent = item->parent();
	int index = parent->index();
	if (index != -1)
		return createIndex(index, 0, parent);
	return QModelIndex();
}

int JabberServiceBrowserModel::rowCount(const QModelIndex &parent) const
{
	auto item = getItem(parent);
	if (!item)
		return m_root ? 1 : 0;
	return item->subitems().count();
}

bool JabberServiceBrowserModel::hasChildren(const QModelIndex &parent) const
{
	auto item = getItem(parent);
	if (!item)
		return true;
	return item->subitems().count() > 0 || item->isExpandable();
}

bool JabberServiceBrowserModel::canFetchMore(const QModelIndex &parent) const
{
	auto item = getItem(parent);
	if (!item)
		return false;
	return item->isExpandable() && !item->isExpanded();
}

void JabberServiceBrowserModel::fetchMore(const QModelIndex &parent)
{
	auto item = getItem(parent);
	item->expand();
	if (m_fetchCount++ == 0)
		fetchStateChanged(true);
}

void JabberServiceBroserProxyModel::setFilter(const QString &filter)
{
	m_filter = filter;
	invalidateFilter();
}

bool JabberServiceBroserProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
	if (m_filter.isEmpty())
		return true;

	auto model = sourceModel();
	auto index = model->index(sourceRow, 0, sourceParent);
	auto item = JabberServiceBrowserModel::getItem(index);

	if (!item)
		return true;

	// accept if the row matches the filter
	auto &disco = item->discoItem();
	if (disco.name().contains(m_filter, Qt::CaseInsensitive) ||
		disco.jid().full().contains(m_filter, Qt::CaseInsensitive))
	{
		return true;
	}

	// accept if at least one of its children matches the filter
	int childCount = model->rowCount(index);
	if (childCount == 0)
		return false;
	for (int i = 0; i < childCount; ++i) {
		if (filterAcceptsRow(i, index))
			return true;
	}
	return false;
}

JabberServiceBrowser::JabberServiceBrowser(JabberAccount *account, QWidget *parent) :
	QWidget(parent),
	m_account(account),
	ui(new Ui::JabberServiceBrowser),
	m_serviceModel(new JabberServiceBrowserModel(account->serviceDiscovery(), this))
{
	ui->setupUi(this);
	setWindowTitle(tr("Search service"));
	ui->featuresView->setVisible(false);
	auto proxyModel = new JabberServiceBroserProxyModel(this);
	proxyModel->setSourceModel(m_serviceModel);
	ui->serviceTree->setModel(proxyModel);
	ui->labelLoader->hide();
	auto loadingMovie = new QMovie(":/pictures/loading.gif", QByteArray(), this);

	ui->searchButton->setIcon(Icon("system-search"));
	ui->clearButton->setIcon(Icon("edit-clear-locationbar-rtl"));
	ui->actionSearch->setIcon(Icon("system-search"));
	ui->actionJoin->setIcon(Icon("conference"));
	ui->actionExecute->setIcon(Icon("utilities-terminal"));
	ui->actionSearch->setIcon(Icon("edit-find-user"));
	ui->actionRegister->setIcon(Icon("dialog-password-service"));
	ui->actionShowVCard->setIcon(Icon("dialog-information-user"));
	ui->actionAdd->setIcon(Icon("list-add-user"));
	ui->labelLoader->setMovie(loadingMovie);

	auto itemMenu = new QMenu(this);
	itemMenu->addAction(ui->actionJoin);
	itemMenu->addAction(ui->actionRegister);
	itemMenu->addAction(ui->actionSearch);
	itemMenu->addAction(ui->actionExecute);
	itemMenu->addAction(ui->actionAdd);
	itemMenu->addAction(ui->actionShowVCard);

	connect(ui->searchButton, SIGNAL(clicked(bool)), this, SLOT(startNewSearch()));
	connect(ui->serviceServer, SIGNAL(currentIndexChanged(int)), this, SLOT(startNewSearch()));
	connect(ui->filterLine, SIGNAL(textChanged(QString)), proxyModel, SLOT(setFilter(QString)));
	connect(ui->clearButton, SIGNAL(clicked(bool)), ui->filterLine, SLOT(clear()));
	lconnect(m_serviceModel, SIGNAL(fetchStateChanged(bool)), ui->labelLoader, [=](bool fetching) {
		ui->labelLoader->setVisible(fetching);
		if (fetching)
			loadingMovie->start();
		else
			loadingMovie->stop();
	});
	lconnect(ui->serviceTree, SIGNAL(customContextMenuRequested(QPoint)), this, [=](const QPoint &pos) {
		auto index = ui->serviceTree->indexAt(pos);
		auto item = JabberServiceBrowserModel::getItem(index);
		if (!item)
			return;
		auto actions = item->actions();
		ui->actionAdd->setVisible(actions & JabberServiceBrowserModelItem::ActionAdd);
		ui->actionExecute->setVisible(actions & JabberServiceBrowserModelItem::ActionExecute);
		ui->actionJoin->setVisible(actions & JabberServiceBrowserModelItem::ActionJoin);
		ui->actionRegister->setVisible(actions & JabberServiceBrowserModelItem::ActionRegister);
		ui->actionSearch->setVisible(actions & JabberServiceBrowserModelItem::ActionSearch);
		ui->actionShowVCard->setVisible(actions & JabberServiceBrowserModelItem::ActionVCard);
		//ui->actionProxy->setVisible(actions & JabberServiceBrowserModelItem::ActionProxy);
		itemMenu->popup(ui->serviceTree->viewport()->mapToGlobal(pos));
	});

	Config cfg = m_account->config("serviceBrowser");
	if (cfg.hasChildKey("geometry"))
		restoreGeometry(cfg.value("geometry", QByteArray()));
	else
		centerizeWidget(this);

	ui->serviceServer->addItems(cfg.value("servers", QStringList() << m_account->jid().domain()));
	auto lastServer = cfg.value("lastServer", QString());
	if (lastServer.isEmpty()) {
		ui->serviceServer->setCurrentIndex(0);
	} else {
		int index = ui->serviceServer->findText(lastServer);
		if (index != -1)
			ui->serviceServer->setCurrentIndex(index);
		else
			ui->serviceServer->setEditText(lastServer);
	}
}

JabberServiceBrowser::~JabberServiceBrowser()
{
	Config cfg = m_account->config("serviceBrowser");
	cfg.setValue("geometry", saveGeometry());

	QSet<QString> servers;
	for (int i = 0, c = ui->serviceServer->count(); i < c; ++i)
		servers.insert(ui->serviceServer->itemText(i));
	cfg.setValue("servers", QStringList(servers.toList()));
	cfg.setValue("lastServer",ui->serviceServer->currentText());

	delete ui;
}

void JabberServiceBrowser::startNewSearch()
{
	auto service = ui->serviceServer->currentText();
	int index = ui->serviceServer->findText(service);
	if (index == -1) {
		ui->serviceServer->blockSignals(true);
		ui->serviceServer->addItem(service);
		ui->serviceServer->blockSignals(false);
	}
	m_serviceModel->setService(service);
	m_serviceModel->root()->childrenAdded += [=] {
		ui->serviceTree->expandAll();
	};
}


} // Lime

