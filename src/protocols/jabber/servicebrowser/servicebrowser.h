/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_SERVICEBROWSER_H
#define LIME_SERVICEBROWSER_H

#include "../jabberservicediscovery.h"
#include <QAbstractItemModel>
#include <QWidget>
#include <QIcon>
#include <QSortFilterProxyModel>

namespace Ui {
	class JabberServiceBrowser;
}

namespace Lime {

class JabberServiceBrowserModelItem
{
public:
	enum Action
	{
		ActionExecute  = 0x0001,
		ActionRegister = 0x0002,
		ActionJoin     = 0x0004,
		ActionSearch   = 0x0008,
		ActionAdd      = 0x0010,
		ActionVCard    = 0x0020,
		ActionProxy    = 0x0040
	};
	Q_DECLARE_FLAGS(Actions, Action)
	JabberServiceBrowserModelItem(const JabberDiscoItem &item, bool expandable = false);
	~JabberServiceBrowserModelItem();
	JabberServiceBrowserModelItem *parent() const { return m_parent; }
	const JabberDiscoItem &discoItem() const { return *m_item; }
	const QList<JabberServiceBrowserModelItem*> &subitems() const { return m_subitems; }
	bool isExpandable() const { return m_isExpandable; }
	bool isExpanded() const { return m_isExpanded; }
	bool isEnabled() const { return m_isEnabled; }
	void expand();
	int index() const { return !m_parent ? 0 : m_parent->m_subitems.indexOf(const_cast<JabberServiceBrowserModelItem*>(this)); }
	QIcon icon() const { return m_icon; }
	Actions actions() const { return m_actions; }
	QString tooltip();
// signals:
	Signal<void()> updated;
	Signal<void(const QList<JabberServiceBrowserModelItem*> &)> childrenReceived;
	Signal<void()> childrenAdded;
private:
	void updateActions();
private:
	JabberServiceBrowserModelItem *m_parent;
	JabberDiscoItem *m_item;
	QList<JabberServiceBrowserModelItem*> m_subitems;
	bool m_isExpandable;
	bool m_isExpanded;
	bool m_isEnabled;
	QIcon m_icon;
	Actions m_actions;
	QString m_tooltip;
};

class JabberServiceBroserProxyModel : public QSortFilterProxyModel
{
	Q_OBJECT
public:
	JabberServiceBroserProxyModel(QObject *parent = 0) : QSortFilterProxyModel(parent) {}
public slots:
	void setFilter(const QString &filter);
protected:
	bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const;
private:
	QString m_filter;
};

class JabberServiceBrowserModel : public QAbstractItemModel
{
	Q_OBJECT
public:
	explicit JabberServiceBrowserModel(JabberServiceDiscovery *discovery, QObject* parent = 0);
	virtual ~JabberServiceBrowserModel();
	void setService(const QString &service);
	JabberServiceBrowserModelItem *root() { return m_root; }
	static JabberServiceBrowserModelItem *getItem(const QModelIndex &index);
signals:
	void fetchStateChanged(bool fetching);
protected:
	virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
	virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	virtual Qt::ItemFlags flags(const QModelIndex& index) const;
	virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
	virtual QModelIndex parent(const QModelIndex &child) const;
	virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
	virtual bool hasChildren(const QModelIndex &parent = QModelIndex()) const;
	virtual bool canFetchMore(const QModelIndex &parent) const;
	virtual void fetchMore(const QModelIndex &parent);
private:
	void handleNewItem(JabberServiceBrowserModelItem *item);
	JabberServiceDiscovery *m_discovery;
	JabberServiceBrowserModelItem *m_root;
	int m_fetchCount;
};

class JabberServiceBrowser : public QWidget
{
	Q_OBJECT
public:
	JabberServiceBrowser(JabberAccount *account, QWidget *parent = 0);
	virtual ~JabberServiceBrowser();
private slots:
	void startNewSearch();
private:
	JabberAccount *m_account;
	Ui::JabberServiceBrowser *ui;
	JabberServiceBrowserModel *m_serviceModel;
};

} // Lime

#endif // LIME_SERVICEBROWSER_H
