/*
    This file is part of Lime.
    Copyright (c) 2009 by Nigmatullin Ruslan <euroelessar@gmail.com>
    Copyright (c) 2011 by Sidorov Aleksey <sauron@citadelspb.com>
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "jabberconference.h"
#include "jabberaccount.h"
#include "jabberroster.h"
#include "jabberprotocol.h"
#include "jabbervcards.h"
#include <jreen/delayeddelivery.h>
#include <jreen/vcardupdate.h>

namespace Lime
{

using namespace Jreen;

JabberConfParticipant::JabberConfParticipant(const QString &id, const QString &nick, Conference *conference) :
	ConferenceParticipant(id, conference, ConferenceParticipantDefaultFlags)
{
	updateName(nick);
}

void JabberConfParticipant::handlePresence(const Jreen::Presence &presence)
{
	switch(presence.subtype())
	{
	case Jreen::Presence::Subscribe:
	case Jreen::Presence::Unsubscribe:
	case Jreen::Presence::Unsubscribed:
	case Jreen::Presence::Subscribed:
		break;
	case Jreen::Presence::Error:
	case Jreen::Presence::Probe:
		return;
	default:
		break;
	}
	updateStatus(presenceToStatus(presence.subtype()), presence.status());
	updatePriority(presence.priority());

	if (auto vcard = presence.payload<Jreen::VCardUpdate>()) {
		if (vcard->hasPhotoInfo()) {
			auto avatar = account()->getAvatar(vcard->photoHash());
			if (!avatar.isEmpty())
				updateAvatar(avatar);
			else if (account()->avatarsAutoLoad())
				account()->vcardManager()->request(id());
		} else {
			updateAvatar(QString());
		}
	}
}

void JabberConfParticipant::handleRole(Jreen::MUCRoom::Affiliation affiliation, Jreen::MUCRoom::Role role)
{
	m_affiliation = affiliation;
	m_role = role;
	updateRole(affiliation << 10 | role);
}

QString JabberConfParticipant::roleString() const
{
	QString role;
	if (m_affiliation == MUCRoom::AffiliationOwner) {
		role = tr("owner");
	} else if (m_affiliation == MUCRoom::AffiliationAdmin) {
		role = tr("administrator");
		if (m_role == MUCRoom::RoleParticipant)
			role = role + tr(" and") + tr(" participant");
		else if (m_role == MUCRoom::RoleVisitor)
			role = role + tr(" and") + tr(" visitor");
	} else if (m_affiliation == MUCRoom::AffiliationMember) {
		role = tr("registered member");
		if (m_role == MUCRoom::RoleModerator)
			role = role + tr(" and") + tr(" moderator");
		else if (m_role == MUCRoom::RoleVisitor)
			role = role + tr(" and") + tr(" visitor");
	} else if (m_role == MUCRoom::RoleModerator) {
		role = tr("moderator");
	} else if (m_role == MUCRoom::RoleParticipant) {
		role = tr("participant");
	} else if (m_role == MUCRoom::RoleVisitor)  {
		role = tr("visitor");
	}
	return role;
}

ChatUnit::SendMessageError JabberConfParticipant::sendMessageImpl(const Lime::Message &message)
{
	return JabberRoster::sendMessage(message);
}

JabberConference::JabberConference(JabberAccount *account, const Jreen::JID &room, const QString &password) :
	Conference(room.bare(), account, ConferenceDefaultFlags)
{
	m_room = new MUCRoom(account->client(), room);
	m_room->setPassword(password);

	m_room->setNick(account->id());

	connect(m_room, SIGNAL(presenceReceived(Jreen::Presence,const Jreen::MUCRoom::Participant*)),
			SLOT(onPresenceReceived(Jreen::Presence,const Jreen::MUCRoom::Participant*)));
	connect(m_room, SIGNAL(messageReceived(Jreen::Message,bool)),
			SLOT(onMessage(Jreen::Message,bool)));
	lconnect(m_room, SIGNAL(serviceMessageReceived(Jreen::Message)), this, [=](const Jreen::Message &msg) {
		// m_topic = msg.subject();
		this->addServiceMessage(msg.body());
	});
}

void JabberConference::joinImpl()
{
	m_room->join();
	handleJoin(new JabberConfParticipant(account()->id(), m_room->nick(), this));
}

void JabberConference::leaveImpl()
{
	m_room->leave();
	handleLeave();
}

JabberConference::SendMessageError JabberConference::sendMessageImpl(const Message &message)
{
	Jreen::Message jMsg(Jreen::Message::Groupchat, id(), message.text());
	jMsg.setID(account()->client()->getID());
	account()->client()->send(jMsg);
	return MessageSent;
}

void JabberConference::onPresenceReceived(const Jreen::Presence &presence,
										  const Jreen::MUCRoom::Participant *participant)
{
	auto nick = presence.from().resource();
	bool isUser = nick == m_room->nick();
	auto user = this->participant(nick);

	if (participant->isBanned() || participant->isKicked()) {
		Q_ASSERT(user);
		auto type = participant->isBanned() ? Banned : Kicked;
		if (isUser)
			handleLeave(type, participant->reason());
		else
			removeParticipant(user, type, participant->reason());
		return;
	}

	if (participant->isNickChanged()) {
		Q_ASSERT(user);
		QString newNick = participant->newNick();
		if (newNick.isEmpty())
			return;
		user->updateName(newNick);
	}

	if (!user && presence.subtype() != Presence::Unavailable) {
		auto realJID = participant->realJID();
		user = new JabberConfParticipant(realJID, nick, this);
		user->handlePresence(presence);
		user->handleRole(participant->affiliation(), participant->role());
		addParticipant(user, user->roleString());
	} else if (!user) {
		return;
	} else if (presence.subtype() == Presence::Unavailable) {
		removeParticipant(user);
		return;
	} else {
		user->handlePresence(presence);
	}

	if (user->role() != participant->role() ||
		user->affiliation() != participant->affiliation())
	{
		user->handleRole(participant->affiliation(), participant->role());
		// TODO: Move it to the core or at least pass some enum value to identify the message
		addServiceMessage(tr("%1 now is %2").arg(nick).arg(user->roleString()));
	}
}

void JabberConference::onMessage(const Jreen::Message &msg, bool isPrivate)
{
	QString nick = msg.from().resource();
	JabberConfParticipant *user = participant(nick);
	Message coreMsg(msg.body(),
					nick == m_room->nick() ? Message::Outgoing : Message::Incoming);
	if (coreMsg.direction() == Message::Outgoing) {
		// TODO: It means that the message has been delivered, right? So handle it!
		return;
	}

	if (isPrivate) {
		if (!user)
			return;
		coreMsg.setUnit(user);
		user->handleIncomingMessage(coreMsg);
	} else {
		coreMsg.setUnit(this);
		coreMsg.setSenderName(nick);
		coreMsg.setSubject(msg.subject());
		const auto when = msg.when();
		if (when) {
			coreMsg.setType(Message::HistoryMessage | Message::TextMessage);
			coreMsg.setTime(when->dateTime());
		} else {
			coreMsg.setTime(QDateTime::currentDateTime());
		}
		handleIncomingMessage(coreMsg);
	}
}


} // Lime

