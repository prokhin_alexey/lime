/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "jabberbookmarks.h"
#include "jabberaccount.h"
#include "jabberconference.h"
#include "jabberprotocol.h"
#include <algorithm>
#include <QFormLayout>
#include <QLineEdit>
#include <QCheckBox>

namespace Lime
{

using namespace Jreen;

JabberBookmarks::JabberBookmarks(JabberAccount *account) :
	QObject(account), m_account(account), m_isInited(false)
{
	m_storage = new BookmarkStorage(account->client());
	m_storage->setPrivateXml(account->privateXml());
	m_storage->setPubSubManager(account->pubSubManager());

	lconnect(m_storage, SIGNAL(bookmarksReceived(Jreen::Bookmark::Ptr)), this, [=](const Bookmark::Ptr &bookmark) {
		if (bookmark) {
			auto oldBookmarks(m_bookmarks);
			m_bookmarks = bookmark->conferences();
			foreach (auto &bookmark, oldBookmarks) {
				auto itr = std::find_if(m_bookmarks.begin(), m_bookmarks.end(),
					[=](const Bookmark::Conference &current)
				{
					return bookmark.name() == current.name();
				});
				if (itr != m_bookmarks.end())
					itr->setPassword(bookmark.password());
			}
		}

		updateConferences(!m_isInited);
		if (!m_isInited) {
			JabberProtocol::instance()->joinAction()->setEnabled(true, m_account);
			m_isInited = true;
		}
	});
}

void JabberBookmarks::sync()
{
	m_storage->requestBookmarks();
}

void JabberBookmarks::clear()
{
	m_isInited = false;
	m_bookmarks.clear();
	JabberProtocol::instance()->joinAction()->setEnabled(false, m_account);
}

void JabberBookmarks::setBookmarks(const QList<Bookmark::Conference> &bookmarks)
{
	m_bookmarks = bookmarks;
	Bookmark::Ptr bookmark = Bookmark::Ptr::create();
	bookmark->setConferences(bookmarks);
	m_storage->storeBookmarks(bookmark);
	updateConferences();
}

void JabberBookmarks::showJoinConferenceWidget()
{
	if (!m_joinWidget) {
		m_joinWidget = new JabberJoinConfWidget(this);
		m_joinWidget->setAttribute(Qt::WA_DeleteOnClose);
	}
	m_joinWidget->show();
	m_joinWidget->raise();
}

void JabberBookmarks::updateConferences(bool allowJoin)
{
	foreach (auto &bookmark, m_bookmarks) {
		auto conf = m_account->conference(bookmark.jid(), true);
		conf->setName(bookmark.name());
		conf->setPassword(bookmark.password());
		conf->setNick(bookmark.nick());
		if (allowJoin && bookmark.autojoin())
			conf->join();
	}
}

static inline QVariant toVariant(Bookmark::Conference *conf)
{
	return QVariant::fromValue(reinterpret_cast<void*>(conf));
}

static inline Bookmark::Conference *fromVariant(const QVariant &var)
{
	return reinterpret_cast<Bookmark::Conference*>(var.value<void*>());
}

JabberJoinConfWidget::JabberJoinConfWidget(JabberBookmarks *manager) :
	m_manager(manager), m_current(0)
{
	auto layout = new QFormLayout;
	m_name = new QLineEdit;
	m_nick = new QLineEdit;
	m_conference = new QLineEdit;
	m_password = new QLineEdit;
	m_autoJoin = new QCheckBox("Auto join");
	m_password->setEchoMode(QLineEdit::Password);

	layout->addRow(tr("Name:"), m_name);
	layout->addRow(tr("Conference:"), m_conference);
	layout->addRow(tr("Nick:"), m_nick);
	layout->addRow(tr("Password:"), m_password);
	layout->addRow(m_autoJoin);
	setBookmarkLayout(layout);

	connect(m_name, SIGNAL(textEdited(QString)),
			SLOT(updateCurrentName(QString)));

	foreach (auto &bookmark, manager->bookmarks()) {
		auto ptr = new Bookmark::Conference(bookmark);
		m_bookmarks << ptr;
		handleBookmark(bookmark.name(), toVariant(ptr));
	}
}

JabberJoinConfWidget::~JabberJoinConfWidget()
{
	qDeleteAll(m_bookmarks);
}

void JabberJoinConfWidget::createBookmark()
{
	auto bookmark = new Bookmark::Conference;
	m_bookmarks.append(bookmark);
	handleBookmark("", toVariant(bookmark));
}

void JabberJoinConfWidget::join(const QVariant &userData)
{
	updateCurrent();
	auto bookmark = fromVariant(userData);
	auto conf = m_manager->account()->conference(bookmark->jid(), true);
	conf->setNick(bookmark->nick());
	conf->setPassword(bookmark->password());
	conf->setName(bookmark->name());
	conf->join();
	conf->activateChat();
}

void JabberJoinConfWidget::removeBookmark(const QVariant &userData)
{
	int index = m_bookmarks.indexOf(fromVariant(userData));
	if (index != -1)
		delete m_bookmarks.takeAt(index);
}

void JabberJoinConfWidget::save()
{
	updateCurrent();
	QList<Bookmark::Conference> bookmarks;
	foreach (auto bookmark, m_bookmarks)
		bookmarks << *bookmark;
	m_manager->setBookmarks(bookmarks);
}

void JabberJoinConfWidget::setBookmark(const QVariant &userData)
{
	updateCurrent();
	m_current = fromVariant(userData);
	m_name->setText(m_current->name());
	m_nick->setText(m_current->nick());
	m_conference->setText(m_current->jid().full());
	m_password->setText(m_current->password());
	m_autoJoin->setChecked(m_current->autojoin());
}

void JabberJoinConfWidget::updateCurrent()
{
	if (!m_current)
		return;
	m_current->setName(m_name->text());
	m_current->setNick(m_nick->text());
	m_current->setJid(m_conference->text());
	m_current->setPassword(m_password->text());
	m_current->setAutojoin(m_autoJoin->isChecked());
}

} // Lime

