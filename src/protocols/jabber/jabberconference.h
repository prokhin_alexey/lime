/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_JABBERCONFERENCE_H
#define LIME_JABBERCONFERENCE_H

#include <lime/conference.h>
#include "jabberresource.h"
#include "jreen/mucroom.h"

namespace Lime {

class JabberAccount;

class JabberConfParticipant : public ConferenceParticipant, public JabberResourceInterface
{
public:
    JabberConfParticipant(const QString &id, const QString &nick, Conference *conference);
	void handlePresence(const Jreen::Presence &presence);
	void handleRole(Jreen::MUCRoom::Affiliation affiliation, Jreen::MUCRoom::Role role);
	Jreen::MUCRoom::Affiliation affiliation() const { return m_affiliation; }
	Jreen::MUCRoom::Role role() const { return m_role; }
	JabberAccount *account() const { return reinterpret_cast<JabberAccount*>(ConferenceParticipant::account()); }
	QString roleString() const;
	void updateAvatar(const QString &avatar) { ConferenceParticipant::updateAvatar(avatar); }
protected:
	virtual SendMessageError sendMessageImpl(const Lime::Message &message);
	Buddy *toBuddy() { return this; }
private:
	friend class JabberConference;
	Jreen::MUCRoom::Affiliation m_affiliation;
	Jreen::MUCRoom::Role m_role;
};

class JabberConference : public Conference
{
	Q_OBJECT
public:
	JabberConference(JabberAccount *account, const Jreen::JID &room, const QString &password = QString());
	Jreen::MUCRoom *room() const { return m_room; }
	JabberConfParticipant *participant(const QString &nick) const;
	JabberAccount *account() const;
	void setName(const QString &name) { updateName(name); }
	void setNick(const QString &nick) { m_room->setNick(nick); };
	void setPassword(const QString &password) { m_room->setPassword(password); }
protected:
	virtual void joinImpl();
	virtual void leaveImpl();
    virtual SendMessageError sendMessageImpl(const Message &message);
private slots:
	void onPresenceReceived(const Jreen::Presence &presence, const Jreen::MUCRoom::Participant *part);
	void onMessage(const Jreen::Message &msg, bool isPrivate);
private:
	Jreen::MUCRoom *m_room;

};

inline JabberConfParticipant *JabberConference::participant(const QString &nick) const
{
	return reinterpret_cast<JabberConfParticipant*>(Conference::participant(nick));
}

inline JabberAccount *JabberConference::account() const
{
	return reinterpret_cast<JabberAccount*>(Conference::account());
}

} // Lime

#endif // LIME_JABBERCONFERENCE_H
