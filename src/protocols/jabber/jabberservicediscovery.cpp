/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "jabberservicediscovery.h"
#include "jabberaccount.h"
#include "jabberprotocol.h"

namespace Lime
{

JabberDiscoItem::JabberDiscoItem(JabberServiceDiscovery *discovery, const Jreen::JID &jid) :
	d(new JabberDiscoItemPrivate)
{
	d->discovery = discovery;
	d->jid = jid;
}

JabberDiscoItem::~JabberDiscoItem()
{
	emit destroyed();
}

bool JabberDiscoItem::hasIdentity(const QString &category, const QString &type) const
{
	foreach (auto &identity, d->identities)
		if (identity.category() == category && (type.isEmpty() || identity.type() == type))
			return true;
	return false;
}

void JabberDiscoItem::requestInfo()
{
	d->discovery->requestInfo(this);
}

void JabberDiscoItem::requestItems()
{
	d->discovery->requestItems(this);
}

JabberServiceDiscovery::JabberServiceDiscovery(JabberAccount *account) :
	QObject(account), m_account(account), m_lastContext(0)
{

}

JabberServiceDiscovery::~JabberServiceDiscovery()
{

}

void JabberServiceDiscovery::handleIQ(const Jreen::IQ &iq, int context)
{
	auto item = m_requests.take(context);
	auto from = iq.from();
	if (!item)
		return;
	if (auto error = iq.error())
		handleDiscoError(from, error, item);
	else if(auto info = iq.payload<Jreen::Disco::Info>())
		handleDiscoInfo(from, info, item);
	else if(auto items = iq.payload<Jreen::Disco::Items>())
		handleDiscoItems(from, items, item);
}

template <typename ExtensionType>
void JabberServiceDiscovery::request(JabberDiscoItem *item)
{
	int context = ++m_lastContext;
	m_requests.insert(context, item);
	Jreen::IQ info(Jreen::IQ::Get, item->jid());
	info.addExtension(new ExtensionType(item->node()));
	m_account->client()->send(info, this, SLOT(handleIQ(Jreen::IQ,int)), context);

	item->destroyed += [=] {
		m_requests.remove(context);
	};
}

void JabberServiceDiscovery::requestInfo(JabberDiscoItem *item)
{
	request<Jreen::Disco::Info>(item);
}

void JabberServiceDiscovery::requestItems(JabberDiscoItem *item)
{
	request<Jreen::Disco::Items>(item);
}

void JabberServiceDiscovery::handleDiscoInfo(const Jreen::JID &from, QSharedPointer<Jreen::Disco::Info> info, JabberDiscoItem *item)
{
	item->d->jid = from;
	item->d->node = info->node();
	item->d->features = info->features();
	item->d->identities = info->identities();
	emit item->infoReceived();
}

void JabberServiceDiscovery::handleDiscoItems(const Jreen::JID &from, QSharedPointer<Jreen::Disco::Items> discoItems, JabberDiscoItem *item)
{
	QList<JabberDiscoItem> items;
	foreach (auto &discoItem, discoItems->items()) {
		items.append(JabberDiscoItem(this, discoItem.jid()));
		auto &appended = items.last();
		appended.d->name = discoItem.name().replace("\n", " | ");
		appended.d->node = discoItem.node();
	}
	emit item->itemsReceived(items);
}

void JabberServiceDiscovery::handleDiscoError(const Jreen::JID &from, QSharedPointer<Jreen::Error> error, JabberDiscoItem *item)
{
	item->d->error = errorString(error);
	emit item->errorOccurred(error);
}

} // Lime

