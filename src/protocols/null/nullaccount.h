/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_NULLACCOUNT_H
#define LIME_NULLACCOUNT_H

#include <lime/account.h>

namespace Lime {

class NullContact;
class NullConference;

class NullAccount : public TagAccount
{
	Q_OBJECT
public:
    NullAccount(const QString &id);
    virtual ~NullAccount();
    NullContact* createContactImpl(const QString &id);
    NullConference *createConference(const QString &id);
protected:
    virtual bool enableImpl();
    virtual bool disableImpl();
    virtual Contact* createContact(const QString &id);
	virtual void setStatusImpl(Status status, const QString &statusText);
	virtual void storeContactList(Config &cfg) { storeEmptyGroups(cfg); }
	virtual void loadContactList(const Config &cfg);
private:
};

}

#endif // LIME_NULLACCOUNT_H
