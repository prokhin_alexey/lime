/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "nullaccount.h"
#include "nullprotocol.h"
#include "nullcontact.h"
#include <QTimer>
#include <QDebug>
#include "nullconference.h"

namespace Lime
{

NullAccount::NullAccount(const QString &id) :
	TagAccount(id, NullProtocol::instance(), AccountDefaultFlags)
{
	NullProtocol::instance()->addAccount(this);
	enable();
}

NullAccount::~NullAccount()
{
}

bool NullAccount::enableImpl()
{
	return true;
}

bool NullAccount::disableImpl()
{
	qDeleteAll(contacts());
	return true;
}

Contact* NullAccount::createContact(const QString &id)
{
	return createContactImpl(id);
}

NullContact* NullAccount::createContactImpl(const QString &id)
{
	auto contact = new NullContact(id, this);
	addContact(contact);
	return contact;
}

NullConference *NullAccount::createConference(const QString &id)
{
	auto conf = new NullConference(this, id);
	addConference(conf);
	return conf;
}

void NullAccount::setStatusImpl(Status status, const QString &statusText)
{
	updateStatus(status, statusText);
}

void NullAccount::loadContactList(const Config &cfg)
{
	if (ExtensionManager::state() == ExtensionManager::Unittesting)
		return;

	loadEmptyGroups(cfg);

	auto contact = createContactImpl("null1");
	contact->setGroup("group1");
	contact->setStatus(Status::Offline);
	contact->addToContactList();
	/*QTimer *timer = new QTimer(this);
	timer->setInterval(4000);
	lconnect(timer, SIGNAL(timeout()), this, [contact]() {
		auto status = contact->status();
		if (status == Offline)
			status = Online;
		//else if (status == Online)
		//	status = Away;
		else
			status = Offline;
		contact->setStatus(status);
	});
	timer->start();*/

	contact = createContactImpl("null2");
	//contact->setGroup("group1");
	contact->setStatus(Status::Online);
	contact->addToContactList();
	/*timer = new QTimer(this);
	timer->setInterval(5000);
	lconnect(timer, SIGNAL(timeout()), this, [contact]() {
		auto group = contact->group();
		if (group == "group1")
			group = "group2";
		else
			group = "group1";
		contact->setGroups(QStringList() << group << "1");
	});
	timer->start();*/

	contact = createContactImpl("null3");
	contact->setGroup("group2");
	contact->setStatus(Status::Online);
	contact->addToContactList();

	NullContact::receiver = contact;

	auto conf = createConference("nulltest");
	conf->join();
	conf->activateChat();
}

} // Lime


