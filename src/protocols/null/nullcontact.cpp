/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "nullcontact.h"

namespace Lime
{

Pointer<NullContact> NullContact::receiver;

NullContact::NullContact(const QString &id, Account *account) :
	Contact(id, account,
			ContactDefaultFlags |
			SupportsMultiGroups |
			NameMayBeChanged    |
			GroupsMayBeChanged  |
			SupportDefaultGroup |
			SupportsChatStates)
{
	updateStatus(Status::Online, QString("%1's status").arg(id));
}

NullContact::~NullContact()
{
}

ChatUnit::SendMessageError NullContact::sendMessageImpl(const Message &message)
{
	if (receiver && message.type() == Message::TextMessage) {
		auto msg = message;
		msg.setDirection(Message::Incoming);
		msg.setTime(QDateTime::currentDateTime());
		receiver->handleIncomingMessage(msg);
	}
	return MessageSent;
}

void NullContact::setGroupsImpl(const QStringList &groups)
{
	updateGroups(groups);
}

void NullContact::setNameImpl(const QString &name)
{
	updateName(name);
}

void NullContact::setInListImpl(bool inList)
{
	updateInList(inList);
}

void NullContact::sendChatStateImpl(ChatState state)
{
	if (receiver)
		receiver->handleChatState(state);
}

} // Lime
