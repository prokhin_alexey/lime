/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "nullconference.h"
#include "nullaccount.h"
#include <lime/extention.h>

namespace Lime
{

NullConfParticipant::NullConfParticipant(const QString &nick, Conference *conference) :
	ConferenceParticipant(QString(), conference)
{
	updateName(nick);
	updateStatus(Status::Online);
}

ChatUnit::SendMessageError  NullConfParticipant::sendMessageImpl(const Lime::Message &message)
{
	if (message.type() == Message::TextMessage) {
		auto msg = message;
		msg.setDirection(Message::Incoming);
		msg.setTime(QDateTime::currentDateTime());
		handleIncomingMessage(msg);
	}
	return MessageSent;
}

NullConference::NullConference(NullAccount *account, const QString &id) :
	Conference(id, account)
{
}

void NullConference::joinImpl()
{
	handleJoin(new NullConfParticipant("me", this));
	if (ExtensionManager::state() == ExtensionManager::Unittesting)
		return;
	addParticipant(new NullConfParticipant("bot", this));
}

void NullConference::leaveImpl()
{
	handleLeave();
}

ChatUnit::SendMessageError NullConference::sendMessageImpl(const Message &message)
{
	if (message.type() == Message::TextMessage) {
		auto msg = message;
		msg.setUnit(this);
		msg.setSenderName("bot");
		msg.setDirection(Message::Incoming);
		msg.setTime(QDateTime::currentDateTime());
		handleIncomingMessage(msg);
	}
	return MessageSent;
}

} // Lime

