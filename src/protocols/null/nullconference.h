/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_NULLCONFERENCE_H
#define LIME_NULLCONFERENCE_H

#include <lime/conference.h>

namespace Lime {

class NullAccount;

class NullConfParticipant : public ConferenceParticipant
{
public:
    NullConfParticipant(const QString &nick, Conference *conference);
protected:
	virtual SendMessageError sendMessageImpl(const Lime::Message &message);
private:
	friend class NullConference;
};

class NullConference : public Conference
{
	Q_OBJECT
public:
	NullConference(NullAccount *account, const QString &id);
	NullConfParticipant *participant(const QString &nick) const;
	NullAccount *account() const;
protected:
	virtual void joinImpl();
	virtual void leaveImpl();
    virtual SendMessageError sendMessageImpl(const Message &message);

};

inline NullConfParticipant *NullConference::participant(const QString &nick) const
{
	return reinterpret_cast<NullConfParticipant*>(Conference::participant(nick));
}

inline NullAccount *NullConference::account() const
{
	return reinterpret_cast<NullAccount*>(Conference::account());
}

} // Lime

#endif // LIME_NULLCONFERENCE_H
