/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_ICONMANAGER_H
#define LIME_ICONMANAGER_H

#include <lime/extention.h>
#include <QHash>
#include <QDir>

namespace Lime {

class IconManager : public QObject, public Extension
{
	Q_OBJECT
public:
	QIcon icon(const QString &name);
	QString path(const QString &name, int size = 16);
    virtual bool enableImpl();
    virtual bool disableImpl();
    virtual QObject* toQObject() { return this; }
    static IconManager *instance() { static IconManager *manager = new IconManager; return manager; }
private:
	IconManager();
	void cacheDir(QDir dir);
	void cacheDirWithSizes(QDir dir);
	QVector<QHash<QString, QString> > m_iconPaths;
};

} // Lime

#endif // LIME_ICONMANAGER_H
