/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "iconmanager.h"
#include <lime/systeminfo.h>
#include <QIcon>
#include <QFileInfo>
#include <QApplication>
#include <QDebug>

static QList<int> sizes = QList<int>() << 16 << 20 << 22 << 32 << 48 << 64 << 128;

namespace Lime
{

QIcon IconManager::icon(const QString &name_helper)
{
	auto name = name_helper;
	QIcon icon;
	forever {
		for (int i = 0; i < sizes.size(); ++i) {
			auto path = m_iconPaths[i].value(name);
			if (!path.isEmpty()) {
				icon.addPixmap(QPixmap(path));
				break;
			}
		}
		if (!icon.isNull())
			break;
		auto i = name.lastIndexOf('-');
		if (i == -1)
			break;
		name = name.mid(0, i);
	}
	return icon;
}

QString IconManager::path(const QString &name_helper, int size)
{
	int sizeId = sizes.indexOf(size);
	if (size == -1)
		return QString();

	auto &iconPaths = m_iconPaths[sizeId];
	auto name = name_helper;
	forever {
		auto path = iconPaths.value(name);
		if (!path.isEmpty())
			return path;
		auto i = name.lastIndexOf('-');
		if (i == -1)
			break;
		name = name.mid(0, i);
	}
	return QString();
}

bool IconManager::enableImpl()
{
	cacheDir(SystemInfo::getDir(SystemInfo::SystemShareDir));
	cacheDir(SystemInfo::getDir(SystemInfo::ShareDir));
	cacheDir(QDir(QApplication::applicationDirPath() + QLatin1String("/share/")));
	cacheDirWithSizes(QDir(":/icons"));
	cacheDirWithSizes(QDir(QApplication::applicationDirPath() + QLatin1String("/share/icons/status")));

	return true;
}

bool IconManager::disableImpl()
{
	return false;
}

IconManager::IconManager()
{
	m_iconPaths.resize(sizes.size());
}

void IconManager::cacheDir(QDir dir)
{
	if (!dir.cd("icons"))
		return;
	auto &hash = m_iconPaths[0];
	foreach (auto &path, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
		dir.cd(path);
		foreach (auto &path, dir.entryInfoList(QDir::Files))
			hash.insert(path.baseName(), path.canonicalFilePath());
		dir.cdUp();
	}
}

void IconManager::cacheDirWithSizes(QDir dir)
{
	for (int i = 0; i < sizes.size(); ++i) {
		if (!dir.cd(QString("%1x%1").arg(sizes[i])))
			continue;
		auto &hash = m_iconPaths[i];
		foreach (auto &path, dir.entryInfoList(QDir::Files))
			hash.insert(path.baseName(), path.canonicalFilePath());
		dir.cdUp();
	}
}

} // Lime

