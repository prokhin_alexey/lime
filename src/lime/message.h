/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_MESSAGE_H
#define LIME_MESSAGE_H

#include "global.h"
#include <QDateTime>
#include <QSharedDataPointer>

namespace Lime {

class ChatUnit;
class MessagePrivate;

class Message
{
public:
	enum Direction
	{
		Incoming,
		Outgoing
	};
	enum Type
	{
		TextMessage      = 0x0000,
		HtmlMessage      = 0x0001,
		ServiceMessage   = 0x0002,
		ActionMessage    = 0x0004,
		HistoryMessage   = 0x0008,
		DontStoreMessage = 0x0010
	};
	Message();
	Message(const QString &text, Direction direction, const QDateTime &time = QDateTime::currentDateTime());
	Message(const Message &other);
	virtual ~Message();
	Message &operator=(const Message &other);
	int id() const;
	QString text() const;
	QString subject() const;
	QDateTime time() const;
	Direction direction() const;
	ChatUnit *unit() const;
	Type type() const;
	QString senderName() const;
	void setText(const QString &text);
	void setSubject(const QString &subject);
	void setTime(const QDateTime &time);
	void setDirection(Direction direction);
	void setUnit(ChatUnit *unit);
	void setType(Type type);
	void setSenderName(const QString &name);
private:
	QSharedDataPointer<MessagePrivate> d;
};
typedef QList<Message> MessageList;


inline Message::Type operator|(Message::Type lhs, Message::Type rhs)
{
	return Message::Type(int(lhs) | int(rhs));
}

inline void operator|=(Message::Type &lhs, Message::Type rhs)
{
	lhs = Message::Type(int(lhs) | int(rhs));
}

inline Message::Type operator^(Message::Type lhs, Message::Type rhs)
{
	return Message::Type(int(lhs) ^ int(rhs));
}

inline void operator^=(Message::Type &lhs, Message::Type rhs)
{
	lhs = Message::Type(int(lhs) ^ int(rhs));
}


} // Lime

Q_DECLARE_METATYPE(Lime::Message);

#endif // LIME_MESSAGE_H
