/*
	 This file is part of Lime.
	 Copyright (C) 2012 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIME_CONTACTINFORMATIONWINDOW_H_
#define LIME_CONTACTINFORMATIONWINDOW_H_

#include <QWidget>

namespace Ui {
	class ContactInformationWindow;
}

namespace Lime
{

class ContactInformationWindow : public QWidget
{
	Q_OBJECT
public:
	ContactInformationWindow(QObject *object, bool isReadOnly, QWidget *parent = 0);
	virtual ~ContactInformationWindow();
	QObject *object() const { return m_object; }
	template<typename T>
	T *object() const { return qobject_cast<T*>(m_object); }
	bool isReadOnly() const { return m_isReadOnly; }
	QString avatar() const { return m_avatarPath; }
protected:
	virtual void initializeFullInfo() = 0;
	virtual void initializeShortInfo() = 0;
	virtual void requestInfoImpl() = 0;
	virtual void saveInfoImpl() = 0;
protected:
	enum Error
	{
		NoErrors,
		NetworkError,
		ProtocolError,
		AccountIsOfflineError,
		UnknownError
	};
	void setAvatar(const QString &avatar);
	void infoReceived(Error error, const QString &errorStr = QString());
	void infoUpdated(Error error, const QString &errorStr = QString());
	void setOfflineMode(bool offlineMode = true);
	// functions that must be called only from initializeFullInfo()
	void addPage(const QString &title, QWidget *widget);
	// functions that must be called only from initializeShortInfo()
	void beginGroup(const QString &title);
	void endGroup();
	void addEntry(const QString &title, const QString &description);
	void addEntry(const QString &description) { addEntry(QString(), description); }
private slots:
	void initialize();
	void requestInfo();
	void clearAvatar();
private:
	void setError(const QString &errorStr);
	void updateSummary();
	enum State {
		Default,
		RequestingInfo,
		InitializingFullInfo,
		InitializingShortInfo
	};
	State m_state;
	bool m_isReadOnly;
	Ui::ContactInformationWindow *ui;
	QObject *m_object;
	QString m_shortInfo;
	QString m_group;
	QString m_avatarPath;
	bool m_offlineMode;
};

} // Lime
#endif // LIME_CONTACTINFORMATIONWINDOW_H_
