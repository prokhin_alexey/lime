/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "accountmodel.h"
#include "account.h"
#include "protocol.h"

using namespace std::placeholders;

namespace Lime
{

AccountDelegate::AccountDelegate(QObject *parent) :
	QStyledItemDelegate(parent)
{
}

void AccountDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	auto acc = index.data(AccountModel::AccountRole).value<Account*>();
	if (acc && !acc->isEnabled()) {
		auto opt = option;
		opt.palette.setCurrentColorGroup(QPalette::Disabled);
		opt.state &= ~QStyle::State_Enabled;
		QStyledItemDelegate::paint(painter, opt, index);
	} else {
		QStyledItemDelegate::paint(painter, option, index);
	}
}

AccountModel::AccountModel(int flags, QObject *parent) :
	QAbstractListModel(parent)
{
	auto addFn    = std::bind(&AccountModel::addAccount, this, _1);
	auto removeFn = std::bind(&AccountModel::removeAccount, this, _1);
	if (flags & ShowOnlyEnabledAccounts) {
		Account::onEnabled.connect(this, addFn);
		Account::onDisabled.connect(this, removeFn);
	} else {
		Account::onCreated.connect(this, addFn);
		Account::onDestroyed.connect(this, removeFn);
	}
}

AccountModel::~AccountModel()
{
}

int AccountModel::rowCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	return m_accounts.count();
}

QVariant AccountModel::data(const QModelIndex &index, int role) const
{
	auto acc = m_accounts.value(index.row());
	if (!acc)
		return QVariant();

	if (role == Qt::DisplayRole)
		return acc->id();
	else if (role == Qt::DecorationRole)
		return acc->statusIcon();
	else if (role == AccountRole)
		return qVariantFromValue(acc);
	return QVariant();
}

void AccountModel::addAccount(Account *account)
{
	Q_ASSERT(!m_accounts.contains(account));
	auto begin = m_accounts.begin();
	auto itr = qLowerBound(begin, m_accounts.end(), account, [](Account *lhs, Account *rhs) -> bool {
		auto lhs_proto = lhs->protocol();
		auto rhs_proto = rhs->protocol();
		if (lhs_proto != rhs_proto)
			return lhs_proto->id() < rhs_proto->id();
		else
			return lhs->id() < rhs->id();
	});
	int row = itr - begin;
	beginInsertRows(QModelIndex(), row, row);
	m_accounts.insert(row, account);
	endInsertRows();

	auto updateAccount = [=] {
		int row = m_accounts.indexOf(account);
		auto index = createIndex(row, 0, 0);
		emit dataChanged(index, index);
	};

	lconnect(account, SIGNAL(statusChanged(Lime::Status,Lime::Status)), this, updateAccount);
	lconnect(account, SIGNAL(enabled()), this, updateAccount);
	lconnect(account, SIGNAL(disabled()), this, updateAccount);
}

void AccountModel::removeAccount(Account *account)
{
	Q_ASSERT(m_accounts.contains(account));
	int row = m_accounts.indexOf(account);
	beginRemoveRows(QModelIndex(), row, row);
	m_accounts.removeAt(row);
	endRemoveRows();
}

} // Lime
