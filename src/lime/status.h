/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_STATUS_H
#define LIME_STATUS_H

#include "global.h"

namespace Lime
{

enum Status
{
	Invalid    = -1,
	Online     = 0x000001,
	FreeChat   = 0x000011,
	Away       = 0x000021,
	NA         = 0x000121,
	DND        = 0x000221,
	Invisible  = 0x000401,
	Offline    = 0x000800,
	Connecting = 0x100000
};

QString defaultStatusIconName(Status status, const QString &protocol = QString());
QString defaultStatusName(Status status);

inline Status operator|(Status lhs, Status rhs)
{
	return Status(int(lhs) | int(rhs));
}

inline void operator|=(Status &lhs, Status rhs)
{
	lhs = Status(int(lhs) | int(rhs));
}

inline Status operator^(Status lhs, Status rhs)
{
	return Status(int(lhs) ^ int(rhs));
}

inline void operator^=(Status &lhs, Status rhs)
{
	lhs = Status(int(lhs) ^ int(rhs));
}


} // Lime

#endif // LIME_STATUS_H
