/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "joinconferencewidget.h"
#include "ui_joinconference.h"
#include "icon.h"
#include "config.h"
#include "utils.h"

namespace Lime
{

JoinConferenceWidget::JoinConferenceWidget(QWidget* parent) :
	QWidget(parent), ui(new Ui::JoinConference)
{
	ui->setupUi(this);
	ui->addConferenceButton->setIcon(Icon("list-add"));
	ui->removeConferenceButton->setIcon(Icon("list-remove"));

	lconnect(ui->addConferenceButton, SIGNAL(clicked(bool)), this, [=] {
		this->createBookmark();
	});
	lconnect(ui->removeConferenceButton, SIGNAL(clicked(bool)), this, [=] {
		if (ui->conferenceListWidget->count() == 0)
			return;
		auto item = ui->conferenceListWidget->takeItem(ui->conferenceListWidget->currentRow());
		this->removeBookmark(item->data(Qt::UserRole));
	});
	lconnect(ui->conferenceListWidget, SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)), this,
		[=](QListWidgetItem *current)
	{
		this->setBookmark(current->data(Qt::UserRole));
	});
	lconnect(ui->joinButton, SIGNAL(clicked(bool)), this, [=] {
		auto bookmark = ui->conferenceListWidget->currentItem();
		if (bookmark) {
			this->save();
			this->join(bookmark->data(Qt::UserRole));
		}
	});
	connect(ui->saveButton, SIGNAL(clicked(bool)), this, SLOT(save()));
	lconnect(ui->conferenceListWidget, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this,
		[=](QListWidgetItem *item)
	{
		this->join(item->data(Qt::UserRole));
	});

	ui->searchButton->setVisible(false);
	Config cfg("", "joinConferenceWidget");
	if (cfg.hasChildKey("geometry"))
		restoreGeometry(cfg.value("geometry", QByteArray()));
	else
		centerizeWidget(this);
}

JoinConferenceWidget::~JoinConferenceWidget()
{
	Config cfg("", "joinConferenceWidget");
	cfg.setValue("geometry", saveGeometry());
	cfg.setValue("splitterState", ui->splitter->saveState());
	delete ui;
}

void JoinConferenceWidget::setBookmarkWidget(QWidget *widget)
{
	Q_ASSERT(ui->mainLayout->count() == 1);
	ui->mainLayout->insertWidget(0, widget);
	loadSplitterStateFromConfig();
}

void JoinConferenceWidget::setBookmarkLayout(QLayout *layout)
{
	Q_ASSERT(ui->mainLayout->count() == 1);
	ui->mainLayout->insertLayout(0, layout);
	loadSplitterStateFromConfig();
}

void JoinConferenceWidget::handleBookmark(const QString &name, const QVariant &userData)
{
	bool isEmpty = name.isEmpty();
	auto text = isEmpty ? tr("New bookmark") : name;
	auto bookmark = new QListWidgetItem(text, ui->conferenceListWidget);
	bookmark->setData(Qt::UserRole, userData);
	if (isEmpty || ui->conferenceListWidget->count() == 1)
		ui->conferenceListWidget->setCurrentItem(bookmark);
}

void JoinConferenceWidget::updateCurrentName(const QString &newName)
{
	ui->conferenceListWidget->currentItem()->setText(newName);
}

void JoinConferenceWidget::loadSplitterStateFromConfig()
{
	Config cfg("", "joinConferenceWidget");
	if (cfg.hasChildKey("splitterState"))
		ui->splitter->restoreState(cfg.value("splitterState", QByteArray()));
	else
		ui->splitter->setSizes(QList<int>() << 200 << 500);
}


} // Lime

