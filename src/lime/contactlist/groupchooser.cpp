/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "groupchooser.h"
#include <lime/protocol.h>
#include <lime/contact.h>
#include <lime/account.h>
#include <QBoxLayout>

namespace Lime {

GroupChooser::GroupChooser(const QStringList &contactGroups, bool exclusive, QWidget *parent) :
	QListWidget(parent)
{
	QSet<QString> groupSet;
	foreach (auto proto, Protocol::all()) {
		foreach (auto acc, proto->accounts())
			groupSet.unite(acc->groups());
	}

	auto groups = groupSet.toList();
	auto checked = contactGroups.toSet();
	qSort(groups.begin(), groups.end(), [](const QString &lhs, const QString &rhs) {
		return QString::localeAwareCompare(lhs, rhs) < 0;
	});

	foreach (auto &group, groups) {
		auto item = new QListWidgetItem(group, this);
		item->setCheckState(checked.contains(group) ? Qt::Checked : Qt::Unchecked);
	}

	if (exclusive) {
		lconnect(this, SIGNAL(itemChanged(QListWidgetItem*)), this, [=](QListWidgetItem *item) {
			bool isChecked = item->checkState() == Qt::Checked;
			if (isChecked) {
				auto othersState = isChecked ? Qt::Unchecked : Qt::Checked;
				for (int i = 0, c = this->count(); i < c; ++i) {
					auto curItem = this->item(i);
					if (curItem == item)
						continue;
					curItem->setCheckState(othersState);
				}
			}
		});
	}
}

QStringList GroupChooser::groups()
{
	QStringList groups;
	for (int i = 0, c = count(); i < c; ++i) {
		auto item = this->item(i);
		if (item->checkState() == Qt::Checked)
			groups << item->text();
	}
	return groups;
}

GroupChooserDialog::GroupChooserDialog(Contact *contact, QWidget *parent) :
	QWidget(parent)
{
	auto layout = new QVBoxLayout(this);
	auto chooser = new GroupChooser(contact->groups(), !(contact->flags() & Contact::SupportsMultiGroups), this);
	auto buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal, this);

	layout->addWidget(chooser);
	layout->addWidget(buttons);

	lconnect(buttons, SIGNAL(accepted()), this, [=] {
		contact->setGroups(chooser->groups());
		this->close();
	});
	connect(buttons, SIGNAL(rejected()), this, SLOT(close()));
}

} // Lime

