/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "contactlistview.h"
#include <lime/config.h>
#include <lime/chatunit.h>
#include "model/abstractitem.h"
#include <QMenu>
#include <lime/icon.h>

namespace Lime
{

ContactListView::ContactListView(QAbstractItemModel *model, QWidget *parent) :
	QTreeView(parent)
{
	setRootIsDecorated(false);
	setIndentation(0);
	setEditTriggers(QAbstractItemView::EditKeyPressed);
	setHeaderHidden(true);
	setModel(model);
	setContextMenuPolicy(Qt::CustomContextMenu);
	setDragEnabled(true);
	viewport()->setAcceptDrops(true);
	setDropIndicatorShown(true);
	//setDragDropMode(QAbstractItemView::InternalMove);

	connect(this, SIGNAL(collapsed(QModelIndex)), SLOT(onCollapsed(QModelIndex)));
	connect(this, SIGNAL(expanded(QModelIndex)), SLOT(onExpanded(QModelIndex)));
	connect(model, SIGNAL(rowsInserted(QModelIndex,int,int)),
			SLOT(onRowsInserted(QModelIndex,int,int)));
	connect(this, SIGNAL(activated(QModelIndex)),
			this, SLOT(onClick(QModelIndex)));

	lconnect(this, SIGNAL(customContextMenuRequested(QPoint)), this, [=](const QPoint &pos) {
		auto index = this->indexAt(pos);
		auto container = index.data(ContainerRole).value<ActionContainer*>();
		if (!container)
			return;
		auto menu = container->menu(Action::ContextMenuAction, this);
		menu->setAttribute(Qt::WA_DeleteOnClose, true);
		menu->popup(this->viewport()->mapToGlobal(pos));
	});
}

ContactListView::~ContactListView()
{

}

void ContactListView::loadSettings()
{
	Config cfg = Config(QString(), "contactList");
	m_closedIndexes = cfg.value("closedGroups", QStringList()).toSet();
	setAlternatingRowColors(cfg.value("useAlternationRowColors", true));
}

void ContactListView::onClick(const QModelIndex &index)
{
	ChatUnit *unit = index.data(UnitRole).value<ChatUnit*>();
	if (unit)
		unit->activateChat();
}

void ContactListView::onCollapsed(const QModelIndex &index)
{
	QString name = index.data(GroupNameRole).toString();
	if (!name.isEmpty()) {
		m_closedIndexes.insert(name);
		storeClosedGroups();
	}
}

void ContactListView::onExpanded(const QModelIndex &index)
{
	QString name = index.data(GroupNameRole).toString();
	if (!name.isEmpty()) {
		m_closedIndexes.remove(name);
		storeClosedGroups();
	}
}

void ContactListView::onRowsInserted(const QModelIndex &parent, int first, int last)
{
	for (; first <= last; ++first) {
		QModelIndex index = model()->index(first, 0, parent);
		if (!m_closedIndexes.contains(index.data(GroupNameRole).toString()))
			setExpanded(index, true);
	}
}

void ContactListView::storeClosedGroups()
{
	if (!model())
		return;
	Config group = Config(QString(), "contactList");
	group.setValue("closedTags", QStringList(m_closedIndexes.toList()));
}

void ContactListView::checkGroup(const QModelIndex &parent, QAbstractItemModel *model)
{
	for (int i = 0, c = model->rowCount(parent); i != c; ++i) {
		QModelIndex index = model->index(i, 0, parent);
		checkGroup(index, model);
		if (!m_closedIndexes.contains(index.data(GroupNameRole).toString()))
			setExpanded(index, true);
	}
}

} // Lime
