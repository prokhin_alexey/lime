/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "contactlist.h"

#include "model/contactlistmodel.h"
#include "delegate/contactlistitemdelegate.h"
#include "contactlistview.h"
#include "groupchooser.h"
#include "model/groupitem.h"
#include "model/contactitem.h"
#include "model/accountitem.h"
#include "settings/contactlistsettings.h"
#include <lime/config.h>
#include <lime/account.h>
#include <lime/protocol.h>
#include <lime/actiontoolbar.h>
#include <lime/icon.h>
#include <lime/conference.h>

#include <QVBoxLayout>
#include <QApplication>
#include <QDesktopWidget>
#include <QToolButton>
#include <QLayoutItem>
#include <QAction>
#include <QInputDialog>
#include <QMessageBox>

#include <3rdparty/qtdwm/qtdwm.h>
#include <3rdparty/qtcustomwidget/qtcustomwidget.h>

namespace Lime
{

QList<Contact*> ContactList::m_contactsToBeAdded;

ContactList::ContactList() :
	m_defaultFlags(windowFlags()), m_custom(0)
{
	qDebug() << m_defaultFlags;

	auto toolBar = new ActionToolBar(this, Action::MainToolbarAction, this);
	toolBar->setFloatable(false);
	toolBar->setMovable(false);
	toolBar->setButtonSize(QSize(22, 22));
	addToolBar(toolBar);

	auto central = new QWidget(this);
	setCentralWidget(central);
	auto layout = new QVBoxLayout(central);

	m_delegate = new ContactListItemDelegate(this);
	m_view = new ContactListView(new ContactListModel(this), this);
	m_view->setItemDelegate(m_delegate);
	layout->addWidget(m_view);
	layout->setContentsMargins(0, 0, 0, 0);

	auto accountsWidget = new QWidget(this);
	layout->addWidget(accountsWidget);
	m_accountLayout = new QHBoxLayout(accountsWidget);
	m_accountLayout->addSpacerItem(new QSpacerItem(20, 0, QSizePolicy::Expanding, QSizePolicy::Preferred));
	m_accountLayout->setContentsMargins(0, 0, 0, 0);
}

ContactList::~ContactList()
{
	Config(QString(), "contactList").setValue("geometry", saveGeometry());
}

void ContactList::addContact(Contact *contact)
{
	auto cl = instance();
	if (cl->isExtensionEnabled()) {
		cl->addContactImpl(contact);
	} else {
		Q_ASSERT(!m_contactsToBeAdded.contains(contact));
		m_contactsToBeAdded << contact;
		lconnect(contact, SIGNAL(destroyed()), cl, [=] {
			m_contactsToBeAdded.removeOne(contact);
		});
	}
}

void ContactList::removeContact(Contact *contact)
{
	auto cl = instance();
	if (cl->isExtensionEnabled())
		cl->removeContactImpl(contact);
	else
		m_contactsToBeAdded.removeOne(contact);
}

void ContactList::enableBlinking(Contact *contact, Notification::Type type, bool enable)
{
	ContactListModel::instance()->enableBlinking(contact, type, enable);
}

bool ContactList::enableImpl()
{
	// Load contact list settings
	reloadSettings();
	auto geometry = Config(QString(), "contactList").value<QByteArray>("geometry");
	if (!geometry.isEmpty()) {
		restoreGeometry(geometry);
	} else {
		QRect screen = QApplication::desktop()->screenGeometry(QCursor::pos());
		setGeometry(screen.adjusted(screen.width() - 250, 50, -20, -50));
	}

	auto act = new Action(Icon("show-menu"), "", this);
	act->setType(Action::MainToolbarAction);
	act->setPriority(Action::HighestPriority + 300);
	lconnect(act, SIGNAL(actionCreated(QAction*,ActionContainer*)), this, [=](QAction *action) {
		auto menu = this->menu(Action::ContextMenuAction, act);
		action->setMenu(menu);
	});
	ActionContainer::addAction<ContactList>(act);

	// Create 'Change group' action.
	act = new Action(Icon("edit-groups"), tr("Change group"), this);
	act->setType(Action::ContextMenuAction);
	act->setPriority(Action::NormalPriority + 100);
	ActionContainer::addAction<Contact>(act);
	lconnect(act, SIGNAL(triggered(ActionContainer*,bool)), this, [](ActionContainer *container) {
		auto chooser = new GroupChooserDialog(sender_cast<Contact*>(container->toQObject()));
		chooser->setAttribute(Qt::WA_DeleteOnClose, true);
		chooser->show();
		chooser->raise();
	});

	// Create 'Rename contact' action
	act = new Action(Icon("user-properties"), tr("Rename"), this);
	act->setType(Action::ContextMenuAction);
	act->setPriority(Action::NormalPriority + 120);
	ActionContainer::addAction<Contact>(act);
	act->addFilter([](ActionContainer *container) {
		auto contact = sender_cast<Contact*>(container->toQObject());
		return contact->flags() & Contact::NameMayBeChanged;
	});
	lconnect(act, SIGNAL(triggered(ActionContainer*,bool)), this, [=](ActionContainer *container) {
		auto contact = sender_cast<Contact*>(container->toQObject());
		bool ok;
		QString name = QInputDialog::getText(this, tr("Rename contact %1 (%2)")
													.arg(contact->name())
													.arg(contact->id()),
											 tr("Contact name:"),
											 QLineEdit::Normal,
											 contact->name(),
											 &ok);
		if (ok)
			contact->setName(name);
	});

	// Create 'Remove group' action
	act = new Action(Icon("list-remove-group"), tr("Remove group"), this);
	act->setType(Action::ContextMenuAction);
	act->setPriority(Action::NormalPriority + 300);
	lconnect(act, SIGNAL(triggered(ActionContainer*,bool)), this, [](ActionContainer *container) {
		auto groupItem = qobject_cast<GroupItem*>(container->toQObject());
		Q_ASSERT(groupItem);
		auto groupName = groupItem->name();
		QString contactsToBeRemoved;
		for (int i = 0, n = groupItem->container()->count(); i < n; ++i) {
			auto contactItem = groupItem->container()->item(i)->as<ContactItem>();
			if (!contactItem)
				return;
			auto contact = contactItem->contact();
			auto groups = contact->groups();
			groups.removeOne(groupName);
			if (groups.isEmpty()) {
				auto name = contact->name();
				auto id = contact->id();
				if (!name.isEmpty() && id != name)
					name += " (" + id + ")";
				else
					name = id;
				contactsToBeRemoved += "\n	" + name;
			}
		}

		QString msg = QT_TR_NOOP("Are you sure that you want to remove the contact list group?\n");
		if (!contactsToBeRemoved.isEmpty()) {
			msg += QT_TR_NOOP("If you will proceed, the contacts will be permanently deleted from your contact list: %1");
			msg = msg.arg(contactsToBeRemoved);
		}

		QMessageBox msgBox;
		msgBox.setWindowTitle("Are you sure?");
		msgBox.setText(msg);
		msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgBox.setDefaultButton(QMessageBox::Yes);
		msgBox.setIcon(QMessageBox::Question);
		int ret = msgBox.exec();
		if (ret != QMessageBox::Yes)
			return;

		if (auto parent = groupItem->parentItem()) {
			Q_ASSERT(parent->type() == AccountItemType);
			auto acc = parent->as<AccountItem>()->account();
			acc->removeGroup(groupName);
		} else {
			foreach (auto acc, Account::all())
				acc->removeGroup(groupName);
		}
	});
	ActionContainer::addAction<ContactListGroupActionContainer>(act);

	// Create 'Rename group' action
	act = new Action(Icon("list-edit-group"), tr("Rename group"), this);
	act->setType(Action::ContextMenuAction);
	act->setPriority(Action::NormalPriority + 1000);
	lconnect(act, SIGNAL(triggered(ActionContainer*,bool)), this, [=](ActionContainer *container) {
		auto groupItem = qobject_cast<GroupItem*>(container->toQObject());
		Q_ASSERT(groupItem);
		QString oldName = groupItem->name();
		bool ok;
		QString newName = QInputDialog::getText(this, tr("Rename group %1").arg(oldName),
											 tr("Group name:"),
											 QLineEdit::Normal,
											 oldName,
											 &ok);
		if (!ok)
			return;

		for (int i = groupItem->container()->count()-1; i >= 0; --i) {
			auto contactItem = static_cast<ContactItem*>(groupItem->container()->item(i));
			if (contactItem->type() != ContactItemType)
				return;
			auto contact = contactItem->contact();
			auto groups = contact->groups();
			groups.removeOne(oldName);
			groups.push_back(newName);
			contact->setGroups(groups);
		}
	});
	ActionContainer::addAction<ContactListGroupActionContainer>(act);

	// Create 'Add group' action
	act = new Action(Icon("list-add-group"), tr("Add new group"), this);
	act->setType(Action::ContextMenuAction);
	act->setPriority(Action::NormalPriority + 2000);
	lconnect(act, SIGNAL(triggered(ActionContainer*,bool)), this, [=](ActionContainer *container) {
		auto groupItem = qobject_cast<GroupItem*>(container->toQObject());
		Q_ASSERT(groupItem);
		bool ok;
		QString name = QInputDialog::getText(this, tr("Create a new group"),
											 tr("Group name:"),
											 QLineEdit::Normal,
											 QString(),
											 &ok);
		if (!ok)
			return;

		if (auto parent = groupItem->parentItem()) {
			Q_ASSERT(parent->type() == AccountItemType);
			auto acc = parent->as<AccountItem>()->account();
			acc->createEmptyGroup(name);
		} else {
			foreach (auto acc, Account::all())
				acc->createEmptyGroup(name);
		}
	});
	ActionContainer::addAction<ContactListGroupActionContainer>(act);

	// Create 'Join/Leave conference' action
	act = new Action(QIcon(), tr("Join/Leave conference"), this);
	act->setType(Action::ContextMenuAction);
	act->setPriority(Action::NormalPriority + 2000);
	auto updateJoinLeaveAction = [=](Conference *conf) {
		act->setText(conf->isJoined() ? tr("Leave conference") : tr("Join conference"), conf);
	};
	Account::onConferenceCreated.connect(this, [=](Account *, Conference *conf) {
		updateJoinLeaveAction(conf);
	});
	Conference::onJoined.connect(this, updateJoinLeaveAction);
	Conference::onLeft.connect(this, updateJoinLeaveAction);
	lconnect(act, SIGNAL(triggered(QObject*,bool)), this, [=](Conference *conf) {
		if (conf->isJoined())
			conf->leave();
		else
			conf->join();
	});
	ActionContainer::addAction<Conference>(act);

	// Create contact list settings page
	auto item = new SettingsItem(Icon("preferences-contact-list"), tr("Contact list"), [=]()->SettingsWidget* {
		auto w = new ContactListSettings;
		connect(w, SIGNAL(saved()), this, SLOT(reloadSettings()));
		return w;
	});
	SettingsManager::addGlobalSettings(item);

	// Populate contact list.
	foreach (auto contact, m_contactsToBeAdded)
		addContactImpl(contact);
	m_contactsToBeAdded.clear();

	// Create account icons at the bottom of the contact list window
	Account::onEnabled.connect(this, [=](Account *acc) {
		auto button = new QToolButton(this);
		button->setIcon(acc->statusIcon());
		button->setToolTip(acc->id());
		button->setAutoRaise(true);
		button->setPopupMode(QToolButton::InstantPopup);
		button->setStyleSheet("QToolButton::menu-indicator{border:none}"); // remove arrow
		button->setMaximumSize(22, 22);
		button->setMinimumSize(22, 22);
		auto menu = acc->menu(Action::ContextMenuAction, button);
		button->setMenu(menu);
		m_accountLayout->addWidget(button);
		lconnect(acc, SIGNAL(statusChanged(Lime::Status,Lime::Status)), button, [acc, button]() {
			button->setIcon(acc->statusIcon());
		});
		connect(acc, SIGNAL(disabled()), button, SLOT(deleteLater()));
	});

	return true;
}

bool ContactList::disableImpl()
{
	ContactListModel::instance()->storeGroups();
	return false;
}

void ContactList::addContactImpl(Contact *contact)
{
	ContactListModel::instance()->addContact(contact);
}

void ContactList::removeContactImpl(Contact *contact)
{
	ContactListModel::instance()->removeContact(contact);
}

void ContactList::reloadSettings()
{
	ContactListModel::instance()->loadSettings();
	m_delegate->reloadSettings();
	m_view->loadSettings();

	Config cfg("", "contactList");
	setWindowOpacity(cfg.value("opacity", 100.0) / 100);

	Qt::WindowFlags flags = m_defaultFlags;
	flags &= ~Qt::Tool;
	flags |= Qt::Window;
	if (cfg.value("alwaysOnTop", false))
		flags |= Qt::WindowStaysOnTopHint;
	else
		flags &= ~Qt::WindowStaysOnTopHint;
	switch (cfg.value("windowStyle", (int)Regular)) {
	case Tool:
		flags |= Qt::Tool;
	default:
	case Regular:
		delete m_custom; m_custom = 0;
		clearMask();
		if (QtDWM::isCompositionEnabled())
			QtDWM::extendFrameIntoClientArea(this);
		//setContentsMargins( 0, 0, 0, 0 );
		break;
	case Themed:
		if (flags & Qt::WindowStaysOnTopHint)
			flags = Qt::WindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
		else
			flags = Qt::FramelessWindowHint;
		if (!m_custom)
			m_custom = new QtCustomWidget(this);
		m_custom->start(cfg.value("borderTheme", theme("border", "default")));
		break;
	case Borderless:
		delete m_custom; m_custom = 0;
		// Strange behaviour happens on Mac OS X, so we will disable this flag for a while
	#if !defined(Q_OS_MAC)
		flags |= Qt::FramelessWindowHint;
	#endif
		//setMask(m_view->geometry());
		break;
	}
	if (windowFlags() != flags)
		setWindowFlags(flags);
	show();
}

} // Lime
