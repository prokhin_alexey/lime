/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_CONTACTLIST_H
#define LIME_CONTACTLIST_H

#include <lime/contact.h>
#include <lime/extention.h>
#include <lime/notification.h>
#include <QMainWindow>

class QBoxLayout;
class QtCustomWidget;

namespace Lime {

class ContactListView;
class ContactListItemDelegate;

class ContactList : public QMainWindow, public Extension, public ActionContainer, protected SignalGuard
{
	Q_OBJECT
public:
	virtual ~ContactList();
	static ContactList *instance() { static ContactList *cl = new ContactList; return cl; }
	static void addContact(Contact *contact);
	static void removeContact(Contact *contact);
	void enableBlinking(Contact *contact, Notification::Type type, bool enable = true);
	QObject *toQObject() { return this; }
protected:
	virtual bool enableImpl();
	virtual bool disableImpl();
protected:
	void addContactImpl(Contact *contact);
	void removeContactImpl(Contact *contact);
private slots:
	void reloadSettings();
private:
	enum WindowStyle
	{
		Regular,
		Themed,
		Borderless,
		Tool
	};
	ContactList();
	QBoxLayout *m_accountLayout;
	ContactListView *m_view;
	ContactListItemDelegate *m_delegate;
	Qt::WindowFlags m_defaultFlags;
	QtCustomWidget *m_custom;
	static QList<Contact*> m_contactsToBeAdded;
};

} // Lime

#endif // LIME_ABSTRACTCONTACTLIST_H
