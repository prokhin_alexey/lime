/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_GROUPITEM_H
#define LIME_GROUPITEM_H

#include <lime/account.h>
#include "abstractitem.h"

namespace Lime {

class ChatUnitContainer;

class GroupItem : public ContactListGroupActionContainer, public AbstractItem
{
	Q_OBJECT
public:
	GroupItem(const QString &name, AbstractContainer *parent);
	virtual QVariant data(int role) const;
	virtual Qt::ItemFlags flags(Qt::ItemFlags itemFlags, int flags) const;
	virtual bool canDropTo(AbstractItem *item, int row, int flags) const;
	virtual void dropTo(AbstractItem *item, int row, int flags);
	virtual QObject *toQObject() { return this; }
	void incInactive() { ++m_active; }
	void decActive() { --m_active; }
	int online() { return m_active; }
	AbstractContainer *parent() const { return AbstractItem::parent(); }
protected:
	ActionContainer *actionContainer() const { return const_cast<GroupItem*>(this); }
private:
	friend class GroupContainer;
	int m_active;
	int m_ref;
};

class GroupContainer : public AbstractContainer
{
public:
	GroupContainer(AbstractItem *parent = 0);
	virtual AbstractItem *item(int index) const;
	virtual int count() const;
	virtual void addUnit(ChatUnit *unit, int flags);
	virtual void removeUnit(ChatUnit *unit, int flags);
	virtual void updateUnit(ChatUnit *unit, int flags);
	virtual void addGroup(Account *account, const QString &group, int flags);
	virtual void removeGroup(Account *account, const QString &group, int flags);
	virtual void setGroups(Contact *contact, const QStringList &newGroups,
						   const QStringList &oldGroups, int flags);
	virtual bool enableBlinking(ChatUnit *unit, Notification::Type type,
								bool blink, int flags);
	virtual void setParent(AbstractItem *parent);
	virtual int offset();
	void move(GroupItem *item, AbstractItem *target, int row);
private:
	void addUnit(ChatUnit *unit, const QString &group, int flags);
	void removeUnit(ChatUnit *unit, const QString &group, int flags);
	void showItem(GroupItem *item);
	void hideItem(GroupItem *item);
	GroupItem *addGroup(const QString &group, int flags);
	void removeGroup(const QList<GroupItem*>::iterator &itr);
	QList<GroupItem*>::iterator findItem(const QString &name);
	GroupItem *getItem(const QString &name);
	QStringList getUnitGroups(ChatUnit *unit);
private:
	QList<AbstractItem*> m_visibleGroups;
	QList<GroupItem*> m_groups;
	ChatUnitContainer *m_defaultGroup;
};

template<>
inline GroupItem *itemAsHelper<GroupItem>(AbstractItem *item)
{
	return item->type() == GroupItemType ? reinterpret_cast<GroupItem*>(item->toQObject()) : 0;
}

} // Lime

#endif // LIME_GROUPITEM_H
