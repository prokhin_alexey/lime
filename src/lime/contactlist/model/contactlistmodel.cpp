/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "contactlistmodel.h"
#include <lime/contact.h>
#include <lime/conference.h>
#include <lime/protocol.h>
#include <lime/account.h>
#include <lime/config.h>
#include <lime/icon.h>
#include <lime/contactlist/contactlist.h>
#include "contactitem.h"
#include "groupitem.h"
#include "accountitem.h"
#include <QTimer>
#include <QMimeData>

namespace Lime {

ContactListModel *ContactListModel::self;
static const char *internalMimeType = "lime-contact-list-item-internal";

ContactListModel::ContactListModel(QObject *parent) :
	QAbstractItemModel(parent),
	m_flags(0),
	m_rootContainer(0),
	m_showHideAction(0),
	m_showAlternativeIcon(false)
{
	Q_ASSERT(!self);
	self = this;
	setSupportedDragActions(Qt::MoveAction);
	m_alternativeIconTimer.setInterval(1000);

	lconnect(&m_alternativeIconTimer, SIGNAL(timeout()), this, [=] {
		m_showAlternativeIcon = !m_showAlternativeIcon;
		foreach (auto contact, m_blinkingUnits)
			contact->update();
	});

	Contact::onStatusChanged.connect(this,
		[=](Buddy *buddy, Status now, Status old)
	{
		if (!m_rootContainer)
			return;

		auto contact = qobject_cast<Contact*>(buddy);
		if (!contact || !contact->isInList())
			return;

		bool isOnline = now & Online;
		bool wasOnline = old & Online;
		int flags = m_flags;
		if (isOnline && !wasOnline) {
			flags |= AbstractContainer::UnitBecomeActive;
			if (this->isVisible(contact))
				flags |= AbstractContainer::ShowUnit | AbstractContainer::UpdateUnitPosition;
		} else if (!isOnline && wasOnline) {
			flags |= AbstractContainer::UnitBecomeInactive;
			if (!(m_flags & ShowInactive))
				flags |= AbstractContainer::HideUnit;
			else
				flags |= AbstractContainer::UpdateUnitPosition;
		} else if (isOnline) {
			flags |= AbstractContainer::UpdateUnitPosition;
		} else {
			return;
		}
		m_rootContainer->updateUnit(contact, flags);
	});

	Contact::onGroupsUpdated.connect(this,
		[=](Contact *contact, const QStringList &newGroups, const QStringList &oldGroups)
	{
		if (!m_rootContainer || !contact->isInList())
			return;

		int flags = m_flags;
		if (isVisible(contact))
			flags |= AbstractContainer::ShowUnit;
		m_rootContainer->setGroups(contact, newGroups, oldGroups, flags);
	});

	Account::onGroupAdded.connect(this, [=](Account *account, const QString &group) {
		if (!m_rootContainer || !(m_flags & ShowGroups))
			return;
		m_rootContainer->addGroup(account, group, m_flags);
	});

	Account::onGroupRemoved.connect(this, [=](Account *account, const QString &group) {
		if (!m_rootContainer || !(m_flags & ShowGroups))
			return;
		m_rootContainer->removeGroup(account, group, m_flags);
	});

	ChatUnit::onNameChanged.connect(this,
		[=](ChatUnit *unit, const QString &/*newName*/, const QString &/*oldName*/)
	{
		if (!m_rootContainer)
			return;

		if (auto contact = qobject_cast<Contact*>(unit)) {
			if (!contact->isInList())
				return;
		} else if (m_flags & ShowConferences) {
			auto conf = qobject_cast<Conference*>(unit);
			if (!conf)
				return;
		} else {
			return;
		}

		m_rootContainer->updateUnit(unit, m_flags | AbstractContainer::UpdateUnitPosition);
	});

	auto update = [=](ChatUnit *unit) {
		if (!m_rootContainer)
			return;

		auto contact = qobject_cast<Contact*>(unit);
		if (!contact || !contact->isInList())
			return;

		m_rootContainer->updateUnit(contact, m_flags | AbstractContainer::UpdateContactData);
	};

	Contact::onAvatarChanged.connect(this,
		[=](Buddy *buddy, const QString &/*newAvatar*/)
	{
		update(buddy);
	});

	Contact::onStatusTextChanged.connect(this,
		[=](Buddy *buddy, const QString &/*text*/)
	{
		update(buddy);
	});

	Contact::onExtendedInfoChanged.connect(this,
		[=](Buddy *buddy, const QString &/*name*/, const QVariantMap &/*info*/)
	{
		update(buddy);
	});

	Account::onConferenceCreated.connect(this, SLOT(addConference(Lime::Account*,Lime::Conference*)));
	Account::onConferenceRemoved.connect(this, SLOT(removeConference(Lime::Account*,Lime::Conference*)));

	Account::onStatusChanged.connect(this,
		[=](Account *account, Status now, Status old)
	{
		if (!m_rootContainer)
			return;

		if (!(m_flags & ShowConferences))
			return;

		bool isOnline = now & Online;
		bool wasOnline = old & Online;
		if (isOnline && !wasOnline) {
			foreach (auto conf, account->conferences()) {
				int flags = m_flags | AbstractContainer::UnitBecomeActive;
				if (isVisible(conf))
					flags |= AbstractContainer::ShowUnit | AbstractContainer::UpdateUnitPosition;
				m_rootContainer->updateUnit(conf, flags);
			}
		} else if (!isOnline && wasOnline) {
			int flags = m_flags | AbstractContainer::UnitBecomeInactive;
			if (!(m_flags & ShowInactive))
				flags |= AbstractContainer::HideUnit;
			else
				flags |= AbstractContainer::UpdateUnitPosition;
			foreach (auto conf, account->conferences())
				m_rootContainer->updateUnit(conf, flags);
		} else {
			return;
		}
	});

	auto updateConference = [=](Conference *conf) {
		if (!m_rootContainer || !(m_flags & ShowConferences) || !isVisible(conf))
			return;

		int flags = m_flags | AbstractContainer::UpdateUnitPosition;
		m_rootContainer->updateUnit(conf, flags);
	};

	Conference::onJoined.connect(this, updateConference);
	Conference::onLeft.connect(this, updateConference);
};

ContactListModel::~ContactListModel()
{
	clear();
	self = 0;
}

void ContactListModel::addContact(Contact *contact)
{
	int flags = m_flags;
	if (isVisible(contact))
		flags |= AbstractContainer::ShowUnit;
	if (contact->status() & Online)
		flags |= AbstractContainer::UnitBecomeActive;
	m_contacts << contact;
	m_rootContainer->addUnit(contact, flags);
}

void ContactListModel::removeContact(Contact *contact)
{
	m_contacts.removeOne(contact);
	int flags = m_flags;
	if (contact->status() & Online)
		flags |= AbstractContainer::UnitBecomeInactive;
	m_rootContainer->removeUnit(contact, flags);
}

void ContactListModel::addConference(Account *account, Conference *conf)
{
	Q_UNUSED(account);
	if (!(m_flags & ShowConferences))
		return;
	int flags = m_flags;
	if (isVisible(conf))
		flags |= AbstractContainer::ShowUnit;
	if (conf->isJoined())
		flags |= AbstractContainer::UnitBecomeActive;
	m_rootContainer->addUnit(conf, flags);
}

void ContactListModel::removeConference(Account *account, Conference *conf)
{
	Q_UNUSED(account);
	if (!(m_flags & ShowConferences))
		return;
	int flags = m_flags;
	if (conf->isJoined())
		flags |= AbstractContainer::UnitBecomeInactive;
	m_rootContainer->removeUnit(conf, flags);
}

QModelIndex ContactListModel::index(int row, int column, const QModelIndex &parent) const
{
	if (column != 0 || row < 0)
		return QModelIndex();
	auto container = getContainer(parent);
	if (!container)
		return QModelIndex();
	auto item = container->item(row);
	return item ? createIndex(row, 0, item) : QModelIndex();
}

QModelIndex ContactListModel::parent(const QModelIndex &child) const
{
	if (!child.isValid())
		return QModelIndex();
	auto item = reinterpret_cast<AbstractItem*>(child.internalPointer());
	Q_ASSERT(item);
	return item->parentModelIndex();
}

int ContactListModel::rowCount(const QModelIndex &parent) const
{
	auto container = getContainer(parent);
	return container ? container->count() : 0;
}

int ContactListModel::columnCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent)
	return 1;
}

bool ContactListModel::hasChildren(const QModelIndex &parent) const
{
	auto container = getContainer(parent);
	return container ? !container->isEmpty() : false;
}

QVariant ContactListModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();
	auto item = reinterpret_cast<AbstractItem*>(index.internalPointer());
	Q_ASSERT(item);
	return item->data(role);
}

Qt::ItemFlags ContactListModel::flags(const QModelIndex &index) const
{
	auto flags = QAbstractItemModel::flags(index);
	if (!index.isValid()) {
		if (!(m_flags & ShowAccounts))
			flags |= Qt::ItemIsDropEnabled;
		return flags;
	}
	auto item = reinterpret_cast<AbstractItem*>(index.internalPointer());
	Q_ASSERT(item);
	return item->flags(flags, m_flags);
}

QStringList ContactListModel::mimeTypes() const
{
	QStringList types;
	types << internalMimeType;
	return types;
}

QMimeData *ContactListModel::mimeData(const QModelIndexList &indexes) const
{
	auto mimeData = new QMimeData;

	QByteArray encodedData;
	QDataStream stream(&encodedData, QIODevice::WriteOnly);

	foreach (auto &index, indexes) {
		if (!index.isValid())
			continue;
		auto item = reinterpret_cast<AbstractItem*>(index.internalPointer());
		Q_ASSERT(item);
		stream.writeRawData((const char*)&item, sizeof(void*));
	}

	mimeData->setData(internalMimeType, encodedData);
	return mimeData;
}

bool ContactListModel::dropMimeData(const QMimeData *data, Qt::DropAction action,
		int row, int column, const QModelIndex &parent)
{
	if (action == Qt::IgnoreAction)
		return true;

	if (column > 0)
		return false;

	if (!data->hasFormat(internalMimeType))
		return false;

	int flags = m_flags;
	auto parentItem = reinterpret_cast<AbstractItem*>(parent.internalPointer());
	QList<AbstractItem*> items;
	QByteArray encodedData = data->data(internalMimeType);
	QDataStream stream(&encodedData, QIODevice::ReadOnly);
	while (!stream.atEnd()) {AbstractItem *item;
		stream.readRawData((char*)&item, sizeof(void*));
		if (!item->canDropTo(parentItem, row, flags))
			return false;
		items << item;
	}

	foreach (auto item, items)
		item->dropTo(parentItem, row, flags);

	return true;
}

Qt::DropActions ContactListModel::supportedDropActions() const
{
	return Qt::MoveAction;
}

void ContactListModel::enableBlinking(Contact *contact, Notification::Type type, bool enable)
{
	Q_ASSERT(m_rootContainer);
	m_rootContainer->enableBlinking(contact, type, enable, m_flags);
}

void ContactListModel::addBlinkingUnit(ChatUnitItem *unit)
{
	Q_ASSERT(!m_blinkingUnits.contains(unit));
	if (m_blinkingUnits.isEmpty()) {
		m_alternativeIconTimer.start();
		m_showAlternativeIcon = true;
		unit->update();
	}
	m_blinkingUnits << unit;
}

void ContactListModel::removeBlinkingUnit(ChatUnitItem *unit)
{
	m_blinkingUnits.removeOne(unit);
	if (m_blinkingUnits.isEmpty()) {
		m_alternativeIconTimer.stop();
		m_showAlternativeIcon = false;
	}
	unit->update();
}

void ContactListModel::loadSettings()
{
	if (!m_showHideAction) {
		m_showHideAction = new Action(Icon("view-user-offline"), tr("Show/hide offline"));
		m_showHideAction->setCheckable(true);
		m_showHideAction->setType(Action::MainToolbarAction);
		m_showHideAction->setPriority(Action::HighPriority + 100);
		lconnect(m_showHideAction, SIGNAL(triggered(ActionContainer*,bool)), this,
			[=](ActionContainer *container, bool checked)
		{
			Config cfg(QString(), "contactList");
			cfg.setValue("showOffline", checked);
			m_flags = checked ? (m_flags | ShowInactive) : (m_flags ^ ShowInactive);
			checkContactsVisiblity();
		});
		ActionContainer::addAction<ContactList>(m_showHideAction);
	}

	int flags = 0;
	Config cfg(QString(), "contactList");
	if (cfg.value("showAccounts", true))
		flags |= ShowAccounts;
	if (cfg.value("showGroups", true))
		flags |= ShowGroups;
	if (cfg.value("showConferences", true))
		flags |= ShowConferences;
	if (cfg.value("showOffline", true))
		flags |= ShowInactive;
	if (cfg.value("showEmptyGroups", false))
		flags |= ShowEmptyGroups;
	if (cfg.value("showSeparators", false))
		flags |= ShowSeparators;
	if (cfg.value("sortByStatus", true))
		flags |= SortByStatus;
	setFlags(flags);
}

void ContactListModel::storeGroups()
{
	auto getGroupList = [](AbstractContainer *container) {
		QStringList groups;
		for (int j = 0, n = container->count(); j < n; ++j) {
			auto item = container->item(j);
			if (item->type() == GroupItemType)
				groups << item->data(Qt::DisplayRole).toString();
		}
		return groups;
	};

	if (m_flags & ShowAccounts) {
		for (int i = 0, n = m_rootContainer->count(); i < n; ++i) {
			auto accountItem = reinterpret_cast<AccountItem*>(m_rootContainer->item(i));
			Q_ASSERT(accountItem->type() == AccountItemType);
			QStringList groups = getGroupList(accountItem->container());
			accountItem->account()->config("contactList").setValue("groups", groups);
		}
	} else {
		QStringList groups = getGroupList(m_rootContainer);
		Config("", "contactList").setValue("groups", groups);
	}
}

void ContactListModel::setFlags(int flags)
{
	if (m_rootContainer) {
		beginResetModel();
		clear();
		endResetModel();
	}

	bool wasAccountsShown = m_flags & ShowAccounts;
	m_flags = flags;

	if (m_showHideAction)
		m_showHideAction->setChecked(m_flags & ShowInactive, 0);

	if (m_flags & ShowAccounts) {
		auto container = new AccountContainer;
		m_rootContainer = container;
		accountEnabledConnId = Account::onEnabled.connect(this, [=](Account *account) {
			AbstractContainer *child = 0;
			if (m_flags & ShowGroups)
				child = new GroupContainer;
			else
				child = new ChatUnitContainer;
			auto container = reinterpret_cast<AccountContainer*>(m_rootContainer);
			container->addAccount(account, child);

			QSet<QString> allGroups = account->groups();
			foreach (auto &group, account->config("contactList").value("groups").toStringList()) {
				if (allGroups.remove(group))
					child->addGroup(account, group, flags);
			}
			foreach (auto &group, allGroups)
				child->addGroup(account, group, flags);
		});
		accountDisabledConnId = Account::onDisabled.connect(this, [=](Account *account) {
			auto container = reinterpret_cast<AccountContainer*>(m_rootContainer);
			container->removeAccount(account);
		});
	} else {
		if (wasAccountsShown) {
			Account::onEnabled.disconnect(accountEnabledConnId);
			Account::onDisabled.disconnect(accountDisabledConnId);
		}
		if (m_flags & ShowGroups) {
			m_rootContainer = new GroupContainer(0);
		} else {
			m_rootContainer = new ChatUnitContainer(0);
		}
	}

	if ((m_flags & ShowGroups) && !(m_flags & ShowAccounts)) {
		int flags = m_flags;
		QSet<QString> allGroups;
		foreach (auto acc, Account::all())
			allGroups.unite(acc->groups());

		Config cfg("", "contactList");
		foreach (auto &group, cfg.value("groups").toStringList()) {
			if (allGroups.remove(group))
				m_rootContainer->addGroup(0, group, flags);
		}
		foreach (auto &group, allGroups)
			m_rootContainer->addGroup(0, group, flags);
	}

	if (m_flags & ShowConferences) {
		foreach(auto acc, Account::all()) {
			foreach(auto conf, acc->conferences())
				addConference(acc, conf);
		}
	}

	foreach (auto contact, m_contacts) {
		int flags = m_flags;
		if (isVisible(contact))
			flags |= AbstractContainer::ShowUnit;
		if (contact->status() & Online)
			flags |= AbstractContainer::UnitBecomeActive;
		m_rootContainer->addUnit(contact, flags);
	}
}

AbstractContainer *ContactListModel::getContainer(const QModelIndex &index) const
{
	if (!index.isValid())
		return m_rootContainer;
	auto item = reinterpret_cast<AbstractItem*>(index.internalPointer());
	return item->container();
}

bool ContactListModel::isVisible(Contact *contact) const
{
	return (m_flags & ShowInactive) || (contact->status() & Online);
}

bool ContactListModel::isVisible(Conference *conf) const
{
	return (m_flags & ShowInactive) || (conf->account()->status() & Online);
}

void ContactListModel::checkContactsVisiblity()
{
	int defFlags = m_flags;
	foreach (auto contact, m_contacts) {
		int flag = isVisible(contact) ? AbstractContainer::ShowUnit : AbstractContainer::HideUnit;
		m_rootContainer->updateUnit(contact, defFlags | flag);
	}
}

void ContactListModel::clear()
{
	for (int i = 0, n = m_rootContainer->count(); i < n; ++i)
		delete m_rootContainer->item(i);
	delete m_rootContainer;
}

} // Lime
