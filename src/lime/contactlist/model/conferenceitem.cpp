/*
	 This file is part of Lime.
	 Copyright (C) 2012 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "conferenceitem.h"
#include <lime/icon.h>
#include <lime/account.h>
#include <lime/protocol.h>

namespace Lime
{

ConferenceItem::ConferenceItem(Conference *conference, AbstractContainer *parent) :
	ChatUnitItem(ConferenceItemType, conference, parent)
{
}

bool ConferenceItem::isActive() const
{
	return conference()->isJoined();
}

Status ConferenceItem::status() const
{
	return conference()->isJoined() ? Online : Offline;
}

QVariant ConferenceItem::data(int role) const
{
	if (role == Qt::DecorationRole) {
		if (model()->showAlternativeIcon() && isBlinking()) {
			return alternativeIcon();
		} else {
			QString name = conference()->isJoined() ? "im-chat-joined" : "im-chat-left";
			name += conference()->account()->protocol()->id();
			return Icon(name);
		}
	}
	return ChatUnitItem::data(role);
}

} // Lime
