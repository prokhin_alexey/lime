/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "contactitem.h"
#include "groupitem.h"
#include "accountitem.h"
#include <lime/contact.h>

namespace Lime
{

ContactItem::ContactItem(Contact *contact, AbstractContainer *parent) :
	ChatUnitItem(ContactItemType, contact, parent)
{
}

bool ContactItem::isActive() const
{
	return contact()->status() != Offline;
}

Status ContactItem::status() const
{
	return contact()->status();
}

QVariant ContactItem::data(int role) const
{
	if (role == Qt::DecorationRole)
		return model()->showAlternativeIcon() && isBlinking() ?
					alternativeIcon() :
					contact()->statusIcon();
	return ChatUnitItem::data(role);
}

Qt::ItemFlags ContactItem::flags(Qt::ItemFlags itemFlags, int flags) const
{
	if (contact()->flags() & Contact::GroupsMayBeChanged)
		itemFlags |= Qt::ItemIsDragEnabled;
	itemFlags |= Qt::ItemIsDropEnabled;
	return itemFlags;
}

bool ContactItem::canDropTo(AbstractItem *item, int row, int flags) const
{
	// If groups are not shown, the contact can be moved nowhere
	if (!(flags & ShowGroups))
		return false;

	int contactFlags = contact()->flags();
	Q_ASSERT(contactFlags & Contact::GroupsMayBeChanged);
	bool supportDefaultGroup = contactFlags & Contact::SupportDefaultGroup;

	// If the target is null and accounts are not shown,
	// it means that the user wants to move the contact
	// to the default account group (i.e. remove all tags).
	if (!item)
		return !supportDefaultGroup || (flags & ShowAccounts) ? false : row != -1;

	if (flags & ShowAccounts) {
		// A contact cannot be moved to another account.
		Account *targetAccount = 0;
		auto type = item->type();
		if (type == AccountItemType) {
			if (!supportDefaultGroup)
				return false;
			targetAccount = item->as<AccountItem>()->account();
		} else if (type == GroupItemType) {
			targetAccount = item->parentItem()->as<AccountItem>()->account();
		} else if (type == ContactItemType) {
			if (item->parentItem()->as<AccountItem>() != 0 && !supportDefaultGroup)
				return false;
			targetAccount = item->as<ContactItem>()->contact()->account();
		}
		return targetAccount == contact()->account();
	}

	// Otherwise, the operation is allowed
	return true;
}

void ContactItem::dropTo(AbstractItem *item, int row, int flags)
{
	GroupItem *newGroupItem = item ? item->as<GroupItem>() : 0;
	if (item && !newGroupItem) {
		if (auto parentItem = item->parentItem())
			newGroupItem = parentItem->as<GroupItem>();
	}
	Q_ASSERT(!newGroupItem || newGroupItem->type() == GroupItemType);

	if (newGroupItem) {
		auto oldItem = parentItem();
		auto oldGroupItem = oldItem ? oldItem->as<GroupItem>() : 0;
		if (oldGroupItem) {
			QStringList groups = contact()->groups();
			int index = groups.indexOf(oldGroupItem->name());
			Q_ASSERT(index != -1);
			groups[index] = newGroupItem->name();
			contact()->setGroups(groups);
		} else {
			contact()->setGroup(newGroupItem->name());
		}
	} else {
		contact()->setGroups(QStringList());
	}
}

} // Lime
