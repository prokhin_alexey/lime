/*
	 This file is part of Lime.
	 Copyright (C) 2012 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "chatunititem.h"
#include <lime/icon.h>
#include "contactlistmodel.h"
#include "conferenceitem.h"
#include "contactitem.h"

namespace Lime
{

ChatUnitItem::ChatUnitItem(ItemType type, ChatUnit *unit, AbstractContainer *parent) :
	AbstractItem(type, parent), m_unit(unit)
{
}

QVariant ChatUnitItem::data(int role) const
{
	if (role == Qt::DisplayRole)
		return m_unit->name();
	else if (role == UnitRole)
		return QVariant::fromValue<ChatUnit*>(m_unit);
	else if (role == ContainerRole)
		return QVariant::fromValue<ActionContainer*>(m_unit);
	return AbstractItem::data(role);
}


bool ChatUnitItem::enableBlinking(Notification::Type type, bool enable)
{
	if (enable) {
		if (m_enabledBlinking.contains(type))
			return false;
		m_enabledBlinking.push_back(type);
		if (m_enabledBlinking.count() == 1)
			model()->addBlinkingUnit(this);
		return true;
	} else {
		bool r = m_enabledBlinking.removeOne(type);
		if (m_enabledBlinking.isEmpty())
			model()->removeBlinkingUnit(this);
		return r;
	}
}

QIcon ChatUnitItem::getNotificationIcon(Notification::Type type)
{
	static QVector<QIcon> icons;
	if (icons.isEmpty()) {
		icons.resize(Notification::LastType + 1);
		QIcon newMessageIcon = Icon("mail-message-new");
		icons[Notification::IncomingMessage] = newMessageIcon;
		icons[Notification::IncomingConferenceMessage] = newMessageIcon;
	}
	return icons.value(type);
}

QVariant SeparatorItem::data(int role) const
{
	if (role == Qt::DisplayRole)
		return m_online ? QObject::tr("Online") : QObject::tr("Offline");
	return AbstractItem::data(role);
}

ChatUnitContainer::ChatUnitContainer(AbstractItem *parent) :
	AbstractContainer(parent), m_onlineSep(0), m_offlineSep(0)
{
}

void ChatUnitContainer::addUnit(ChatUnit *unit, int flags)
{
	ChatUnitItem *item = 0;
	if (auto conf = qobject_cast<Conference*>(unit))
		item = new ConferenceItem(conf, this);
	else
		item = new ContactItem(sender_cast<Contact*>(unit), this);
	if (flags & ShowUnit)
		showItem(item, flags);
	m_items.insert(item->unit(), item);
}

void ChatUnitContainer::removeUnit(ChatUnit *unit, int flags)
{
	Q_UNUSED(flags);
	auto itr = m_items.find(unit);
	Q_ASSERT(itr != m_items.end());
	hideItem(*itr, flags);
	delete *itr;
}

void ChatUnitContainer::addGroup(Account *account, const QString &group, int flags)
{
	Q_UNUSED(account);
	Q_UNUSED(group);
	Q_UNUSED(flags);
}

void ChatUnitContainer::removeGroup(Account *account, const QString &group, int flags)
{
	Q_UNUSED(account);
	Q_UNUSED(group);
	Q_UNUSED(flags);
}

void ChatUnitContainer::updateUnit(ChatUnit *unit, int flags)
{
	auto item = m_items.value(unit);
	Q_ASSERT(item);

	if (flags & HideUnit) {
		hideItem(item, flags);
		return;
	}

	if ((flags & ShowUnit) && !m_visibleItems.contains(item)) {
		showItem(item, flags);
		return;
	}

	int current = m_visibleItems.indexOf(item);
	if (current == -1) {
		return;
	} else if (flags & UpdateUnitPosition) {
		int to = evaluateRow(item, flags);
		Q_ASSERT(to >= 0 && to <= m_visibleItems.size());
		bool needMoving = current != to && beginMoveRow(current, to);
		if (needMoving) {
			if (current < to)
				--to;
			m_visibleItems.move(current, to);
			endMoveRow();
		}

		if (flags & ShowSeparators) {
			showSeparatorsIfNecessary(item);
			hideSeparatorsIfNecessary();
		}

		if (needMoving)
			return;
	}

	item->update();
}

void ChatUnitContainer::setGroups(Contact *contact, const QStringList &newGroups,
								 const QStringList &oldGroups, int flags)
{
	Q_UNUSED(contact);
	Q_UNUSED(newGroups);
	Q_UNUSED(oldGroups);
	Q_UNUSED(flags);
}

bool ChatUnitContainer::enableBlinking(ChatUnit *unit, Notification::Type type,
									  bool blink, int flags)
{
	Q_UNUSED(flags);
	auto unitItem = m_items.value(unit);
	Q_ASSERT(unitItem);
	return unitItem->enableBlinking(type, blink);
}

int ChatUnitContainer::evaluateRow(AbstractItem *item, int flags)
{
	auto begin = m_visibleItems.constBegin();
	auto itr = qLowerBound(begin, m_visibleItems.constEnd(), item, [=](const AbstractItem *lhs_, const AbstractItem *rhs_) {
		auto lhs = reinterpret_cast<const ChatUnitItem*>(lhs_);
		auto rhs = reinterpret_cast<const ChatUnitItem*>(rhs_);

		if (lhs->type() == SeparatorItemType) {
			Q_ASSERT(rhs->type() != SeparatorItemType);
			if (reinterpret_cast<const SeparatorItem*>(lhs)->isOnline())
				return true;
			else
				return !rhs->isActive();
		}
		if (rhs->type() == SeparatorItemType) {
			Q_ASSERT(lhs->type() != SeparatorItemType);
			if (reinterpret_cast<const SeparatorItem*>(rhs)->isOnline())
				return false;
			else
				return lhs->isActive();
		}

		if (flags & SortByStatus) {
			int result = lhs->status() - rhs->status();
			if (result != 0)
				return result < 0;
		} else if (flags & ShowSeparators) {
			bool lhsActive = lhs->isActive();
			bool rhsActive = rhs->isActive();
			if (lhsActive != rhsActive)
				return !rhsActive;
		}
		return lhs->unit()->name().compare(rhs->unit()->name(), Qt::CaseInsensitive) < 0;
	});
	return itr - begin;
}

void ChatUnitContainer::hideItem(ChatUnitItem *item, int flags)
{
	int row = m_visibleItems.indexOf(item);
	if (row == -1)
		return;
	beginRemoveRow(row);
	m_visibleItems.takeAt(row);
	endRemoveRow();
	if (flags & ShowSeparators)
		hideSeparatorsIfNecessary();
}

void ChatUnitContainer::showItem(ChatUnitItem *item, int flags)
{
	Q_ASSERT(!m_visibleItems.contains(item));
	int index = evaluateRow(item, flags);
	beginInsertRow(index);
	m_visibleItems.insert(index, item);
	endInsertRow();
	if (flags & ShowSeparators)
		showSeparatorsIfNecessary(item);
}

void ChatUnitContainer::hideSeparatorsIfNecessary()
{
	if (m_onlineSep &&
		((m_visibleItems.count() > 1 && m_visibleItems.at(1) == m_offlineSep) ||
		 (m_visibleItems.count() == 1 && m_visibleItems.first() == m_onlineSep)))
	{
		Q_ASSERT(m_visibleItems.first() == m_onlineSep);
		beginRemoveRow(0);
		m_visibleItems.removeFirst();
		delete m_onlineSep; m_onlineSep = 0;
		endRemoveRow();
	}
	if (!m_visibleItems.isEmpty() && m_visibleItems.last() == m_offlineSep) {
		beginRemoveRow(m_visibleItems.count()-1);
		m_visibleItems.removeLast();
		delete m_offlineSep; m_offlineSep = 0;
		endRemoveRow();
	}
}

void ChatUnitContainer::showSeparatorsIfNecessary(ChatUnitItem *addedItem)
{
	bool isActive = addedItem->isActive();
	if (!m_onlineSep && isActive) {
		beginInsertRow(0);
		m_onlineSep = new SeparatorItem(true, this);
		m_visibleItems.prepend(m_onlineSep);
		endInsertRow();
	}
	if (!m_offlineSep && !isActive) {
		int index = m_visibleItems.count() - 1;
		beginInsertRow(index);
		m_offlineSep = new SeparatorItem(false, this);
		m_visibleItems.insert(index, m_offlineSep);
		endInsertRow();
	}
}

} // Lime
