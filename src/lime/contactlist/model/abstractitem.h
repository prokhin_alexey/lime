/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ABSTRACTITEM_H
#define ABSTRACTITEM_H

#include "contactlistmodel.h"

namespace Lime {

class AbstractContainer;
class ContactListModel;
class Contact;

enum ItemType
{
	InvalidItemType,
	ContactItemType,
	GroupItemType,
	AccountItemType,
	SeparatorItemType,
	ConferenceItemType
};

enum Role
{
	UnitRole = Qt::UserRole + 61,
	ItemTypeRole,
	GroupNameRole,
	StatusRole,
	ContactsCountRole,
	OnlineContactsCountRole,
	AvatarRole,
	AccountRole,
	Color,
	ContainerRole
};

template<typename T>
T *itemAsHelper(AbstractItem *item);

// A representation of one row in the model.
class AbstractItem
{
public:
	AbstractItem(ItemType type, AbstractContainer *parent) :
		m_type(type),
		m_parent(parent),
		m_container(0)
	{}
	virtual ~AbstractItem();
	virtual QVariant data(int role) const;
	virtual Qt::ItemFlags flags(Qt::ItemFlags defaultFlags, int flags) const;
	virtual bool canDropTo(AbstractItem *item, int row, int flags) const;
	virtual void dropTo(AbstractItem *item, int row, int flags);
	virtual QObject *toQObject() { return 0; }
	void setContainer(AbstractContainer *container) { m_container = container; }
	// Returns the container of the subitems.
	AbstractContainer *container() const { return m_container; }
	AbstractContainer *parent() const { return m_parent; }
	AbstractItem *parentItem() const;
	int index() const;
	QModelIndex modelIndex() const;
	QModelIndex parentModelIndex() const;
	void update() { auto index = this->modelIndex(); emit model()->dataChanged(index, index); }
	ContactListModel *model() const { return ContactListModel::instance(); }
	ItemType type() const { return m_type; }

	template<typename T>
	T *as()
	{
		return itemAsHelper<T>(this);
	}
private:
	ItemType m_type;
	AbstractContainer *m_parent;
	AbstractContainer *m_container;
};

// A representation of group of items in the model.
class AbstractContainer
{
public:
	enum Flags
	{
		UpdateContactData   = 0x00000,
		UpdateUnitPosition  = 0x00001,
		ShowUnit            = 0x00002,
		HideUnit            = 0x00004,
		UnitBecomeActive    = 0x00008,
		UnitBecomeInactive  = 0x00010,
		ShowEmptyGroups     = 0x01000,
		ShowSeparators      = 0x02000,
		SortByStatus        = 0x04000
	};
	AbstractContainer(AbstractItem *parent) : m_parent(parent) {}
	virtual AbstractItem *item(int index) const = 0;
	virtual int count() const = 0;
	virtual void addUnit(ChatUnit *unit, int flags) = 0;
	virtual void removeUnit(ChatUnit *unit, int flags) = 0;
	virtual void updateUnit(ChatUnit *unit, int flags) = 0;
	virtual void addGroup(Account *account, const QString &group, int flags) = 0;
	virtual void removeGroup(Account *account, const QString &group, int flags) = 0;
	virtual void setGroups(Contact *contact, const QStringList &newGroups,
						   const QStringList &oldGroups, int flags) = 0;
	virtual bool enableBlinking(ChatUnit *unit, Notification::Type type, bool blink, int flags) = 0;
	AbstractItem *parent() const { return m_parent; }
	virtual void setParent(AbstractItem *parent) { m_parent = parent; }
	virtual int offset() { return 0; }
	QModelIndex parentModelIndex() const;
	ContactListModel *model() const { return ContactListModel::instance(); }
	bool isEmpty() { return count() == 0; }
protected:
	void beginInsertRow(int row);
	void endInsertRow();
	void beginRemoveRow(int row);
	void endRemoveRow();
	bool beginMoveRow(int from, int to);
	void endMoveRow();
private:
	AbstractItem *m_parent;
};

inline AbstractItem::~AbstractItem()
{
	if (m_container) {
		for (int i = 0, n = m_container->count(); i < n; ++i)
			delete m_container->item(i);
		delete m_container;
	}
}

inline int AbstractItem::index() const
{
	int row = 0;
	for (int n = m_parent->count(); row < n; ++row) {
		if (m_parent->item(row) == const_cast<AbstractItem*>(this))
			return row;
	}
	return -1;
}

inline QModelIndex AbstractItem::modelIndex() const
{
	Q_ASSERT(m_parent);
	int row = index();
	Q_ASSERT(row != -1);
	return model()->createIndex(row, 0, (void*)this);
}

inline QModelIndex AbstractItem::parentModelIndex() const
{
	Q_ASSERT(m_parent);
	return m_parent->parentModelIndex();
}

inline QModelIndex AbstractContainer::parentModelIndex() const
{
	return m_parent ? m_parent->modelIndex() : QModelIndex();
}

inline void AbstractContainer::beginInsertRow(int row)
{
	row += offset();
	model()->beginInsertRows(parentModelIndex(), row, row);
}

inline void AbstractContainer::endInsertRow()
{
	model()->endInsertRows();
}

inline void AbstractContainer::beginRemoveRow(int row)
{
	row += offset();
	model()->beginRemoveRows(parentModelIndex(), row, row);
}

inline void AbstractContainer::endRemoveRow()
{
	model()->endRemoveRows();
}

inline bool AbstractContainer::beginMoveRow(int from, int to)
{
	int offset = this->offset();
	from += offset;
	to += offset;
	auto parent = parentModelIndex();
	return model()->beginMoveRows(parent, from, from, parent, to);
}

inline void AbstractContainer::endMoveRow()
{
	model()->endMoveRows();
}

} // Lime

#endif // ABSTRACTITEM_H
