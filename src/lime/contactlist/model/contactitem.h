/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_CONTACTITEM_H
#define LIME_CONTACTITEM_H

#include "chatunititem.h"
#include <lime/contact.h>

namespace Lime {

class ContactItem : public ChatUnitItem
{
public:
	ContactItem(Contact *contact, AbstractContainer *parent);
	virtual bool isActive() const;
	virtual Status status() const;
	virtual QVariant data(int role) const;
	virtual Qt::ItemFlags flags(Qt::ItemFlags itemFlags, int flags) const;
	bool canDropTo(AbstractItem *item, int row, int flags) const;
	void dropTo(AbstractItem *item, int row, int flags);
	Contact *contact() const { return sender_cast<Contact*>(unit()); }
};

template<>
inline ContactItem *itemAsHelper<ContactItem>(AbstractItem *item)
{
	return item->type() == ContactItemType ? reinterpret_cast<ContactItem*>(item) : 0;
}

} // Lime

#endif // LIME_CONTACTITEM_H
