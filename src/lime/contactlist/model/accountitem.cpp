/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "accountitem.h"
#include <lime/contact.h>
#include <lime/account.h>

namespace Lime
{

QVariant AccountItem::data(int role) const
{
	if (role == Qt::DisplayRole)
		return m_account->name();
	else if (role == GroupNameRole)
		return m_account->id();
	else if (role == ContainerRole)
		return QVariant::fromValue<ActionContainer*>(m_account);
	return AbstractItem::data(role);
}

Qt::ItemFlags AccountItem::flags(Qt::ItemFlags flags) const
{
	flags |= Qt::ItemIsDropEnabled;
	return flags;
}

void AccountContainer::addAccount(Account *account, AbstractContainer *container)
{
	Q_ASSERT(!m_items.contains(account));
	auto item = new AccountItem(account, this);
	item->setContainer(container);
	container->setParent(item);
	beginInsertRow(m_accounts.size());
	m_accounts.push_back(item);
	m_items.insert(account, item);
	endInsertRow();
}

void AccountContainer::removeAccount(Account *account)
{
	Q_ASSERT(m_items.contains(account));
	auto item = m_items.take(account);
	Q_ASSERT(item);
	int row = m_accounts.indexOf(item);
	beginRemoveRow(row);
	m_accounts.removeAt(row);
	endRemoveRow();
}

void AccountContainer::addUnit(ChatUnit *unit, int flags)
{
	getContainer(unit)->addUnit(unit, flags);
}

void AccountContainer::removeUnit(ChatUnit *unit, int flags)
{
	getContainer(unit)->removeUnit(unit, flags);
}

void AccountContainer::updateUnit(ChatUnit *unit, int flags)
{
	getContainer(unit)->updateUnit(unit, flags);
}

void AccountContainer::addGroup(Account *account, const QString &group, int flags)
{
	getContainer(account)->addGroup(account, group, flags);
}

void AccountContainer::removeGroup(Account *account, const QString &group, int flags)
{
	getContainer(account)->removeGroup(account, group, flags);
}

void AccountContainer::setGroups(Contact *contact, const QStringList &newGroups,
								 const QStringList &oldGroups, int flags)
{
	getContainer(contact)->setGroups(contact, newGroups, oldGroups, flags);
}

bool AccountContainer::enableBlinking(ChatUnit *unit, Notification::Type type, bool enabled, int flags)
{
	return getContainer(unit)->enableBlinking(unit, type, enabled, flags);
}

inline AbstractContainer *AccountContainer::getContainer(ChatUnit *unit)
{
	return getContainer(unit->account());
}

inline AbstractContainer *AccountContainer::getContainer(Account *account)
{
	auto item = m_items.value(account);
	Q_ASSERT(item);
	return item->container();
}


} // Lime
