/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "groupitem.h"
#include <lime/contact.h>
#include <lime/conference.h>
#include "contactitem.h"
#include "accountitem.h"
#include <QSet>
#include <algorithm>

namespace Lime
{

GroupItem::GroupItem(const QString &name, AbstractContainer *parent) :
	ContactListGroupActionContainer(name),
	AbstractItem(GroupItemType, parent),
	m_active(0),
	m_ref(0)
{
}

QVariant GroupItem::data(int role) const
{
	if (role == Qt::DisplayRole)
		return name();
	else if (role == GroupNameRole) {
		if (auto parentItem = parent()->parent()) {
			QVariant d = parentItem->data(role);
			return d.toString() + "/" + name();
		} else {
			return name();
		}
	} else if (role == OnlineContactsCountRole) {
		return m_active;
	} else if (role == ContactsCountRole) {
		auto container = static_cast<ChatUnitContainer*>(this->container());
		return container->unitsCount();
	} else if (role == ContainerRole) {
		return QVariant::fromValue(actionContainer());
	}
	return AbstractItem::data(role);
}

Qt::ItemFlags GroupItem::flags(Qt::ItemFlags itemFlags, int flags) const
{
	itemFlags |= Qt::ItemIsDropEnabled;
	itemFlags |= Qt::ItemIsDragEnabled;
	return itemFlags;
}

bool GroupItem::canDropTo(AbstractItem *item, int row, int flags) const
{
	Q_UNUSED(row);

	if (!item && flags & ShowGroups)
		return false;

	if (item->type() == ContactItemType)
		item = item->parentItem();

	// Make sure that the group is not moved to another account
	if (item->type() == GroupItemType)
		return parent() == item->parent();
	else if (item->type() == AccountItemType)
		return item == parentItem();

	return false;
}

void GroupItem::dropTo(AbstractItem *item, int row, int flags)
{
	Q_UNUSED(flags);
	Q_ASSERT(flags & ShowGroups && item);
	static_cast<GroupContainer*>(parent())->move(this, item, row);
}

GroupContainer::GroupContainer(AbstractItem *parent) :
	AbstractContainer(parent)
{
	m_defaultGroup = new ChatUnitContainer(parent);
}

AbstractItem *GroupContainer::item(int index) const
{
	int count = m_defaultGroup->count();
	if (index < count)
		return m_defaultGroup->item(index);
	return m_visibleGroups.value(index - count);
}

int GroupContainer::count() const
{
	return m_visibleGroups.count() + m_defaultGroup->count();
}

void GroupContainer::addUnit(ChatUnit *unit, int flags)
{
	auto groups = getUnitGroups(unit);
	if (!groups.isEmpty()) {
		foreach (auto &group, groups)
			addUnit(unit, group, flags);
	} else {
		m_defaultGroup->addUnit(unit, flags);
	}
}

void GroupContainer::removeUnit(ChatUnit *unit, int flags)
{
	auto groups = getUnitGroups(unit);
	if (!groups.isEmpty()) {
		foreach (auto &group, groups)
			removeUnit(unit, group, flags);
	} else {
		m_defaultGroup->removeUnit(unit, flags);
	}
}

void GroupContainer::updateUnit(ChatUnit *unit, int flags)
{
	auto groups = getUnitGroups(unit);
	if (!groups.isEmpty()) {
		foreach (auto &group, groups) {
			auto item = getItem(group);
			Q_ASSERT(item);
			auto container = item->container();
			Q_ASSERT(container);
			if (flags & UnitBecomeInactive)
				item->decActive();
			else if (flags & UnitBecomeActive)
				item->incInactive();
			if ((flags & ShowUnit) && !(flags & ShowEmptyGroups) && container->isEmpty())
				showItem(item);
			container->updateUnit(unit, flags);
			if ((flags & HideUnit) && !(flags & ShowEmptyGroups) && container->isEmpty())
				hideItem(item);
		}
	} else {
		m_defaultGroup->updateUnit(unit, flags);
	}
}

void GroupContainer::addGroup(Account *account, const QString &group, int flags)
{
	addGroup(group, flags);
}

GroupItem *GroupContainer::addGroup(const QString &group, int flags)
{
	auto item = getItem(group);
	if (!item) {
		item = new GroupItem(group, this);
		item->setContainer(new ChatUnitContainer(item));
		m_groups.push_back(item);
		if (flags & ShowEmptyGroups)
			showItem(item);
	}
	++item->m_ref;
	return item;
}

void GroupContainer::removeGroup(Account *account, const QString &group, int flags)
{
	removeGroup(findItem(group));
}

void GroupContainer::removeGroup(const QList<GroupItem*>::iterator &itemItr)
{
	Q_ASSERT(itemItr != m_groups.end());
	auto item = *itemItr;
	if (--item->m_ref == 0) {
		// The assert fails because Account::groupRemoved() may be called before
		// all contacts are removed from the group. It is plain wrong (a contact cannot be in
		// a non-existent group!), however, I don't see any merits of changing the current
		// behavior and the fix will probably make protocol implementations more complicated
		// (it surely will complicate the support for Account::AutoDetermineGroups flag).
		// Q_ASSERT(item->container()->items(true).isEmpty());
		hideItem(item);
		m_groups.erase(itemItr);
		delete item;
	}
}

void GroupContainer::setGroups(Contact *contact, const QStringList &newGroups,
							   const QStringList &oldGroups, int flags)
{
	auto newSet = newGroups.toSet();
	auto oldSet = oldGroups.toSet();

	foreach (auto &group, oldSet - newSet)
		removeUnit(contact, group, flags);
	if (oldSet.isEmpty())
		m_defaultGroup->removeUnit(contact, flags);

	foreach (auto &group, newSet - oldSet)
		addUnit(contact, group, flags);
	if (newSet.isEmpty())
		m_defaultGroup->addUnit(contact, flags);
}

bool GroupContainer::enableBlinking(ChatUnit *unit, Notification::Type type,
									bool blink, int flags)
{
	auto contact = sender_cast<Contact*>(unit);
	auto groups = contact->groups();
	if (!groups.isEmpty()) {
		bool enabled = false;
		foreach (auto &group, groups) {
			auto groupItem = getItem(group);
			Q_ASSERT(groupItem);
			enabled = groupItem->container()->enableBlinking(contact, type, blink, flags) || enabled;
		}
		return enabled;
	} else {
		return m_defaultGroup->enableBlinking(contact, type, blink, flags);
	}
}

void GroupContainer::setParent(AbstractItem *parent)
{
	AbstractContainer::setParent(parent);
	m_defaultGroup->setParent(parent);
}

int GroupContainer::offset()
{
	return m_defaultGroup->count();
}

void GroupContainer::move(GroupItem *item, AbstractItem *target, int row)
{
	// Find where the group should be moved.
	int to = -1, globalTo = -1;
	bool afterTarget = row != -1;
	if (target->type() == ContactItemType) {
		// If the group dropped onto a contact, the group must be moved after the contact's parent.
		target = target->parentItem();
		afterTarget = true;
	}
	if (target->type() == GroupItemType) {
		// If the group dropped onto another group, we should consider 'row' parameter.
		// If it is -1, the group must be moved before the other group, otherwise,
		// after the target group.
		to = m_visibleGroups.indexOf(target);
		globalTo = m_groups.indexOf(target->as<GroupItem>());
		Q_ASSERT(target->parent() == this && to != -1 && globalTo != -1);
		if (afterTarget) {
			++to;
			++globalTo;
		}
	} else if (target->type() == AccountItemType) {
		// If the group dropped onto the account, 'row' parameter is where the group should be moved.
		to = globalTo = row == -1 ? 0 : row - offset();
	} else {
		return;
	}

	// Find where the group is located now
	int from = m_visibleGroups.indexOf(item);
	int globalFrom = m_groups.indexOf(item);
	Q_ASSERT(item->parent() == this && from != -1 && globalTo != -1);

	// Move the group
	if (beginMoveRow(from, to)) {
		if (from < to) {
			Q_ASSERT(globalFrom < globalTo);
			--to;
			--globalTo;
		}
		m_visibleGroups.move(from, to);
		m_groups.move(globalFrom, globalTo);
		endMoveRow();
	}
}

void GroupContainer::addUnit(ChatUnit *unit, const QString &group, int flags)
{
	auto item = getItem(group);
	if (!item && qobject_cast<Conference*>(unit))
		item = addGroup(QT_TR_NOOP("Conferences"), flags);
	Q_ASSERT(item);
	if ((flags & ShowUnit || flags & ShowEmptyGroups) && !m_visibleGroups.contains(item))
		showItem(item);
	item->container()->addUnit(unit, flags);
	if (flags & UnitBecomeActive)
		item->incInactive();
}

void GroupContainer::removeUnit(ChatUnit *unit, const QString &group, int flags)
{
	auto itr = findItem(group);
	// The group may be already removed. See commentary in GroupContainer::removeGroup().
	if (itr == m_groups.end())
		return;
	auto item = *itr;
	auto container = item->container();
	Q_ASSERT(container);
	container->removeUnit(unit, flags);
	if (container->isEmpty() && qobject_cast<Conference*>(unit)) {
		removeGroup(itr);
	} else {
		if (flags & UnitBecomeInactive)
			item->decActive();
		if (container->isEmpty() && !(flags & ShowEmptyGroups))
			hideItem(item);
	}
}

void GroupContainer::showItem(GroupItem *item)
{
	Q_ASSERT(!m_visibleGroups.contains(item));
	// A weird way to find out index of the group, but
	// we need it to preserve the right order of groups.
	int index = 0;
	for (int j = 0, gc = m_groups.count(), vc = m_visibleGroups.count(); j < gc && index != vc; ++j) {
		auto currentGroup = m_groups.at(j);
		if (currentGroup == item)
			break; // The index is found.
		if (currentGroup == m_visibleGroups.at(index)) {
			// We found a visible group that is before our group,
			// increase the index.
			++index;
		}
	}
	beginInsertRow(index);
	m_visibleGroups.insert(index, item);
	endInsertRow();
}

void GroupContainer::hideItem(GroupItem *item)
{
	int row = m_visibleGroups.indexOf(item);
	if (row == -1)
		return;
	beginRemoveRow(row);
	m_visibleGroups.takeAt(row);
	endRemoveRow();
}

QList<GroupItem*>::iterator GroupContainer::findItem(const QString &name)
{
	return std::find_if(m_groups.begin(), m_groups.end(), [&](GroupItem *item) {
		return item->name() == name;
	});
}

GroupItem *GroupContainer::getItem(const QString &name)
{
	auto itr = findItem(name);
	if (itr == m_groups.end())
		return 0;
	return *itr;
}

QStringList GroupContainer::getUnitGroups(ChatUnit *unit)
{
	if (auto conf = qobject_cast<Conference*>(unit)) {
		return QStringList() << QT_TR_NOOP("Conferences");
	} else {
		auto contact = sender_cast<Contact*>(unit);
		return contact->groups();
	}
}

} // Lime
