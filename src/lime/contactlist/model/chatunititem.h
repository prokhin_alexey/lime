/*
	 This file is part of Lime.
	 Copyright (C) 2012 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIME_UNITITEM_H_
#define LIME_UNITITEM_H_

#include "abstractitem.h"

namespace Lime
{

class ChatUnitItem : public AbstractItem
{
public:
	ChatUnitItem(ItemType type, ChatUnit *unit, AbstractContainer *parent);
	virtual bool isActive() const = 0;
	virtual Status status() const = 0;
	virtual QVariant data(int role) const;
	bool enableBlinking(Notification::Type type, bool enable);
	ChatUnit *unit() const { return m_unit; }
	bool isBlinking() const { return !m_enabledBlinking.isEmpty(); }
	QIcon alternativeIcon() const { return getNotificationIcon(m_enabledBlinking.first()); }
private:
	static QIcon getNotificationIcon(Notification::Type type);
private:
	ChatUnit *m_unit;
	QList<Notification::Type> m_enabledBlinking;
};

class SeparatorItem : public AbstractItem
{
public:
	SeparatorItem(bool online, AbstractContainer *parent) :
		AbstractItem(SeparatorItemType, parent), m_online(online)
	{}
	virtual QVariant data(int role) const;
	bool isOnline() const { return m_online; }
private:
	bool m_online;
};

class ChatUnitContainer : public AbstractContainer
{
public:
	ChatUnitContainer(AbstractItem *parent = 0);
	virtual AbstractItem *item(int index) const { return m_visibleItems.value(index); }
	virtual int count() const { return m_visibleItems.count(); }
	virtual void addUnit(ChatUnit *unit, int flags);
	virtual void removeUnit(ChatUnit *unit, int flags);
	virtual void updateUnit(ChatUnit *unit, int flags);
	virtual void addGroup(Account *account, const QString &group, int flags);
	virtual void removeGroup(Account *account, const QString &group, int flags);
	virtual void setGroups(Contact *contact, const QStringList &newGroups,
						   const QStringList &oldGroups, int flags);
	virtual bool enableBlinking(ChatUnit *unit, Notification::Type type,
								bool blink, int flags);
	int unitsCount() const { return m_items.count(); }
protected:
	virtual int evaluateRow(AbstractItem *item, int flags);
private:
	void hideItem(ChatUnitItem *item, int flags);
	void showItem(ChatUnitItem *item, int flags);
	void hideSeparatorsIfNecessary();
	void showSeparatorsIfNecessary(ChatUnitItem *addedItem);
private:
	QList<AbstractItem*> m_visibleItems;
	QHash<ChatUnit*, ChatUnitItem*> m_items;
	SeparatorItem *m_onlineSep;
	SeparatorItem *m_offlineSep;
};

} // Lime

#endif // LIME_UNITITEM_H_
