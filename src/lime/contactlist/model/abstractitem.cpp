/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "abstractitem.h"

namespace Lime
{

QVariant AbstractItem::data(int role) const
{
	if (role == ItemTypeRole)
		return (int)m_type;
	return QVariant();
}

Qt::ItemFlags AbstractItem::flags(Qt::ItemFlags defaultFlags, int flags) const
{
	Q_UNUSED(flags);
	return defaultFlags | Qt::ItemIsDropEnabled;
}

bool AbstractItem::canDropTo(AbstractItem *item, int row, int flags) const
{
	Q_UNUSED(item);
	Q_UNUSED(row);
	Q_UNUSED(flags);
	return false;
}

void AbstractItem::dropTo(AbstractItem *item, int row, int flags)
{
	Q_UNUSED(item);
	Q_UNUSED(row);
	Q_UNUSED(flags);
}

AbstractItem *AbstractItem::parentItem() const
{
	return m_parent ? m_parent->parent() : 0;
}

} // Lime
