/*
	 This file is part of Lime.
	 Copyright (C) 2012 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIME_CONFERENCEITEM_H_
#define LIME_CONFERENCEITEM_H_

#include "chatunititem.h"
#include <lime/conference.h>

namespace Lime
{

class ConferenceItem : public ChatUnitItem
{
public:
	ConferenceItem(Conference *conference, AbstractContainer *parent);
	virtual bool isActive() const;
	virtual Status status() const;
	virtual QVariant data(int role) const;
	Conference *conference() const { return sender_cast<Conference*>(unit()); }
};

template<>
inline ConferenceItem *itemAsHelper<ConferenceItem>(AbstractItem *item)
{
	return item->type() == ConferenceItemType ? reinterpret_cast<ConferenceItem*>(item) : 0;
}

} // Lime
#endif // LIME_CONFERENCEITEM_H_
