/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CONTACTLISTMODEL_H
#define CONTACTLISTMODEL_H

#include <lime/signal.h>
#include <lime/notification.h>
#include <QAbstractItemModel>
#include <QTimer>

namespace Lime
{

class Account;
class Contact;
class Conference;
class AbstractContainer;
class AbstractItem;
class ChatUnitItem;
class Action;

enum ContactListModelFlags
{
	InvalidFlags    = -1,
	ShowAccounts    = 0x010000,
	ShowGroups      = 0x020000,
	ShowInactive    = 0x040000,
	ShowConferences = 0x100000,
	ShowEmptyGroups = 0x001000,
	ShowSeparators  = 0x002000,
	SortByStatus    = 0x004000
};

class ContactListModel : public QAbstractItemModel, protected SignalGuard
{
	Q_OBJECT
public:
	ContactListModel(QObject *parent = 0);
	virtual ~ContactListModel();
	virtual void addContact(Contact *contact);
	virtual void removeContact(Contact *contact);
	virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
	virtual QModelIndex parent(const QModelIndex &child) const;
	virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
	virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
	virtual bool hasChildren(const QModelIndex &parent = QModelIndex()) const;
	virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	virtual Qt::ItemFlags flags(const QModelIndex &index) const;
	virtual QStringList mimeTypes() const;
	virtual QMimeData *mimeData(const QModelIndexList &indexes) const;
	virtual bool dropMimeData(const QMimeData *data, Qt::DropAction action,
							  int row, int column, const QModelIndex &parent);
	virtual Qt::DropActions supportedDropActions() const;
	AbstractContainer *rootContainer() { return m_rootContainer; }
	void enableBlinking(Contact *contact, Notification::Type type, bool enable = true);
	void addBlinkingUnit(ChatUnitItem *unit);
	void removeBlinkingUnit(ChatUnitItem *unit);
	bool showAlternativeIcon() { return m_showAlternativeIcon; }
	static ContactListModel *instance() { Q_ASSERT(self); return self; }
public slots:
	void loadSettings();
	void storeGroups();
	void setFlags(int flags);
	void addConference(Lime::Account *account, Lime::Conference *conf);
	void removeConference(Lime::Account *account, Lime::Conference *conf);
private:
	AbstractContainer *getContainer(const QModelIndex &index) const;
	bool isVisible(Contact *contact) const;
	bool isVisible(Conference *conf) const;
	void checkContactsVisiblity();
	void clear();
private:
	friend class AbstractItem;
	friend class AbstractContainer;
	friend class ContactModelTest;
	int m_flags;
	AbstractContainer *m_rootContainer;
	Action *m_showHideAction;
	QTimer m_alternativeIconTimer;
	QList<Contact*> m_contacts;
	int accountEnabledConnId;
	int accountDisabledConnId;
	bool m_showAlternativeIcon;
	QList<ChatUnitItem*> m_blinkingUnits;
	static ContactListModel *self;
};

} // Lime

#endif // CONTACTLISTMODEL_H
