/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_ACCOUNTITEM_H
#define LIME_ACCOUNTITEM_H

#include "abstractitem.h"

namespace Lime {

class Account;

class AccountItem : public AbstractItem
{
public:
	AccountItem(Account *account, AbstractContainer *parent) :
		AbstractItem(AccountItemType, parent), m_account(account)
	{}
	virtual QVariant data(int role) const;
	virtual Qt::ItemFlags flags(Qt::ItemFlags flags) const;
	Account *account() { return m_account; }
private:
	Account *m_account;
};

class AccountContainer : public AbstractContainer
{
public:
	AccountContainer() : AbstractContainer(0) {}
	void addAccount(Account *account, AbstractContainer *container);
	void removeAccount(Account *account);
	virtual AbstractItem *item(int index) const { return m_accounts.value(index); }
	virtual int count() const { return m_accounts.count(); }
	virtual void addUnit(ChatUnit *unit, int flags);
	virtual void removeUnit(ChatUnit *unit, int flags);
	virtual void updateUnit(ChatUnit *unit, int flags);
	virtual void addGroup(Account *account, const QString &group, int flags);
	virtual void removeGroup(Account *account, const QString &group, int flags);
	virtual void setGroups(Contact *contact, const QStringList &newGroups,
						   const QStringList &oldGroups, int flags);
	virtual bool enableBlinking(ChatUnit *unit, Notification::Type type,
								bool enabled, int flags);
private:
	AbstractContainer *getContainer(ChatUnit *unit);
	AbstractContainer *getContainer(Account *account);
	QList<AbstractItem*> m_accounts;
	QHash<Account*, AccountItem*> m_items;
};

template<>
inline AccountItem *itemAsHelper<AccountItem>(AbstractItem *item)
{
	return item->type() == AccountItemType ? reinterpret_cast<AccountItem*>(item) : 0;
}

} // Lime

#endif // LIME_ACCOUNTITEM_H
