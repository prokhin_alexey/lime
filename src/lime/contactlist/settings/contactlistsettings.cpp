/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "contactlistsettings.h"
#include "ui_contactlistsettings.h"
#include <lime/config.h>
#include <lime/buddy.h>
#include <QDir>

namespace Lime
{

ContactListSettings::ContactListSettings() :
	ui(new Ui::ContactListSettingsClass)
{
	auto central = new QWidget(this);
	setWidget(central);
	ui->setupUi(central);

	bind("alwaysOnTop", ui->alwaysOnTopBox, false);
	bind("showStatusText", ui->showStatusText, true);
	bind("showAvatars", ui->avatarIconCheckBox, true);
	bind("showAccounts", ui->showAccountsBox, true);
	bind("showGroups", ui->showGroupsBox, true);
	bind("showOffline", ui->offlineBox, true, true);
	bind("useAlternationRowColors", ui->alternatingCheckBox, true);
	bind("showEmptyGroups", ui->hideEmptyBox, false, true);
	bind("showSeparators", ui->separatorBox, false, true);
	bind("sortByStatus", ui->sortstatusCheckBox, true);
	bind("opacity", ui->opacitySlider, 100);
	bind("delegateTheme", ui->themeBox, "default.ListQutim");

	ExtendedInfoDescriptions extendedIconsDescriptions;
	Buddy::supportedExtendedInfoHook(extendedIconsDescriptions);
	foreach (auto &iconDesc, extendedIconsDescriptions) {
		auto checkBox = new QCheckBox(iconDesc.description(), this);
		m_extendedInfoWidgets.insert(iconDesc.name(), checkBox);
		ui->formLayout->addRow(checkBox);
	}

	foreach (auto &dir, themeDirs("delegate")) {
		auto themes = dir.entryList(QStringList() << "*.ListQutim" << "*.ListTheme",
				QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot);
		foreach (auto &theme, themes)
			ui->themeBox->addItem(theme);
	}

	foreach (auto &dir, themeDirs("border")) {
		auto themes = dir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot);
		foreach (auto &theme, themes)
			ui->borderThemeBox->addItem(theme.fileName(), theme.absoluteFilePath());
	}

	lconnect(ui->opacitySlider, SIGNAL(valueChanged(int)), this, [=](int val) {
		ui->clOpacityPersLbl->setText(QString("%1%").arg(val));
	});

	lconnect(ui->winStyleComboBox, SIGNAL(currentIndexChanged(int)), this, [=](int winStyle) {
		ui->borderThemeBox->setEnabled(winStyle == 1 /* Themed */);
	});
}

ContactListSettings::~ContactListSettings()
{
	delete ui;
}

void ContactListSettings::loadImpl()
{
	Config cfg("", "contactList");
	loadBinds(cfg);

	int winStyle = cfg.value("windowStyle", 0);
	ui->winStyleComboBox->setCurrentIndex(winStyle);
	ui->borderThemeBox->setEnabled(winStyle == 1 /* Themed */);

	auto borderTheme = cfg.value("borderTheme", "default");
	for (int i = 0, c = ui->borderThemeBox->count(); i < c; ++i) {
		if (borderTheme == ui->borderThemeBox->itemData(i, Qt::UserRole).toString()) {
			ui->borderThemeBox->setCurrentIndex(i);
			break;
		}
	}

	cfg.beginGroup("extendedStatuses");
	auto end = m_extendedInfoWidgets.constEnd();
	for (auto itr = m_extendedInfoWidgets.constBegin(); itr != end; ++itr)
		itr.value()->setChecked(cfg.value(itr.key(), true));
	cfg.endGroup();

	/*int smallIconSize = qApp->style()->pixelMetric(QStyle::PM_SmallIconSize);
	cfg.value("statusIconSize", smallIconSize);
	cfg.value("extIconSize", smallIconSize);
	cfg.value("avatarIconSize", smallIconSize);
	cfg.value("accountIconSize", smallIconSize);
	cfg.value("tagIconSize", smallIconSize);
	cfg.value("showExtendedInfoIcons", true))
	*/
}

void ContactListSettings::saveImpl()
{
	Config cfg("", "contactList");
	saveBinds(cfg);
	cfg.setValue("windowStyle", ui->winStyleComboBox->currentIndex());

	auto borderTheme = ui->borderThemeBox->itemData(ui->borderThemeBox->currentIndex(), Qt::UserRole).toString();
	cfg.setValue("borderTheme", borderTheme);

	cfg.beginGroup("extendedStatuses");
	auto end = m_extendedInfoWidgets.constEnd();
	for (auto itr = m_extendedInfoWidgets.constBegin(); itr != end; ++itr)
		cfg.setValue(itr.key(), itr.value()->isChecked());
	cfg.endGroup();
}

} // Lime
