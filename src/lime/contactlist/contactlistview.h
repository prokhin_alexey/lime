/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CONTACTLISTVIEW_H
#define CONTACTLISTVIEW_H

#include <lime/global.h>
#include <QTreeView>

namespace Lime
{

class ContactListView : public QTreeView
{
	Q_OBJECT
public:
	ContactListView(QAbstractItemModel *model, QWidget *parent);
	virtual ~ContactListView();
public slots:
	void loadSettings();
private slots:
	void onClick(const QModelIndex &index);
	void onCollapsed(const QModelIndex &index);
	void onExpanded(const QModelIndex &index);
	void onRowsInserted(const QModelIndex &parent, int first, int last);
private:
	void storeClosedGroups();
	void checkGroup(const QModelIndex &parent, QAbstractItemModel *model);
	QSet<QString> m_closedIndexes;
};

} // Lime

#endif // CONTACTLISTVIEW_H
