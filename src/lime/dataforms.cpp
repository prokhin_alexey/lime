/*
	This file is part of Lime.
	Copyright (C) 2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "dataforms.h"

#include <QTimer>
#include <QComboBox>
#include <QLabel>
#include <QDebug>
#include <QTextEdit>
#include <QLineEdit>
#include <QCheckBox>
#include <iostream>
#include <algorithm>
#include <QSpinBox>
#include <QDateTimeEdit>
#include <QBoxLayout>
#include <QPushButton>
#include <QFileDialog>
#include <QDialogButtonBox>
#include <QStringListModel>
#include <QListView>
#include <QStyledItemDelegate>
#include <QFormLayout>

#include "utils.h"

namespace Lime {

class DataFormLayout : public QFormLayout
{
public:
	DataFormLayout(DataForm *dataForm, QWidget *widget = 0) :
		QFormLayout(widget), m_dataForm(dataForm)
	{}
	virtual ~DataFormLayout();
	StringChooserField *addStringChooser(const QString &name, const QString &label, const QString &value);
	StringField *addString(const QString &name, const QString &label, const QString &value);
	StringListField *addStringList(const QString &name, const QString &label, const QStringList &value);
	MultiStringField *addMultiString(const QString &name, const QString &label, const QString &value);
	BoolField *addBool(const QString &name, const QString &label, bool value);
	IntField *addInt(const QString &name, const QString &label, int value);
	DoubleField *addDouble(const QString &name, const QString &label, double value);
	DateField *addDate(const QString &name, const QString &label, const QDate &value);
	DateTimeField *addDateTime(const QString &name, const QString &label, const QDateTime &value);
	PixmapChooserField *addPixmap(const QString &name, const QString &label, const QString &value);
private:
	template <typename FieldType, typename DataType>
	FieldType *addField(const QString &name, const QString &label, const DataType &value);
private:
	QList<DataField*> m_fields;
	DataForm *m_dataForm;
};

class DataWidgetBase
{
public:
	DataForm *dataForm() { return m_dataForm; }
	void setDataForm(DataForm *dataForm) { m_dataForm = dataForm; }
protected:
	void incIncompleteWidgetsCount() { m_dataForm->incIncompleteWidgetsCount(); }
	void decIncompleteWidgetsCount() { m_dataForm->decIncompleteWidgetsCount(); }
protected:
	DataForm *m_dataForm;
};

template <typename ParentType, typename WidgetType>
class DataWidget : public ParentType, public DataWidgetBase
{
public:
	DataWidget() : m_visible(true) { m_widget = new WidgetType; }
	virtual ~DataWidget() { m_widget->deleteLater(); destroyed(); }
	QString name() { return m_widget->objectName(); }
	void setName(const QString &name) { m_widget->setObjectName(name); }
	bool isVisible() { return m_visible; }
	void setVisible(bool visible)
	{
		m_visible = visible;
		m_widget->setVisible(visible);
		if (auto label = labelWidget())
			label->setVisible(visible);
	}
	bool isEnabled() { return m_widget->isEnabled(); }
	void setEnabled(bool enabled) { m_widget->setEnabled(enabled); }
	QWidget *widget() { return m_widget; }
	virtual QLabel *labelWidget() { return 0; }
	Signal<void(QLabel*)> labelAdded;
	Signal<void()> destroyed;
protected:
	WidgetType *m_widget;
	bool m_visible;
};

template <typename ParentType, typename WidgetType>
class TitledDataWidget : public DataWidget<ParentType, WidgetType>
{
public:
	virtual ~TitledDataWidget()
	{
		if (m_labelWidget)
			m_labelWidget->deleteLater();
	}
	QString label()
	{
		return m_label;
	}
	void setLabel(const QString &label)
	{
		m_label = label;
		if (!label.isEmpty()) {
			if (!m_labelWidget) {
				typedef DataWidget<ParentType, WidgetType> Parent;
				m_labelWidget = new QLabel;
				m_labelWidget->setBuddy(Parent::m_widget);
				m_labelWidget->setVisible(Parent::m_visible);
				Parent::labelAdded(m_labelWidget);
			}
			m_labelWidget->setText(label + ":");
		} else if (m_labelWidget) {
			m_labelWidget->deleteLater();
		}
	}
	QLabel *labelWidget() { return m_labelWidget; }
protected:
	Pointer<QLabel> m_labelWidget;
	QString m_label;
};

template <typename ParentType, typename WidgetType>
class ValidatedDataWidget : public TitledDataWidget<ParentType, WidgetType>
{
public:
	ValidatedDataWidget() :
		m_isComplete(true)
	{}
protected:
	void setComplete(bool complete)
	{
		if (m_isComplete != complete) {
			if (complete)
				this->decIncompleteWidgetsCount();
			else
				this->incIncompleteWidgetsCount();
			m_isComplete = complete;
		}
	}
	void setMandatory(bool mandatory)
	{
		m_isMandatory = mandatory;
		if (!m_isMandatory)
			setComplete(true);
		else
			checkCompletion();
	}
	bool isMandatory() const
	{
		return m_isMandatory;
	}
	void checkCompletion()
	{
		if (!isMandatory())
			return;
		setComplete(this->isComplete());
	}
	bool checkText(QString text, const QValidator *validator) const
	{
		if (validator) {
			int pos = 0;
			return validator->validate(text, pos) == QValidator::Acceptable;
		} else {
			return !text.isEmpty();
		}
	}
private:
	bool m_isComplete;
	bool m_isMandatory;
};

DataField::DataField()
{
}

DataField::~DataField()
{
}

DataFormLayout::~DataFormLayout()
{
	qDeleteAll(m_fields);
}

#define RETURN_FIELD \
	if (m_dataForm->flags() & DataForm::ReadOnly) \
		return addField<ReadOnlyFieldImpl>(name, label, value); \
	else \
		return addField<FieldImpl>(name, label, value)

StringChooserField *DataFormLayout::addStringChooser(const QString &name, const QString &label, const QString &value)
{
	class FieldImpl : public ValidatedDataWidget<StringChooserField, QComboBox>
	{
	public:
		FieldImpl() :
			m_validator(0)
		{
			lconnect(m_widget, SIGNAL(currentIndexChanged(int)), m_widget, [=] {
				if (!m_widget->isEditable()) {
					onValueChanged();
					checkCompletion();
				}
			});
		}
		QVariant value()
		{
			return m_widget->currentText();
		}
		void setValue(const QVariant &value)
		{
			m_current.clear();
			if (value.type() != QVariant::String) {
				m_widget->setCurrentIndex(-1);
				return;
			}
			auto text = value.toString();
			int index = m_widget->findText(text);
			if (index != -1)
				m_widget->setCurrentIndex(index);
			else if (m_widget->isEditable())
				m_widget->setEditText(text);
			else
				m_current = text;
		}
		void setValidator(QValidator *validator)
		{
			m_validator = validator;
			m_widget->setValidator(validator);
			checkCompletion();
		}
		const QValidator *validator() const
		{
			return m_validator;
		}
		void addOption(const QString &option, const QVariant &userData)
		{
			m_widget->addItem(option, userData);
			if (m_current == option) {
				m_widget->setCurrentIndex(m_widget->count()-1);
				m_current.clear();
			}
		}
		void addOptions(const QStringList &options, const QVariantList &userData)
		{
			Q_ASSERT(userData.isEmpty() || options.size() == userData.size());
			bool hasUserData = !userData.isEmpty();
			int currentIndex = -1;
			m_widget->blockSignals(true);
			for (int i = 0, n = options.size(); i < n; ++i) {
				auto &option = options.at(i);
				m_widget->addItem(option, hasUserData ? userData.at(i) : QVariant());
				if (m_current == option)
					currentIndex = i;
			}
			m_widget->blockSignals(false);
			if (currentIndex != -1) {
				m_widget->setCurrentIndex(m_widget->count()-1);
				m_current.clear();
			}
		}
		void removeOption(const QString &option)
		{
			int index = m_widget->findText(option);
			if (index != -1)
				m_widget->removeItem(index);
		}
		QStringList options() const
		{
			QStringList options;
			for (int i = 0, n = m_widget->count(); i < n; ++i)
				options << m_widget->itemText(i);
			return options;
		}
		void setEditable(bool editable)
		{
			m_widget->setEditable(editable);
			if (editable)
				m_widget->setValidator(m_validator);
			if (editable && !m_current.isEmpty()) {
				m_widget->setEditText(m_current);
				onValueChanged();
			} else if (m_widget->count() == 0) {
				onValueChanged();
			}

			if (editable) {
				lconnect(m_widget, SIGNAL(editTextChanged(QString)), m_widget,
						 [=](const QString &text)
				{
					m_current = text;
					onValueChanged();
					checkCompletion();
				});
			} else {
				ldisconnect(m_widget, SIGNAL(editTextChanged(QString)), m_widget);
			}
		}
		bool isEditable() const
		{
			return m_widget->isEditable();
		}
		QVariant currentUserData()
		{
			return m_widget->itemData(m_widget->currentIndex());
		}
		bool isComplete() const
		{
			return checkText(m_widget->currentText(), m_validator);
		}
	private:
		QString m_current;
		QValidator *m_validator;
	};

	class ReadOnlyFieldImpl : public TitledDataWidget<StringChooserField, QLabel>
	{
	public:
		QVariant value()
		{
			return m_widget->text();
		}
		void setValue(const QVariant &value)
		{
			if (value.type() != QVariant::String)
				m_widget->clear();
			else
				m_widget->setText(value.toString());
		}
		void setValidator(QValidator *) { }
		const QValidator *validator() const { return 0; }
		void setMandatory(bool mandatory) { }
		bool isMandatory() const { return false; }
		void addOption(const QString &option, const QVariant &userData)
		{
			m_userData.insert(option, userData);
		}
		void removeOption(const QString &option)
		{
			m_userData.remove(option);
		}
		QStringList options() const
		{
			return QStringList(m_userData.keys());
		}
		void setEditable(bool /*editable*/) { }
		bool isEditable() const
		{
			return false;
		}
		QVariant currentUserData()
		{
			return m_userData.value(m_widget->text());
		}
		bool isComplete() const
		{
			return true;
		}
	private:
		QHash<QString, QVariant> m_userData;
	};

	RETURN_FIELD;
}

StringField *DataFormLayout::addString(const QString &name, const QString &label, const QString &value)
{
	class FieldImpl : public ValidatedDataWidget<StringField, QLineEdit>
	{
	public:
		FieldImpl()
		{
			lconnect(m_widget, SIGNAL(textChanged(QString)), m_widget, [=] {
				onValueChanged();
				checkCompletion();
			});
		}
		QVariant value()
		{
			return m_widget->text();
		}
		void setValue(const QVariant &value)
		{
			if (value.type() != QVariant::String)
				m_widget->clear();
			else
				m_widget->setText(value.toString());
		}
		void setValidator(QValidator *validator)
		{
			m_widget->setValidator(validator);
			checkCompletion();
		}
		const QValidator *validator() const
		{
			return m_widget->validator();
		}
		void setPasswordMode(bool passwordMode)
		{
			m_widget->setEchoMode(passwordMode ? QLineEdit::Password : QLineEdit::Normal);
		}
		bool isPasswordMode() const
		{
			return m_widget->echoMode() != QLineEdit::Normal;
		}
		bool isComplete() const
		{
			return checkText(m_widget->text(), m_widget->validator());
		}
	};

	class ReadOnlyFieldImpl : public TitledDataWidget<StringField, QLabel>
	{
	public:
		QVariant value()
		{
			return m_widget->text();
		}
		void setValue(const QVariant &value)
		{
			if (value.type() != QVariant::String)
				m_widget->clear();
			else
				m_widget->setText(value.toString());
		}
		void setValidator(QValidator */*validator*/) { }
		const QValidator *validator() const { return 0; }
		void setMandatory(bool /*mandatory*/) { }
		bool isMandatory() const { return false; }
		void setPasswordMode(bool /*passwordMode*/) { }
		bool isPasswordMode() const
		{
			return false;
		}
		bool isComplete() const { return true; }
	};

	RETURN_FIELD;
}

StringListField *DataFormLayout::addStringList(const QString &name, const QString &label, const QStringList &value)
{
	class ItemDelegate : public QStyledItemDelegate
	{
	public:
		ItemDelegate(QValidator* &validator, QObject *parent = 0) :
			QStyledItemDelegate(parent), m_validator(validator)
		{
		}
		QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
		{
			auto w = QStyledItemDelegate::createEditor(parent, option, index);
			if (auto lineEdit = qobject_cast<QLineEdit*>(w))
				lineEdit->setValidator(m_validator);
			return w;
		}
		void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
		{
			if (auto lineEdit = qobject_cast<QLineEdit*>(editor)) {
				if (lineEdit->text() == model->data(index).toString())
					return;
			}
			QStyledItemDelegate::setModelData(editor, model, index);
		}
	private:
		QValidator* &m_validator;
	};

	class FieldImpl : public ValidatedDataWidget<StringListField, QWidget>
	{
	public:
		FieldImpl() :
			m_validator(0)
		{
			m_view = new QListView(m_widget);
			m_view->setObjectName("listView");
			auto layout = new QGridLayout(m_widget);
			layout->setContentsMargins(0, 0, 0, 0);
			layout->addWidget(m_view, 0, 0, 3, 1);

			m_model = new QStringListModel(m_view);
			m_view->setModel(m_model);
			m_view->setItemDelegate(new ItemDelegate(m_validator, m_widget));

			auto addButton = new QPushButton("Add", m_widget);
			addButton->setObjectName("addButton");
			layout->addWidget(addButton, 0, 1);
			lconnect(addButton, SIGNAL(clicked(bool)), m_view, [=] {
				int row = m_model->rowCount();
				m_model->insertRows(row, 1);
				auto index = m_model->index(row);
				m_view->setCurrentIndex(index);
				m_view->edit(index);
				checkCompletion();
				onValueChanged();
			});

			auto removeButton = new QPushButton("Remove", m_widget);
			removeButton->setObjectName("removeButton");
			layout->addWidget(removeButton, 1, 1);
			lconnect(removeButton, SIGNAL(clicked(bool)), m_view, [=] {
				auto selectedRows = m_view->selectionModel()->selectedRows();
				for (int i = selectedRows.count()-1; i >= 0; --i)
					m_model->removeRow(selectedRows.at(i).row());
				checkCompletion();
				onValueChanged();
			});

			lconnect(m_model, SIGNAL(dataChanged(QModelIndex,QModelIndex)), m_widget, [=] {
				checkCompletion();
				onValueChanged();
			});
		}
		QVariant value()
		{
			return m_model->stringList();
		}
		void setValue(const QVariant &value)
		{
			m_model->setStringList(value.value<QStringList>());
			checkCompletion();
			onValueChanged();
		}
		void setValidator(QValidator *validator)
		{
			m_validator = validator;
			checkCompletion();
		}
		QValidator *validator() const
		{
			return m_validator;
		}
		bool isComplete() const
		{
			auto list = m_model->stringList();

			if (list.isEmpty())
				return false;

			foreach (auto &str, list) {
				bool isStrComplete = checkText(str, m_validator);
				if (!isStrComplete)
					return false;
			}

			return true;
		}
	private:
		QValidator *m_validator;
		QStringListModel *m_model;
		QListView *m_view;
	};

	class ReadOnlyFieldImpl : public TitledDataWidget<StringListField, QLabel>
	{
	public:
		QVariant value()
		{
			return m_value;
		}
		void setValue(const QVariant &value)
		{
			if (value.type() == QVariant::StringList) {
				m_value = value.toStringList();
				m_widget->setText(m_value.join("\n"));
			} else {
				m_widget->clear();
			}
		}
		void setValidator(QValidator */*validator*/) { }
		const QValidator *validator() const { return 0; }
		void setMandatory(bool /*mandatory*/) { }
		bool isMandatory() const { return false; }
		bool isComplete() const { return true; }
	private:
		QStringList m_value;
	};

	RETURN_FIELD;
}

MultiStringField *DataFormLayout::addMultiString(const QString &name, const QString &label, const QString &value)
{
	class FieldImpl : public TitledDataWidget<MultiStringField, QTextEdit>
	{
	public:
		FieldImpl()
		{
			lconnect(m_widget, SIGNAL(textChanged()), m_widget, onValueChanged);
		}
		QVariant value()
		{
			return m_widget->toPlainText();
		}
		void setValue(const QVariant &value)
		{
			if (value.type() == QVariant::String)
				m_widget->setPlainText(value.toString());
			else
				m_widget->clear();
		}
	};

	class ReadOnlyFieldImpl : public TitledDataWidget<MultiStringField, QLabel>
	{
	public:
		QVariant value()
		{
			return m_widget->text();
		}
		void setValue(const QVariant &value)
		{
			if (value.type() == QVariant::String)
				m_widget->setText(value.toString());
			else
				m_widget->clear();
		}
	};

	RETURN_FIELD;
}

BoolField *DataFormLayout::addBool(const QString &name, const QString &label, bool value)
{
	class FieldImpl : public DataWidget<BoolField, QCheckBox>
	{
	public:
		FieldImpl()
		{
			lconnect(m_widget, SIGNAL(toggled(bool)), m_widget, onValueChanged);
		}
		QString label()
		{
			return m_widget->text();
		}
		void setLabel(const QString &label)
		{
			m_widget->setText(label);
		}
		QVariant value()
		{
			return m_widget->isChecked();
		}
		void setValue(const QVariant &value)
		{
			bool v = value.type() == QVariant::Bool ? value.toBool() : false;
			m_widget->setChecked(v);
		}
	};

	class ReadOnlyFieldImpl : public TitledDataWidget<BoolField, QLabel>
	{
	public:
		QVariant value()
		{
			return m_value;
		}
		void setValue(const QVariant &value)
		{
			m_value = value.type() == QVariant::Bool ? value.toBool() : false;
			m_widget->setText(m_value ? tr("yes") : tr("no"));
		}
	private:
		bool m_value;
	};

 	RETURN_FIELD;
}

IntField *DataFormLayout::addInt(const QString &name, const QString &label, int value)
{
	class FieldImpl : public TitledDataWidget<IntField, QSpinBox>
	{
	public:
		FieldImpl()
		{
			lconnect(m_widget, SIGNAL(valueChanged(int)), m_widget, onValueChanged);
		}
		QVariant value()
		{
			return m_widget->value();
		}
		void setValue(const QVariant &value)
		{
			if (value.type() != QVariant::Int)
				return;
			auto newData = value.toInt();
			if (newData > m_widget->maximum())
				m_widget->setMaximum(newData);
			else if (newData < m_widget->minimum())
				m_widget->setMinimum(newData);
			m_widget->setValue(newData);
		}
		int minimum() const
		{
			return m_widget->minimum();
		}
		virtual int maximum() const
		{
			return m_widget->maximum();
		}
		virtual void setMaximum(int max)
		{
			m_widget->setMaximum(max);
		}
		virtual void setMinimum(int min)
		{
			m_widget->setMinimum(min);
		}
	};

	class ReadOnlyFieldImpl : public TitledDataWidget<IntField, QLabel>
	{
	public:
		QVariant value()
		{
			return m_value;
		}
		void setValue(const QVariant &value)
		{
			if (value.type() != QVariant::Int)
				return;
			m_value = value.toInt();
			m_widget->setText(QString::number(m_value));
		}
		int minimum() const
		{
			return 0;
		}
		virtual int maximum() const
		{
			return 99;
		}
		virtual void setMaximum(int /*max*/)
		{
		}
		virtual void setMinimum(int /*min*/)
		{
		}
	private:
		int m_value;
	};

 	RETURN_FIELD;
}

DoubleField *DataFormLayout::addDouble(const QString &name, const QString &label, double value)
{
	class FieldImpl : public TitledDataWidget<DoubleField, QDoubleSpinBox>
	{
	public:
		FieldImpl()
		{
			lconnect(m_widget, SIGNAL(valueChanged(double)), m_widget, onValueChanged);
		}
		QVariant value()
		{
			return m_widget->value();
		}
		void setValue(const QVariant &value)
		{
			if (value.type() != QVariant::Double)
				return;
			auto newData = value.toDouble();
			if (newData > m_widget->maximum())
				m_widget->setMaximum(newData);
			else if (newData < m_widget->minimum())
				m_widget->setMinimum(newData);
			m_widget->setValue(newData);
		}
		double minimum() const
		{
			return m_widget->minimum();
		}
		virtual double maximum() const
		{
			return m_widget->maximum();
		}
		virtual void setMaximum(double max)
		{
			m_widget->setMaximum(max);
		}
		virtual void setMinimum(double min)
		{
			m_widget->setMinimum(min);
		}
	};

	class ReadOnlyFieldImpl : public TitledDataWidget<DoubleField, QLabel>
	{
	public:
		QVariant value()
		{
			return m_value;
		}
		void setValue(const QVariant &value)
		{
			if (value.type() != QVariant::Double)
				return;
			m_value = value.toDouble();
			m_widget->setText(QString::number(m_value));
		}
		double minimum() const
		{
			return 0;
		}
		virtual double maximum() const
		{
			return 99.99;
		}
		virtual void setMaximum(double /*max*/)
		{
		}
		virtual void setMinimum(double /*min*/)
		{
		}
	private:
		double m_value;
	};

 	RETURN_FIELD;
}

DateField *DataFormLayout::addDate(const QString &name, const QString &label, const QDate &value)
{
	class FieldImpl : public TitledDataWidget<DateField, QDateEdit>
	{
	public:
		FieldImpl()
		{
			lconnect(m_widget, SIGNAL(dateChanged(QDate)), m_widget, onValueChanged);
		}
		QVariant value()
		{
			return m_widget->date();
		}
		void setValue(const QVariant &value)
		{
			if (value.type() != QVariant::Date)
				return;
			m_widget->setDate(value.toDate());
		}
		void setDisplayFormat(const QString &format)
		{
			m_widget->setDisplayFormat(format);
		}
		QString displayFormat() const
		{
			return m_widget->displayFormat();
		}
		QDate minimum() const
		{
			return m_widget->minimumDate();
		}
		QDate maximum() const
		{
			return m_widget->maximumDate();
		}
		void setMaximum(const QDate &max)
		{
			m_widget->setMaximumDate(max);
		}
		void setMinimum(const QDate &min)
		{
			m_widget->setMinimumDate(min);
		}
	};

	class ReadOnlyFieldImpl : public TitledDataWidget<DateField, QLabel>
	{
	public:
		QVariant value()
		{
			return m_value;
		}
		void setValue(const QVariant &value)
		{
			if (value.type() != QVariant::Date)
				return;
			m_value = value.toDate();
			update();
		}
		void setDisplayFormat(const QString &format)
		{
			m_format = format;
			update();
		}
		QString displayFormat() const
		{
			return m_format;
		}
		QDate minimum() const
		{
			return QDate(7999, 9, 14);
		}
		QDate maximum() const
		{
			return QDate(7999, 12, 31);
		}
		void setMaximum(const QDate &/*max*/)
		{
		}
		void setMinimum(const QDate &/*min*/)
		{
		}
	private:
		void update()
		{
			auto text = m_format.isEmpty() ?
							m_value.toString(Qt::DefaultLocaleLongDate) :
							m_value.toString(m_format);
			m_widget->setText(text);
		}
	private:
		QDate m_value;
		QString m_format;
	};

 	RETURN_FIELD;
}

DateTimeField *DataFormLayout::addDateTime(const QString &name, const QString &label, const QDateTime &value)
{
	class FieldImpl : public TitledDataWidget<DateTimeField, QDateTimeEdit>
	{
	public:
		FieldImpl()
		{
			lconnect(m_widget, SIGNAL(dateTimeChanged(QDateTime)), m_widget, onValueChanged);
		}
		QVariant value()
		{
			return m_widget->dateTime();
		}
		void setValue(const QVariant &value)
		{
			if (value.type() != QVariant::DateTime)
				return;
			m_widget->setDateTime(value.toDateTime());
		}
		void setDisplayFormat(const QString &format)
		{
			m_widget->setDisplayFormat(format);
		}
		QString displayFormat() const
		{
			return m_widget->displayFormat();
		}
		QDateTime minimum() const
		{
			return m_widget->minimumDateTime();
		}
		QDateTime maximum() const
		{
			return m_widget->maximumDateTime();
		}
		void setMaximum(const QDateTime &max)
		{
			m_widget->setMaximumDateTime(max);
		}
		void setMinimum(const QDateTime &min)
		{
			m_widget->setMinimumDateTime(min);
		}
	};

	class ReadOnlyFieldImpl : public TitledDataWidget<DateTimeField, QLabel>
	{
	public:
		QVariant value()
		{
			return m_value;
		}
		void setValue(const QVariant &value)
		{
			if (value.type() != QVariant::DateTime)
				return;
			m_value = value.toDateTime();
			update();
		}
		void setDisplayFormat(const QString &format)
		{
			m_format = format;
			update();
		}
		QString displayFormat() const
		{
			return m_format;
		}
		QDateTime minimum() const
		{
			return QDateTime(QDate(1752, 9, 14), QTime(0, 0));
		}
		QDateTime maximum() const
		{
			return QDateTime(QDate(7999, 12, 31), QTime(23, 59, 59, 999));
		}
		void setMaximum(const QDateTime &max)
		{
		}
		void setMinimum(const QDateTime &min)
		{
		}
	private:
		void update()
		{
			auto text = m_format.isEmpty() ?
							m_value.toString(Qt::DefaultLocaleLongDate) :
							m_value.toString(m_format);
			m_widget->setText(text);
		}
	private:
		QDateTime m_value;
		QString m_format;
	};

 	RETURN_FIELD;
}

PixmapChooserField *DataFormLayout::addPixmap(const QString &name, const QString &label, const QString &value)
{
	class FieldImpl : public TitledDataWidget<PixmapChooserField, QWidget>
	{
	public:
		FieldImpl()
		{
			auto layout = new QGridLayout(m_widget);
			m_pixmapWidget = new QLabel(m_widget);
			m_pixmapWidget->setObjectName("pixmapLabel");
			m_pixmapWidget->setFrameShape(QFrame::Panel);
			m_pixmapWidget->setFrameShadow(QFrame::Sunken);
			m_pixmapWidget->setAlignment(Qt::AlignCenter);

			auto setButton = new QPushButton(QIcon(), QString(), m_widget);
			setButton->setObjectName("setButton");
			setButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
			//setButton->setIcon(Icon("list-add"));
			auto removeButton = new QPushButton(QIcon(), QString(), m_widget);
			removeButton->setObjectName("removeButton");
			//removeButton->setIcon(Icon("list-remove"));

			layout->addWidget(m_pixmapWidget, 0, 0, 3, 1);
			layout->addWidget(setButton, 0, 1);
			layout->addItem(new QSpacerItem(1, 1, QSizePolicy::Preferred, QSizePolicy::Expanding), 2, 1);
			layout->addWidget(removeButton, 2, 1, Qt::AlignBottom);

			lconnect(setButton, SIGNAL(clicked(bool)), m_widget, [this]() {
				m_path = QFileDialog::getOpenFileName(
							m_widget,
							QT_TRANSLATE_NOOP("DataForms", "Open image"),
							QDir::homePath(),
							QT_TRANSLATE_NOOP("DataForms",
											"Images (*.gif *.bmp *.jpg *.jpeg *.png);;"
											"All files (*.*)"));
				updatePixmap();
			});
			lconnect(removeButton, SIGNAL(clicked(bool)), m_widget, [this]() {
				m_path.clear();
				m_pixmapWidget->setPixmap(m_defaultPixmap);
				onValueChanged();
			});
		}
		QPixmap pixmap() const
		{
			return *m_pixmapWidget->pixmap();
		}
		QVariant value()
		{
			return m_path;
		}
		void setValue(const QVariant &value)
		{
			if (value.type() != QVariant::String)
				return;
			auto newPath = value.toString();
			if (newPath != m_path) {
				m_path = newPath;
				updatePixmap();
			}
		}
		void setDefaultImage(const QPixmap &pixmap)
		{
			m_defaultPixmap = pixmap;
			if (m_path.isEmpty() && pixmap.cacheKey() != m_pixmapWidget->pixmap()->cacheKey()) {
				m_pixmapWidget->setPixmap(m_defaultPixmap);
				onValueChanged();
			}
		}
		QPixmap defaultImage() const
		{
			return m_defaultPixmap;
		}
		void setMaximumSize(const QSize &size)
		{
			m_size = size;
			updatePixmap();
		}
		QSize maximumSize() const
		{
			return m_size;
		}
	private:
		void updatePixmap()
		{
			QPixmap pixmap(m_path);
			if (!pixmap.isNull() && m_size.isValid() && pixmap.size() != m_size)
				pixmap = pixmap.scaled(m_size, Qt::KeepAspectRatio);
			m_pixmapWidget->setPixmap(pixmap.isNull() ? m_defaultPixmap : pixmap);
			onValueChanged();
		}
	private:
		QString m_path;
		QPixmap m_defaultPixmap;
		QLabel *m_pixmapWidget;
		QSize   m_size;
	};

	class ReadOnlyFieldImpl : public TitledDataWidget<PixmapChooserField, QLabel>
	{
	public:
		ReadOnlyFieldImpl()
		{
			m_widget->setFrameShape(QFrame::Panel);
			m_widget->setFrameShadow(QFrame::Sunken);
			m_widget->setAlignment(Qt::AlignCenter);
		}
		QPixmap pixmap() const
		{
			return *m_widget->pixmap();
		}
		QVariant value()
		{
			return m_path;
		}
		void setValue(const QVariant &value)
		{
			if (value.type() != QVariant::String)
				return;
			auto newPath = value.toString();
			if (newPath != m_path) {
				m_path = newPath;
				updatePixmap();
			}
		}
		void setDefaultImage(const QPixmap &pixmap)
		{
			m_defaultPixmap = pixmap;
			if (m_path.isEmpty() && pixmap.cacheKey() != m_pixmapWidget->pixmap()->cacheKey()) {
				m_pixmapWidget->setPixmap(m_defaultPixmap);
				onValueChanged();
			}
		}
		QPixmap defaultImage() const
		{
			return m_defaultPixmap;
		}
		void setMaximumSize(const QSize &size)
		{
			m_size = size;
			updatePixmap();
		}
		QSize maximumSize() const
		{
			return m_size;
		}
	private:
		void updatePixmap()
		{
			QPixmap pixmap(m_path);
			if (!pixmap.isNull() && m_size.isValid() && pixmap.size() != m_size)
				pixmap = pixmap.scaled(m_size, Qt::KeepAspectRatio);
			m_widget->setPixmap(pixmap.isNull() ? m_defaultPixmap : pixmap);
		}
	private:
		QString m_path;
		QPixmap m_defaultPixmap;
		QLabel *m_pixmapWidget;
		QSize   m_size;
	};

	 RETURN_FIELD;
}

template <typename FieldType, typename DataType>
FieldType *DataFormLayout::addField(const QString &name, const QString &label, const DataType &value)
{
	auto field = new FieldType;
	field->setDataForm(m_dataForm);
	if (!name.isEmpty())
		field->setName(name);
	if (!label.isEmpty())
		field->setLabel(label);
	field->setValue(value);

	auto widget = field->widget();
	auto labelWidget = field->labelWidget();
	if (labelWidget)
		addRow(labelWidget, widget);
	else
		addRow(widget);

	m_fields << field;
	field->destroyed.connect([this, field]() {
		m_fields.removeOne(field);
	});

	field->labelAdded.connect([=](QLabel *labelWidget) {
		int row;
		getWidgetPosition(widget, &row, 0);
		Q_ASSERT(row != -1);
		insertRow(row, labelWidget, widget);
	});

	return field;
}

void StringChooserField::addOptions(const QStringList &options, const QVariantList &userData)
{
	Q_ASSERT(userData.isEmpty() || options.size() == userData.size());
	bool hasUserData = !userData.isEmpty();
	for (int i = 0, c = options.size(); i < c; ++i)
		addOption(options.at(i), hasUserData ? userData.at(i) : QVariant());
}

void IntField::setRange(int min, int max)
{
	setMinimum(min);
	setMaximum(max);
}

void DoubleField::setRange(double min, double max)
{
	setMinimum(min);
	setMaximum(max);
}

void DateField::setRange(const QDate &min, const QDate &max)
{
	setMinimum(min);
	setMaximum(max);
}

void DateTimeField::setRange(const QDateTime &min, const QDateTime &max)
{
	setMinimum(min);
	setMaximum(max);
}

DataForm::DataForm(int flags, QWidget *parent) :
	QWidget(parent), m_buttonBox(0), m_flags(flags), m_incompleteWidgetsCount(0)
{
	m_mainLayout = new QVBoxLayout(this);
	m_layout = new DataFormLayout(this);
	m_mainLayout->addLayout(m_layout);
}

int DataForm::flags()
{
	return m_flags;
}

void DataForm::addDialogButton(const QString &text, ButtonRole role, const function<void()> &clickHandler)
{
	ensureButtonBox();
	auto button = m_buttonBox->addButton(text, static_cast<QDialogButtonBox::ButtonRole>(role));
	lconnect(button, SIGNAL(clicked(bool)), button, clickHandler);
}

void DataForm::addDialogButton(StandardButton standartButton, const function<void()> &clickHandler)
{
	ensureButtonBox();
	auto button = m_buttonBox->addButton(static_cast<QDialogButtonBox::StandardButton>(standartButton));
	lconnect(button, SIGNAL(clicked(bool)), button, clickHandler);
}

void DataForm::ensureButtonBox()
{
	if (!m_buttonBox) {
		m_buttonBox = new QDialogButtonBox(this);
		m_mainLayout->addSpacerItem(new QSpacerItem(0, 20, QSizePolicy::Preferred, QSizePolicy::Expanding));
		m_mainLayout->addWidget(m_buttonBox);
	}
}

bool DataForm::isComplete() const
{
	return m_incompleteWidgetsCount == 0;
}

void DataForm::incIncompleteWidgetsCount()
{
	if (m_incompleteWidgetsCount == 0) {
		if (m_buttonBox) {
			foreach (auto button, m_buttonBox->buttons()) {
				auto role = static_cast<ButtonRole>(m_buttonBox->buttonRole(button));
				if (role == ApplyRole || role == AcceptRole || role == YesRole) {
					button->setEnabled(false);
				}
			}
		}
		emit completeChanged(false);
	}
	++m_incompleteWidgetsCount;
}

void DataForm::decIncompleteWidgetsCount()
{
	--m_incompleteWidgetsCount;
	Q_ASSERT(m_incompleteWidgetsCount >= 0);
	if (m_incompleteWidgetsCount == 0) {
		if (m_buttonBox) {
			foreach (auto button, m_buttonBox->buttons())
				button->setEnabled(true);
		}
		emit completeChanged(true);
	}
}

#define IMPLEMENT_METHOD(Name, Type) \
	IMPLEMENT_METHOD2(Name, Name, Type)

#define IMPLEMENT_METHOD2(Name, ClassName, Type) \
	ClassName##Field *DataForm::add##Name(Type value, const QString &label, const QString &name) \
	{ \
		return m_layout->add##Name(name, label, value); \
	}

IMPLEMENT_METHOD(StringChooser, const QString &)
IMPLEMENT_METHOD(String, const QString &)
IMPLEMENT_METHOD(StringList, const QStringList &)
IMPLEMENT_METHOD(MultiString, const QString &)
IMPLEMENT_METHOD(Bool, bool)
IMPLEMENT_METHOD(Int, int)
IMPLEMENT_METHOD(Double, double)
IMPLEMENT_METHOD(Date, const QDate &)
IMPLEMENT_METHOD(DateTime, const QDateTime &)
IMPLEMENT_METHOD2(Pixmap, PixmapChooser, const QString &)

} // Lime
