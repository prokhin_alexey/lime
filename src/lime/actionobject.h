/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LIME_ACTIONOBJECT_H
#define LIME_ACTIONOBJECT_H

#include <QIcon>
#include <QMultiMap>
#include <QHash>
#include "global.h"
#include "signal.h"

class QAction;
class QMenu;

namespace Lime
{

class ActionContainer;

// Since an action may be added to several containers and, for different
// containers, the action may have different states, almost every
// Action's member has "container" parameter.
// A caller may pass 0 as the container, in that case, the call will
// change action's state for all containers.

class Action: public QObject
{
	Q_OBJECT
public:
	/*
		The higher priority the higher an action is in a context menu
		and nearer to the left in a toolbar.

		Also, depending on priority, separators are added.
		TODO: describe separation algorithm.
		if you want to add an action at the top of a context menu, set priority
		to HighestPriority + 5000;
		if you want to add the action just before the first separator, set priority
		to HighestPriority - 5000;
		if you want the action at the bottom of the context menu, priority should be
		something like LowestPriority - 5000.
	*/
	enum Priority
	{
		LowestPriority    = 0x01000000,
		LowPriority       = 0x09000000,
		NormalPriority    = 0x11000000,
		HighPriority      = 0x19000000,
		HighestPriority   = 0x21000000
	};

	enum Type
	{
		ContextMenuAction       = 0x00000001, // show the action in the context menu
		MainToolbarAction       = 0x00000002, // show the action in the chat top toolbar
		AdditionalToolbarAction = 0x00000004, // show the action in the chat bottom toolbar
		AllActions              = 0xffffffff  // show the action everywhere
	};
	Q_DECLARE_FLAGS(Types, Type)

	explicit Action(QObject *parent = 0);
	Action(const QIcon &icon, const QString &text, QObject *parent = 0);
	Action(const QString &text, QObject *parent = 0);
	virtual ~Action();
	int priority() const;
	// For sake of performance, the priority will be used only for newly created QActions.
	void setPriority(int priority);
	Types type() const;
	void setType(Types type);
	QIcon icon(ActionContainer *container) const;
	void setIcon(const QIcon &icon, ActionContainer *container);
	QString text(ActionContainer *container) const;
	void setText(const QString &text, ActionContainer *container);
	bool isCheckable() const;
	void setCheckable(bool checkable);
	bool isChecked(ActionContainer *container) const;
	void setChecked(bool checked, ActionContainer *container);
	bool isVisible(ActionContainer *container) const;
	void setVisible(bool visible, ActionContainer *container);
	bool isEnabled(ActionContainer *container) const;
	void setEnabled(bool enabled, ActionContainer *container);
	bool hasSubActions(ActionContainer *container) const;
	template<typename T>
	void addSubAction(Action *action);
	QString name() const { return m_name; }
	void setName(const QString &name) { m_name = name; }
	void addFilter(const function<bool(ActionContainer *)> &filter) { m_filters << filter; }
	/* The method sets a function that accepts a container and return another container
	 * the first container shares state with.
	 * So if you have an action registered for Contact, and you set a function via the method
	 * that returns an account of a contact:
	 * 		action->setSettingsContainerGetter([](ActionContainer *container) {
	 * 			if (auto contact = qobject_cast<Contact*>(container->toQObject())
	 * 				container = contact->account();
	 *			return container;
	 * 		});
	 * All contacts that have the same account will have always the same state of the action.
	 * You also will be able to pass an account as container argument to the setters and getters
	 * of the action:
	 * 		foreach (auto account, Account::all())
	 * 			action->setEnabled(false, account);
	 * The code will disable the action for every contact.
	 *
	 * Notice that, since contacts that have the same account share their state of the action,
	 * if you will change a state of one contact the state will be in fact changed for all the
	 * contacts:
	 * 		auto contacts = account->contacts();
	 * 		auto itr = contacts.constBegin();
	 * 		auto end = contacts.constEnd();
	 * 		if (itr != end) {
	 * 			action->setVisible(false, *itr);
	 * 			++itr;
	 * 			for (; itr != end; ++itr)
	 * 				Q_ASSERT(action->isVisible(*itr) == false);
	 * 		}
	 */
	void setProxyContainerGetter(const function<ActionContainer*(ActionContainer*)> &getter);
signals:
	void actionCreated(QAction *action, ActionContainer *container);
	void actionCreated(QAction *action, QObject *container);
	void triggered(ActionContainer *container, bool checked);
	void triggered(QObject *container, bool checked);
private:
	friend class ActionContainer;
	friend class ActionToolBar;
	struct ActionState
	{
		ActionState() :
			isChecked(false),
			isVisible(true),
			isEnabled(true)
		{}
		QIcon icon;
		QString text;
		bool isChecked : 1;
		bool isVisible : 1;
		bool isEnabled : 1;
	};
	ActionState getState(ActionContainer *container) const;
	ActionState &getState(ActionContainer *container);
	// FIXME: maybe we should make it public?
	QList<QAction*> getActions(ActionContainer *container);
	QAction *createAction(ActionContainer *container, QObject *parent);
	void connectToActionSignals(QAction *action, ActionContainer *container);
	void addSubAction(const QMetaObject *meta, Action *action);
	static QMenu *createMenu(ActionContainer *container, const QList<Action*> &actions, QObject *parent);
private:
	QString m_name;
	ActionState m_defaultState;
	mutable QMap<ActionContainer*, ActionState> m_states;
	mutable QMultiMap<ActionContainer*, QAction*> m_actions;
	QMultiMap<const QMetaObject*, Action*> m_subactions;
	int m_priority;
	Types m_type;
	bool m_isCheckable;
	QList<function<bool(ActionContainer *)>> m_filters;
	function<ActionContainer*(ActionContainer *)> m_settingsContainerGetter;
};
Q_DECLARE_OPERATORS_FOR_FLAGS(Action::Types)

class ActionContainer
{
	Q_DISABLE_COPY(ActionContainer)
public:
	ActionContainer();
	virtual ~ActionContainer();
	QList<Action*> actions(Action::Types type);
	QList<QAction*> qActions(Action::Types type, QObject *parent = 0);
	QMenu *menu(Action::Types type, QObject *parent = 0);
	virtual QObject *toQObject() = 0;
	template<typename T>
	static void addAction(Action *action);
	template<typename T>
	static Action *submenuAction(const QString &name, const QString &title);
public:
	Signal<void(Action*)> actionAdded;
protected:
	static void addAction(const QMetaObject *meta, Action *action);
	static Action *submenuAction(const QMetaObject *meta, const QString &name, const QString &title);
private:
	QList<QMenu*> m_menus;
};

template<typename T>
void Action::addSubAction(Action *action)
{
	addSubAction(&T::staticMetaObject, action);
}

template<typename T>
void ActionContainer::addAction(Action *action)
{
	addAction(&T::staticMetaObject, action);
}

template<typename T>
Action *ActionContainer::submenuAction(const QString &name, const QString &title)
{
	submenuAction(&T::staticMetaObject, name, title);
}

}

Q_DECLARE_INTERFACE(Lime::ActionContainer, "lime.ActionContainer")
Q_DECLARE_METATYPE(Lime::ActionContainer*)

#endif // LIME_ACTIONOBJECT_H
