/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_CHATUNIT_H
#define LIME_CHATUNIT_H

#include "global.h"
#include "actionobject.h"
#include "message.h"
#include <QDate>
#include <QPointer>

class QTextDocument;

namespace Lime {

class ChatUnit;
class Account;
class ChatWidget;
class ToolTip;


// Everything users can send messages to or received them from.
class ChatUnit : public QObject, public ActionContainer
{
	Q_OBJECT
public:
	enum ChatUnitFlags
	{
		SupportsOutgoingMessages = 0x00000001, // The chat unit may send messages.
		SupportsIncomingMessages = 0x00000002, // The chat unit may receive messages.
		SupportsHtmlMessages     = 0x00000004, // The chat unit may send and(or) receive html messages.
		ShowIdInToolTip          = 0x00000008,
		SupportsChatStates       = 0x00000010, // The chat unit supports notifications about typing.
		SupportsMessageReceipts  = 0x00000020, // The chat unit may send delivery receipts.
		ChatUnitDefaultFlags     = SupportsOutgoingMessages | SupportsIncomingMessages | ShowIdInToolTip
	};
	enum SendMessageError
	{
		MessageSent,         // The message has been sent and no error has happened
		LongMessageError,    // The message is too long and cannot be sent
		FloodError,          // User has been sending message too often
		UnsupportedTypeError // The message format is not supported by the protocol
	};
	enum ChatState
	{
		StateActive = 0,    // User is actively participating in the chat session.
		StateInActive,      // User has not been actively participating in the chat session.
		StateGone,          // User has effectively ended their participation in the chat session.
		StateComposing,     // User is composing a message.
		StatePaused         // User had been composing but now has stopped.
	};
	enum MessageReceipt
	{
		MessageNotDelivered,
		MessageDelivered,
		MessageProbablyDelivered
	};

	ChatUnit(const QString &id, Account *account, int flags = ChatUnitDefaultFlags);
	virtual ~ChatUnit();
	QString id() const { return m_id; }
	QString name() const { return m_name.isEmpty() ? m_id : m_name; }
	QString title() const { return m_title.isEmpty() ? name() : m_title; }
	Account *account() const { return m_account; }
	int flags() const { return m_flags; }
	MessageList unreadMessages() const { return m_unread; }
	void sendMessage(const Message &message);
	ChatUnit *getHistoryUnit() const;
	QTextDocument *document() const;
	ChatState chatState() const { return m_state; }
	bool isChatOpened() { return m_isOpened; }
	bool isChatActive() { return m_isActive; }
public: // hooks
	Signal<void(ToolTip *toolTip)> toolTipHook;
public slots:
	void activateChat();
	void clearUnreadMessages();
public: // static signals
	static Signal<void(ChatUnit* unit, QString newName, QString oldName)> onNameChanged;
	static Signal<void(ChatUnit* unit, QString newTitle, QString oldTitle)> onTitleChanged;
	static Signal<void(ChatUnit* unit, Message message)> onMessageReceived;
	static Signal<void(ChatUnit* unit, Message message)> onNewUnreadMessage;
	static Signal<void(ChatUnit* unit)> onUnreadMessagesCleared;
	static Signal<void(ChatUnit* unit)> onChatOpened;
	static Signal<void(ChatUnit* unit)> onChatActivated;
	static Signal<void(ChatUnit* unit)> onChatDeactivated;
	static Signal<void(ChatUnit* unit)> onChatClosed;
	static Signal<void(ChatUnit* unit, ChatState state)> onChatStateChanged;
	static Signal<void(ChatUnit* unit, int messageId, MessageReceipt success)> onMessageReceipt;
signals:
	void nameChanged(const QString &newName, const QString &oldName);
	void titleChanged(const QString &newTitle, const QString &oldTitle);
	void messageReceived(const Lime::Message &message);
	void newUnreadMessage(const Lime::Message &message);
	void unreadMessagesCleared();
	void chatOpened();
	void chatActivated();
	void chatDeactivated();
	void chatClosed();
	void chatStateChanged(Lime::ChatUnit::ChatState state);
	void messageReceipt(int messageId, Lime::ChatUnit::MessageReceipt success);
protected:
	friend class ChatManager;
	// Protocols must call the method whenever unit's name has changed
	void updateName(const QString &name);
	// Protocols must call the method whenever unit's title has changed
	void updateTitle(const QString &title);
	// Protocols must call the method when a new message has been received.
	void handleIncomingMessage(const Message &message);
	// Protocol must call the method when unit's chat state has been changed.
	void handleChatState(ChatState state);
	// Protocol must call the method whenever a message receipt has been received.
	void handleMessageReceipt(int messageId, MessageReceipt success);
protected:
	virtual SendMessageError sendMessageImpl(const Message &message) = 0;
	virtual void sendChatStateImpl(ChatState state);
private:
	virtual QObject* toQObject() { return this; }
protected:
	ChatState m_state;
private:
	int m_flags;
	Account *m_account;
	QString m_id;
	QString m_name;
	QString m_title;
	MessageList m_unread;
	bool m_isActive;
	bool m_isOpened;
};

}

Q_DECLARE_METATYPE(Lime::ChatUnit*)
Q_DECLARE_METATYPE(Lime::ChatUnit::ChatState)

#endif // LIME_CHATUNIT_H
