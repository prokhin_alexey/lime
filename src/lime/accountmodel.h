/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LIME_ACCOUNTMODEL_H_
#define LIME_ACCOUNTMODEL_H_

#include "global.h"
#include "signal.h"
#include <QAbstractListModel>
#include <QStyledItemDelegate>

namespace Lime
{

class Account;

class AccountDelegate : public QStyledItemDelegate
{
public:
	explicit AccountDelegate(QObject *parent = 0);
	void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

class AccountModel : public QAbstractListModel, protected SignalGuard
{
	Q_OBJECT
public:
	enum Roles
	{
		AccountRole = Qt::UserRole
	};
	enum Flags
	{
		ShowOnlyEnabledAccounts = 0x0001
	};
	explicit AccountModel(int flags = ShowOnlyEnabledAccounts, QObject *parent = 0);
	virtual ~AccountModel();
	virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
	virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	QList<Account*> accounts() const { return m_accounts; }
private:
	void addAccount(Account *account);
	void removeAccount(Account *account);
private:
	QList<Account*> m_accounts;
};

} // Lime
#endif // LIME_ACCOUNTMODEL_H_
