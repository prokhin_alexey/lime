/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ACTIONTOOLBAR_H
#define ACTIONTOOLBAR_H

#include "actionobject.h"
#include <QToolBar>

namespace Lime {

class ActionToolBar : public QToolBar
{
public:
	explicit ActionToolBar(const QString &title, ActionContainer *container = 0,
						   Action::Type type = Action::MainToolbarAction,
						   QWidget *parent = 0);
	explicit ActionToolBar(ActionContainer *container = 0,
						   Action::Type type = Action::MainToolbarAction,
						   QWidget *parent = 0);
	virtual ~ActionToolBar();
	void setButtonSize(const QSize &size);
	void setContainer(ActionContainer *container);
	void setType(Action::Type type);
protected:
	using QToolBar::addAction;
	using QToolBar::insertAction;
	using QToolBar::removeAction;
private:
	ActionContainer *m_container;
	Action::Type m_type;
	QSize m_buttonSize;
};

} // Lime

#endif // ACTIONTOOLBAR_H
