/*
    This file is part of Lime.
    Copyright (c) 2010  Nigmatullin Ruslan <euroelessar@gmail.com>
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_SETTINGS_H
#define LIME_SETTINGS_H

#include "signal.h"
#include "config.h"
#include <QWidget>
#include <QScrollArea>

class QCheckBox;
class QLineEdit;
class QSlider;
class QComboBox;

namespace Lime {

class SettingsWidgetPrivate;
class SettingsItemPrivate;
class NotificationFilterWidget;
class NotificationFilter;

class SettingsWidget : public QScrollArea
{
	Q_OBJECT
	Q_PROPERTY(bool modified READ isModified)
public:
	SettingsWidget(QWidget *parent = 0);
	virtual ~SettingsWidget();
	bool isModified() const;
public slots:
	void load();
	void save();
	void cancel();
signals:
	void modified();
	void unmodified();
	void saved();
protected:
	virtual void loadImpl() = 0;
	virtual void saveImpl() = 0;
	virtual void cancelImpl();
	void listenChildrenStates(const QWidgetList &exceptions = QWidgetList());
	const char *lookForWidgetState(QWidget *widget, const char *property = 0, const char *signal = 0);
	void bind(const QString &configEntry, QCheckBox *box, bool defaultValue, bool reversed = false);
	void bind(const QString &configEntry, QLineEdit *edit, const QString &defaultValue = QString());
	void bind(const QString &configEntry, QSlider *slider, int defaultValue = 0);
	void bind(const QString &configEntry, QComboBox *comboBox, const QString &defaultValue = QString());
	void bind(const QString &configEntry, NotificationFilterWidget *filter, const NotificationFilter &defaultValues);
	void saveBinds(Config &cfg);
	void loadBinds(const Config &cfg);
	virtual void virtual_hook(int id, void *data);
private slots:
	void onStateChanged(int index);
private:
	void addBind(const function<void(const Config &)> &loader, const function<void(Config &)> &saver);
	QScopedPointer<SettingsWidgetPrivate> p;
};

class SettingsItem
{
	Q_DECLARE_PRIVATE(SettingsItem)
public:
	SettingsItem(const QIcon &icon, const QString &title, function<SettingsWidget*()> creationFunc);
	SettingsItem(const QString &title, function<SettingsWidget*()> creationFunc);
	~SettingsItem();
	void setIcon(const QIcon &icon);
	QIcon icon() const;
	void setTitle(const QString &title);
	QString title() const;
	void setPriority(int priority);
	int priority() const;
	SettingsWidget *createWidget();
	Signal<void()> destroyed;
private:
	QScopedPointer<SettingsItemPrivate> d_ptr;
};

class SettingsManager
{
public:
	static QList<SettingsItem*> items();
	template<typename T>
	static QList<SettingsItem*> items();
	static void addGlobalSettings(SettingsItem *item);
	template<typename SettingsType>
	static SettingsItem *addGlobalSettings(const QIcon &icon, const QString &title);
	static SettingsItem *addGlobalSettings(const QIcon &icon, const QString &title,
			 function<SettingsWidget*()> creationFunc);
	template<typename T>
	static void addSettings(SettingsItem *item);
	template<typename T, typename SettingsType>
	static SettingsItem *addSettings(const QIcon &icon, const QString &title);
	template<typename T>
	static SettingsItem *addSettings(const QIcon &icon, const QString &title,
									 function<SettingsWidget*()> creationFunc);
private:
	static QList<SettingsItem*> items(const QMetaObject *meta);
	static void addSettings(const QMetaObject *meta, SettingsItem *item);
};

template<typename T>
QList<SettingsItem*> SettingsManager::items()
{
	return items(&T::staticMetaObject);
}

template<typename SettingsType>
SettingsItem *SettingsManager::addGlobalSettings(const QIcon &icon, const QString &title)
{
	auto item = new SettingsItem(icon, title, []()->SettingsWidget* {
		return new SettingsType();
	});
	addGlobalSettings(item);
	return item;
}

template<typename T>
void SettingsManager::addSettings(SettingsItem *item)
{
	addSettings(&T::staticMetaObject, item);
}

template<typename T, typename SettingsType>
SettingsItem *SettingsManager::addSettings(const QIcon &icon, const QString &title)
{
	auto item = new SettingsItem(icon, title, []()->SettingsWidget* {
		return new SettingsType();
	});
	addSettings(&T::staticMetaObject, item);
	return item;
}

template<typename T>
SettingsItem *SettingsManager::addSettings(const QIcon &icon, const QString &title,
										   function<SettingsWidget*()> creationFunc)
{
	auto item = new SettingsItem(icon, title, creationFunc);
	addSettings(&T::staticMetaObject, item);
	return item;
}

}

#endif // LIME_SETTINGSCONTAINER_H
