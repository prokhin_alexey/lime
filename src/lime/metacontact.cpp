/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "metacontact.h"
#include <QSet>

namespace Lime
{

Signal<void(MetaContact *contact, Buddy *buddy)> MetaContact::onResourceAdded;
Signal<void(MetaContact *contact, Buddy *buddy)> MetaContact::onResourceRemoved;

void MetaContact::addResource(Buddy *buddy)
{
	// Declare helper lambda that inserts the buddy to m_resources
	auto insertResource = [=](Buddy *buddy) {
		auto itr = qLowerBound(m_resources.begin(), m_resources.end(), buddy, [](Buddy *lhs, Buddy *rhs)
		{
			int p1 = lhs->priority(), p2 = rhs->priority();
			if (p1 == p2) {
				int statusDiff = lhs->status() - rhs->status();
				if (statusDiff != 0)
					return statusDiff > 0;
				else
					return lhs->id() < rhs->id();
			} else {
				return p1 < p2;
			}
		});

		m_resources.insert(itr, buddy);
		if (m_resources.first() == buddy)
			updateData(buddy);
	};

	lconnect(buddy, SIGNAL(statusChanged(Lime::Status,Lime::Status)), this,
		[=](Status status, Status old)
	{
		if (status != old) {
			deleteResource(buddy);
			insertResource(buddy);
		}
		if (m_resources.first() == buddy)
			this->updateStatus(status, buddy->statusText());
	});
	lconnect(buddy, SIGNAL(extendedInfoChanged(QString,QVariantMap)), this,
		[=](const QString &name, const QVariantMap &info)
	{
		if (m_resources.first() == buddy)
			this->setExtendedInfo(name, info);
	});
	if (!(this->flags() & HasAvatar)) {
		lconnect(buddy, SIGNAL(avatarChanged(QString)), this,
			[=](const QString &avatar)
		{
			if (m_resources.first() == buddy)
				this->updateAvatar(avatar);
		});
	}
	lconnect(buddy, SIGNAL(chatStateChanged(Lime::ChatUnit::ChatState)), this, [=](ChatState state) {
		if (m_resources.first() == buddy) {
			if (state == m_state)
				return;

			m_state = state;
			emit chatStateChanged(state);
			onChatStateChanged(this, state);
		}
	});

	// Insert the buddy
	insertResource(buddy);
	reinterpret_cast<MetaContact*>(buddy)->updateParentUnit(this);
	emit resourceAdded(buddy);
	emit onResourceAdded(this, buddy);

	// Handle buddy's priority change event
	lconnect(buddy, SIGNAL(priorityChanged(int,int)), this, [=] {
		deleteResource(buddy);
		insertResource(buddy);
	});

	// Remove the buddy from the metacontact when the buddy is destroyed
	lconnect(buddy, SIGNAL(destroyed()), this, [=] {
		removeResource(buddy);
	});
}

void MetaContact::removeResource(Buddy *buddy)
{
	ldisconnect(buddy, 0, this);
	deleteResource(buddy);
	if (m_resources.isEmpty()) {
		updateStatus(Status::Offline);
		if (!(this->flags() & HasAvatar))
			updateAvatar(QString());
		// Extended info has been already updated in updateStatus()
	} else {
		updateData(m_resources.first());
	}
	emit resourceRemoved(buddy);
	emit onResourceRemoved(this, buddy);
}

void MetaContact::deleteResource(Buddy *buddy)
{
	int index = m_resources.indexOf(buddy);
	Q_ASSERT(index != -1);
	m_resources.removeAt(index);
}

void MetaContact::updateData(Buddy *buddy)
{
	updateStatus(buddy->status(), buddy->statusText());
	if (!(flags() & HasAvatar))
		updateAvatar(buddy->avatar());

	auto removedInfos = allExtendedInfo().keys().toSet();
	auto extInfos = buddy->allExtendedInfo();
	auto itr = extInfos.constBegin(), end = extInfos.constEnd();
	for (; itr != end; ++itr) {
		setExtendedInfo(itr.key(), itr.value());
		removedInfos.remove(itr.key());
	}
	foreach (auto key, removedInfos)
		removeExtendedInfo(key);
}

} // Lime

