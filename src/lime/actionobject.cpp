/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "actionobject.h"
#include <QAction>
#include <QMenu>
#include <QApplication>
#include <functional>
#include <algorithm>
#include "utils.h"

namespace Lime
{

struct Submenu
{
	const QMetaObject *meta;
	QString name;
	Action *action;
};

static QMultiMap<const QMetaObject*, Action*> m_staticActions;
static QList<Submenu> m_staticSubmenus;

static QList<Action*> getContainerActions(ActionContainer *container, Action::Types type,
										  const QMultiMap<const QMetaObject*, Action*> &actionsMap)
{
	QList<Action*> actions;
	auto meta = container->toQObject()->metaObject();
	do {
		foreach (auto action, actionsMap.values(const_cast<QMetaObject*>(meta))) {
			if (action->type() & type) {
				auto itr = qLowerBound(actions.begin(), actions.end(), action,
					[=](Action *lhs, Action *rhs)->bool
				{
					int p1 = lhs->priority(), p2 = rhs->priority();
					if (p1 == p2)
						return QString::localeAwareCompare(lhs->text(container), rhs->text(container)) > 0;
					else
						return p1 > p2;
				});
				actions.insert(itr, action);
			}
		}
	} while (!!(meta = meta->superClass()));
	return actions;
}

Action::Action(QObject *parent) :
	QObject(parent),
	m_priority(NormalPriority),
	m_type(AllActions),
	m_isCheckable(false)
{
	m_settingsContainerGetter = [](ActionContainer *container) {
		return container;
	};
}

Action::Action(const QIcon &icon, const QString &title, QObject *parent) :
	QObject(parent),
	m_priority(NormalPriority),
	m_type(AllActions),
	m_isCheckable(false)
{
	m_defaultState.icon = icon;
	m_defaultState.text = title;
	m_settingsContainerGetter = [](ActionContainer *container) {
		return container;
	};
}

Action::Action(const QString &title, QObject *parent) :
	QObject(parent),
	m_priority(NormalPriority),
	m_type(AllActions),
	m_isCheckable(false)
{
	m_defaultState.text = title;
	m_settingsContainerGetter = [](ActionContainer *container) {
		return container;
	};
}

Action::~Action()
{
	foreach (auto act, m_actions)
		act->deleteLater();
}

int Action::priority() const
{
	return m_priority;
}

void Action::setPriority(int priority)
{
	m_priority = priority;
}

Action::Types Action::type() const
{
	return m_type;
}

void Action::setType(Action::Types type)
{
	m_type = type;
}

QIcon Action::icon(ActionContainer *container) const
{
	return getState(container).icon;
}

void Action::setIcon(const QIcon &icon, ActionContainer *container)
{
	getState(container).icon = icon;
	foreach (auto action, getActions(container))
		action->setIcon(icon);
}

QString Action::text(ActionContainer *container) const
{
	return getState(container).text;
}

void Action::setText(const QString &text, ActionContainer *container)
{
	auto &state = getState(container);
	if (state.text == text)
		return;
	state.text = text;
	foreach (auto action, getActions(container))
		action->setText(text);
}

bool Action::isCheckable() const
{
	return m_isCheckable;
}

void Action::setCheckable(bool checkable)
{
	if (checkable == m_isCheckable)
		return;
	m_isCheckable = checkable;
	auto itr = m_actions.constBegin(), end = m_actions.constEnd();
	if (checkable) {
		for (; itr != end; ++itr) {
			auto state = getState(itr.key());
			itr.value()->setCheckable(true);
			itr.value()->setChecked(state.isChecked);
		}
	} else {
		for (; itr != end; ++itr)
			itr.value()->setCheckable(false);
	}
}

bool Action::isChecked(ActionContainer *container) const
{
	return getState(container).isChecked;
}

void Action::setChecked(bool checked, ActionContainer *container)
{
	auto &state = getState(container);
	if (state.isChecked == checked)
		return;
	state.isChecked = checked;
	foreach (auto action, getActions(container))
		action->setChecked(checked);
}

bool Action::isVisible(ActionContainer *container) const
{
	return getState(container).isVisible;
}

void Action::setVisible(bool visible, ActionContainer *container)
{
	auto &state = getState(container);
	if (state.isVisible == visible)
		return;
	state.isVisible = visible;
	foreach (auto action, getActions(container))
		action->setVisible(visible);
}

bool Action::isEnabled(ActionContainer *container) const
{
	return getState(container).isEnabled;
}

void Action::setEnabled(bool enabled, ActionContainer *container)
{
	auto &state = getState(container);
	if (state.isEnabled == enabled)
		return;
	state.isEnabled = enabled;
	foreach (auto action, getActions(container))
		action->setEnabled(enabled);
}

void Action::addSubAction(const QMetaObject *meta, Action *action)
{
	if (m_subactions.contains(meta, action))
		return;

	m_subactions.insert(meta, action);
	lconnect(action, SIGNAL(destroyed()), this, [=]() {
		m_subactions.remove(meta, action);
	});
	// TODO: update QMenus
}

bool Action::hasSubActions(ActionContainer *container) const
{
	auto meta = container->toQObject()->metaObject();
	do {
		if (m_subactions.contains(const_cast<QMetaObject*>(meta)))
			return true;
	} while (!!(meta = meta->superClass()));

	return false;
}

void Action::setProxyContainerGetter(const function<ActionContainer*(ActionContainer*)> &getter)
{
	m_settingsContainerGetter = getter;
}

inline Action::ActionState Action::getState(ActionContainer *container) const
{
	container = m_settingsContainerGetter(container);
	if (container) {
		auto itr = m_states.find(container);
		return itr == m_states.end() ? m_defaultState : *itr;
	} else {
		return m_defaultState;
	}
}

inline Action::ActionState &Action::getState(ActionContainer *container)
{
	container = m_settingsContainerGetter(container);
	if (container) {
		auto itr = m_states.find(container);
		if (itr == m_states.end())
			itr = m_states.insert(container, m_defaultState);
		return *itr;
	} else {
		return m_defaultState;
	}
}

inline QList<QAction*> Action::getActions(ActionContainer *container)
{
	return container ? m_actions.values(m_settingsContainerGetter(container)) : m_actions.values();
}

QAction *Action::createAction(ActionContainer *container, QObject *parent)
{
	foreach (auto &filter, m_filters) {
		if (!filter(container))
			return 0;
	}

	auto state = static_cast<const Action*>(this)->getState(container);
	auto action = new QAction(state.icon, state.text, parent);
    m_actions.insert(m_settingsContainerGetter(container), action);
	action->setEnabled(state.isEnabled);
    action->setVisible(state.isVisible);
	action->setProperty("actionPriority", m_priority);
	action->setProperty("container", qVariantFromValue(container));
	if (m_isCheckable) {
		action->setCheckable(true);
		action->setChecked(state.isChecked);
	}
	auto subactions = getContainerActions(container, Action::AllActions, m_subactions);
	if (!subactions.isEmpty()) {
		auto menu = Action::createMenu(container, subactions, action);
		action->setMenu(menu);
	}
	connectToActionSignals(action, container);
	connect(this, SIGNAL(destroyed()), action, SLOT(deleteLater()));
	emit actionCreated(action, container);
	emit actionCreated(action, container->toQObject());
    return action;
}

void Action::connectToActionSignals(QAction *action, ActionContainer *container)
{
	lconnect(action, SIGNAL(triggered(bool)), this, [=](bool checked) {
		if (m_isCheckable) {
			foreach (auto act, getActions(container)) {
				if (act != action)
					act->setChecked(checked);
			}
		}
		auto obj = container->toQObject();
		emit triggered(container, checked);
		emit triggered(obj, checked);
	});
	lconnect(action, SIGNAL(destroyed(QObject*)), this, [&](QObject *act) {
		auto itr = std::find(m_actions.begin(), m_actions.end(), reinterpret_cast<QAction*>(act));
		m_actions.erase(itr);
	});
}

QMenu *Action::createMenu(ActionContainer *container, const QList<Action*> &actions, QObject *parent)
{
	auto menu = new QMenu;
	int  lastGroup = 0;
	bool first = true;
	foreach (auto action, actions) {
		auto qaction = action->createAction(container, menu);
		if (!qaction)
			continue;
		// action group is the priority high-order byte divided by 2
		int group = action->priority() >> 25;
		if (lastGroup != group && !first) {
			auto sep = menu->addSeparator();
			sep->setProperty("actionPriority", (group * 2 + 1) << 24);
		}
		menu->addAction(qaction);
		first = false;
		lastGroup = group;
	}
	QObject::connect(parent, SIGNAL(destroyed()), menu, SLOT(deleteLater()));
	return menu;
}

typedef QList<ActionContainer*> Containers;
Q_GLOBAL_STATIC(Containers, allContainers)

ActionContainer::ActionContainer()
{
	*allContainers() << this;
}

ActionContainer::~ActionContainer()
{
	allContainers()->removeOne(this);
}

QList<Action*> ActionContainer::actions(Action::Types type)
{
	return getContainerActions(this, type, m_staticActions);
}

QList<QAction*> ActionContainer::qActions(Action::Types type, QObject *parent)
{
	QList<QAction*> qactions;
	foreach (auto action, actions(type)) {
		auto qaction = action->createAction(this, parent);
		if (qaction)
			qactions << qaction;
	}
	return qactions;
}

QMenu *ActionContainer::menu(Action::Types type, QObject *parent)
{
	auto menu = Action::createMenu(this, actions(type), parent);
	menu->setProperty("actionsType", (int)type);
	m_menus << menu;
	lconnect(menu, SIGNAL(destroyed()), toQObject(), [=]() {
		m_menus.removeOne(menu);
	});
	return menu;
}

void ActionContainer::addAction(const QMetaObject *meta, Action *action)
{
	m_staticActions.insert(meta, action);

	foreach (auto container, *allContainers()) {
		auto containerMeta = container->toQObject()->metaObject();
		do {
			if (meta == containerMeta) {
				// Add the action to the menus
				foreach (auto menu, container->m_menus) {
					if (!(menu->property("actionsType").toInt() & action->type()))
						continue;
					auto newQAction = action->createAction(container, menu);
					if (!newQAction)
						continue;

					qDebug() << action->text(0);
					QAction *next = 0;
					auto actions = menu->actions();
					int priority = action->priority();
					// action group is the priority high-order byte divided by 2
					int group = priority >> 25;
					int nextPriority = 0;

					int i = 0;
					for (int c = actions.size(); i < c; ++i) {
						nextPriority = actions.at(i)->property("actionPriority").toInt();
						if (priority > nextPriority) {
							next = actions.value(i);
							break;
						}
					}

					if (next)
						menu->insertAction(next, newQAction);
					else
						menu->addAction(newQAction);

					if (nextPriority != 0 && group != (nextPriority >> 25)) {
						auto sep = menu->insertSeparator(next);
						sep->setProperty("actionPriority", (group * 2 - 1) << 24);
					}
					if (auto prev = actions.value(i-1)) {
						int prevPriority = prev->property("actionPriority").toInt();
						if (prevPriority != 0 && group != (prevPriority >> 25)) {
							auto sep = menu->insertSeparator(newQAction);
							sep->setProperty("actionPriority", (group * 2 + 1) << 24);
						}
					}

				}
				emit container->actionAdded(action);
				break;
			}
		} while (!!(containerMeta = containerMeta->superClass()));
	}
}

template <typename T, typename L>
void mergeMap(T &map, const T &other, const L &handler)
{
	auto itr = other.constBegin();
	auto end = other.constEnd();
	for (; itr != end; ++itr) {
		handler(itr.value());
		map.insert(itr.key(), itr.value());
	}
}

Action *ActionContainer::submenuAction(const QMetaObject *meta, const QString &name, const QString &title)
{
	// Check if the submenu is already added. Notice that the submenu may be already registered for
	// a derived class or, even worse, for a several such classes.
	QList<QList<Submenu>::iterator> candidates;
	for (auto itr = m_staticSubmenus.begin(), end = m_staticSubmenus.end(); itr != end; ++itr) {
		auto &submenu = *itr;

		if (name != submenu.name)
			continue;

		if (meta == submenu.meta) {
			submenu.action->setText(title, 0);
			return submenu.action;
		}

		auto metaItr = submenu.meta;
		while (!!(metaItr = metaItr->superClass())) {
			if (metaItr == meta) {
				candidates.append(itr);
				break;
			}
		}
	}

	if (candidates.isEmpty()) {
		// The submenu is not found, so create it.
		auto menu = new Action(title, qApp);
		addAction(meta, menu);
		menu->addFilter([=](ActionContainer *container) {
			return menu->hasSubActions(container);
		});

		Submenu submenu = { meta, name, menu };
		m_staticSubmenus << submenu;
		return menu;
	} else if (candidates.count() == 1) {
		// The submenu is added for a derived class. Register the action
		// for the meta (and remove from an old one).
		auto submenu = candidates.first();
		m_staticActions.remove(submenu->meta, submenu->action);
		m_staticActions.insert(meta, submenu->action);
		submenu->meta = meta;
		return submenu->action;
	} else {
		// Several derived classes have submenus with the name. We have to merge the submenus.
		auto submenu = candidates.first();
		auto action = submenu->action;
		// NOTE: first element skipped intentionally
		for (auto itr = --candidates.constEnd(), begin = candidates.constBegin(); itr != begin; --itr) {
			auto canditate = (*itr)->action;
			mergeMap(action->m_states, canditate->m_states, [](const Action::ActionState &state) {});
			mergeMap(action->m_actions, canditate->m_actions, [=](QAction *act) {
				auto container = act->property("container").value<ActionContainer*>();
				Q_ASSERT(container);
				action->connectToActionSignals(act, container);
			});
			mergeMap(action->m_subactions, canditate->m_subactions, [=](Action *act) {
				lconnect(act, SIGNAL(destroyed()), action, [=] {
					action->m_subactions.remove(meta, action);
				});
			});

			delete canditate;
			m_staticSubmenus.erase(*itr);
		}

		m_staticActions.remove(submenu->meta, submenu->action);
		m_staticActions.insert(meta, submenu->action);
		submenu->meta = meta;
		return action;
	}

	return 0;
}

} // Lime
