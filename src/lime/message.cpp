/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "message.h"
#include "chatunit.h"
#include "account.h"
#include "conference.h"

namespace Lime
{

class MessagePrivate : public QSharedData
{
public:
	Message::Direction direction;
	Message::Type type;
	int id;
	ChatUnit *unit;
	QString text;
	QDateTime time;
	QString subject;
	mutable QString senderName;
};

Message::Message() :
	d(new MessagePrivate)
{
	d->unit = 0;
	d->type = TextMessage;
	d->direction = Message::Outgoing;
}

Message::Message(const QString &text, Direction direction, const QDateTime &time) :
	d(new MessagePrivate)
{
	d->direction = direction;
	d->type = TextMessage;
	d->text = text;
	d->time = time;
	d->unit = 0;
}

Message::Message(const Message &other) :
	d(other.d)
{
}

Message::~Message()
{
}

Message &Message::operator=(const Message &other)
{
	d = other.d;
	return *this;
}

int Message::id() const
{
	return d->id;
}

QString Message::text() const
{
	return d->text;
}
QString Message::subject() const
{
	return d->subject;
}

QDateTime Message::time() const
{
	return d->time;
}

Message::Direction Message::direction() const
{
	return d->direction;
}

ChatUnit *Message::unit() const
{
	return d->unit;

}

Message::Type Message::type() const
{
	return d->type;
}

QString Message::senderName() const
{
	if (d->senderName.isEmpty() && d->unit) {
		if (d->direction == Outgoing) {
			auto conf = qobject_cast<const Conference*>(unit());
			if (conf && conf->user())
				d->senderName = conf->user()->title();
			else
				d->senderName = d->unit->account()->name();
		} else {
			d->senderName = d->unit->title();
		}
	}
	return d->senderName;
}

void Message::setText(const QString &text)
{
	d->text = text;
}

void Message::setSubject(const QString &subject)
{
	d->subject = subject;
}

void Message::setTime(const QDateTime &time)
{
	d->time = time;
}

void Message::setDirection(Direction direction)
{
	d->direction = direction;
}

void Message::setUnit(ChatUnit *unit)
{
	d->unit = unit;
}

void Message::setType(Type type)
{
	d->type = type;
}

void Message::setSenderName(const QString &name)
{
	d->senderName = name;
}

} // Lime

