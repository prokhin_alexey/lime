/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_ABSTRACTCHATLOG_H
#define LIME_ABSTRACTCHATLOG_H

#include <lime/chatunit.h>

namespace Lime {

class ChatWidget;

struct AbstractChatLogState
{
};

class AbstractChatLog
{
public:
	virtual void appendMessage(const Message &msg, AbstractChatLogState *state = 0) = 0;
	virtual QWidget *widget() = 0;
	virtual AbstractChatLogState *createState(ChatUnit *unit) = 0;
	virtual void setState(AbstractChatLogState *state, ChatWidget *widget) = 0;
	virtual void clearChat() = 0;
	virtual QString quote() = 0;
};

} // Lime

#endif // LIME_ABSTRACTCHATLOG_H
