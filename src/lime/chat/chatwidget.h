/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_CHATWIDGET_H
#define LIME_CHATWIDGET_H

#include <QWidget>
#include <QTextCursor>
#include <QTextDocument>
#include <QTextEdit>
#include <QTimer>
#include "abstractchatlog.h"
#include "conferencemodel.h"
#include "chatmanager.h"

class QSplitter;
class QListView;
class QToolButton;

namespace Lime {

class ChatUnit;
class MetaContact;
class ActionToolBar;
class ExtendedIconsWidget;

struct ChatWidgetState
{
	ChatWidgetState(AbstractChatLogState *logState, ChatUnit *unit);
	~ChatWidgetState();
	void setChatState(ChatUnit::ChatState state);
	void updateIcon();
	ChatUnit *getResource() { return resource ? resource.data() : unit; }
	AbstractChatLogState *logState;
	union {
		ChatUnit *unit;
		Buddy *buddy;
		Conference *conference;
		MetaContact *metaContact;
	};
	QTextDocument *enteredText;
	QTextCursor textCursor;
	ChatUnit::ChatState chatState;
	QTimer activeTimer;
	QIcon icon;
	Pointer<ChatUnit> resource;
	bool isBuddy : 1;
	bool isConference : 1;
	bool isMetaContact : 1;
	Signal<void(const QIcon &icon)> onIconUpdated;
};

class ChatWidget : public QWidget
{
	Q_OBJECT
public:
	ChatWidget(AbstractChatLog *chatLog, QWidget *parent = 0);
	virtual ~ChatWidget();
	void appendMessage(const Message &message, ChatWidgetState *state = 0);
	QTextEdit *textEdit() const { return m_textEdit; }
	ChatUnit *unit() const { return m_currentState->unit; }
	QTextDocument *document() { return m_textEdit->document(); }
	ChatWidgetState *createState(ChatUnit *unit);
	void setState(ChatWidgetState *state);
	void saveState(ChatWidgetState *state);
	void setSendMessageKey(SendMessageKey key, bool force = false);
	SendMessageKey sendMessageKey() { return m_sendKey; }
	void enableSendingChatStates(bool enable, bool force = false);
	void clearChat();
	QString quote();
signals:
	void messageSent();
protected:
	virtual bool eventFilter(QObject *obj, QEvent *ev);
private slots:
	void send();
private:
	QString getPlainText();
private:
	QSplitter *m_splitter;
	QTextEdit *m_textEdit;
	QToolButton *m_sendButton;
	QAction *m_sendAction;
	AbstractChatLog *m_chatLog;
	ActionToolBar *m_mainToolBar;
	ActionToolBar *m_additionalToolBar;
	ExtendedIconsWidget *m_contactExtIcons;
	bool m_waitingSecondEnter;
	SendMessageKey m_sendKey;
	bool m_sendKeyForced;
	QTextCursor m_enterPosition;
	QListView *m_confView;
	QSplitter *m_confSplitter;
	ChatWidgetState *m_currentState;
	QMenu *m_resourcesMenu;
	bool m_sendChatStates;
	bool m_sendChatStatesForced;
};

} // Lime

#endif // LIME_CHATWIDGET_H
