/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "tabbedwidget.h"
#include "chatmanager.h"
#include <lime/utils.h>
#include <lime/icon.h>
#include <algorithm>
#include <QBoxLayout>
#include <QStackedLayout>
#include <QDebug>

namespace Lime
{

TabbedWidget::TabbedWidget(AbstractChatLog *chatLog, QWidget *parent) :
	QWidget(parent),
	m_chatWidget(new ChatWidget(chatLog, this)),
	m_currentState(0)
{
	auto layout = new QVBoxLayout(this);
	layout->setSpacing(0);
	layout->setMargin(1);
	m_tabBar = new TabBar(this);
	m_tabBar->setMovable(true);
	layout->addWidget(m_tabBar);
	layout->addWidget(m_chatWidget);

	m_tabBar->onTabRemoved += [=](int index) {
		auto unit = m_units.takeAt(index);
		delete m_states.take(unit);
		emit chatUnitRemoved(unit);
	};
	lconnect(m_tabBar, SIGNAL(tabMoved(int,int)), this, [=](int from, int to) {
		m_units.move(from, to);
	});
	lconnect(m_tabBar, SIGNAL(currentChanged(int)), this, [=](int index) {
		if (m_currentState)
			m_chatWidget->saveState(m_currentState);
		auto unit = m_units.value(index);
		m_currentState = unit ? m_states.value(unit) : 0;
		m_chatWidget->setState(m_currentState);
		setWindowIcon(m_currentState ? m_currentState->icon : Icon("lime"));
		emit currentChatUnitChanged(unit);
	});

}

TabbedWidget::~TabbedWidget()
{
	foreach (auto state, m_states) {
		emit chatUnitRemoved(state->unit);
		delete state;
	}
}

void TabbedWidget::addChatUnit(ChatUnit *unit)
{
	if (m_units.contains(unit))
		return;

	auto chatState = m_chatWidget->createState(unit);
	m_units << unit;
	m_states.insert(unit, chatState);
	m_tabBar->addTab(chatState->icon, unit->name());
	emit chatUnitAdded(unit);
	Q_ASSERT(m_tabBar->count() == m_units.count());

	lconnect(unit, SIGNAL(nameChanged(QString,QString)), m_tabBar, [=](const QString &name) {
		int index = m_units.indexOf(unit);
		Q_ASSERT(index != -1);
		m_tabBar->setTabText(index, name);
	});
	lconnect(unit, SIGNAL(destroyed()), this, [=] {
		int index = m_units.indexOf(unit);
		Q_ASSERT(index != -1);
		m_tabBar->removeTab(index);
	});
	m_currentState->onIconUpdated += [=](const QIcon &icon) {
		int index = m_units.indexOf(unit);
		Q_ASSERT(index != -1);
		m_tabBar->setTabIcon(index, icon);
		if (index == m_tabBar->currentIndex())
			setWindowIcon(icon);
	};
}

void TabbedWidget::activate(ChatUnit *unit)
{
	int index = m_units.indexOf(unit);
	if (index == -1)
		return;
	m_tabBar->setCurrentIndex(index);
}

bool TabbedWidget::appendMessage(const Message &message)
{
	auto unit = message.unit();
	Q_ASSERT(unit);
	unit = unit->getHistoryUnit();
	auto state = m_states.value(unit);
	if (state)
		m_chatWidget->appendMessage(message, state);
	return state;
}

ChatUnit *TabbedWidget::currentUnit()
{
	return m_units.value(m_tabBar->currentIndex());
}

QTextDocument *TabbedWidget::document(ChatUnit *unit)
{
	auto state = m_states.value(unit);
	return state ? state->enteredText : 0;
}

void TabbedWidget::closeCurrent()
{
	m_tabBar->removeTab(m_tabBar->currentIndex());
}

} // Lime
