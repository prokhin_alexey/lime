/*
	 This file is part of Lime.
	 Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "chatsettings.h"
#include "ui_chatsettings.h"

namespace Lime
{

ChatSettings::ChatSettings() :
	ui(new Ui::ChatSettings)
{
	auto central = new QWidget(this);
	setWidget(central);
	ui->setupUi(central);

	bind("tabbedMode", ui->tabbedModBox, true);
	bind("sendOnEnter", ui->sendOnEnterBox, true);
	bind("sendOnDoubleEnter", ui->sendOnDoubleEnterBox, false);
	bind("openChatOnNewMessage", ui->openChatOnNewMessageBox, false);
	bind("closeChatOnOutgoingMessage", ui->closeChatOnOutgoingMessageBox, false);
	bind("sendChatStates", ui->sendTypingNotifsBox, true);
}

ChatSettings::~ChatSettings()
{
	delete ui;
}

void ChatSettings::loadImpl()
{
	loadBinds(Config("", "chat"));
}

void ChatSettings::saveImpl()
{
	Config cfg("", "chat");
	saveBinds(cfg);
}

} // Lime
