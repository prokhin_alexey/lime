/*
    This file is part of Lime.
    Copyright (c) 2011  Nigmatullin Ruslan <euroelessar@gmail.com>
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "chatmanager.h"
#include <QRegExp>
#include <QSplitter>
#include <QWidgetAction>
#include "chatwidget.h"
#include "chatwidget.h"
#include "tabbedwidget.h"
#include "settings/chatsettings.h"
#include "textlog/textchatlog.h"
#include <lime/config.h>
#include <lime/icon.h>
#include <lime/history/history.h>
#include <lime/emoticons/emoticonswidget.h>
#include <QMenu>

namespace Lime {


ChatManagerImpl::ChatManagerImpl() :
	m_tabMode(false)
{
}

ChatManagerImpl::~ChatManagerImpl()
{

}

void ChatManagerImpl::activateChat(ChatUnit *unit)
{
	unit = unit->getHistoryUnit();
	QWidget *widget = 0;
	if (m_tabMode)
		widget = tabbedWidget();
	else
		widget = chatWidget(unit);
	widget->setVisible(true);
	widget->raise();
	if (m_tabMode) {
		m_tabbedWidget->addChatUnit(unit);
		m_tabbedWidget->activate(unit);
	}
	setActive(unit, true);
}

void ChatManagerImpl::appendMessage(const Message &message)
{
	auto unit = message.unit();
	Q_ASSERT(unit);
	unit = unit->getHistoryUnit();

	auto type = message.type();
	if (!(type & Message::DontStoreMessage) &&
		(m_storeServiceMessages || !(type & Message::ServiceMessage)))
	{
		History::instance()->store(message);
	}

	if (m_tabMode && (m_tabbedWidget || m_openChatOnNewMessage)) {
		if (m_openChatOnNewMessage) {
			tabbedWidget(); // ensure that widget is created
			m_tabbedWidget->setVisible(true);
			m_tabbedWidget->addChatUnit(unit);
		}
		Q_ASSERT(m_tabbedWidget);
		if (m_tabbedWidget->appendMessage(message))
			return;
	} else if (!m_tabMode) {
		if (auto widget = chatWidget(unit, m_openChatOnNewMessage)) {
			widget->appendMessage(message);
			widget->setVisible(true);
			return;
		}
	}

	// We have tried to append the message to an opened chat, but we have failed
	Q_ASSERT(!m_openChatOnNewMessage); // sanity check
	m_unopened[unit] << message;
}

QTextDocument *ChatManagerImpl::document(ChatUnit *unit)
{
	unit = unit->getHistoryUnit();
	if (m_tabMode && m_tabbedWidget) {
		return m_tabbedWidget->document(unit);
	} else if (!m_tabMode) {
		auto widget = chatWidget(unit, false);
		return widget->document();
	}
	return 0;
}

AbstractChatLog *ChatManagerImpl::createChatLog()
{
	return new TextChatLog;
}

void ChatManagerImpl::sendState(ChatUnit *unit, ChatUnit::ChatState state)
{
	auto this_ = sender_cast<ChatManagerImpl*>(instance()->toQObject());
	this_->sendChatState(unit, state);
}

MessageList ChatManagerImpl::takeUnopenedMessages(ChatUnit *unit)
{
	auto this_ = sender_cast<ChatManagerImpl*>(instance()->toQObject());
	return this_->m_unopened.take(unit);
}

ChatWidget *ChatManagerImpl::chatWidget(ChatUnit *unit, bool create)
{
	Q_ASSERT(!m_tabMode);
	auto widget = m_widgets.value(unit);
	if (widget || !create)
		return widget;

	auto chatLog = createChatLog();
	widget = new ChatWidget(chatLog);
	auto state = widget->createState(unit);
	widget->setState(state);
	widget->setWindowIcon(state->icon);
	updateChatWidgetSettings(widget);
	widget->setAttribute(Qt::WA_DeleteOnClose, true);
	widget->installEventFilter(this);
	m_widgets.insert(unit, widget);
	handleChatOpening(unit);
	restoreWidgetState(widget);

	lconnect(widget, SIGNAL(destroyed()), this, [=] {
		m_widgets.remove(unit);
		delete state;
		setActive(unit, false);
		handleChatClosing(unit);
	});
	lconnect(widget, SIGNAL(messageSent()), widget, [=] {
		if (m_closeChatOnOutgoingMessage)
			widget->deleteLater();
	});
	state->onIconUpdated += [=](const QIcon &icon) {
		widget->setWindowIcon(icon);
	};
	connect(unit, SIGNAL(destroyed()), widget, SLOT(deleteLater()));

	return widget;
}

ChatWidget *ChatManagerImpl::activeChatWidget()
{
	return m_tabMode ? tabbedWidget()->chatWidget() : chatWidget(activeUnit(), false);
}

TabbedWidget *ChatManagerImpl::tabbedWidget()
{
	if (m_tabbedWidget)
		return m_tabbedWidget;

	m_tabbedWidget = new TabbedWidget(createChatLog());
	m_tabbedWidget->setAttribute(Qt::WA_DeleteOnClose, true);
	m_tabbedWidget->installEventFilter(this);
	updateChatWidgetSettings(m_tabbedWidget->chatWidget());
	restoreWidgetState(m_tabbedWidget);

	lconnect(m_tabbedWidget, SIGNAL(chatUnitAdded(Lime::ChatUnit*)), this, [=](ChatUnit *unit) {
		handleChatOpening(unit);
	});
	lconnect(m_tabbedWidget, SIGNAL(chatUnitRemoved(Lime::ChatUnit*)), this, [=](ChatUnit *unit) {
		if (m_tabbedWidget->unitCount() == 0)
			m_tabbedWidget->deleteLater();
		setActive(unit, false);
		handleChatClosing(unit);
	});
	lconnect(m_tabbedWidget, SIGNAL(currentChatUnitChanged(Lime::ChatUnit*)), this, [=](ChatUnit *unit) {
		if (m_tabbedWidget->isActiveWindow())
			setActive(unit, true);
	});
	lconnect(m_tabbedWidget->chatWidget(), SIGNAL(messageSent()), m_tabbedWidget, [=] {
		if (m_closeChatOnOutgoingMessage)
			m_tabbedWidget->closeCurrent();
	});
	return m_tabbedWidget;
}

void ChatManagerImpl::updateChatWidgetSettings(ChatWidget *widget)
{
	widget->setSendMessageKey(m_sendKey);
	widget->enableSendingChatStates(m_sendChatStates);
}

UrlTokenList ChatManagerImpl::parseUrls(const QString &text) {
	UrlTokenList result;
	static QRegExp linkRegExp("([a-zA-Z0-9\\-\\_\\.]+@([a-zA-Z0-9\\-\\_]+\\.)+[a-zA-Z]+)|"
	                          "([a-z]+(\\+[a-z]+)?://|www\\.)"
	                          "[\\w-]+(\\.[\\w-]+)*\\.\\w+"
	                          "(:\\d+)?"
	                          "(/[\\w\\+\\.\\[\\]!%\\$/\\(\\),:;@'&=~-]*"
	                          "(\\?[\\w\\+\\.\\[\\]!%\\$/\\(\\),:;@\\'&=~-]*)?"
	                          "(#[\\w\\+\\.\\[\\]!%\\$/\\\\\\(\\)\\|,:;@&=~-]*)?)?",
	                          Qt::CaseInsensitive);
	Q_ASSERT(linkRegExp.isValid());
	int pos = 0;
	int lastPos = 0;
	while (((pos = linkRegExp.indexIn(text, pos)) != -1)) {
		UrlToken tok = { text.midRef(lastPos, pos - lastPos), QString() };
		if (!tok.text.isEmpty()) {
			if (!result.isEmpty() && result.last().url.isEmpty()) {
				QStringRef tmp = result.last().text;
				result.last().text = QStringRef(tmp.string(), tmp.position(), tmp.size() + tok.text.size());
			} else {
				result << tok;
			}
		}
		QString link = linkRegExp.cap(0);
		tok.text = text.midRef(pos, link.size());
		pos += link.size();
		if (link.startsWith(QLatin1String("www."), Qt::CaseInsensitive))
			link.prepend(QLatin1String("http://"));
		else
			if (!link.contains(QLatin1String("//")))
				link.prepend(QLatin1String("mailto:"));
		tok.url = link;
		result << tok;
		lastPos = pos;
	}
	if (!result.isEmpty() && result.last().url.isEmpty()) {
		result.last().text = text.midRef(result.last().text.position());
	} else {
		UrlToken tok = { text.midRef(lastPos), QString() };
		result << tok;
	}
	return result;
}

void ChatManagerImpl::saveWidgetState(QWidget *widget)
{
	Config cfg(QString(), "chat");
	cfg.setValue("geometry", widget->saveGeometry());
}

void ChatManagerImpl::restoreWidgetState(QWidget *widget)
{
	Config cfg;
	if (cfg.hasChildGroup("chat")) {
		cfg.beginGroup("chat");
		widget->restoreGeometry(cfg.value("geometry").toByteArray());
		cfg.endGroup();
	} else {
		widget->resize(500, 400);
		centerizeWidget(widget);
	}
}

bool ChatManagerImpl::enableImpl()
{
	// Create emoticon chat action
	auto action = new Action(Icon("face-smile"), tr("Insert emoticon"), this);
	action->setType(Action::MainToolbarAction);
	ActionContainer::addAction<ChatUnit>(action);
	lconnect(action, SIGNAL(actionCreated(QAction*,QObject*)), this,
		[=](QAction *action, QObject *unit)
	{
		auto menu = new QMenu;
		auto widgetAction = new QWidgetAction(menu);
		auto emoWidget = new EmoticonsWidget;
		widgetAction->setDefaultWidget(emoWidget);
		menu->addAction(widgetAction);
		action->setMenu(menu);
		connect(widgetAction, SIGNAL(destroyed()), emoWidget, SLOT(deleteLater()));
		connect(action, SIGNAL(destroyed()), menu, SLOT(deleteLater()));
		connect(menu, SIGNAL(aboutToShow()), emoWidget, SLOT(ensureGeometry()));
		lconnect(emoWidget, SIGNAL(emoticonClicked(QString,QString)), unit,
			[=](const QString &icon, const QString &token)
		{
			Q_ASSERT(activeUnit() == unit);
			auto chatWidget = activeChatWidget();
			auto edit = chatWidget->textEdit();
			edit->insertHtml(QString("<img src=\"%1\" alt=\"%2\" title=\"%2\" />").arg(icon).arg(token));
			edit->setFocus();
			menu->close();
		});
	});

	// Create 'send on enter' chat action
	action = new Action(Icon("enter"), tr("Send message on enter"), this);
	action->setType(Action::AdditionalToolbarAction);
	action->setCheckable(true);
	ActionContainer::addAction<ChatUnit>(action);
	lconnect(action, SIGNAL(actionCreated(QAction*,QObject*)), this,
		[=] (QAction *action)
	{
		action->setChecked(m_sendKey & SendEnter);
	});
	lconnect(action, SIGNAL(triggered(QObject*,bool)), this,
		[=](ChatUnit *unit, bool checked)
	{
		Q_ASSERT(activeUnit() == unit);
		auto chatWidget = activeChatWidget();
		int sendKey = chatWidget->sendMessageKey();
		sendKey = checked ? (sendKey | SendEnter) : (sendKey ^ SendEnter);
		chatWidget->setSendMessageKey(SendMessageKey(sendKey));
	});

	// Create 'send typing notifications' action
	action = new Action(Icon("im-status-message-edit"), tr("Send typing notifications"), this);
	action->setType(Action::AdditionalToolbarAction);
	action->setCheckable(true);
	ActionContainer::addAction<ChatUnit>(action);
	action->addFilter([](ActionContainer *container) {
		auto unit = sender_cast<ChatUnit*>(container->toQObject());
		return unit->flags() & ChatUnit::SupportsChatStates;
	});
	lconnect(action, SIGNAL(actionCreated(QAction*,QObject*)), this,
		[=] (QAction *action)
	{
		action->setChecked(m_sendChatStates);
	});
	lconnect(action, SIGNAL(triggered(QObject*,bool)), this,
		[=](ChatUnit *unit, bool checked)
	{
		Q_ASSERT(activeUnit() == unit);
		auto chatWidget = activeChatWidget();
		chatWidget->enableSendingChatStates(checked, true);
	});

	// Create 'quote' action
	action = new Action(Icon("insert-text-quote"), tr("Quote"), this);
	action->setType(Action::AdditionalToolbarAction);
	ActionContainer::addAction<ChatUnit>(action);
	lconnect(action, SIGNAL(triggered(QObject*,bool)), this,
		[=](ChatUnit *unit, bool checked)
	{
		Q_ASSERT(activeUnit() == unit);
		auto chatWidget = activeChatWidget();
		auto quote = chatWidget->quote();
		if (quote.isEmpty())
			return;

		auto edit = chatWidget->textEdit();
		const QString newLine = QLatin1String("\n> ");
		QString text;
		if (edit->textCursor().atStart())
			text = QLatin1String("> ");
		else
			text = newLine;
		text.reserve(text.size() + quote.size() * 1.2);
		for (int i = 0; i < quote.size(); ++i) {
			if (quote[i] == QLatin1Char('\n') || quote[i].unicode() == QChar::ParagraphSeparator)
				text += newLine;
			else
				text += quote[i];
		}
		text += QLatin1Char('\n');
		edit->insertPlainText(text);
	});

	// Create 'clear chat' action
	action = new Action(Icon("edit-clear"), tr("Clear chat"), this);
	action->setType(Action::AdditionalToolbarAction);
	ActionContainer::addAction<ChatUnit>(action);
	lconnect(action, SIGNAL(triggered(QObject*,bool)), this,
		[=](ChatUnit *unit, bool checked)
	{
		Q_ASSERT(activeUnit() == unit);
		auto chatWidget = activeChatWidget();
		chatWidget->clearChat();
	});

	// Register chat settings
	SettingsManager::addGlobalSettings(Icon("view-choose-chat"), tr("Chat window"), [=] {
		auto settings = new ChatSettings;
		connect(settings, SIGNAL(saved()), this, SLOT(reloadSettings()));
		return settings;
	});

	// load settings
	reloadSettings();

	return true;
}

bool ChatManagerImpl::eventFilter(QObject *obj, QEvent *event)
{
	auto type = event->type();
	if (type == QEvent::WindowActivate ||
		type == QEvent::WindowDeactivate)
	{
		bool active = event->type() == QEvent::WindowActivate;
		if (m_tabMode && m_tabbedWidget == obj) {
			setActive(m_tabbedWidget->currentUnit(), active);
		} else if (auto widget = qobject_cast<ChatWidget*>(obj)) {
			setActive(widget->unit(), active);
		}
	} else if (type == QEvent::Close) {
		if (m_tabMode && m_tabbedWidget == obj) {
			saveWidgetState(m_tabbedWidget);
		} else if (auto widget = qobject_cast<ChatWidget*>(obj)) {
			if (m_widgets.size() == 1)
				saveWidgetState(widget);
		}
	}
	return QObject::eventFilter(obj, event);
}

void ChatManagerImpl::reloadSettings()
{
	int oldTabMod = m_tabMode;
	Config cfg("", "chat");
	m_tabMode = cfg.value("tabbedMode", true);
	m_storeServiceMessages = cfg.value("storeServiceMessages", true);
	m_openChatOnNewMessage = cfg.value("openChatOnNewMessage", true);
	m_closeChatOnOutgoingMessage = cfg.value("closeChatOnOutgoingMessage", true);
	m_sendChatStates = cfg.value("sendChatStates", true);

	// update send key
	int sendKey = 0;
	sendKey |= cfg.value("sendOnEnter", true);
	sendKey |= cfg.value("sendOnDoubleEnter", true) << 1;
	m_sendKey = SendMessageKey(sendKey);

	if (m_tabMode && !oldTabMod && !m_widgets.isEmpty()) {
		// temporary deactivate an active unit
		auto activeUnit = this->activeUnit();
		if (activeUnit)
			setActive(activeUnit, false);
		// convert opened chat windows into tabs.
		auto tabbedWidget = this->tabbedWidget();
		foreach (auto widget, m_widgets) {
			// temporary close the chat
			handleChatClosing(widget->unit());
			// add a new tab
			tabbedWidget->addChatUnit(widget->unit());
			// destroy the opened chat
			ldisconnect(widget, 0, this);
			widget->deleteLater();
		}
		// clear up cache of chat windows
		m_widgets.clear();
		// finally, open tabbed window
		tabbedWidget->activate(activeUnit);
		tabbedWidget->setVisible(true);
		// reactivate previously an active unit
		if (activeUnit)
			activateChat(activeUnit);
	} else if (!m_tabMode && oldTabMod && m_tabbedWidget) {
		// temporary deactivate an active unit
		auto activeUnit = this->activeUnit();
		if (activeUnit)
			setActive(activeUnit, false);
		// convert tabs into chat windows
		foreach (auto unit, m_tabbedWidget->units()) {
			// temporary close the chat
			handleChatClosing(unit);
			// create a new chat window
			auto w = chatWidget(unit);
			w->setVisible(true);
		}
		// destroy the tabbed widget
		ldisconnect(m_tabbedWidget, 0, this);
		m_tabbedWidget->deleteLater();
		// reactivate previously an active unit
		if (activeUnit)
			activateChat(activeUnit);
	}


	// update chat widgets settings
	if (!m_tabMode) {
		foreach (auto w, m_widgets)
			updateChatWidgetSettings(w);
	} else if (m_tabbedWidget) {
		updateChatWidgetSettings(m_tabbedWidget->chatWidget());
	}
}

}

