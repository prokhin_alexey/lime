/*
	 This file is part of Lime.
	 Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "extendediconswidget.h"
#include <QBoxLayout>
#include <QLabel>

namespace Lime
{

ExtendedIconsWidget::ExtendedIconsWidget(QWidget *parent) :
	QWidget(parent), m_buddy(0)
{
	m_layout = new QHBoxLayout(this);
	m_layout->setMargin(0);
}

ExtendedIconsWidget::~ExtendedIconsWidget()
{
}

void ExtendedIconsWidget::setBuddy(Buddy *buddy)
{
	auto updateLabel = [](QLabel *label, const QVariantMap &info) {
		Q_ASSERT(label);
		label->setPixmap(info.value("icon").value<QIcon>().pixmap(QSize(16, 16)));

		auto title = info.value("title").toString();
		auto description = info.value("description").toString();
		if (!title.isEmpty() && !description.isEmpty())
			title += QString(": ") + description;
		else if (title.isEmpty())
			title = description;
		label->setToolTip(title);
	};

	auto addLabel = [=](const QString &name, const QVariantMap &info) {
		static const char *priorityProperty = "chat.priority";
		auto label = new QLabel(this);
		int currentPriority = info.value(priorityProperty).toInt();
		label->setProperty(priorityProperty, currentPriority);

		int i = 0;
		for (int n = m_iconWidgets.count(); i < n; ++i) {
			auto itr = m_layout->itemAt(i)->widget();
			Q_ASSERT(itr);
			int itrPriority = itr->property(priorityProperty).toInt();
			if (currentPriority < itrPriority)
				break;
		}

		m_iconWidgets.insert(name, label);
		m_layout->insertWidget(i, label);
		return label;
	};

	// start off with removing old icons
	if (m_buddy) {
		ldisconnect(m_buddy, 0, this);
		foreach (auto iconWidget, m_iconWidgets) {
			m_layout->removeWidget(iconWidget);
			iconWidget->deleteLater();
		}
		m_iconWidgets.clear();
	}

	m_buddy = buddy;
	if (!buddy)
		return;

	// ... and now we may add new icons
	auto extInfo = buddy->allExtendedInfo();
	for (auto info = extInfo.constBegin(), end = extInfo.constEnd(); info != end; ++info) {
		if (!info->value("chat.showIcon", false).toBool())
			continue;
		auto label = addLabel(info.key(), *info);
		updateLabel(label, *info);
	}

	// .. and we'll need to update the icons whenever the contact's extended info change.
	lconnect(buddy, SIGNAL(extendedInfoChanged(QString,QVariantMap)), this,
		[=](const QString &name, const QVariantMap &info)
	{
		bool showIcon = !info.isEmpty() && info.value("chat.showIcon", false).toBool();
		auto labelItr = m_iconWidgets.find(name);
		bool isAlreadyShown = labelItr != m_iconWidgets.end();

		if (!showIcon && !isAlreadyShown) {
			return;
		} else if (!showIcon && isAlreadyShown) {
			delete *labelItr;
			m_iconWidgets.erase(labelItr);
			return;
		}

		Q_ASSERT(showIcon);
		auto label = isAlreadyShown ? *labelItr : addLabel(name, info);
		updateLabel(label, info);
	});
}

} // Lime
