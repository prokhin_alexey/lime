/*
    This file is part of Lime.
    Copyright (c) 2011  Nigmatullin Ruslan <euroelessar@gmail.com>
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_CHATMANAGER_H
#define LIME_CHATMANAGER_H

#include <lime/chatunit.h>
#include <lime/extention.h>
#include <lime/utils.h>
#include <lime/chatmanager.h>

class QTextDocument;

namespace Lime {

class ChatWidget;
class TabbedWidget;
class AbstractChatLog;

struct UrlToken
{
	QStringRef text;
	QString url;
};
typedef QList<UrlToken> UrlTokenList;

enum SendMessageKey
{
	SendCtrlEnter   = 0x00,
	SendEnter       = 0x01,
	SendDoubleEnter = SendEnter | 0x02
};

class ChatManagerImpl : public QObject, public ChatManager
{
	Q_OBJECT
public:
	ChatManagerImpl();
	virtual ~ChatManagerImpl();
	void activateChat(ChatUnit *unit);
	void appendMessage(const Message &message);
	QTextDocument *document(ChatUnit *unit);
	virtual QObject *toQObject() { return this; }
	AbstractChatLog *createChatLog();
	static void sendState(ChatUnit *unit, ChatUnit::ChatState state);
	static MessageList takeUnopenedMessages(ChatUnit *unit);
	// I don't like it there
	static UrlTokenList parseUrls(const QString &text);
	static void saveWidgetState(QWidget *widget);
	static void restoreWidgetState(QWidget *widget);
protected:
	virtual bool enableImpl();
	virtual bool disableImpl() { return false; }
	ChatWidget *chatWidget(ChatUnit *unit, bool create = true);
	ChatWidget *activeChatWidget();
	TabbedWidget *tabbedWidget();
	void updateChatWidgetSettings(ChatWidget *widget);
	bool eventFilter(QObject *obj, QEvent *event);
private slots:
	void reloadSettings();
private:
	QMap<ChatUnit*, ChatWidget*> m_widgets;
	QMap<ChatUnit*, MessageList> m_unopened;
	Pointer<TabbedWidget> m_tabbedWidget;
	bool m_tabMode;
	bool m_storeServiceMessages;
	bool m_openChatOnNewMessage;
	bool m_closeChatOnOutgoingMessage;
	bool m_sendChatStates;
	SendMessageKey m_sendKey;
};

} // Lime

#endif // LIME_CHATMANAGER_H
