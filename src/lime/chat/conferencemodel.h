/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_CONFERENCEMODEL_H
#define LIME_CONFERENCEMODEL_H

#include <lime/global.h>
#include <QAbstractListModel>
#include <lime/conference.h>

namespace Lime {

typedef QList<ConferenceParticipant*> Participants;

class ConferenceModel : public QAbstractListModel
{
public:
	explicit ConferenceModel(QObject* parent = 0) : QAbstractListModel(0), m_conference(0) {}
	virtual ~ConferenceModel() {}
	virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
	void setConference(Conference *conference);
private:
	Participants m_participants;
	Conference *m_conference;
};

}

#endif // LIME_CONFERENCEMODEL_H
