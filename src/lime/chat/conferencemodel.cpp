/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "conferencemodel.h"
#include <lime/contactlist/model/abstractitem.h>

namespace Lime {

QVariant ConferenceModel::data(const QModelIndex &index, int role) const
{
	auto contact = m_participants.value(index.row());
	if (!contact)
		return QVariant();

	if (role == Qt::DisplayRole)
		return contact->name();
	else if (role == Qt::DecorationRole)
		return contact->statusIcon();
	else if (role == UnitRole)
		return QVariant::fromValue<ChatUnit*>(contact);
	else if (role == ItemTypeRole)
		return (int)ContactItemType;
	return QVariant();
}

int ConferenceModel::rowCount(const QModelIndex &parent) const
{
	return !parent.isValid() ? m_participants.size() : 0;
}

static bool participantLessThan(ConferenceParticipant *lhs, ConferenceParticipant *rhs)
{
	int roleDiff = lhs->role() - rhs->role();
	if (roleDiff == 0)
		return QString::localeAwareCompare(lhs->name(), rhs->name()) < 0;
	else
		return roleDiff < 0;
}

void ConferenceModel::setConference(Conference *conference)
{
	if (m_conference == conference)
		return;

	beginResetModel();
	if (m_conference)
		ldisconnect(conference, 0, this);
	m_conference = conference;
	m_participants = conference->participants();
	qSort(m_participants.begin(), m_participants.end(), &participantLessThan);

	lconnect(conference, SIGNAL(participantJoined(Lime::ConferenceParticipant*)), this,
		[this](ConferenceParticipant *participant)
	{
		auto findPos = [](const Participants &participants, ConferenceParticipant *participant)
		{
			auto begin = participants.begin();
			auto itr = qLowerBound(begin, participants.end(), participant, &participantLessThan);
			return itr - begin;
		};

		int pos = findPos(m_participants, participant);
		this->beginInsertRows(QModelIndex(), pos, pos);
		m_participants.insert(pos, participant);
		this->endInsertRows();

		auto checkPos = [=] {
			int oldPos = m_participants.indexOf(participant);
			Q_ASSERT(oldPos != -1);
			int newPos = findPos(m_participants, participant);
			if (oldPos != newPos && this->beginMoveRows(QModelIndex(), oldPos, oldPos, QModelIndex(), newPos)) {
				if (oldPos < newPos)
					--newPos;
				this->endInsertRows();
			} else {
				auto index = this->createIndex(oldPos, 0);
				emit this->dataChanged(index, index);
			}
		};

		lconnect(participant, SIGNAL(roleChanged(int)), this, checkPos);
		lconnect(participant, SIGNAL(nameChanged(QString,QString)), this, checkPos);
	});
	lconnect(conference, SIGNAL(participantLeft(Lime::ConferenceParticipant*)), this,
		[this](ConferenceParticipant *participant)
	{
		int pos = m_participants.indexOf(participant);
		Q_ASSERT(pos != -1);
		this->beginRemoveRows(QModelIndex(), pos, pos);
		m_participants.removeAt(pos);
		this->endRemoveRows();
		ldisconnect(participant, 0, this);
	});

	endResetModel();
}

} // Lime

