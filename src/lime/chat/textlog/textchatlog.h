/****************************************************************************
 *
 *  This file is part of qutIM
 *
 *  Copyright (c) 2011 by Nigmatullin Ruslan <euroelessar@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This file is part of free software; you can redistribute it and/or    *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************
 ****************************************************************************/

/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TEXTVIEWCONTROLLER_H
#define TEXTVIEWCONTROLLER_H

#include "../chatmanager.h"
#include "../abstractchatlog.h"
#include "../chatwidget.h"
#include <QTextDocument>
#include <QTextBrowser>
#include <QCache>
#include <QPointer>
#include <QDateTime>
#include <QTextObjectInterface>
#include <QMovie>

namespace Lime
{

class EmoticonMovie : public QMovie
{
public:
	EmoticonMovie(const QString &fileName, int index, QObject *parent) :
	    QMovie(fileName, QByteArray(), parent)
	{
		m_index = index;
	}

	QVector<int> indexes;

	int index() const { return m_index; }
private:
	int m_index;
};

struct EmoticonTrack
{
	EmoticonMovie *movie;
};

class TextChatDocument : public QTextDocument, public QTextObjectInterface
{
	Q_OBJECT
	Q_INTERFACES(QTextObjectInterface)
public:
    TextChatDocument(ChatUnit *unit, QObject *parent = 0);
	virtual ~TextChatDocument();
	void setChatWidget(ChatWidget *widget);
	void appendMessage(const Message &msg);
	void appendText(QTextCursor &cursor, const QString &text, const QTextCharFormat &format, bool emo);
	void clearChat();
	QString quote();
	void setTextEdit(QTextBrowser *edit);
	int scrollBarPosition() const { return m_scrollBarPosition; }
	void setScrollBarPosition(int pos) { m_scrollBarPosition = pos; }
	bool isNearBottom();

	// From QTextObjectInterface
    virtual void drawObject(QPainter *painter, const QRectF &rect, QTextDocument *doc,
	                        int posInDocument, const QTextFormat &format);
    virtual QSizeF intrinsicSize(QTextDocument *doc, int posInDocument, const QTextFormat &format);
public slots:
	void ensureScrolling();
protected slots:
	void onAnchorClicked(const QUrl &url);
	void animate();
private:
	void init();
	void loadHistory(ChatUnit *unit);
	int addEmoticon(const QString &filename);
	bool shouldBreak(const QDateTime &time);
private:
	QPointer<QTextBrowser> m_textEdit;
	ChatWidget *m_widget;
	QCache<qint64, int> m_cache;
	QDateTime m_lastTime;
	QString m_lastSender;
	QString m_lastIncomingMessage;
	bool m_isLastIncoming;
	bool m_storeServiceMessages;
	bool m_animateEmoticons;
	bool m_atAnimation;
	short m_groupUntil;
	int m_scrollBarPosition;
	QFont m_font;
	QColor m_incomingColor;
	QColor m_outgoingColor;
	QColor m_serviceColor;
	QColor m_baseColor;
	QColor m_urlColor;
	QSet<QString> m_images;
	QHash<QString, int> m_hash;
	QList<EmoticonTrack> m_emoticons;
};

struct TextChatLogState : public AbstractChatLogState
{
	TextChatLogState(ChatUnit *unit) :
		m_document(new TextChatDocument(unit))
	{ }
	TextChatDocument *m_document;
};

class TextChatLog : public QTextBrowser, public Lime::AbstractChatLog
{
public:
	TextChatLog() : m_document(0) {}
	virtual AbstractChatLogState *createState(ChatUnit *unit);
	virtual void setState(AbstractChatLogState *state, ChatWidget *widget);
	virtual void appendMessage(const Message &msg, AbstractChatLogState *state);
	virtual QWidget *widget() { return this; }
	virtual void clearChat() { m_document->clearChat(); }
	virtual QString quote() { return m_document->quote(); }
private:
	TextChatDocument *m_document;
};

}

#endif // TEXTVIEWCONTROLLER_H
