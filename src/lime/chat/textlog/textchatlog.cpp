/****************************************************************************
 *
 *  This file is part of qutIM
 *
 *  Copyright (c) 2011 by Nigmatullin Ruslan <euroelessar@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This file is part of free software; you can redistribute it and/or    *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************
 ****************************************************************************/

/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "textchatlog.h"
#include <QTextCursor>
#include <QUrl>
#include <QDateTime>
#include <QScrollBar>
#include <QPainter>
#include <QDesktopServices>
#include <QPlainTextEdit>
#include <QTimer>

#include <lime/config.h>
#include <lime/utils.h>
#include <lime/history/history.h>
#include <lime/emoticons/emoticonsmanager.h>

namespace Lime
{

enum { EmoticonObjectType = 0x666 };

TextChatDocument::TextChatDocument(ChatUnit *unit, QObject *parent) :
	QTextDocument(parent), m_widget(0)
{
	m_cache.setMaxCost(40);
	m_isLastIncoming = false;
	m_scrollBarPosition = 0;
	Config cfg = Config(QLatin1String("appearance"), QLatin1String("chat"));
	m_groupUntil = cfg.value<ushort>(QLatin1String("groupUntil"), 900);
	cfg.beginGroup(QLatin1String("textview"));
	m_animateEmoticons = cfg.value(QLatin1String("animateEmoticons"), true);
	m_incomingColor.setNamedColor(cfg.value(QLatin1String("incomingColor"), QLatin1String("#ff6600")));
	m_outgoingColor.setNamedColor(cfg.value(QLatin1String("outgoingColor"), QLatin1String("#0078ff")));
	m_serviceColor .setNamedColor(cfg.value(QLatin1String("serviceColor"),  QLatin1String("gray")));
	m_baseColor    .setNamedColor(cfg.value(QLatin1String("baseColor"),     QLatin1String("black")));
	m_urlColor     .setNamedColor(cfg.value(QLatin1String("urlColor"),      QLatin1String("#0033aa")));

	cfg.beginGroup(QLatin1String("font"));
#ifdef Q_WS_MAEMO_5
	m_font.setFamily(cfg.value(QLatin1String("family"), QLatin1String("Nokia Sans")));
	m_font.setPointSize(cfg.value(QLatin1String("size"), 15));
#elif defined(Q_WS_S60)
	m_font.setFamily(cfg.value(QLatin1String("family"), m_font.family()));
	m_font.setPointSize(cfg.value(QLatin1String("size"), m_font.pointSize()));
#else
	m_font.setFamily(cfg.value(QLatin1String("family"), QLatin1String("verdana")));
	m_font.setPointSize(cfg.value(QLatin1String("size"), 10));
#endif
	cfg.endGroup();
	cfg.endGroup();
	cfg.beginGroup(QLatin1String("history"));
	m_storeServiceMessages = cfg.value(QLatin1String("storeServiceMessages"), true);
	cfg.endGroup();
	documentLayout()->registerHandler(EmoticonObjectType, this);
	init();
	loadHistory(unit);

	if (unit->flags() & ChatUnit::SupportsMessageReceipts) {
		lconnect(unit, SIGNAL(messageReceipt(int,Lime::ChatUnit::MessageReceipt)), this,
			[=](int id, ChatUnit::MessageReceipt receipt)
		{
			int *pos = m_cache.take(id);
			if (receipt != ChatUnit::MessageNotDelivered) {
				if (!pos)
					return;
				QTextCursor cursor(this);
				cursor.beginEditBlock();
				cursor.setPosition(*pos);
				cursor.deleteChar();
				cursor.insertImage(QLatin1String("bullet-received"));
				cursor.endEditBlock();

			}
			delete pos;
		});
	}
}

TextChatDocument::~TextChatDocument()
{
}

void TextChatDocument::setChatWidget(ChatWidget *widget)
{
	if (m_widget)
		m_widget->removeEventFilter(this);
	m_widget = widget;
	if (m_widget)
		m_widget->installEventFilter(this);
}

void TextChatDocument::appendMessage(const Message &msg)
{
	if (msg.text().isEmpty())
		return;
	QTextCursor cursor(this);
	cursor.beginEditBlock();
	bool shouldScroll = isNearBottom();
	QTextCharFormat defaultFormat;
	defaultFormat.setFont(m_font);
	defaultFormat.setForeground(m_baseColor);
	cursor.setCharFormat(defaultFormat);
	cursor.movePosition(QTextCursor::End);
	QString currentSender = msg.senderName();
	bool isMe = msg.text().startsWith(QLatin1String("/me "));
	bool isService = msg.type() & Message::ServiceMessage;
	bool isIncoming = msg.direction() == Message::Incoming;
	if(isMe || (msg.type() & Message::ActionMessage)) {
		QString text = isMe ? msg.text().mid(4) : msg.text();
		cursor.insertText(QLatin1String("\n"));
		QTextCharFormat format = defaultFormat;
		format.setFontWeight(QFont::Bold);
		format.setForeground(isIncoming ? m_incomingColor : m_outgoingColor);
		if (isIncoming) {
			format.setAnchor(true);
			format.setAnchorHref(QLatin1String("nick:///") + QUrl::toPercentEncoding(currentSender));
		}
		cursor.insertText(currentSender, format);
		if (isIncoming) {
			format.setAnchor(false);
			format.setAnchorHref(QString());
		}
		cursor.insertText(QLatin1String(" "), format);
		format.setFontWeight(QFont::Normal);
		appendText(cursor, text, format, true);
		m_lastSender.clear();
	} else if (isService) {
		cursor.insertText(QLatin1String("\n"));
		QTextCharFormat format = defaultFormat;
		format.setForeground(m_serviceColor);
		appendText(cursor, msg.text(), format, false);
		m_lastSender.clear();
	} else {
		if (m_isLastIncoming != isIncoming || currentSender != m_lastSender || shouldBreak(msg.time())) {
			cursor.insertBlock();
			QTextCharFormat format = defaultFormat;
			format.setFontWeight(QFont::Bold);
			format.setForeground(isIncoming ? m_incomingColor : m_outgoingColor);
			if (isIncoming) {
				format.setAnchor(true);
				format.setAnchorHref(QLatin1String("nick:///") + QUrl::toPercentEncoding(currentSender));
			}
			cursor.insertText(currentSender, format);
			if (isIncoming) {
				format.setAnchor(false);
				format.setAnchorHref(QString());
			}
			cursor.insertText(QLatin1String(" "), format);
			format.setFontWeight(QFont::Normal);
			QString timeFormat = QLatin1String((msg.time().date() == m_lastTime.date())
			                                   ? "(hh:mm)" : "(dd.MM.yyyy hh:mm)");
			cursor.insertText(msg.time().toString(timeFormat), format);
		}
		m_lastSender = currentSender;
		m_lastTime = msg.time();
		m_isLastIncoming = isIncoming;
		if (m_isLastIncoming)
			m_lastIncomingMessage = msg.text();
		cursor.insertText(QLatin1String("\n"));
		bool showReceived = isIncoming || msg.unit()->flags() & ChatUnit::SupportsMessageReceipts;
		if (msg.type() & Message::HistoryMessage)
			showReceived = true;
		if (!showReceived)
			m_cache.insert(msg.id(), new int(cursor.position()));
		cursor.insertImage(QLatin1String(showReceived ? "bullet-received" : "bullet-send"));

		cursor.insertText(QLatin1String(" "), defaultFormat);
		appendText(cursor, msg.text(), defaultFormat, true);
	}
	if (shouldScroll)
		QTimer::singleShot(0, this, SLOT(ensureScrolling()));
	cursor.endEditBlock();
}

void TextChatDocument::appendText(QTextCursor &cursor, const QString &text,
							 const QTextCharFormat &format, bool emo)
{
	QTextCharFormat urlFormat = format;
	urlFormat.setForeground(m_urlColor);
	urlFormat.setFontUnderline(true);
	urlFormat.setAnchor(true);
	foreach (const UrlToken &textToken, ChatManagerImpl::parseUrls(text)) {
		if (!textToken.url.isEmpty()) {
			urlFormat.setAnchorHref(textToken.url);
			cursor.insertText(textToken.text.toString(), urlFormat);
			continue;
		} else if (!emo) {
			cursor.insertText(textToken.text.toString(), format);
			continue;
		}

		const auto tokens = EmoticonsManager::instance()->parse(textToken.text.toString(), false);
		QString objectReplacement(QChar::ObjectReplacementCharacter);
		QTextCharFormat emoticonFormat;
		emoticonFormat.setObjectType(EmoticonObjectType);
		int emoticonIndex;
		foreach (auto &token, tokens) {
			if (token.type == EmoToken::Text) {
				cursor.insertText(token.text, format);
			} else if (token.type == EmoToken::Image) {
				if (m_animateEmoticons) {
					emoticonIndex = addEmoticon(token.icon);
					emoticonFormat.setProperty(QTextFormat::UserProperty, emoticonIndex);
					m_emoticons.at(emoticonIndex).movie->indexes << cursor.position();
					cursor.insertText(objectReplacement, emoticonFormat);
				} else {
					if (!m_images.contains(token.icon)) {
						addResource(ImageResource, QUrl(token.icon), QPixmap(token.icon));
						m_images.insert(token.icon);
					}
					QTextImageFormat imageFormat;
					imageFormat.setName(token.icon);
					imageFormat.setToolTip(token.text);
					cursor.insertImage(imageFormat);
				}
			}
		}
	}
}

bool TextChatDocument::isNearBottom()
{
	if (!m_textEdit)
		return true;
	QScrollBar *scrollBar = m_textEdit->verticalScrollBar();
	qreal percentage = scrollBar->maximum() - scrollBar->value();
	percentage /= m_textEdit->viewport()->height();
	return percentage < 0.2;
}

void TextChatDocument::drawObject(QPainter *painter, const QRectF &rect, QTextDocument *doc,
                                int posInDocument, const QTextFormat &format)
{
	Q_UNUSED(posInDocument);
	Q_UNUSED(doc);
	int index = format.intProperty(QTextFormat::UserProperty);
	const EmoticonTrack &track = m_emoticons.at(index);
	QPixmap pixmap = track.movie->currentPixmap();
	painter->drawPixmap(rect, pixmap, track.movie->frameRect());
}

QSizeF TextChatDocument::intrinsicSize(QTextDocument *doc, int posInDocument, const QTextFormat &format)
{
	Q_UNUSED(posInDocument);
	Q_UNUSED(doc);
	int index = format.intProperty(QTextFormat::UserProperty);
	const EmoticonTrack &track = m_emoticons.at(index);
	return track.movie->frameRect().size();
}

int TextChatDocument::addEmoticon(const QString &filename)
{
	int index = m_hash.value(filename, m_emoticons.size());
	if (index == m_emoticons.size()) {
		m_emoticons.append(EmoticonTrack());
		EmoticonTrack &track = m_emoticons.last();
		track.movie = new EmoticonMovie(filename, index, this);
		connect(track.movie, SIGNAL(frameChanged(int)), this, SLOT(animate()));
		track.movie->setCacheMode(QMovie::CacheAll);
//		QTimer::singleShot(0, track.movie, SLOT(start()));
		m_hash.insert(filename, index);
		track.movie->start();
	}
	return index;
}

void TextChatDocument::ensureScrolling()
{
	if (m_textEdit) {
		QScrollBar *scrollBar = m_textEdit->verticalScrollBar();
//		qreal percentage = scrollBar->maximum() - scrollBar->value();
//		percentage /= m_textEdit->viewport()->height();
//		debug() << percentage;
//		if (percentage < 0.1)
			scrollBar->setValue(scrollBar->maximum());
	}
}

void TextChatDocument::onAnchorClicked(const QUrl &url)
{
	Q_ASSERT(m_textEdit);
	if (url.scheme() == QLatin1String("nick")) {
		auto textEdit = m_widget->textEdit();
		auto cursor = textEdit->textCursor();
		bool atStart = cursor.atStart();
		cursor.insertText(url.path().mid(1));
		cursor.insertText(QLatin1String(atStart ? ": " : " "));
		textEdit->setFocus();
	} else {
		QDesktopServices::openUrl(url);
	}
}

void TextChatDocument::animate()
{
	EmoticonMovie *movie = static_cast<EmoticonMovie*>(sender());
	if (!m_textEdit) {
		movie->stop();
		return;
	}
	QAbstractTextDocumentLayout *layout = documentLayout();
	QRect visibleRect(0, m_textEdit->verticalScrollBar()->value(),
	                  m_textEdit->viewport()->width(),
	                  m_textEdit->viewport()->height());
	int begin = layout->hitTest(visibleRect.topLeft(), Qt::FuzzyHit);
	int end = layout->hitTest(visibleRect.bottomRight(), Qt::FuzzyHit);
	int *indexesEnd = movie->indexes.data() + movie->indexes.size();
	int *beginIndex = qLowerBound(movie->indexes.data(), indexesEnd, begin);
	int *endIndex = qUpperBound(beginIndex, indexesEnd, end);
	if (beginIndex == endIndex)
		return;
	QRegion region;
	QTextCursor cursor(this);
	QSize emoticonSize = movie->frameRect().size();
	for (int *i = beginIndex; i != endIndex; i++) {
		cursor.setPosition(*i);
		QRect cursorRect = m_textEdit->cursorRect(cursor);
		region += QRectF(cursorRect.topLeft(), emoticonSize).toAlignedRect();
	}
	region &= m_textEdit->viewport()->visibleRegion();
	if (!region.isEmpty())
		m_textEdit->viewport()->update(region);
}

void TextChatDocument::init()
{
	QPixmap pixmap;
	pixmap.load(":/pictures/bullet-received.png");
	addResource(QTextDocument::ImageResource, QUrl("bullet-received"), pixmap);
	pixmap.load(":/pictures//bullet-send.png");
	addResource(QTextDocument::ImageResource, QUrl("bullet-send"), pixmap);
	for (int i = 0; i < m_emoticons.size(); i++)
		m_emoticons.at(i).movie->deleteLater();
	m_cache.clear();
	m_images.clear();
	m_emoticons.clear();
	m_lastSender.clear();
	m_lastTime = QDateTime();
	m_isLastIncoming = false;
}

void TextChatDocument::loadHistory(ChatUnit *unit)
{
	Config config = Config("", "chat/history");
	int max_num = config.value(QLatin1String("maxDisplayMessages"), 5);
	MessageList messages = History::instance()->read(unit, max_num);
	foreach (auto msg, messages) {
		if (!msg.unit())
			msg.setUnit(unit);
		appendMessage(msg);
	}
	m_lastSender.clear();
}

bool TextChatDocument::shouldBreak(const QDateTime &time)
{
	if (time.date() != m_lastTime.date())
		return true;
	if (m_lastTime.secsTo(time) > m_groupUntil)
		return true;
	return false;
}

void TextChatDocument::clearChat()
{
	QTextDocument::clear();
	init();
}

QString TextChatDocument::quote()
{
	QTextCursor cursor = m_textEdit->textCursor();
	return cursor.hasSelection() ? cursor.selectedText() : m_lastIncomingMessage;
}

void TextChatDocument::setTextEdit(QTextBrowser *edit)
{
	if (m_textEdit)
		disconnect(m_textEdit, 0, this, 0);
	m_textEdit = edit;
	if (m_textEdit)
		connect(m_textEdit, SIGNAL(anchorClicked(QUrl)), this, SLOT(onAnchorClicked(QUrl)));
	for (int i = 0; i < m_emoticons.size(); i++)
		m_emoticons.at(i).movie->setPaused(!edit);
}

AbstractChatLogState *TextChatLog::createState(ChatUnit *unit)
{
	return new TextChatLogState(unit);
}

void TextChatLog::setState(AbstractChatLogState *state, ChatWidget *widget)
{
	if (m_document)
		m_document->setChatWidget(0);
	if (state) {
		m_document = reinterpret_cast<TextChatLogState*>(state)->m_document;
		setDocument(m_document);
		m_document->setTextEdit(this);
		m_document->setChatWidget(widget);
	} else {
		m_document = 0;
	}
}

void TextChatLog::appendMessage(const Message &msg, AbstractChatLogState *state)
{
	auto doc = state ? reinterpret_cast<TextChatLogState*>(state)->m_document : m_document;
	doc->appendMessage(msg);
}

}
