/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "chatwidget.h"
#include <QBoxLayout>
#include <QSplitter>
#include <QPushButton>
#include <QKeyEvent>
#include <QListView>
#include <QTextBlock>
#include <QTextDocumentFragment>
#include <QActionGroup>
#include <QMenu>
#include <QAction>
#include <QToolButton>
#include <lime/utils.h>
#include <lime/actiontoolbar.h>
#include <lime/conference.h>
#include <lime/icon.h>
#include <lime/metacontact.h>
#include "chatmanager.h"
#include <lime/config.h>
#include <lime/contactlist/delegate/contactlistitemdelegate.h>
#include "extendediconswidget.h"

namespace Lime
{

ChatWidgetState::ChatWidgetState(AbstractChatLogState *logState_, ChatUnit *unit_) :
	logState(logState_),
	unit(unit_),
	enteredText(new QTextDocument),
	resource(unit_)
{
	isBuddy = qobject_cast<Buddy*>(unit) != 0;
	isConference = qobject_cast<Conference*>(unit) != 0;
	isMetaContact = qobject_cast<MetaContact*>(unit) != 0;
}

ChatWidgetState::~ChatWidgetState()
{
	delete logState;
	delete enteredText;
}

void ChatWidgetState::setChatState(ChatUnit::ChatState state)
{
	Q_ASSERT(unit->flags() & ChatUnit::SupportsChatStates);
	if (state == chatState) {
		// restart timer
		activeTimer.stop();
		activeTimer.start();
		return;
	}

	ChatManagerImpl::sendState(getResource(), state);
	chatState = state;

	if (state == ChatUnit::StateGone)
		return;

	static int timeouts[] = {
		/* StateActive    = */ 120000,
		/* StateInActive  = */ 600000,
		/* StateGone      = */ 0,
		/* StateComposing = */ 10000,
		/* StatePaused    = */ 30000
	};
	activeTimer.setInterval(timeouts[state]);
	activeTimer.start();
}

void ChatWidgetState::updateIcon()
{
	auto unitChatState = unit->chatState();
	icon = QIcon();

	if (unitChatState != ChatUnit::StateComposing) {
		if (isBuddy)
			icon = buddy->statusIcon();
		else if (isConference)
			icon = Icon("im-chat");
	}

	if (icon.isNull()) {
		QString iconName;
		switch (unitChatState) {
		case ChatUnit::StateActive:
			iconName = "im-user"; break;
		case ChatUnit::StateInActive:
			iconName = "im-user-away"; break;
		case ChatUnit::StateGone:
			iconName = "im-user-offline"; break;
		case ChatUnit::StateComposing:
			iconName = "im-status-message-edit"; break;
		case ChatUnit::StatePaused:
			iconName = "im-user-busy"; break;
		default:
			break;
		}
		icon = Icon(iconName);
	}

	emit onIconUpdated(icon);
}

ChatWidget::ChatWidget(AbstractChatLog *chatLog, QWidget *parent) :
	QWidget(parent),
	m_chatLog(chatLog),
	m_sendKey(SendDoubleEnter),
	m_sendKeyForced(false),
	m_waitingSecondEnter(false),
	m_confView(0),
	m_currentState(0),
	m_resourcesMenu(0)
{
	auto mainWidget = new QWidget(this);
	auto layout = new QVBoxLayout(mainWidget);
	layout->setContentsMargins(0, 0, 0, 0);
	m_splitter = new QSplitter(this);
	m_splitter->setObjectName("horizontalSplitter");
	layout->addWidget(m_splitter);
	m_splitter->setOrientation(Qt::Vertical);
	m_splitter->addWidget(chatLog->widget());

	auto mainLayout = new QVBoxLayout(this);
	auto bottomToolbarLayout = new QHBoxLayout;
	mainLayout->setContentsMargins(5, 5, 5, 5);
	m_confSplitter = new QSplitter(this);
	m_confSplitter->addWidget(mainWidget);
	mainLayout->addWidget(m_confSplitter);
	mainLayout->addLayout(bottomToolbarLayout);

	auto bottomWidget = new QWidget(m_splitter);
	auto bottomVLayout = new QVBoxLayout(bottomWidget);
	auto mainToolBarLayout = new QHBoxLayout;

	m_mainToolBar = new ActionToolBar(0, Action::MainToolbarAction, this);
	m_contactExtIcons = new ExtendedIconsWidget(this);
	mainToolBarLayout->addWidget(m_mainToolBar);
	mainToolBarLayout->addSpacerItem(new QSpacerItem(20, 0, QSizePolicy::Expanding, QSizePolicy::Fixed));
	mainToolBarLayout->addWidget(m_contactExtIcons);

	m_textEdit = new QTextEdit(this);
	m_textEdit->installEventFilter(this);
	bottomVLayout->setMargin(0);
	bottomVLayout->addLayout(mainToolBarLayout);
	bottomVLayout->addWidget(m_textEdit);
	m_splitter->addWidget(bottomWidget);

	m_sendButton = new QToolButton(bottomWidget);
	m_sendAction = new QAction(this);
	m_sendAction->setText(tr("Send"));
	m_sendButton->setText(tr("Send"));
	m_sendButton->setToolButtonStyle(Qt::ToolButtonTextOnly);
	m_sendButton->setPopupMode(QToolButton::InstantPopup);
	m_additionalToolBar = new ActionToolBar(0, Action::AdditionalToolbarAction, this);
	m_additionalToolBar->setIconSize(QSize(16, 16));
	connect(m_sendButton, SIGNAL(clicked(bool)), this, SLOT(send()));
	bottomToolbarLayout->setContentsMargins(0, 0, 0, 0);
	bottomToolbarLayout->addWidget(m_additionalToolBar);
	bottomToolbarLayout->addSpacerItem(new QSpacerItem(20, 0, QSizePolicy::Expanding, QSizePolicy::Preferred));
	bottomToolbarLayout->addWidget(m_sendButton);

	Config cfg(QString(), "chat/splitterStates");
	if (cfg.hasChildKey("mainSplitter"))
		m_splitter->restoreState(cfg.value<QByteArray>("mainSplitter"));
	else
		m_splitter->setSizes(QList<int>() << 400 << 100);

	lconnect(m_textEdit, SIGNAL(textChanged()), this, [=] {
		if (!m_sendChatStates)
			return;
		if (!m_currentState || !(m_currentState->unit->flags() & ChatUnit::SupportsChatStates))
			return;
		if (getPlainText().trimmed().isEmpty())
			m_currentState->setChatState(ChatUnit::StateActive);
		else
			m_currentState->setChatState(ChatUnit::StateComposing);
	});
}

ChatWidget::~ChatWidget()
{
	delete m_resourcesMenu;
	Config cfg(QString(), "chat/splitterStates");
	cfg.setValue("mainSplitter", m_splitter->saveState());
}

void ChatWidget::appendMessage(const Message &message, ChatWidgetState *state)
{
	m_chatLog->appendMessage(message, state ? state->logState : 0);
}

ChatWidgetState *ChatWidget::createState(ChatUnit *unit)
{
	auto state = new ChatWidgetState(m_chatLog->createState(unit), unit);
	state->updateIcon();
	foreach (auto &msg, ChatManagerImpl::takeUnopenedMessages(unit))
		m_chatLog->appendMessage(msg, state->logState);

	if (unit->flags() & ChatUnit::SupportsChatStates) {
		lconnect(unit, SIGNAL(chatActivated()), &state->activeTimer, [=] {
			if (m_sendChatStates)
				state->setChatState(ChatUnit::StateActive);
		});
		lconnect(unit, SIGNAL(chatDeactivated()), &state->activeTimer, [=] {
			if (m_sendChatStates && state->chatState != ChatUnit::StateGone)
				state->setChatState(ChatUnit::StateInActive);
		});
		lconnect(&state->activeTimer, SIGNAL(timeout()), &state->activeTimer, [=] {
			if (!m_sendChatStates)
				return;
			Q_ASSERT(state->chatState != ChatUnit::StateGone);
			static ChatUnit::ChatState stateShift[] = {
				/* StateActive    -> */ ChatUnit::StateInActive,
				/* StateInActive  -> */ ChatUnit::StateGone,
				/* StateGone      -> */ ChatUnit::StateGone,
				/* StateComposing -> */ ChatUnit::StatePaused,
				/* StatePaused    -> */ ChatUnit::StateActive
			};
			state->setChatState(stateShift[state->chatState]);
		});
		lconnect(unit, SIGNAL(chatStateChanged(Lime::ChatUnit::ChatState)), &state->activeTimer, [=] {
			state->updateIcon();
		});
	}

	if (state->isBuddy) {
		lconnect(unit, SIGNAL(statusChanged(Lime::Status,Lime::Status)), &state->activeTimer, [=] {
			state->updateIcon();
		});
	}

	return state;
}

void ChatWidget::setState(ChatWidgetState *state)
{
	m_currentState = state;

	delete m_resourcesMenu; m_resourcesMenu = 0;
	if (!state) {
		m_chatLog->setState(0, 0);
		m_textEdit->clear();
		m_mainToolBar->setContainer(0);
		m_contactExtIcons->setBuddy(0);
		m_additionalToolBar->setContainer(0);
		m_textEdit->setDocument(0);
		m_sendButton->setMenu(0);
		return;
	}

	m_chatLog->setState(state->logState, this);
	m_textEdit->setDocument(state->enteredText);
	if (!state->textCursor.isNull())
		m_textEdit->setTextCursor(state->textCursor);
	m_mainToolBar->setContainer(state->unit);
	m_additionalToolBar->setContainer(state->unit);
	m_contactExtIcons->setBuddy(qobject_cast<Buddy*>(state->unit));

	if (state->isMetaContact) {
		auto group = new QActionGroup(0);

		function<QMenu*(MetaContact*)> createMenu = [&](MetaContact *contact) {
			auto menu = new QMenu;

			auto createAction = [=](ChatUnit *unit, const QString &title) {
				auto act = new QAction(menu);
				act->setText(title);
				act->setData(qVariantFromValue(unit));
				act->setCheckable(true);
				act->setChecked(unit == state->resource);
				group->addAction(act);
				menu->addAction(act);
				lconnect(act, SIGNAL(toggled(bool)), menu, [=] {
					state->resource = unit;
				});
				lconnect(unit, SIGNAL(nameChanged(QString,QString)), act, [=](const QString &name) {
					act->setText(name);
				});
				return act;
			};

			auto handleNewResource = [=](Buddy *resource) {
				QObject *act = 0;
				if (auto metaContact = qobject_cast<MetaContact*>(resource)) {
					act = createMenu(metaContact);
					connect(menu, SIGNAL(destroyed()), act, SLOT(deleteLater()));
				} else {
					act = createAction(resource, resource->name());
				}
				connect(resource, SIGNAL(destroyed()), act, SLOT(deleteLater()));
			};

			createAction(contact, tr("Auto"));
			menu->addSeparator();

			foreach (auto resource, contact->resources())
				handleNewResource(resource);

			lconnect(contact, SIGNAL(resourceAdded(Lime::Buddy*)), this, handleNewResource);
			lconnect(contact, SIGNAL(resourceRemoved(Lime::Buddy*)), this, [=](Buddy *buddy) {
				foreach (auto act, menu->actions()) {
					if (act->data().value<ChatUnit*>() == buddy) {
						act->deleteLater();
						break;
					}
				}
			});

			return menu;
		};

		m_resourcesMenu = createMenu(state->metaContact);
		group->setParent(m_resourcesMenu);
		m_sendAction->setMenu(m_resourcesMenu);
		m_sendButton->setDefaultAction(m_sendAction);
	}

	if (state->isConference) {
		if (!m_confView) {
			m_confView = new QListView(this);
			auto confModel = new ConferenceModel(m_confView);
			m_confView->setModel(confModel);
			m_confSplitter->addWidget(m_confView);
			m_confView->setItemDelegate(new ContactListItemDelegate(m_confView));

			Config cfg(QString(), "chat/splitterStates");
			if (cfg.hasChildKey("conferenceSplitter"))
				m_confSplitter->restoreState(cfg.value<QByteArray>("conferenceSplitter"));
			else
				m_confSplitter->setSizes(QList<int>() << (width() - 150) << 150);
		}

		auto confModel = sender_cast<ConferenceModel*>(m_confView->model());
		confModel->setConference(state->conference);
	} else if (m_confView) {
		Config cfg(QString(), "chat/splitterStates");
		cfg.setValue("conferenceSplitter", m_confSplitter->saveState());
		m_confView->deleteLater();
		m_confView = 0;
	}
}

void ChatWidget::saveState(ChatWidgetState *state)
{
	state->enteredText = m_textEdit->document();
	state->textCursor = m_textEdit->textCursor();
}

void ChatWidget::setSendMessageKey(SendMessageKey key, bool force)
{
	if (force || !m_sendKeyForced) {
		m_sendKey = key;
		m_sendKeyForced = force;
	}
}

void ChatWidget::enableSendingChatStates(bool enable, bool force)
{
	if (force || !m_sendChatStatesForced) {
		m_sendChatStates = enable;
		m_sendChatStatesForced = force;
		// FIXME: if chat states has been disabled, should we send StateActive to all units?
	}
}

void ChatWidget::clearChat()
{
	if (m_currentState)
		m_chatLog->clearChat();
}

QString ChatWidget::quote()
{
	return m_currentState ? m_chatLog->quote() : QString();
}

bool ChatWidget::eventFilter(QObject *obj, QEvent *ev)
{
	if (obj == m_textEdit && ev->type() == QEvent::KeyPress) {
		QKeyEvent *event = static_cast<QKeyEvent *>(ev);
		if (event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter) {
			if (event->modifiers() == Qt::ControlModifier)  {
				if (m_sendKey == SendCtrlEnter) {
					send();
					return true;
				} else if (m_sendKey == SendEnter || m_sendKey == SendDoubleEnter) {
					m_textEdit->insertPlainText(QLatin1String("\n"));
				}
			} else if (event->modifiers() == Qt::NoModifier ||
					   event->modifiers() == Qt::KeypadModifier)
			{
				if (m_sendKey == SendEnter) {
					send();
					return true;
				} else if (m_sendKey == SendDoubleEnter) {
					if (m_waitingSecondEnter) {
						m_enterPosition.deletePreviousChar();
						m_waitingSecondEnter = false;
						send();
						return true;
					} else {
						m_waitingSecondEnter = true;
						m_enterPosition = m_textEdit->textCursor();
					}
				}
			}
		} else {
			m_waitingSecondEnter = false;
		}
	}
	return QWidget::eventFilter(obj, ev);
}

void ChatWidget::send()
{
	if (!m_currentState)
		return;

	Message msg(getPlainText(), Message::Outgoing);
	msg.setUnit(m_currentState->getResource());
	m_currentState->getResource()->sendMessage(msg);
	m_textEdit->clear();
	m_textEdit->setFocus();
	emit messageSent();
}

QString ChatWidget::getPlainText()
{
	QTextDocument *doc = m_textEdit->document();
	QString result;
	result.reserve(doc->characterCount());
	QTextCursor begin(doc);
	QTextCursor end;
	QString specialChar = QChar(QChar::ObjectReplacementCharacter);
	bool first = true;
	while (!begin.atEnd()) {
		end = doc->find(specialChar, begin, QTextDocument::FindCaseSensitively);
		QString postValue;
		bool atEnd = end.isNull();
		if (atEnd) {
			end = QTextCursor(doc);
			QTextBlock block = doc->lastBlock();
			end.setPosition(block.position() + block.length() - 1);
		} else {
			postValue = end.charFormat().toolTip();
		}
		begin.movePosition(QTextCursor::Right, QTextCursor::KeepAnchor,
						   end.position() - begin.position() - (atEnd ? 0 : 1));
		QString selectionText = begin.selection().toPlainText();
		if (!first)
			result += selectionText.midRef(1);
		else
			result += selectionText;
		result += postValue;
		begin = end;
		end.clearSelection();
		first = false;
	}
	return result;
}

}

