/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TABBEDWIDGET_H
#define TABBEDWIDGET_H

#include "chatwidget.h"
#include <QTabBar>

class QStackedLayout;

namespace Lime
{

class TabBar : public QTabBar
{
	Q_OBJECT
public:
	TabBar(QWidget *parent = 0) : QTabBar(parent) {}
	Signal<void(int index)> onTabRemoved;
private:
	void tabRemoved(int index) { QTabBar::tabRemoved(index); emit onTabRemoved(index); }
};

class TabbedWidget : public QWidget
{
	Q_OBJECT
public:
	TabbedWidget(AbstractChatLog *chatLog, QWidget *parent = 0);
    virtual ~TabbedWidget();
	void addChatUnit(ChatUnit *unit);
	void activate(ChatUnit *unit);
	bool appendMessage(const Message &message);
	QList<ChatUnit*> units() { return m_units; }
	ChatUnit *currentUnit();
	QTextDocument *document(ChatUnit *unit);
	ChatWidget *chatWidget() { return m_chatWidget; }
	int unitCount() const { return m_units.count(); }
public slots:
	void closeCurrent();
signals:
	void chatUnitAdded(Lime::ChatUnit *unit);
	void chatUnitRemoved(Lime::ChatUnit *unit);
	void currentChatUnitChanged(Lime::ChatUnit *unit);
private:
	TabBar *m_tabBar;
	ChatWidget *m_chatWidget;
	QList<ChatUnit*> m_units;
	QMap<ChatUnit *, ChatWidgetState*> m_states;
	ChatWidgetState *m_currentState;
};

} // Lime

#endif // TABBEDWIDGET_H
