/*
	 This file is part of Lime.
	 Copyright (C) 2011-2012 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIME_CHATMANAGER_H_
#define LIME_CHATMANAGER_H_

#include "chatunit.h"
#include "extention.h"

namespace Lime
{

class ChatManager : public Extension
{
public:
	virtual ~ChatManager();
	static ChatManager *instance();
protected:
	friend class ChatUnit;
	ChatManager();
	virtual void activateChat(ChatUnit *unit) = 0;
	virtual void appendMessage(const Message &message) = 0;
	virtual QTextDocument *document(ChatUnit *unit) = 0;
	ChatUnit *activeUnit() { return m_activeUnit; }
protected:
	void setActive(ChatUnit *unit, bool activate);
	void handleChatOpening(ChatUnit *unit);
	void handleChatClosing(ChatUnit *unit);
	void sendChatState(ChatUnit *unit, ChatUnit::ChatState state);
private:
	static ChatManager *self;
	ChatUnit *m_activeUnit;
};

} // Lime

#endif // LIME_CHATMANAGER_H_
