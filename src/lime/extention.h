/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_EXTENTION_H
#define LIME_EXTENTION_H

#include "global.h"
#include <QtPlugin>

namespace Lime {

class Extension
{
public:
	Extension();
	bool enable();
	bool disable();
	bool isExtensionEnabled() { return m_isEnabled; }
	virtual QObject *toQObject() = 0;
protected:
	virtual bool enableImpl() = 0;
	virtual bool disableImpl() = 0;
private:
	bool m_isEnabled;
};

class ExtensionManager : public QObject
{
	Q_OBJECT
public:
	enum State
	{
		Unittesting,
		Inititializing,
		Working,
		Quiting
	};
	static ExtensionManager *instance();
	void initExtensions();
	static State state() { return instance()->m_state; }
private:
	ExtensionManager();
	virtual ~ExtensionManager();
private:
	QList<Extension*> m_extensions;
	State m_state;
};

}

Q_DECLARE_INTERFACE(Lime::Extension, "Lime.Extension")

#endif // LIME_EXTENTION_H
