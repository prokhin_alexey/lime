/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_METACONTACT_H
#define LIME_METACONTACT_H

#include "contact.h"

namespace Lime {

class MetaContact : public Contact
{
	Q_OBJECT
public:
	enum MetaContactFlags
	{
		HasAvatar = 0x01000000, // The metacontact has its own avatar and, thus, it should not be loaded from resources
		MetaContactDefaultFlags = ContactDefaultFlags
	};
	MetaContact(const QString &id, Account *account, int flags = MetaContactDefaultFlags) :
		Contact(id, account, flags)
	{}
	QList<Buddy*> resources() const { return m_resources; }
public: // static signals
	static Signal<void(MetaContact *contact, Buddy *buddy)> onResourceAdded;
	static Signal<void(MetaContact *contact, Buddy *buddy)> onResourceRemoved;
signals:
	void resourceAdded(Lime::Buddy *buddy);
	void resourceRemoved(Lime::Buddy *buddy);
protected:
	void addResource(Buddy *buddy);
	void removeResource(Buddy *buddy);
private:
	void deleteResource(Buddy *buddy);
	void updateData(Buddy *buddy);
	QList<Buddy*> m_resources;
};

} // Lime

#endif // LIME_METACONTACT_H
