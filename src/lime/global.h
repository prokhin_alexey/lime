/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_GLOBAL_H
#define LIME_GLOBAL_H

#include <QObject>
#include <QDebug>
#include "qtboostintegration.h"

namespace Lime
{

using std::function;

template<typename T>
T sender_cast(QObject *obj)
{
#if DEBUG
	T *val = qobject_cast<T>(obj);
	Q_ASSERT(val);
	return val;
#else
	return reinterpret_cast<T>(obj);
#endif
};

inline QString versionString() { return "0.1"; }


} // Lime

#endif // GLOBAL_H
