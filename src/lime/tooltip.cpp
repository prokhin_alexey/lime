/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "tooltip.h"
#include "iconmanager/iconmanager.h"
#include <QLatin1Literal>
#include <QIcon>
#include <QBuffer>
#include <QToolTip>
#include <QTextDocument>

namespace Lime {

typedef QMultiMap<int, QString> FieldsMap;

class ToolTipPrivate
{
public:
	inline void addIcon(QString &text, const QString &icon);
	inline void addIcon(QString &text, const QIcon &icon);
	template<typename IconType>
	void addField(const QString &title, const QString &description,
				  const IconType &icon, ToolTip::IconPosition iconPosition,
				  int priority);
	FieldsMap fields;
	bool generateLayout;
};

void ToolTipPrivate::addIcon(QString &text, const QString &icon)
{
	if (!icon.isEmpty()) {
		QString iconPath = IconManager::instance()->path(icon, 16);
		if (!iconPath.isEmpty()) {
			if (!text.isEmpty())
				text += " ";
			text += "<img width='16' height='16' src='" + iconPath + "'> ";
		}
	}
}

void ToolTipPrivate::addIcon(QString &text, const QIcon &icon)
{
	if (!icon.isNull()) {
		auto pixmap = icon.pixmap(16);
		if (!pixmap.isNull()) {
			QByteArray bytes;
			QBuffer buffer(&bytes);
			buffer.open(QIODevice::WriteOnly);
			pixmap.save(&buffer, "PNG");
			if (!text.isEmpty())
				text += " ";
			text += "<img width='16' height='16' src='data:image/png;base64," + bytes.toBase64() +"'> ";
		}
	}
}

template<typename IconType>
void ToolTipPrivate::addField(const QString &title, const QString &description,
							  const IconType &icon, ToolTip::IconPosition iconPosition,
							  int priority)
{
	QString text;
	bool descriptionEmpty = description.isEmpty();
	if (iconPosition == ToolTip::IconBeforeTitle)
		addIcon(text, icon);
	if (!title.isEmpty()) {
		text += QLatin1Literal("<b>") % Qt::escape(title);
		if (!descriptionEmpty)
			text += ":";
		text += "</b>";
	}
	if (iconPosition == ToolTip::IconBeforeDescription)
		addIcon(text, icon);
	if (!descriptionEmpty) {
		if (!text.isEmpty())
			text += " ";
		text += Qt::escape(description);
	}
	if (!text.isEmpty())
		text.prepend("<br/>");
	fields.insert(priority, text);
}

ToolTip::ToolTip(bool generateLayout) :
	d(new ToolTipPrivate)
{
	d->generateLayout = generateLayout;
}

ToolTip::~ToolTip()
{
}

void ToolTip::addHtml(const QString &html, int priority)
{
	d->fields.insert(priority, html);
}

void ToolTip::addField(const QString &title, const QString &description,
					   int priority)
{
	d->addField(title, description, QString(), IconBeforeTitle, priority);
}

void ToolTip::addField(const QString &title, const QString &description,
					   const QString &icon, int priority)
{
	d->addField(title, description, icon, IconBeforeTitle, priority);
}

void ToolTip::addField(const QString &title, const QString &description,
					   const QIcon &icon, int priority)
{
	d->addField(title, description, icon, IconBeforeTitle, priority);
}

void ToolTip::addField(const QString &title, const QString &description,
					   const QString &icon, ToolTip::IconPosition iconPosition,
					   int priority)
{
	d->addField(title, description, icon, iconPosition, priority);
}

void ToolTip::addField(const QString &title, const QString &description,
					   const QIcon &icon, ToolTip::IconPosition iconPosition,
					   int priority)
{
	d->addField(title, description, icon, iconPosition, priority);
}

bool ToolTip::generateLayout() const
{
	return d->generateLayout;
}

QString ToolTip::html() const
{
	QString text;
	QMapIterator<int, QString> i(d->fields);
	i.toBack();
	while (i.hasPrevious())
		text += i.previous().value();
	text.remove(QRegExp("^<br/>"));
	return text;
}

void ToolTip::show(const QPoint &pos, QWidget *w)
{
	QString text = html();
	if (text.isEmpty())
		QToolTip::hideText();
	else
		QToolTip::showText(pos, text, w);
}

} // namespace Lime
