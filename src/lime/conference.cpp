/*
    This file is part of Lime.
    Copyright (c) 2009 by Nigmatullin Ruslan <euroelessar@gmail.com>
    Copyright (c) 2011 by Sidorov Aleksey <sauron@citadelspb.com>
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "conference.h"
#include <algorithm>
#include <QMessageBox>
#include "message.h"
#include "account.h"

namespace Lime {

Signal<void(ConferenceParticipant *participant, int role)> ConferenceParticipant::onRoleChanged;
Signal<void(Conference *conference)> Conference::onJoined;
Signal<void(Conference *conference)> Conference::onLeft;
Signal<void(Conference *conference, ConferenceParticipant *participant)> Conference::onParticipantJoined;
Signal<void(Conference *conference, ConferenceParticipant *participant)> Conference::onParticipantLeft;
Signal<void(Conference *conference, ConferenceParticipant *user)> Conference::onUserChanged;

ConferenceParticipant::ConferenceParticipant(const QString &id, Conference *conference, int flags) :
	Buddy(id, conference->account(), flags), m_conference(conference)
{
}

ConferenceParticipant::~ConferenceParticipant()
{
}

void ConferenceParticipant::updateRole(int role)
{
	if (role == m_role)
		return;
	m_role = role;
	emit roleChanged(role);
	emit onRoleChanged(this, role);
}

Conference::Conference(const QString &id, Account *account, int flags) :
	ChatUnit(id, account, flags), m_user(0), m_isJoined(false)
{
}

Conference::~Conference()
{
	account()->removeConference(this);
}

ConferenceParticipant *Conference::participant(const QString &name) const
{
	auto end = m_participants.constEnd();
	auto itr = std::find_if(m_participants.constBegin(), end,
		[=](ConferenceParticipant *part)
	{
		return part->name() == name;
	});
	return itr != end ? *itr : 0 ;
}

void Conference::join()
{
	if (!m_isJoined)
		joinImpl();
}

void Conference::leave()
{
	if (m_isJoined)
		leaveImpl();
}

void Conference::addParticipant(ConferenceParticipant *participant, const QString &role)
{
	Q_ASSERT(!m_participants.contains(participant));
	m_participants.append(participant);
	emit participantJoined(participant);

	lconnect(participant, SIGNAL(nameChanged(QString,QString)), this,
		[=](const QString &newName, const QString &oldName)
	{
		addServiceMessage(tr("%1 is now known as %2").arg(oldName).arg(newName));
	});

	if (m_user != participant) {
		auto id = participant->id();
		auto msg = participant->name();
		if (!id.isEmpty())
			msg += QString(" (%1)").arg(id);
		msg += tr(" has joined");
		if (!role.isEmpty())
			msg += tr(" as ") + role;
		addServiceMessage(msg);
	}
}

void Conference::removeParticipant(ConferenceParticipant *participant, LeaveReason reason, const QString &reasonDesc)
{
	Q_UNUSED(reason);
	Q_UNUSED(reasonDesc);
	bool res = m_participants.removeOne(participant);
	Q_ASSERT(res);
	emit participantLeft(participant);

	QString msg;
	if (reason == Kicked)
		msg = tr("%1 has been kicked");
	else if (reason == Banned)
		msg = tr("%1 has been banned");
	else
		msg = tr("%1 has left");
	msg = msg.arg(participant->name());
	if (!reasonDesc.isEmpty())
		msg += tr(" with reason: %1").arg(reasonDesc);
	addServiceMessage(msg);

	participant->deleteLater();
}

void Conference::handleJoin(ConferenceParticipant *user)
{
	Q_ASSERT(!m_isJoined);
	m_user = user;
	addParticipant(user);
	m_isJoined = true;
	emit userChanged(user);
	emit onUserChanged(this, user);
	emit joined();
	emit onJoined(this);
}

void Conference::handleLeave(LeaveReason reason, const QString &reasonDesc)
{
	if (!m_isJoined)
		return;

	foreach (auto part, m_participants) {
		emit participantLeft(part);
		emit onParticipantLeft(this, part);
		part->deleteLater();
	}
	m_participants.clear();
	m_user = 0;
	m_isJoined = false;
	emit userChanged(m_user);
	emit onUserChanged(this, m_user);
	emit left();
	emit onLeft(this);


	if (reason == Banned || reason == Kicked) {
		if (reason == Kicked) {
			auto msg = tr("You have been kicked %1\n").arg(id());
			if (!reasonDesc.isEmpty())
				msg += tr(" with reason %1").arg(reasonDesc);
			msg = msg + tr("Do you want to rejoin?");
			if (QMessageBox::warning(0, tr("You have been kicked"), msg,
				QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes) == QMessageBox::Yes)
			{
				join();
			}
		}

		{
			auto msg = reason == Banned ?
								tr("You have been banned") :
								tr("You have been kicked");
			if (!msg.isEmpty())
				msg += tr(" with reason: %1").arg(reason);
			addServiceMessage(msg);
		}
	}

}

void Conference::addServiceMessage(const QString &msg)
{
	Message message(msg, Message::Incoming);
	message.setType(Message::ServiceMessage);
	message.setUnit(this);
	handleIncomingMessage(message);
}

} //lime

