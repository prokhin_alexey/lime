/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_BUDDY_H
#define LIME_BUDDY_H

#include "chatunit.h"
#include "status.h"
#include <QIcon>
#include <QVariant>

namespace Lime
{

class Contact;

class ExtendedInfoDescription
{
public:
	ExtendedInfoDescription(const QString &name, const QString &description) :
		m_name(name), m_description(description)
	{}
	QString name() const { return m_name; }
	QString description() const { return m_description; }
	void setName(const QString &name) { m_name = name; }
	void setDescription(const QString &description) { m_description = description; }
	bool operator==(const ExtendedInfoDescription &other) const { return m_name == other.m_name; }
private:
	QString m_name;
	QString m_description;
};

inline uint qHash(const ExtendedInfoDescription &desc)
{
	return qHash(desc.name());
}

class ExtendedInfoDescriptions : public QSet<ExtendedInfoDescription>
{
public:
	using QSet<ExtendedInfoDescription>::insert;
	const_iterator insert(const QString &name, const QString &description)
	{
		return QSet<ExtendedInfoDescription>::insert(ExtendedInfoDescription(name, description));
	}
private:

};

// Everything that may have avatar and status.
class Buddy : public ChatUnit
{
	Q_OBJECT
public:
	enum BuddyFlags
	{
		BuddyDefaultFlags = ChatUnitDefaultFlags
	};
	Buddy(const QString &id, Account *account, int flags = BuddyDefaultFlags);
    virtual ~Buddy();
	QString avatar() const { return m_avatar; }
	Status  status() const { return m_status; }
	QString statusText() const { return m_statusText; }
	virtual QString statusIconName();
	QString statusName();
	QIcon statusIcon();
	// Extended statuses like icq x-status, jabber mood, activity, etc.
	QVariantMap extendedInfo(const QString &name) const { return m_extendedInfo.value(name); }
	void setExtendedInfo(const QString &name, const QVariantMap &info);
	void removeExtendedInfo(const QString &name) { setExtendedInfo(name, QVariantMap()); }
	QMap<QString, QVariantMap> allExtendedInfo() { return m_extendedInfo; }
	Contact *parentContact() const;
	int priority() const { return m_priority; }
public: // hooks
	static Signal<void(ExtendedInfoDescriptions &descriptions)> supportedExtendedInfoHook;
public: // static signals
	static Signal<void(Buddy* buddy, QString path)> onAvatarChanged;
	static Signal<void(Buddy* buddy, Status newStatus, Status oldStatus)> onStatusChanged;
	static Signal<void(Buddy* buddy, QString text)> onStatusTextChanged;
	static Signal<void(Buddy* buddy, QString name, QVariantMap info)> onExtendedInfoChanged;
	static Signal<void(Buddy* buddy, int newPriority, int oldPriority)> onPriorityChanged;
signals:
	void avatarChanged(const QString &path);
	void statusChanged(Lime::Status newStatus, Lime::Status oldStatus);
	void statusTextChanged(const QString &text);
	void extendedInfoChanged(const QString &name, const QVariantMap &info);
	void priorityChanged(int newPriority, int oldPriority);
protected:
	void updateAvatar(const QString &avatar);
	void updateStatus(Status status, const QString &text = QString());
	void updateParentUnit(ChatUnit *unit) { m_parentUnit = unit; }
	void updatePriority(int priority);
private:
	friend class Account;
	// Removes all extended information fields that does not have 'keepOffline' property set to be true.
	void checkExtendedInfo();
	int m_priority;
	ChatUnit *m_parentUnit;
	Status  m_status;
	QString m_avatar;
	QString m_statusText;
	QMap<QString, QVariantMap> m_extendedInfo;
};

}

Q_DECLARE_METATYPE(Lime::Buddy*)
Q_DECLARE_METATYPE(Lime::Status)

#endif // LIME_BUDDY_H
