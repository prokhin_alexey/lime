/****************************************************************************
**
** qutIM instant messenger
**
** Copyright (C) 2011 Ruslan Nigmatullin <euroelessar@ya.ru>
**
*****************************************************************************
**
** $QUTIM_BEGIN_LICENSE$
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see http://www.gnu.org/licenses/.
** $QUTIM_END_LICENSE$
**
****************************************************************************/

/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef JSONHISTORY_H
#define JSONHISTORY_H

#include <QRunnable>
#include <QDir>
#include <QLinkedList>
#include <QPointer>
#include <QMutex>
#include <lime/message.h>
#include <lime/extention.h>
#include <lime/utils.h>

namespace Lime
{

class HistoryWindow;
class JsonHistoryScope;

class JsonHistoryRunnable : public QRunnable
{
public:
	JsonHistoryRunnable(JsonHistoryScope *scope);
	virtual void run();

private:
	JsonHistoryScope *d;
};

class JsonHistoryScope
{
public:
	uint findEnd(QFile &file);
	QString getFileName(const Message &message) const;
	QDir getAccountDir(const ChatUnit *unit) const;

	struct EndValue
	{
		EndValue(const QDateTime &t, uint e) : lastModified(t), end(e) {}
		QDateTime lastModified;
		uint end;
	};

	typedef QHash<QString, EndValue> EndCache;
	bool hasRunnable;
	EndCache cache;
	QLinkedList<Message> queue;
	QMutex mutex;
};

class History : public QObject, public Extension
{
	Q_OBJECT
public:
	History();
	virtual ~History();
	uint findEnd(QFile &file) { return m_scope.findEnd(file); }
	virtual void store(const Message &message);
	virtual MessageList read(const ChatUnit *unit, const QDateTime &from, const QDateTime &to, int max_num);
	virtual void showHistory(const ChatUnit *unit);
	MessageList read(const ChatUnit *unit, const QDateTime &to, int max_num) { return read(unit, QDateTime(), to, max_num); }
	MessageList read(const ChatUnit *unit, int max_num) { return read(unit, QDateTime(), QDateTime::currentDateTime(), max_num); }
	static History *instance() { static auto history = new History; return history; }
	static QString quote(const QString &str);
	static QString unquote(const QString &str);
protected:
	virtual bool enableImpl();
	virtual bool disableImpl();
	virtual QObject* toQObject() { return this; }
private:
	QString getFileName(const Message &message) const { return m_scope.getFileName(message); }
	QDir getAccountDir(const ChatUnit *unit) const { return m_scope.getAccountDir(unit); }

	JsonHistoryScope m_scope;
	Pointer<HistoryWindow> m_historyWindow;
};

typedef History JsonHistory;

} // Lime

#endif // JSONHISTORY_H
