/*
	 This file is part of Lime.
	 Copyright (C) 2012 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "contactinformationwindow.h"
#include "ui_contactinformation.h"
#include <QTimer>
#include <QFileDialog>
#include "buddy.h"
#include "account.h"
#include "icon.h"
#include "iconmanager/iconmanager.h"

namespace Lime
{

ContactInformationWindow::ContactInformationWindow(QObject *object, bool isReadOnly, QWidget *parent) :
	m_state(Default), m_isReadOnly(isReadOnly), ui(new Ui::ContactInformationWindow), m_object(object)
{
	ui->setupUi(this);
	ui->splitter->setSizes(QList<int>() << 130 << 500);
	ui->splitter->setStretchFactor(1, 1);
	QTimer::singleShot(0, this, SLOT(initialize()));
	connect(ui->requestButton, SIGNAL(clicked(bool)), this, SLOT(requestInfo()));
	if (isReadOnly) {
		ui->saveButton->setVisible(false);
		ui->setAvatarButton->setVisible(false);
		ui->removeAvatarButton->setVisible(false);
	} else {
		ui->setAvatarButton->setIcon(Icon("list-add-avatar"));
		ui->removeAvatarButton->setIcon(Icon("list-remove-avatar"));

		lconnect(ui->setAvatarButton, SIGNAL(clicked(bool)), this, [=] {
			auto path = QFileDialog::getOpenFileName(
							this,
							tr("Open image"),
							QDir::homePath(),
							tr("Images (*.gif *.bmp *.jpg *.jpeg *.png);;All files (*.*)"));
			setAvatar(path);
		});
		connect(ui->removeAvatarButton, SIGNAL(clicked(bool)), this, SLOT(clearAvatar()));
		lconnect(ui->saveButton, SIGNAL(clicked(bool)), this, [=] {
			saveInfoImpl();
		});
	}

	if (auto unit = qobject_cast<ChatUnit*>(object)) {
		QString id = unit->id();
		QString name = unit->name();
		if (name != id)
			name = QString("%1 <%2>").arg(name).arg(id);
		setWindowTitle(tr("Information about %1").arg(name));
		if (auto buddy = qobject_cast<Buddy*>(object))
			setAvatar(buddy->avatar());
	} else if (auto account = qobject_cast<Account*>(object)) {
		setWindowTitle(tr("Information about %1").arg(account->name()));
		setAvatar(account->avatar());
	} else {
		setWindowTitle(tr("Information window"));
	}
}

ContactInformationWindow::~ContactInformationWindow()
{
	delete ui;
}

void ContactInformationWindow::setAvatar(const QString &avatar)
{
	m_avatarPath = avatar;
	QPixmap pixmap;
	if (!avatar.isEmpty())
		pixmap = QPixmap(avatar).scaled(QSize(64, 64), Qt::KeepAspectRatio);
	if (pixmap.isNull())
		clearAvatar();
	else
		ui->avatarLabel->setPixmap(pixmap);
}

void ContactInformationWindow::infoReceived(Error error, const QString &errorStr)
{
	Q_ASSERT(m_state == RequestingInfo);
	updateSummary();

	if (errorStr.isEmpty()) {
		switch (error) {
		case NoErrors:
			setError(QString());
			break;
		case NetworkError:
			setError(tr("Information is not received due to a network problem"));
			break;
		case AccountIsOfflineError:
			setError(tr("Cannot request the information while the account is offline"));
			break;
		default:
			setError(tr("Information is not received"));
			break;
		}

	} else {
		setError(errorStr);
	}
}

void ContactInformationWindow::infoUpdated(Error error, const QString &errorStr)
{
	updateSummary();

	if (errorStr.isEmpty()) {
		switch (error) {
		case NoErrors:
			setError(QString());
			break;
		case NetworkError:
			setError(tr("Information is not updated due to a network problem"));
			break;
		case AccountIsOfflineError:
			setError(tr("Cannot update the information while the account is offline"));
			break;
		default:
			setError(tr("Information is not updated"));
			break;
		}

	} else {
		setError(errorStr);
	}
}

void ContactInformationWindow::setOfflineMode(bool offlineMode)
{
	m_offlineMode = offlineMode;
	ui->saveButton->setEnabled(!offlineMode);
	ui->requestButton->setEnabled(!offlineMode);
}

void ContactInformationWindow::addPage(const QString &title, QWidget *widget)
{
	Q_ASSERT(m_state == InitializingFullInfo);
	ui->pageListWidget->addItem(title);
	ui->stackedWidget->addWidget(widget);
	Q_ASSERT(ui->pageListWidget->count() == ui->stackedWidget->count());
}

void ContactInformationWindow::beginGroup(const QString &title)
{
	Q_ASSERT(m_state == InitializingShortInfo);
	m_group = title;
}

void ContactInformationWindow::endGroup()
{
	Q_ASSERT(m_state == InitializingShortInfo);
	m_group.clear();
}

void ContactInformationWindow::addEntry(const QString &title, const QString &description)
{
	Q_ASSERT(m_state == InitializingShortInfo);
	if (description.isEmpty())
		return;
	if (!m_group.isEmpty()) {
		m_shortInfo += QString("<b>[%1]:</b><br/>").arg(m_group);
		m_group.clear();
	}
	if (!title.isEmpty())
		m_shortInfo += QString("<b>%1:</b> %2<br/>").arg(title).arg(description);
	else
		m_shortInfo += QString("%1<br/>").arg(description);
}

void ContactInformationWindow::initialize()
{
	m_state = InitializingFullInfo;
	initializeFullInfo();
	requestInfo();
}

void ContactInformationWindow::requestInfo()
{
	m_state = RequestingInfo;
	requestInfoImpl();
}

void ContactInformationWindow::clearAvatar()
{
	QPixmap pixmap(IconManager::instance()->path("lime", 64));
	m_avatarPath.clear();
	ui->avatarLabel->setPixmap(pixmap);
}

void ContactInformationWindow::setError(const QString &errorStr)
{
	if (!errorStr.isEmpty())
		ui->errorLabel->setText(QString("<font color=\"red\">%1</font>").arg(errorStr));
	else
		ui->errorLabel->setText(QString());
}

void ContactInformationWindow::updateSummary()
{
	m_state = InitializingShortInfo;
	m_shortInfo.clear();
	initializeShortInfo();
	Q_ASSERT(m_group.isEmpty());
	ui->summaryLabel->setText(m_shortInfo);
	m_state = Default;
}

} // Lime
