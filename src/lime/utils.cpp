/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "utils.h"
#include "systeminfo.h"
#include <QApplication>
#include <QRect>
#include <QApplication>
#include <QWidget>
#include <QDesktopWidget>

namespace Lime
{

QList<QDir> themeDirs(const QString &type)
{
	QList<QDir> res;
	// Looking in the application directory
	auto dir = QDir(QApplication::applicationDirPath() + "/share/style");
	if (dir.cd(type))
		res << dir;

	// Looking in the local share dir.
	dir = SystemInfo::getDir(SystemInfo::ShareDir);
	if (dir.cd("style"))
		if (dir.cd(type))
			res << dir;

	// Looking in the system share dir
	dir = SystemInfo::getDir(SystemInfo::SystemShareDir);
	if (dir.cd("style"))
		if (dir.cd(type))
			res << dir;

	return res;
}

QStringList themes(const QString &type)
{
	QStringList res;
	foreach (auto &dir, themeDirs(type))
		res << dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
	return res;
}

QString theme(const QString &type, const QString &theme)
{
	foreach (auto dir, themeDirs(type)) {
		if (dir.cd(theme))
			return dir.absolutePath();
	}
	return QString();
}

void centerizeWidget(QWidget *widget)
{
	QRect rect = QApplication::desktop()->screenGeometry(QCursor::pos());
	QPoint position(rect.left() + rect.width() / 2 - widget->size().width() / 2,
					rect.top() + rect.height() / 2 - widget->size().height() / 2);
	widget->move(position);
}

}
