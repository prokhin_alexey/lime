/*
    This file is part of Lime.
    Copyright (c) 2010  Ruslan Nigmatullin <euroelessar@gmail.com>
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_MULTIMEDIASOUNDPLAYER_H
#define LIME_MULTIMEDIASOUNDPLAYER_H

#include "abstractsoundplayer.h"
#include <QThread>
#include <QtMultimediaKit/QAudioOutput>

namespace Lime {

class MultimediaSoundThread : public QThread
{
	Q_OBJECT
public:
	MultimediaSoundThread(const QString &filename, QObject *parent = 0);
	virtual void run();
public slots:
	void finishedPlaying(QAudio::State state);
private:
	QString m_filename;
};

class MultimediaSoundPlayer : public AbstractSoundPlayer
{
public:
	virtual void play(const QString& file);
};

}

#endif // LIME_MULTIMEDIASOUNDPLAYER_H
