/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "soundmanager.h"
#include <lime/config.h>
#include <lime/icon.h>
#include <lime/contactlist/contactlist.h>
#include "players/externalplayer.h"
#include "players/multimediasoundplayer.h"
#include "players/qsoundplayer.h"
#include "settings/soundsettings.h"

namespace Lime {

static QIcon soundIcon(bool isEnabled)
{
	return Icon(isEnabled ? "audio-volume-high" : "audio-volume-muted");
}

SoundManager::SoundManager() :
	m_player(0)
{
}

bool SoundManager::enableImpl()
{
	reloadSettings();
	SettingsManager::addGlobalSettings(Icon("speaker"), tr("Sound"), [=] {
		auto widget = new SoundSettings;
		connect(widget, SIGNAL(saved()), this, SLOT(reloadSettings()));
		return widget;
	});

	auto action = new Action(soundIcon(m_isSoundEnabled), tr("Enable/disable sound"), this);
	action->setType(Action::MainToolbarAction);
	action->setCheckable(true);
	action->setChecked(m_isSoundEnabled, 0);
	lconnect(action, SIGNAL(triggered(QObject*,bool)), this, [=](QObject */*container*/, bool checked) {
		m_isSoundEnabled = checked;
		action->setIcon(soundIcon(m_isSoundEnabled), 0);
		Config("", "sound").setValue("isEnabled", m_isSoundEnabled);
	});
	ActionContainer::addAction<ContactList>(action);

	return true;
}

void SoundManager::play(Notification::Type type)
{
	if (!m_isSoundEnabled)
		return;
	if (!m_filter.isFiltered(type) && m_player) {
		auto path = m_currentTheme.value(type);
		if (!path.isEmpty())
			m_player->play(path);
	}
}

NotificationFilter SoundManager::defaultFilter()
{
	auto ret = { Notification::System, Notification::ParticipantJoined, Notification::ParticipantLeft };
	return ret;
}

void SoundManager::reloadSettings()
{
	Config cfg("", "sound");
	delete m_player;
	auto engine = cfg.value("engine", "multimediakit");
	if (engine == "multimediakit")
		m_player = new MultimediaSoundPlayer;
	else if (engine == "qsound")
		m_player = new QSoundPlayer;
	else if (engine == "external")
		m_player = new ExternalPlayer(cfg.value("command", "aplay"));
	else {
		qWarning() << "Unknown sound engine" << engine;
		m_player = 0;
	}
	m_currentTheme = SoundThemes::getTheme(cfg.value("theme", "default"));
	m_filter = NotificationFilter::loadFromConfig(cfg, "types", defaultFilter());
	m_isSoundEnabled = cfg.value("isEnabled", true);
}

} // Lime

