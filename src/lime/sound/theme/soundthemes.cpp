/*
    This file is part of Lime.
    Copyright (C) 2011 Ruslan Nigmatullin <euroelessar@ya.ru>
    Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "soundthemes.h"
#include <lime/utils.h>
#include <QDomDocument>

namespace Lime
{

QStringList SoundThemes::allThemes()
{
	QStringList themes = Lime::themes("sounds");
	QStringList themeList;
	foreach (auto &name, themes) {
		QDir dir(Lime::theme("sounds", name));
		auto entries = dir.entryList(QStringList("*.xml"), QDir::Files);
		if (entries.isEmpty())
			continue;
		bool single = entries.size() == 1;
		foreach (const QString &entry, entries) {
			QFile file(dir.filePath(entry));
			if (!file.open(QIODevice::ReadOnly))
				continue;
			QDomDocument doc;
			doc.setContent(&file);
			auto docType = doc.doctype().name();
			if (docType == "limesounds" || docType == "qutimsounds") {
				if (single) {
					themeList << name;
				} else {
					themeList << (name + " (" + entry.mid(0, entry.length() - 4) + ")");
				}
			}
		}
	}
	return themeList;
}

SoundTheme SoundThemes::getTheme(const QString &name)
{
	QString themeName = name;
	QString variant;
	if (name.endsWith(")") && name.contains(" (")) {
		int index = name.indexOf(" (");
		themeName = name.mid(0, index);
		variant = name.mid(index + 2, name.length() - 3 - index);
	}

	const Notification::Type xmlEventTypes[] = {
		Notification::UserWentOnline,
		Notification::UserWentOffline,
		Notification::UserChangedStatus,
		Notification::UserHasBirthday,
		Notification::AppStartup,
		Notification::IncomingMessage,
		Notification::IncomingConferenceMessage,
		Notification::OutgoingMessage,
		Notification::OutgoingConferenceMessage,
		Notification::System,
		Notification::UserTyping,
		Notification::BlockedMessage
	};
	const char* const xmlEventNames[] = {
		"c_online",
		"c_offline",
		"c_changed_status",
		"c_birth",
		"start",
		"m_get",
		"m_chat_get",
		"m_send",
		"m_chat_send",
		"sys_event",
		"c_typing",
		"c_blocked_message",
		"sys_custom"
	};

	SoundTheme theme;
	QDir dir(Lime::theme("sounds", name));
	if (variant.isEmpty())
		variant = dir.entryList(QStringList("*.xml"), QDir::Files).value(0);
	else
		variant += ".xml";
	QFile file(dir.filePath(variant));
	if (file.open(QIODevice::ReadOnly)) {
		QDomDocument doc;
		doc.setContent(&file);
		auto docType = doc.doctype().name();
		if (docType != "limesounds" && docType != "qutimsounds")
			return theme;
		QDomElement rootElement = doc.documentElement();
		QDomNodeList soundsNodeList = rootElement.elementsByTagName("sounds");
		if (soundsNodeList.count() != 1)
			return theme;
		QDomElement soundsElement = soundsNodeList.at(0).toElement();
		soundsNodeList = soundsElement.elementsByTagName("sound");
		QDomElement soundElement;
		QString eventName, soundFileName;

		for (int i = 0; i < soundsNodeList.count(); i++) {
			soundElement = soundsNodeList.at(i).toElement();
			eventName = soundElement.attribute("event");
			if (eventName.isEmpty() || !soundElement.elementsByTagName("file").count())
				continue;
			soundFileName = dir.filePath(soundElement.elementsByTagName("file").at(0).toElement().text());
			if (!QFile::exists(soundFileName))
				continue;
			for (int i = 0, size = sizeof(xmlEventNames) / sizeof(const char*); i < size; i++) {
				if (eventName == QLatin1String(xmlEventNames[i])) {
					theme.insert(xmlEventTypes[i], soundFileName);
					break;
				}
			}
		}
	}
	return theme;
}

} // Lime

