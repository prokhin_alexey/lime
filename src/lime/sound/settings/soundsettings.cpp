/*
 This file is part of Lime.
 Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "soundsettings.h"
#include <lime/notificationfilter.h>
#include <lime/icon.h>
#include "../soundmanager.h"
#include "../players/externalplayer.h"
#include "../players/multimediasoundplayer.h"
#include "../players/qsoundplayer.h"
#include <QFormLayout>
#include <QPushButton>
#include <QGroupBox>
#include <QRadioButton>
#include <QComboBox>
#include <QLineEdit>

namespace Lime
{

SoundSettings::SoundSettings() :
	m_currentPlayer(0)
{
	auto central = new QWidget(this);
	auto layout = new QFormLayout(central);
	m_themeBox = new QComboBox(this);
	auto engineGroup = new QGroupBox(tr("Engine:"), this);
	auto engineLayout = new QVBoxLayout(engineGroup);
	auto multimediaButton = new QRadioButton(tr("MultimediKit"), this);
	auto qsoundButton = new QRadioButton(tr("QSound"), this);
	auto externalLayout = new QHBoxLayout;
	auto externalButton = new QRadioButton(tr("External player"), this);
	auto externalEdit = new QLineEdit(this);
	auto filter = new NotificationFilterWidget("Play sound when:", NotificationFilter{ Notification::System }, this);

	setWidget(central);
	layout->addRow(engineGroup);
	layout->addRow(filter);
	layout->addRow(tr("Theme:"), m_themeBox);
	engineLayout->addWidget(multimediaButton);
	engineLayout->addWidget(qsoundButton);
	engineLayout->addLayout(externalLayout);
	externalLayout->addWidget(externalButton);
	externalLayout->addWidget(externalEdit);
	m_themeBox->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);

	bind("types", filter, SoundManager::defaultFilter());
	bind("externalPlayer", externalEdit, "aplay");
	bind("theme", m_themeBox, "default");

	multimediaButton->setObjectName("multimediakit");
	qsoundButton->setObjectName("qsound");
	externalButton->setObjectName("external");
	m_players << multimediaButton << qsoundButton << externalButton;

	auto clearCurrentPlayer = [=] {
		delete m_currentPlayer; m_currentPlayer = 0;
	};
	lconnect(multimediaButton, SIGNAL(toggled(bool)), this, clearCurrentPlayer);
	lconnect(qsoundButton, SIGNAL(toggled(bool)), this, clearCurrentPlayer);
	lconnect(externalButton, SIGNAL(toggled(bool)), this, clearCurrentPlayer);
	lconnect(externalEdit, SIGNAL(textChanged(QString)), this, clearCurrentPlayer);

	QList<QPushButton*> testButtons;
	filter->addTestButtonActions([=,&testButtons](Notification::Type type) {
		auto button = new QPushButton(Icon("media-playback-start"), QString(), this);
		lconnect(button, SIGNAL(clicked(bool)), this, [=] {
			if (!m_currentPlayer) {
				auto player = playerName();
				if (player == "multimediakit")
					m_currentPlayer = new MultimediaSoundPlayer;
				else if (player == "qsound")
					m_currentPlayer = new QSoundPlayer;
				else if (player == "external")
					m_currentPlayer = new ExternalPlayer(externalEdit->text());
			}
			Q_ASSERT(m_currentPlayer);
			auto file = m_currentTheme.value(type);
			if (!file.isEmpty())
				m_currentPlayer->play(file);
		});
		button->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Ignored);
		button->setProperty("notificationType", int(type));
		testButtons << button;
		return button;
	});

	lconnect(m_themeBox, SIGNAL(currentIndexChanged(QString)), this, [=](const QString &theme) {
		m_currentTheme = SoundThemes::getTheme(m_themeBox->currentText());
		foreach (auto button, testButtons) {
			auto type = Notification::Type(button->property("notificationType").toInt());
			button->setEnabled(m_currentTheme.contains(type));
		}
	});
}

SoundSettings::~SoundSettings()
{
}

void SoundSettings::loadImpl()
{
	m_themeBox->clear();
	m_themeBox->addItems(SoundThemes::allThemes());

	Config cfg("", "sound");
	loadBinds(cfg);

	auto playerName = cfg.value("engine", "multimediakit");
	foreach (auto player, m_players)
		player->setChecked(player->objectName() == playerName);
}

void SoundSettings::cancelImpl()
{
	loadImpl();
}

void SoundSettings::saveImpl()
{
	Config cfg("", "sound");
	saveBinds(cfg);
	cfg.setValue("engine", playerName());
}

QString SoundSettings::playerName()
{
	foreach (auto player, m_players) {
		if (player->isChecked()) {
			return player->objectName();
		}
	}
	return QString();
}

} // Lime
