/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LIME_TOOLTIP_H_
#define LIME_TOOLTIP_H_

#include "global.h"
#include <QPoint>

namespace Lime {

class ToolTipPrivate;

class ToolTip
{
public:
	ToolTip(bool generateLayout = true);
	virtual ~ToolTip();
	enum IconPosition {
		IconBeforeTitle = 0,
		IconBeforeDescription
	};
	enum Priority {
		Top     = 0x010000000,
		Middle  = 0x000100000,
		Bottom  = 0x000001000

	};
	void addHtml(const QString &html, int priority = Middle);
	void addField(const QString &title,
				  const QString &description = QString(),
				  int priority = Middle);
	void addField(const QString &title,
				  const QString &description,
				  const QString &icon,
				  int priority = Middle);
	void addField(const QString &title,
				  const QString &description,
				  const QIcon &icon,
				  int priority = Middle);
	void addField(const QString &title,
				  const QString &description,
				  const QString &icon,
				  IconPosition iconPosition,
				  int priority = Middle);
	void addField(const QString &title,
				  const QString &description,
				  const QIcon &icon,
				  IconPosition iconPosition,
				  int priority = Middle);
	bool generateLayout() const;
	QString html() const;
	template <typename T>
	static void show(T *obj, const QPoint &pos, QWidget *w = 0);
	void show(const QPoint &pos, QWidget *w = 0);
	inline void hide() { show(QPoint(), 0); }
private:
	QScopedPointer<ToolTipPrivate> d;
};

template <typename T>
void ToolTip::show(T *obj, const QPoint &pos, QWidget *w)
{
	ToolTip tip;
	obj->toolTipHook(&tip);
	tip.show(pos, w);
}

} // namespace Lime

#endif /*LIME_TOOLTIP_H_ */
