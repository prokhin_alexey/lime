/*
    This file is part of Lime.
    Copyright (C) 2011 Sidorov Aleksey <sauron@citadelspb.com>
    Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CORE_KINETICPOPUPS_BACKEND_H
#define CORE_KINETICPOPUPS_BACKEND_H

#include <lime/notification.h>
#include <lime/extention.h>
#include <lime/notificationfilter.h>
#include <QHash>

namespace Lime {

class QuickPopupWidget;
typedef QMultiHash<QuickPopupWidget*, Pointer<Notification> > NotificationHash;

class WidgetPlacer;

class PopupManager : public QObject, public Extension
{
	Q_OBJECT
public:
	virtual ~PopupManager();
	void showPopup(Notification *notification);
	void showPopup(Notification *notification, const QString &theme);
	static PopupManager *instance() { static auto manager = new PopupManager; return manager; }
	static NotificationFilter defaultFilter();
protected slots:
	bool split(Lime::Notification *notify);
protected:
	virtual QObject *toQObject() { return this; }
	virtual bool enableImpl();
	virtual bool disableImpl() { return false; }
private slots:
	void reloadSettings();
private:
	PopupManager();
	WidgetPlacer *m_placer;
	NotificationHash m_activeNotifyHash;
	int m_timeout;
	QString m_theme;
	NotificationFilter m_filter;
};

} // Lime

#endif // CORE_KINETICPOPUPS_BACKEND_H
