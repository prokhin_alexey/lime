/*
    This file is part of Lime.
    Copyright (C) 2011 Sidorov Aleksey <sauron@citadelspb.com>
    Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "kineticpopup.h"
#include <3rdparty/qtdwm/qtdwm.h>
#include <lime/config.h>
#include <lime/utils.h>
#include <QVBoxLayout>
#include <QStringBuilder>
#include <QAction>

#include <QDeclarativeView>
#include <QDeclarativeContext>

namespace Lime {

QuickNotify::QuickNotify(Notification* notify) :
	QObject(notify),
	m_notify(notify)
{
	foreach (auto name, notify->dynamicPropertyNames())
		setProperty(name, notify->property(name));

	//add Actions
	foreach (auto action, notify->actions()) {
		auto notifyAction = new QAction(action->icon(), action->title(), this);
		lconnect(notifyAction, SIGNAL(triggered()), this, [=]() {
			action->triggered();
		});
		action->destroyed.connect(notifyAction, SLOT(deleteLater()));
		m_actions.append(notifyAction);
	}
}

QString QuickNotify::text() const
{
	return m_notify->text();
}

QString QuickNotify::title() const
{
	auto title = m_notify->title();
	if (title.isEmpty())
		title = tr("Notification from Lime");
	return title;
}

Notification::Type QuickNotify::type() const
{
	return m_notify->type();
}

QObject* QuickNotify::object() const
{
	return m_notify->object();
}

QPixmap QuickNotify::image() const
{
	return m_notify->image();
}

QObjectList QuickNotify::actions() const
{
	return m_actions;
}

PopupAttributes::PopupAttributes(QObject* parent) :
	QObject(parent),
	m_frameStyle(Tool)
{
	setObjectName(QLatin1String("attributes"));
}

PopupAttributes::FrameStyle PopupAttributes::frameStyle() const
{
	return m_frameStyle;
}

void PopupAttributes::setFrameStyle(FrameStyle frameStyle)
{
	if (frameStyle == m_frameStyle)
		return;
	m_frameStyle = frameStyle;
	emit frameStyleChanged(m_frameStyle);
}

QuickPopupWidget::QuickPopupWidget(const QString &theme, int timeout, QWidget* parent) :
	QWidget(parent),
	m_view(new QDeclarativeView(this))
{
	setAttribute(Qt::WA_DeleteOnClose);
	setAttribute(Qt::WA_ShowWithoutActivating);
	setAttribute(Qt::WA_X11NetWmWindowTypeToolTip);

	//transparency
	setAttribute(Qt::WA_NoSystemBackground);
	setAttribute(Qt::WA_TranslucentBackground);
	m_view->viewport()->setAttribute(Qt::WA_TranslucentBackground);
	m_view->viewport()->setAutoFillBackground(false);

	QVBoxLayout *l = new QVBoxLayout(this);
	l->addWidget(m_view);
	l->setMargin(0);
	setLayout(l);
	connect(m_view, SIGNAL(sceneResized(QSize)), SIGNAL(sizeChanged(QSize)));
	m_view->setResizeMode(QDeclarativeView::SizeViewToRootObject);
	m_view->rootContext()->setContextProperty(QLatin1String("popup"), this);

	loadTheme(theme);
	m_timeout.setInterval(timeout);

	connect(&m_timeout, SIGNAL(timeout()), this, SLOT(reject()));
}

void QuickPopupWidget::loadTheme(const QString &themeName)
{
	auto themePath = theme("quickpopup", themeName);
	auto filename = themePath % QLatin1Literal("/main.qml");
	m_view->setSource(QUrl::fromLocalFile(filename));//url - main.qml
	if (m_view->status() == QDeclarativeView::Error)
		reject();

	auto attributes = m_view->rootObject()->findChild<PopupAttributes*>("attributes");
	if (attributes) {
		lconnect(attributes, SIGNAL(frameStyleChanged(Lime::PopupAttributes::FrameStyle)), this, [=] {
			setPopupAttributes(attributes);
		});
	}
	setPopupAttributes(attributes);

	//reload notifications
	//TODO
}

QList<Notification*> QuickPopupWidget::notifications() const
{
	return m_notifyHash.keys();
}

void QuickPopupWidget::addNotification(Notification* notify)
{
	auto quick = new QuickNotify(notify);
	m_notifyHash.insert(notify, quick);
	emit notifyAdded(quick);
	m_timeout.start();
}

QuickPopupWidget::~QuickPopupWidget()
{

}

QSize QuickPopupWidget::sizeHint() const
{
	return m_view->sizeHint();
}

void QuickPopupWidget::ignore()
{
	foreach (Notification *notify, m_notifyHash.keys())
		notify->ignore();
	emit finished();
}

void QuickPopupWidget::accept()
{
	foreach (Notification *notify, m_notifyHash.keys())
		notify->accept();
	emit finished();
}

void QuickPopupWidget::reject()
{
	emit finished();
}

void QuickPopupWidget::setPopupAttributes(PopupAttributes *attributes)
{
	PopupAttributes::FrameStyle style = attributes ? attributes->frameStyle() :
													 PopupAttributes::ToolTip;

	Qt::WindowFlags flags = Qt::WindowStaysOnTopHint;
	//set flags
	switch(style) {
	case PopupAttributes::AeroBlur:
		flags |= Qt::WindowStaysOnTopHint;
		flags |= Qt::CustomizeWindowHint;
		break;
	case PopupAttributes::ToolTipBlurAero:
		flags |= Qt::ToolTip;
		break;
	case PopupAttributes::ToolTip:
		flags |= Qt::ToolTip;
		flags |= Qt::X11BypassWindowManagerHint;
		flags |= Qt::FramelessWindowHint;
		break;
	case PopupAttributes::ToolBlurAero:
		flags |= Qt::WindowStaysOnTopHint;
		flags |= Qt::CustomizeWindowHint;
		flags |= Qt::WindowShadeButtonHint;
	case PopupAttributes::Tool:
		flags |= Qt::Tool;
		break;
	case PopupAttributes::Normal:
		break;
	}
	setWindowFlags(flags);

	//enable aero
	switch(style) {
	case PopupAttributes::ToolBlurAero:
	case PopupAttributes::AeroBlur:
		QtDWM::extendFrameIntoClientArea(this);
		break;
	case PopupAttributes::ToolTipBlurAero:
		QtDWM::enableBlurBehindWindow(this, true);
		break;
	default:
		break;
	}

	emit sizeChanged(sizeHint());
}

void QuickPopupWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
		accept();
	else if (event->button() == Qt::RightButton)
		ignore();
}

} // Lime
