/*
    This file is part of Lime.
    Copyright (C) 2011 Sidorov Aleksey <sauron@citadelspb.com>
    Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KINETICPOPUPS_KINETICPOPUP_H
#define KINETICPOPUPS_KINETICPOPUP_H

#include <QWidget>
#include <lime/notification.h>
#include <QtDeclarative>
#include <QTimer>

namespace Lime {

class QuickNotify : public QObject
{
	Q_OBJECT
	Q_PROPERTY(QString title READ title)
	Q_PROPERTY(QString text  READ text)
	Q_PROPERTY(QObject* object READ object)
	Q_PROPERTY(Lime::Notification::Type type READ type)
	Q_PROPERTY(QPixmap image READ image)
	Q_PROPERTY(QObjectList actions READ actions)
public:
	QuickNotify(Notification *notify);
	QString title() const;
	QString text() const;
	QObject* object() const;
	Notification::Type type() const;
	QPixmap image() const;
	QObjectList actions() const;
private:
	Notification *m_notify;
	QObjectList m_actions;
};

class PopupAttributes : public QObject
{
	Q_OBJECT
	Q_ENUMS(FrameStyle)
	Q_PROPERTY(FrameStyle frameStyle READ frameStyle WRITE setFrameStyle NOTIFY frameStyleChanged)
public:
	enum FrameStyle
	{
		ToolTip,
		Normal, /** \ Normal window */
		Tool, /** \note Unsupported on X11 platform */
		ToolTipBlurAero,  /** \note Only Win7 Supported */
		ToolBlurAero,  /** \note Only Win7 Supported */
		AeroBlur  /** \note Only Win7 Supported */
	};
public:
	PopupAttributes(QObject *parent = 0);
	void setFrameStyle(FrameStyle frameStyle);
	FrameStyle frameStyle() const;
signals:
	void frameStyleChanged(Lime::PopupAttributes::FrameStyle);
private:
	FrameStyle m_frameStyle;
};

class QuickPopupWidget : public QWidget
{
	Q_OBJECT
public:
	QuickPopupWidget(const QString &theme, int timeout, QWidget *parent = 0);
	void addNotification(Notification *notify);
	QList<Notification*> notifications() const;
	~QuickPopupWidget();
	QSize sizeHint() const;
	void setPopupAttributes(PopupAttributes *attributes = 0);
	void loadTheme(const QString &themePath);
signals:
	void notifyAdded(QObject *notify);
	void finished();
	void sizeChanged(QSize size);
public slots:
	void ignore();
	void accept();
	void reject();
protected:
	void mouseReleaseEvent(QMouseEvent *event);
private:
	QHash<Notification*, QuickNotify*>  m_notifyHash;
	QDeclarativeView *m_view;
	QTimer m_timeout;
};

} // Lime

#endif // KINETICPOPUPS_KINETICPOPUP_H
