/*
    This file is part of Lime.
    Copyright (C) 2011 Sidorov Aleksey <sauron@citadelspb.com>
    Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "popupmanager.h"
#include "widgetplacer.h"
#include "kineticpopup.h"
#include "settings/popupappearance.h"
#include <lime/settings.h>
#include <lime/icon.h>

namespace Lime {

PopupManager::PopupManager() :
	m_placer(new WidgetPlacer(this))
{

	qmlRegisterUncreatableType<Notification>("qutIM", 0, 3,
											 "Notification",
											 tr("Unable to create notification inside QtQuick"));
	qmlRegisterType<PopupAttributes>("qutIM", 0, 3, "PopupAttributes");
}

PopupManager::~PopupManager()
{
}

void PopupManager::showPopup(Notification *notification)
{
	showPopup(notification, m_theme);
}

void PopupManager::showPopup(Notification *notification, const QString &theme)
{
	if (m_filter.isFiltered(notification->type())) {
		delete notification;
		return;
	}
	if (!split(notification)) {
		auto popup = new QuickPopupWidget(theme, m_timeout);
		popup->addNotification(notification);
		m_placer->addWidget(popup);
		m_activeNotifyHash.insert(popup, notification);
		lconnect(popup, SIGNAL(destroyed(QObject*)), this, [=] {
			foreach (auto notify, m_activeNotifyHash.values(popup)) {
				if (notify)
					notify->deleteLater();
			}
			m_activeNotifyHash.remove(popup);
		});
		connect(popup, SIGNAL(finished()), popup, SLOT(deleteLater()));
	}
}

NotificationFilter PopupManager::defaultFilter()
{
	NotificationFilter defValues;
	defValues.enableAll();
	defValues.setFilter(Notification::IncomingMessage, false);
	defValues.setFilter(Notification::System, false);
	defValues.setFilter(Notification::AppStartup, false);
	return defValues;
}

bool PopupManager::split(Notification *notify)
{
	auto it = m_activeNotifyHash.constBegin();
	for (; it != m_activeNotifyHash.constEnd(); ++it) {
		if (!it.value())
			continue;
		auto other = it.value();
		if (notify->object() && notify->object() == other->object() &&
			notify->type() == notify->type())
		{
			it.key()->addNotification(notify);
			return true;
		}
	}
	return false;
}

bool PopupManager::enableImpl()
{
	SettingsManager::addGlobalSettings(Icon("dialog-information"), tr("Popups"), [=] {
		auto widget = new PopupAppearance;
		connect(widget, SIGNAL(saved()), this, SLOT(reloadSettings()));
		return widget;
	});
	reloadSettings();
	return true;
}

void PopupManager::reloadSettings()
{
	Config cfg("", "popup");
	m_theme = cfg.value("themeName", "default");
	m_timeout = cfg.value("timeout", 5000);
	m_filter = NotificationFilter::loadFromConfig(cfg, "types", defaultFilter());
}

} // Lime
