/*
    This file is part of Lime.
    Copyright (C) 2011 Sidorov Aleksey <sauron@citadelspb.com>
    Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "widgetplacer.h"
#include "kineticpopup.h"
#include <QPropertyAnimation>
#include <QApplication>
#include <QDesktopWidget>

#include <lime/utils.h>
#include <lime/config.h>

namespace Lime {


WidgetPlacer::WidgetPlacer(QObject* parent) :
	QObject(parent)
{
	loadSettings();
}

WidgetPlacer::~WidgetPlacer()
{

}

void WidgetPlacer::addWidget(QuickPopupWidget* widget)
{
	m_popups.append(widget);
	doLayout();
	doShow(widget);

	lconnect(widget, SIGNAL(finished()), this, [=]() {
		this->doHide(widget);
	});
	lconnect(widget, SIGNAL(destroyed(QObject*)), this, [=] {
		m_popups.removeOne(widget);
		this->doLayout();
	});
	lconnect(widget, SIGNAL(sizeChanged(QSize)), this, [=] {
		this->doLayout();
	});
}

WidgetPlacer::Corner WidgetPlacer::corner() const
{
	return m_corner;
}

void WidgetPlacer::setCorner(WidgetPlacer::Corner corner)
{
	m_corner = corner;
	doLayout();
}

PopupWidgetList WidgetPlacer::widgets() const
{
	return m_popups;
}

void WidgetPlacer::updatePlace()
{
	doLayout();
}

void WidgetPlacer::loadSettings()
{
	Config cfg("behaviour", "popup");
	m_corner = cfg.value("corner", BottomRight);
	m_duration = cfg.value("duration", 600);
	m_margin = cfg.value("margin", 20);
}

QRect WidgetPlacer::actualGeometry(QuickPopupWidget *popup) const
{
	if (auto a = m_runningAnimations.value(popup))
		return a->endValue().toRect();
	else
		return popup->geometry();
}

QRect WidgetPlacer::calculateGeometry(int after, QuickPopupWidget *widget) const
{
	auto size = widget->sizeHint();
	auto desktopRect = QApplication::desktop()->availableGeometry(QCursor::pos());
	//place first widget
	if (after == -1 || after >= m_popups.count()) {
		switch (m_corner) {
		//TODO add additional corners
		case WidgetPlacer::BottomRight:
		default:
			QPoint topLeft(desktopRect.bottomRight().x() - m_margin - size.width(),
							desktopRect.bottomRight().y() - m_margin - size.height());
			return QRect(topLeft, size);
		}
	}
	auto beforeWidget = m_popups.at(after);
	switch (m_corner) {
	//TODO add additional corners
	case WidgetPlacer::BottomRight:
	default:
		QPoint topLeft(desktopRect.bottomRight().x() - m_margin - size.width(),
						actualGeometry(beforeWidget).topRight().y() - m_margin - size.height());
		return QRect(topLeft, size);
	}
}

void WidgetPlacer::doInsert(int after, QuickPopupWidget *widget)
{
	auto geometry = calculateGeometry(after, widget);
	doMove(widget, geometry);
}

void WidgetPlacer::doLayout(int first)
{
	if (!m_popups.count())
		return;
	//simple check
	if (first < 0 || first >= (m_popups.count())) {
		first = 0;
		qWarning() << "WidgetPlacer::invalid first popup";
	}

	for (int i = first; i < m_popups.count(); i++) {
		auto widget = m_popups.at(i);
		doInsert(i - 1, widget);
	}
}

void WidgetPlacer::doMove(QuickPopupWidget *popup, const QRect &endGeometry)
{
	if (auto animation = m_runningAnimations.value(popup))
		animation->stop();

	QRect startGeometry;
	if (popup->isVisible())
		startGeometry = popup->geometry();
	else {
		startGeometry = endGeometry;
		startGeometry.translate(0, m_margin/2);
	}

	auto animation = new QPropertyAnimation(popup, "geometry", popup);
	animation->setDuration(m_duration);
	animation->setStartValue(startGeometry);
	animation->setEndValue(endGeometry);
	animation->start(QAbstractAnimation::DeleteWhenStopped);
	m_runningAnimations.insert(popup, animation);

	lconnect(animation, SIGNAL(destroyed(QObject*)), this, [=] {
		m_runningAnimations.remove(popup);
	});
}

void WidgetPlacer::doShow(QuickPopupWidget *popup)
{
	popup->show();
	auto animation = new QPropertyAnimation(popup, "windowOpacity", popup);
	animation->setDuration(m_duration);
	animation->setStartValue(0);
	animation->setEndValue(1);
	animation->start(QAbstractAnimation::DeleteWhenStopped);
}

void WidgetPlacer::doHide(QuickPopupWidget *popup, bool deleteOnClose)
{
	auto animation = new QPropertyAnimation(popup, "windowOpacity", popup);
	animation->setDuration(m_duration);
	animation->setStartValue(1);
	animation->setEndValue(0);
	if (deleteOnClose)
		animation->connect(animation, SIGNAL(destroyed()), popup, SLOT(deleteLater()));
	animation->start(QAbstractAnimation::DeleteWhenStopped);
}

} // Lime
