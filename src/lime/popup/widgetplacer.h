/*
    This file is part of Lime.
    Copyright (C) 2011 Sidorov Aleksey <sauron@citadelspb.com>
    Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CORE_KINETICPOPUPS_WIDGETPLACER_H
#define CORE_KINETICPOPUPS_WIDGETPLACER_H

#include <QObject>
#include <QRect>
#include <QSize>
#include <QHash>

class QPropertyAnimation;

namespace Lime {

class QuickPopupWidget;
typedef QList<QuickPopupWidget*> PopupWidgetList;

class WidgetPlacer : public QObject
{
    Q_OBJECT
public:
	enum Corner
	{
		TopLeft,
		TopRight,
		BottomLeft,
		BottomRight
	};
	explicit WidgetPlacer(QObject *parent = 0);
	virtual ~WidgetPlacer();
	void addWidget(QuickPopupWidget *widget);
	PopupWidgetList widgets() const;
	void setCorner(Corner corner);
	Corner corner() const;
public slots:
	void updatePlace();
	void loadSettings();
private:
	QRect actualGeometry(QuickPopupWidget *popup) const;
	QRect calculateGeometry(int after, QuickPopupWidget *widget) const;
	void doInsert(int after, QuickPopupWidget *widget);
	void doLayout(int first = 0);
	void doMove(QuickPopupWidget *popup,const QRect &endGeometry);
	void doShow(QuickPopupWidget *popup);
	void doHide(QuickPopupWidget *popup, bool deleteOnClose = true);
private:
	PopupWidgetList m_popups;
	WidgetPlacer::Corner m_corner;
	int m_duration;
	int m_margin;
	QHash<QuickPopupWidget*, QPropertyAnimation*> m_runningAnimations;
};

} // Lime

#endif // CORE_KINETICPOPUPS_WIDGETPLACER_H
