/*
    This file is part of Lime.
    Copyright (c) 2011 Aleksey Sidorov <sauron@citadelspb.com>
                       Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "popupappearance.h"
#include <QVBoxLayout>

#include <lime/notification.h>
#include <lime/config.h>
#include <lime/utils.h>
#include <lime/protocol.h>
#include <lime/account.h>
#include <lime/chatunit.h>
#include <lime/notificationfilter.h>
#include <lime/popup/popupmanager.h>

namespace Lime
{

PopupAppearance::PopupAppearance()
{
	auto central = new QWidget(this);
	setWidget(central);
	auto layout = new QVBoxLayout(central);

	auto filterWidget = new NotificationFilterWidget(tr("Show a popup when:"), NotificationFilter(), this);
	layout->addWidget(filterWidget);
	m_comboBox = new QComboBox(this);
	m_comboBox->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);
	auto testButton = new QPushButton(tr("Test"));
	auto hlayout = new QHBoxLayout;
	auto formLayout = new QFormLayout;
	hlayout->addWidget(m_comboBox);
	hlayout->addWidget(testButton);
	formLayout->addRow(tr("Theme:"), hlayout);
	layout->addLayout(formLayout);
	layout->addSpacerItem(new QSpacerItem(0, 20, QSizePolicy::Preferred, QSizePolicy::Expanding));

	bind("types", filterWidget, PopupManager::defaultFilter());
	bind("themeName", m_comboBox, "default");

	lconnect(m_comboBox, SIGNAL(currentIndexChanged(int)), this, [=] {
		emit modified();
		preview();
	});
	lconnect(testButton, SIGNAL(clicked(bool)), this, [=] {
		auto notif = new Notification(Notification::System, tr("Test"), tr("This is a test popup"), 0);
		PopupManager::instance()->showPopup(notif, m_comboBox->currentText());
	});
}

PopupAppearance::~PopupAppearance()
{
}

void PopupAppearance::loadImpl()
{
	Config cfg("", "popup");
	getThemes();
	preview();
	loadBinds(cfg);
}


void PopupAppearance::saveImpl()
{
	Config cfg("", "popup");
	saveBinds(cfg);
}

void PopupAppearance::cancelImpl()
{
	loadImpl();
}

void PopupAppearance::getThemes()
{
	m_comboBox->clear();
	auto list = themes("quickpopup");
	for (auto &theme : list)
		m_comboBox->addItem(theme, theme);
	m_comboBox->setCurrentIndex(0);
}

void PopupAppearance::preview()
{
}

} // Lime
