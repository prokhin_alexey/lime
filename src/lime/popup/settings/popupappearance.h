/*
    This file is part of Lime.
    Copyright (c) 2011 Aleksey Sidorov <sauron@citadelspb.com>
                       Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef POPUPAPPEARANCE_H
#define POPUPAPPEARANCE_H

#include <QWidget>
#include "../kineticpopup.h"
#include <lime/settings.h>

class QComboBox;

namespace Lime
{

class AbstractPopupWidget;
class PopupAppearance : public Lime::SettingsWidget
{
	Q_OBJECT
public:
	PopupAppearance();
	virtual ~PopupAppearance();
	virtual void loadImpl();
	virtual void cancelImpl();
	virtual void saveImpl();
private:
	void getThemes();
	void preview();
	QComboBox *m_comboBox;
};

} // Lime

#endif // POPUPAPPEARANCE_H
