/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_JOINCONFERENCEWIDGET_H
#define LIME_JOINCONFERENCEWIDGET_H

#include "global.h"
#include <QWidget>

class QListWidgetItem;
namespace Ui {
class JoinConference;
}

namespace Lime {

class JoinConferenceWidget : public QWidget
{
	Q_OBJECT
public:
	explicit JoinConferenceWidget(QWidget* parent = 0);
	virtual ~JoinConferenceWidget();
protected:
	void setBookmarkWidget(QWidget *widget);
	void setBookmarkLayout(QLayout *layout);
	void handleBookmark(const QString &name, const QVariant &userData);
protected slots:
	void updateCurrentName(const QString &name);
private slots:
	virtual void save() = 0;
protected:
	virtual void join(const QVariant &userData) = 0;
	virtual void setBookmark(const QVariant &userData) = 0;
	virtual void createBookmark() = 0;
	virtual void removeBookmark(const QVariant &userData) = 0;
private:
	void loadSplitterStateFromConfig();
	Ui::JoinConference *ui;
	QListWidgetItem *m_current;
	QList<QListWidgetItem*> m_bookmarks;
};

} // Lime

#endif // LIME_JOINCONFERENCEWIDGET_H
