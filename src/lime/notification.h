/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_NOTIFICATION_H
#define LIME_NOTIFICATION_H

#include "utils.h"
#include "signal.h"
#include "message.h"
#include "extention.h"
#include <QPixmap>
#include <QIcon>
#include "status.h"
#include "chatunit.h"

namespace Lime {

class Buddy;

class NotificationAction
{
public:
	enum Type
	{
		Accept,
		Reject,
		Additional
	};

	NotificationAction(Type type, const QString &title) :
		m_type(type), m_title(title)
	{}
	~NotificationAction() { emit destroyed(); }
	Type type() const { return m_type; }
	void setType(Type type) { m_type = type; }
	QString title() const { return m_title; }
	void setTitle(const QString &title) { m_title = title; }
	QIcon icon() const { return m_icon; }
	void setIcon(const QIcon &icon) { m_icon = icon; }
// signals:
	Signal<void()> triggered;
	Signal<void()> destroyed;
private:
	Type m_type;
	QString m_title;
	QIcon m_icon;
};

class Notification : public QObject
{
	Q_OBJECT
	Q_ENUMS(Type)
public:
	enum Type
	{
		IncomingMessage,
		OutgoingMessage,
		BlockedMessage,
		IncomingConferenceMessage,
		OutgoingConferenceMessage,
		ParticipantJoined,
		ParticipantLeft,
		UserWentOnline,
		UserWentOffline,
		UserChangedStatus,
		UserTyping,
		UserHasBirthday,
		System,
		AppStartup,
		LastType = AppStartup
	};

	Notification(Type type, const QString &title, const QString &text, QObject *object) :
		m_type(type), m_title(title), m_text(text), m_object(object)
	{}
	~Notification();
	Type type() const { return m_type; }
	void setType(Type type) { m_type = type; }
	QString title() const { return m_title; }
	void setTitle(const QString &title) { m_title = title; }
	QString text() const { return m_text; }
	void setText(const QString &text) { m_text = text; }
	QObject *object() const { return m_object; }
	void setObject(QObject *object) { m_object = object; }
	QPixmap image() const { return m_image; }
	void setImage(const QPixmap &image) { m_image = image; }
	void addAction(NotificationAction *action);
	QList<NotificationAction*> actions() { return m_actions; }
public slots:
	void accept();
	void reject();
	void ignore();
signals:
	void accepted();
	void rejected();
	void ignored();
private:
	void acceptImpl();
	void rejectImpl();
	Type m_type;
	QString m_title;
	QString m_text;
	Pointer<QObject> m_object;
	QPixmap m_image;
	QList<NotificationAction *> m_actions;
};

class NotificationManager : public QObject, public Extension, public SignalGuard
{
	Q_OBJECT
public:
	static NotificationManager *instance() { static auto manager = new NotificationManager; return manager; }
	void handleNewMessage(const Message &message);
	void handleStatusChange(Buddy *buddy, Status newStatus, Status oldStatus, const QString &statusText);
	void handleChatState(ChatUnit *unit, ChatUnit::ChatState state);
protected:
	QObject *toQObject() { return this; }
	bool enableImpl();
	bool disableImpl() { return false; }
private:
	explicit NotificationManager();
};

}

#endif // LIME_NOTIFICATION_H
