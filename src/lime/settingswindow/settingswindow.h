/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_SETTINGSWIDGET_H
#define LIME_SETTINGSWIDGET_H

#include <lime/settings.h>
#include <QWidget>

class QVBoxLayout;
class QListWidget;
class QStackedLayout;
class QSplitter;

namespace Lime {

class SettingsWindow : public QWidget
{
	Q_OBJECT
public:
	SettingsWindow(const QList<SettingsItem*> &items, QWidget *parent = 0);
	static void enable();
	static void disable();
	static void openSettings();
public slots:
	void addItem(SettingsItem *item);
	void removeItem(SettingsItem *item);
private:
	struct Item
	{
		Item() : settingsItem(0) {}
		Item(SettingsItem *settingsItem_) : settingsItem(settingsItem_) {}
		bool operator==(SettingsItem *other) const { return settingsItem == other; }
		SettingsItem *settingsItem;
		Pointer<SettingsWidget> widget;
	};
	SettingsWidget *currentWidget();
	QList<Item> m_items;
	QListWidget *m_settingsList;
	QStackedLayout *m_widgetLayout;

};

} // Lime

#endif // LIME_SETTINGSWIDGET_H
