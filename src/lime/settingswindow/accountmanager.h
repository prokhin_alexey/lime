/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_ACCOUNTMANAGER_H
#define LIME_ACCOUNTMANAGER_H

#include <lime/settings.h>
#include <QWizard>
#include <QIcon>

class QListView;

namespace Lime {

class AccountCreationWizardPrivate;
class Account;
class AccountModel;

class AccountCreationWizard
{
public:
	AccountCreationWizard(const QIcon &icon, const QString &title);
	virtual ~AccountCreationWizard();
	QIcon icon() const;
	QString title() const;
protected:
	int addWizardPage(QWizardPage *page);
	virtual void initializePages() = 0;
private:
	friend class Wizard;
	friend class MainWizardPage;
	QScopedPointer<AccountCreationWizardPrivate> d;
};

class AccountManager : public SettingsWidget, protected SignalGuard
{
public:
	AccountManager();
	Account *currentAccount();
	static void addWizard(AccountCreationWizard *wizard);
protected:
	virtual void loadImpl();
	virtual void saveImpl();
private:
	Account *m_current;
};

} // Lime

#endif // LIME_ACCOUNTMANAGER_H
