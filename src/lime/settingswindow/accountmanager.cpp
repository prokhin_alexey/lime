/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "accountmanager.h"
#include <QBoxLayout>
#include <QListWidget>
#include <QPushButton>
#include <QSpacerItem>
#include <QLabel>
#include <lime/icon.h>
#include <lime/accountmodel.h>
#include <lime/account.h>

namespace Lime
{

static QList<AccountCreationWizard*> allWizards;

class AccountCreationWizardPrivate
{
public:
	QIcon icon;
	QString title;
	QWizard *currentWizard;
	int firstId;
};

class Wizard : public QWizard
{
public:
	Wizard();
    virtual ~Wizard();
private:

};

class MainWizardPage : public QWizardPage
{
public:
    MainWizardPage(Wizard *wizard, QWidget *parent = 0);
    virtual ~MainWizardPage();
    virtual int nextId() const;
    virtual bool isComplete() const;
    virtual bool validatePage();
private:
	QList<AccountCreationWizard*> m_wizards;
	QListWidget *m_protocolList;
	bool m_isComplete;
};

AccountCreationWizard::AccountCreationWizard(const QIcon &icon, const QString &title) :
	d(new AccountCreationWizardPrivate)
{
	d->icon = icon;
	d->title = title;
}

AccountCreationWizard::~AccountCreationWizard()
{
}

QIcon AccountCreationWizard::icon() const
{
	return d->icon;
}

QString AccountCreationWizard::title() const
{
	return d->title;
}

int AccountCreationWizard::addWizardPage(QWizardPage *page)
{
	Q_ASSERT(d->currentWizard);
	auto pageId = d->currentWizard->addPage(page);
	QObject::connect(d->currentWizard, SIGNAL(destroyed(QObject*)), page, SLOT(deleteLater()));
	if (d->firstId == 0)
		d->firstId = pageId;
	return pageId;
}

Wizard::Wizard()
{
    setWizardStyle(ModernStyle);
	setPixmap(QWizard::LogoPixmap, QPixmap(":/icons/64x64/lime.png"));
    setWindowTitle(tr("Add Account Wizard"));
	setStartId(addPage(new MainWizardPage(this, this)));
	setAttribute(Qt::WA_DeleteOnClose);
}

Wizard::~Wizard()
{

}

MainWizardPage::MainWizardPage(Wizard *parentWizard, QWidget *parent) :
	m_isComplete(false)
{
	setTitle(tr("Please choose IM protocol"));
	setPixmap(QWizard::WatermarkPixmap, QPixmap(":/pictures/wizard.png"));

	auto topLabel = new QLabel(tr("This wizard will help you add your account of "
								"chosen protocol. You always can add or delete "
								"accounts from Main settings -> Accounts"), this);
    m_protocolList = new QListWidget(this);
	topLabel->setWordWrap(true);

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->addWidget(topLabel);
    layout->addWidget(m_protocolList);

	m_wizards = allWizards;
	foreach (auto wizard, m_wizards) {
		auto protoItem = new QListWidgetItem(wizard->icon(), wizard->title(), m_protocolList);
		wizard->d->currentWizard = parentWizard;
		wizard->d->firstId = 0;
	}

	lconnect(m_protocolList, SIGNAL(currentRowChanged(int)), this, [this](int row) {
		bool oldIsComplete = m_isComplete;
		m_isComplete = row >= 0 && row < m_wizards.size();
		if (oldIsComplete != m_isComplete)
			emit this->completeChanged();
	});
}

MainWizardPage::~MainWizardPage()
{

}

int MainWizardPage::nextId() const
{
	if (m_wizards.isEmpty())
		return -1;
	auto wizard = m_wizards.value(m_protocolList->currentRow());
	if (!wizard)
		return 0;
	if (wizard->d->firstId == 0) {
		wizard->initializePages();
		if (wizard->d->firstId == 0)
			wizard->d->firstId = -1;
	}
	return wizard->d->firstId;
}

bool MainWizardPage::isComplete() const
{
	return m_isComplete;
}

bool MainWizardPage::validatePage()
{
	return m_isComplete;
}

AccountManager::AccountManager()
{
	auto central = new QWidget(this);
	auto mainLayout = new QHBoxLayout(central);
	auto buttonLayout = new QVBoxLayout;
	auto addButton = new QPushButton(Icon("list-add-user-account"), tr("Add"), this);
	auto enabledDisableButton = new QPushButton(Icon("list-edit-user-account"), tr("Enable"), this);
	auto removeButton = new QPushButton(Icon("list-remove-user-account"), tr("Remove"), this);
	auto accountListWidget = new QListView(this);
	auto accountModel = new AccountModel(0, this);

	accountListWidget->setModel(accountModel);
	accountListWidget->setItemDelegate(new AccountDelegate(this));
	enabledDisableButton->setDisabled(true);
	removeButton->setDisabled(true);

	mainLayout->addWidget(accountListWidget);
	mainLayout->addLayout(buttonLayout);
	buttonLayout->addWidget(addButton);
	buttonLayout->addWidget(enabledDisableButton);
	buttonLayout->addWidget(removeButton);
	buttonLayout->addSpacerItem(new QSpacerItem(0, 20, QSizePolicy::Preferred, QSizePolicy::Expanding));
	setWidget(central);

	lconnect(addButton, SIGNAL(clicked(bool)), this, [=] {
		static Pointer<Wizard> wizard;
		if (!wizard) {
			wizard = new Wizard;
			addButton->setEnabled(false);
			wizard->show();
			lconnect(wizard, SIGNAL(destroyed(QObject*)), addButton, [addButton, &wizard]() {
				addButton->setEnabled(true);
			});
		}
		wizard->raise();
	});

	lconnect(enabledDisableButton, SIGNAL(clicked(bool)), this, [=] {
		if (!m_current)
			return;
		if (m_current->isEnabled())
			m_current->disable();
		else
			m_current->enable();
	});

	lconnect(accountListWidget->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)), this,
		[=](const QModelIndex &current)
	{
		enabledDisableButton->setEnabled(current.isValid());
		removeButton->setEnabled(current.isValid());
		if (current.isValid()) {
			m_current = accountModel->accounts().value(accountListWidget->currentIndex().row());
			Q_ASSERT(m_current);
			enabledDisableButton->setText(m_current->isEnabled() ? tr("Disable") : tr("Enable"));;
		}
	});

	Account::onEnabled.connect(this, [=](Account *acc) {
		if (acc == m_current)
			enabledDisableButton->setText(tr("Disable"));
	});
	Account::onDisabled.connect(this, [=](Account *acc) {
		if (acc == m_current)
			enabledDisableButton->setText(tr("Enable"));
	});
}

void AccountManager::loadImpl()
{
}

void AccountManager::saveImpl()
{
}

void AccountManager::addWizard(AccountCreationWizard *wizard)
{
	allWizards << wizard;
}


} // Lime

