/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "settingswindow.h"
#include <QBoxLayout>
#include <QSplitter>
#include <QDialogButtonBox>
#include <QListWidget>
#include <QStackedLayout>
#include <QApplication>
#include <QLayoutItem>
#include <QDesktopWidget>
#include "accountmanager.h"
#include <lime/contactlist/contactlist.h>
#include <lime/icon.h>
#include <lime/config.h>
#include <lime/tray/tray.h>

namespace Lime
{

static Pointer<SettingsWindow> m_window;

SettingsWindow::SettingsWindow(const QList<SettingsItem*> &items_, QWidget *parent)
{
	auto geometry = Config(QString(), "settingsWindow").value<QByteArray>("geometry");
	if (!geometry.isEmpty()) {
		restoreGeometry(geometry);
	} else {
		QRect screen = QApplication::desktop()->screenGeometry(QCursor::pos());
		setGeometry(screen.adjusted(300, 200, -300, -200));
	}

	auto mainLayout = new QVBoxLayout(this);
	auto splitter = new QSplitter(this);
	auto buttonBox = new QDialogButtonBox(QDialogButtonBox::Save | QDialogButtonBox::Cancel, Qt::Horizontal, this);
	auto mainWidget = new QWidget(this);
	m_widgetLayout = new QStackedLayout(mainWidget);
	m_settingsList = new QListWidget(this);

	mainWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	splitter->addWidget(m_settingsList);
	splitter->addWidget(mainWidget);
	splitter->setSizes(QList<int>() << 150 << 500);
	mainLayout->addWidget(splitter);
	mainLayout->addWidget(buttonBox);
	m_widgetLayout->setContentsMargins(0, 0, 0, 0);

	auto items = items_;
	qSort(items.begin(), items.end(), [](SettingsItem *lhs, SettingsItem *rhs) {
		return QString::localeAwareCompare(lhs->title(), rhs->title()) < 0;
	});
	foreach (auto &item, items)
		addItem(item);

	lconnect(m_settingsList, SIGNAL(currentRowChanged(int)), this, [this](int index) {
		auto oldWidget = this->currentWidget();
		if (oldWidget && !oldWidget->isModified())
			oldWidget->deleteLater();

		if (index < 0 || index >= m_items.count())
			return;

		auto &item = m_items[index];
		if (!item.widget) {
			item.widget = item.settingsItem->createWidget();
			item.widget->load();
			m_widgetLayout->addWidget(item.widget);
			connect(this, SIGNAL(destroyed(QObject*)), item.widget, SLOT(deleteLater()));
		}
		m_widgetLayout->setCurrentWidget(item.widget);
	});
	lconnect(buttonBox, SIGNAL(accepted()), this, [this]() {
		auto currentWidget = this->currentWidget();
		foreach (auto &item, m_items) {
			if (item.widget) {
				item.widget->save();
				if (currentWidget != item.widget)
					item.widget->deleteLater();
			}
		}
	});
	lconnect(buttonBox, SIGNAL(rejected()), this, [this]() {
		auto currentWidget = this->currentWidget();
		foreach (auto &item, m_items) {
			if (item.widget) {
				if (currentWidget != item.widget)
					item.widget->deleteLater();
				else
					item.widget->cancel();
			}
		}
	});

	m_settingsList->setCurrentRow(0);
}

void SettingsWindow::enable()
{
	auto act = new Action(Icon("configure"), tr("Settings..."));
	act->setType(Action::ContextMenuAction);
	act->setPriority(Action::HighPriority);
	ActionContainer::addAction<ContactList>(act);
	ActionContainer::addAction<Tray>(act);
	lconnect(act, SIGNAL(triggered(ActionContainer*,bool)), qApp, []() {
		SettingsWindow::openSettings();
	});

	SettingsManager::addGlobalSettings<AccountManager>(Icon("meeting-attending"), QObject::tr("Accounts"));
}

void SettingsWindow::disable()
{
	delete m_window.data();
}

void SettingsWindow::openSettings()
{
	if (!m_window)
		m_window = new SettingsWindow(SettingsManager::items());
	m_window->show();
}

void SettingsWindow::addItem(SettingsItem *item)
{
	foreach (auto &i, m_items) {
		if (i == item)
			return;
	}

	auto listItem = new QListWidgetItem(item->icon(), item->title(), m_settingsList);
	m_items << Item(item);
	item->destroyed.connect([this, item]() {
		this->removeItem(item);
	});
}

void SettingsWindow::removeItem(SettingsItem *item)
{
	Q_ASSERT(m_items.size() > 0);
	int index = 0;
	for (int c = m_items.size(); index < c; ++index) {
		if (m_items.at(index) == item)
			break;
	}
	Q_ASSERT(m_items.count() >= index);
	m_items.removeAt(index);
	delete m_settingsList->takeItem(index);
}

SettingsWidget *SettingsWindow::currentWidget()
{
	auto widget = m_widgetLayout->currentWidget();
	return widget ? sender_cast<SettingsWidget*>(widget) : 0;
}

} // Lime

