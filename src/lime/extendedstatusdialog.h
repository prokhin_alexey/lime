/*
	 This file is part of Lime.
	 Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIME_EXTENDEDSTATUSDIALOG_H_
#define LIME_EXTENDEDSTATUSDIALOG_H_

#include "global.h"
#include <QWidget>
#include <QStack>
#include <QVariant>

class QTreeWidget;
class QTreeWidgetItem;
class QLineEdit;
class QTextEdit;

namespace Lime
{

class ExtendedStatusDialog : public QWidget
{
	Q_OBJECT
public:
	enum Flags
	{
		HasTitle = 0x0001,
		HasDescrition = 0x0002
	};
	typedef QTreeWidgetItem *Item;
	ExtendedStatusDialog(int flags, QWidget *parent = 0);
	virtual ~ExtendedStatusDialog();
	Item beginGroup(const QIcon &icon, const QString &title, const QVariant &userData = QVariant());
	void endGroup();
	Item addStatus(const QIcon &icon, const QString &title, const QVariant &userData);
	void finish(const QIcon &emptyIcon, const QString &emptyTitle);
	void setTitle(const QString &title);
	void setDescription(const QString &description);
	void setCurrentItem(Item item);
	QString title();
	QString description();
	QVariant currentUserData();
signals:
	void accepted();
	void rejected();
private:
	QTreeWidgetItem *createItem(const QIcon &icon, const QString &title, const QVariant &userData);
private:
	QTreeWidget *m_statusTree;
	QStack<QTreeWidgetItem*> m_groups;
	QTreeWidgetItem *m_emptyItem;
	QLineEdit *m_titleEdit;
	QTextEdit *m_descriptionEdit;
};

} // Lime
#endif // LIME_EXTENDEDSTATUSDIALOG_H_
