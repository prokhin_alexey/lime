/*
    This file is part of Lime.
    Copyright (c) 2010  Nigmatullin Ruslan <euroelessar@gmail.com>
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "settings.h"
#include "utils.h"
#include "notificationfilter.h"
#include <QAbstractButton>
#include <QAbstractSlider>
#include <QComboBox>
#include <QDateTimeEdit>
#include <QLineEdit>
#include <QListWidget>
#include <QSpinBox>
#include <QSignalMapper>
#include <QMetaProperty>
#include <QCheckBox>
#include <algorithm>

namespace Lime {

static QMultiMap<const QMetaObject *, SettingsItem*> settingsItems;

class AbstractWidgetInfo
{
public:
	AbstractWidgetInfo(const char *p, const char *s) : property(p), signal(s) {}
	~AbstractWidgetInfo() {}
	virtual bool handle(QObject *obj) = 0;
	const char * const property;
	const char * const signal;
};

template <typename T>
class WidgetMetaInfo : public AbstractWidgetInfo
{
public:
	WidgetMetaInfo(const char *p, const char *s) : AbstractWidgetInfo(p, s) {}
	virtual bool handle(QObject *obj) { return qobject_cast<T*>(obj) != NULL; }
};

static AbstractWidgetInfo * widget_infos[] = {
	new WidgetMetaInfo<QAbstractButton>         ("checked",      SIGNAL(toggled(bool))),
	new WidgetMetaInfo<QAbstractSlider>         ("value",        SIGNAL(valueChanged(int))),
	new WidgetMetaInfo<QComboBox>               ("currentIndex", SIGNAL(currentIndexChanged(int))),
	new WidgetMetaInfo<QDateTimeEdit>           ("dateTime",     SIGNAL(dateTimeChanged(QDateTime))),
	new WidgetMetaInfo<QLineEdit>               ("text",         SIGNAL(textChanged(QString))),
	new WidgetMetaInfo<QListWidget>             ("currentRow",   SIGNAL(currentRowChanged(int))),
	new WidgetMetaInfo<QSpinBox>                ("value",        SIGNAL(valueChanged(int))),
	new WidgetMetaInfo<NotificationFilterWidget>("values",       SIGNAL(valuesChanged()))
};

struct WidgetInfo
{
	Pointer<QWidget> obj;
	const char *property;
	QVariant value;
	bool is_changed;
};

struct SettingsWidgetPrivate
{
	QSignalMapper *mapper;
	QList<WidgetInfo> infos;
	uint changed_num;
	bool sleep; // Used to block signal sending on widget changing it's status
	QList<function<void(const Config &)> > loaders;
	QList<function<void(Config &)> > savers;
	void clearValues();
};

void SettingsWidgetPrivate::clearValues()
{
	for(int i = 0, size = infos.size(); i < size; i++)
	{
		WidgetInfo &info = infos[i];
		if(info.obj)
			info.value = info.obj->property(info.property);
		else
			info.value.clear();
		info.is_changed = false;
	}
	changed_num = 0;
}

SettingsWidget::SettingsWidget(QWidget *parent) :
	QScrollArea(parent),
	p(new SettingsWidgetPrivate)
{
	p->mapper = new QSignalMapper(this);
	connect(p->mapper, SIGNAL(mapped(int)), this, SLOT(onStateChanged(int)));
	p->changed_num = 0;
	p->sleep = true;
	setWidgetResizable(true);
}

SettingsWidget::~SettingsWidget()
{
}

bool SettingsWidget::isModified() const
{
	return p->changed_num > 0;
}

void SettingsWidget::load()
{
	p->sleep = true;
	loadImpl();
	p->clearValues();
	p->sleep = false;
}

void SettingsWidget::save()
{
//		if(!isModified())
//			return;
	p->sleep = true;
	saveImpl();
	p->clearValues();
	p->sleep = false;
	emit saved();
}

void SettingsWidget::cancel()
{
	if(!isModified())
		return;
	p->sleep = true;
	for(int i = 0, size = p->infos.size(); i < size; i++)
	{
		WidgetInfo &info = p->infos[i];
		if(info.obj && info.is_changed)
			 info.obj->setProperty(info.property, info.value);
		info.is_changed = false;
	}
	cancelImpl();
	p->changed_num = 0;
	p->sleep = false;
}

void SettingsWidget::cancelImpl()
{
	loadImpl();
}

#ifdef Q_CC_MSVC
#   pragma warning (push)
#   pragma warning (disable : 4996) // MSVC thinks that QList is unsafe
#endif
void SettingsWidget::listenChildrenStates(const QWidgetList &exceptions)
{
	QWidgetList widgets;
	if (!exceptions.isEmpty()) {
		QWidgetList ls1 = findChildren<QWidget *>();
		QWidgetList ls2 = exceptions;
		std::sort(ls1.begin(), ls1.end());
		std::sort(ls2.begin(), ls2.end());
		std::set_difference(ls1.begin(), ls1.end(), ls2.begin(), ls2.end(),
							std::insert_iterator<QWidgetList>(widgets, widgets.begin()));
	} else {
		widgets = findChildren<QWidget *>();
	}
	foreach (QWidget *widget, widgets)
		lookForWidgetState(widget);
}
#ifdef Q_CC_MSVC
#   pragma warning (pop)
#endif

const char *SettingsWidget::lookForWidgetState(QWidget *widget, const char *property, const char *signal)
{
	const QMetaObject *meta = widget->metaObject();
	WidgetInfo info = { widget, NULL, QVariant(), false };
	bool free_signal = false;
	// Firstly try to search this widget in predefined classes
	if (!signal && !property) {
		for (int i = 0, size = sizeof(widget_infos) / sizeof(AbstractWidgetInfo*); i < size; i++) {
			if (widget_infos[i]->handle(widget)) {
				info.property = widget_infos[i]->property;
				signal = widget_infos[i]->signal;
				break;
			}
		}
	}
	// Then try to find "User" property with signal or signal by property
	if (!signal) {
		for (int i = 0, size = meta->propertyCount(); i < size; i++) {
			QMetaProperty prop = meta->property(i);
			if (prop.hasNotifySignal()
				&& ((property && !qstrcmp(prop.name(), property))
					|| (!property && prop.isUser()))) {
				info.property = prop.name();
				const char *sig = prop.notifySignal().signature();
				int len = strlen(sig);
				char *str = (char *)qMalloc(sizeof(char) * (len + 2));
				str[0] = QSIGNAL_CODE;
				qstrcpy(str + 1, sig);
				signal = str;
				free_signal = true;
				break;
			}
		}
	}
	bool result(signal);
	if (result) {
		p->mapper->setMapping(widget, p->infos.size());
		connect(widget, signal, p->mapper, SLOT(map()));
		p->infos << info;
	}
	if (free_signal)
		qFree((void *)signal);
	return result ? info.property : 0;
}

void SettingsWidget::bind(const QString &configEntry, QCheckBox *box, bool defaultValue, bool reversed)
{
	if (reversed) {
		auto loader = [=](const Config &cfg) {
			box->setChecked(!cfg.value(configEntry, defaultValue));
		};
		auto saver = [=](Config &cfg) {
			cfg.setValue(configEntry, !box->isChecked());
		};
		addBind(loader, saver);
	} else {
		auto loader = [=](const Config &cfg) {
			box->setChecked(cfg.value(configEntry, defaultValue));
		};
		auto saver = [=](Config &cfg) {
			cfg.setValue(configEntry, box->isChecked());
		};
		addBind(loader, saver);
	}
	lookForWidgetState(box);
}

void SettingsWidget::bind(const QString &configEntry, QLineEdit *edit, const QString &defaultValue)
{
	auto loader = [=](const Config &cfg) {
		edit->setText(cfg.value(configEntry, defaultValue));
	};
	auto saver = [=](Config &cfg) {
		cfg.setValue(configEntry, edit->text());
	};
	addBind(loader, saver);
	lookForWidgetState(edit);
}

void SettingsWidget::bind(const QString &configEntry, QSlider *slider, int defaultValue)
{
	auto loader = [=](const Config &cfg) {
		slider->setValue(cfg.value(configEntry, defaultValue));
	};
	auto saver = [=](Config &cfg) {
		cfg.setValue(configEntry, slider->value());
	};
	addBind(loader, saver);
	lookForWidgetState(slider);
}

void SettingsWidget::bind(const QString &configEntry, QComboBox *comboBox, const QString &defaultValue)
{
	auto loader = [=](const Config &cfg) {
		int index = comboBox->findText(cfg.value(configEntry, defaultValue));
		if (index == -1 && comboBox->isEditable())
			comboBox->setEditText(defaultValue);
		else
			comboBox->setCurrentIndex(index);
	};
	auto saver = [=](Config &cfg) {
		cfg.setValue(configEntry, comboBox->currentText());
	};
	addBind(loader, saver);
	lookForWidgetState(comboBox);
}

void SettingsWidget::bind(const QString &configEntry, NotificationFilterWidget *filter, const NotificationFilter &defaultValues)
{
	auto loader = [=](const Config &cfg) {
		filter->setValues(NotificationFilter::loadFromConfig(cfg, configEntry, defaultValues));
	};
	auto saver = [=](Config &cfg) {
		NotificationFilter::saveToConfig(cfg, configEntry, filter->values());
	};
	addBind(loader, saver);
	lookForWidgetState(filter);
}

void SettingsWidget::saveBinds(Config &cfg)
{
	foreach (auto &saver, p->savers)
		saver(cfg);
}

void SettingsWidget::loadBinds(const Config &cfg)
{
	foreach (auto &loader, p->loaders)
		loader(cfg);
}

void SettingsWidget::addBind(const function<void(const Config &cfg)> &loader, const function<void(Config &cfg)> &saver)
{
	p->loaders << loader;
	p->savers << saver;
}

void SettingsWidget::onStateChanged(int index)
{
	if (index < 0 || index >= p->infos.size() || p->sleep)
		return;
	WidgetInfo &info = p->infos[index];
	QVariant value = info.obj->property(info.property);
	bool equal = info.value == value;
	if (equal && info.is_changed)
		p->changed_num--;
	else if (!info.is_changed && !equal)
		p->changed_num++;
	else
		return;
	info.is_changed = !equal;
	if (p->changed_num == 0 && equal)
		emit modified();
	else if (p->changed_num == 1 && !equal)
		emit unmodified();
}

void SettingsWidget::virtual_hook(int id, void *data)
{
	Q_UNUSED(id);
	Q_UNUSED(data);
}

class SettingsItemPrivate
{
public:
	QIcon icon;
	QString title;
	int priority;
	function<SettingsWidget*()> creationFunc;
};

SettingsItem::SettingsItem(const QIcon &icon, const QString &title, function<SettingsWidget*()> creationFunc) :
	d_ptr(new SettingsItemPrivate)
{
	Q_D(SettingsItem);
	d->icon = icon;
	d->title = title;
	d->creationFunc = creationFunc;
}

SettingsItem::SettingsItem(const QString &title, function<SettingsWidget*()> creationFunc) :
	d_ptr(new SettingsItemPrivate)
{
	Q_D(SettingsItem);
	d->title = title;
	d->creationFunc = creationFunc;
}

SettingsItem::~SettingsItem()
{
	destroyed();
}

void SettingsItem::setIcon(const QIcon &icon)
{
	d_ptr->icon = icon;
}

QIcon SettingsItem::icon() const
{
	return d_ptr->icon;
}

void SettingsItem::setTitle(const QString &title)
{
	d_ptr->title = title;
}

QString SettingsItem::title() const
{
	return d_ptr->title;
}

void SettingsItem::setPriority(int priority)
{
	d_ptr->priority = priority;
}

int SettingsItem::priority() const
{
	return d_ptr->priority;
}

SettingsWidget *SettingsItem::createWidget()
{
	return d_ptr->creationFunc();
}

QList<SettingsItem*> SettingsManager::items()
{
	return items(0);
}

void SettingsManager::addGlobalSettings(SettingsItem *item)
{
	addSettings(0, item);
}

SettingsItem *SettingsManager::addGlobalSettings(const QIcon &icon, const QString &title,
												 function<SettingsWidget*()> creationFunc)
{
	auto item = new SettingsItem(icon, title, creationFunc);
	addGlobalSettings(item);
	return item;
}

QList<SettingsItem*> SettingsManager::items(const QMetaObject *meta)
{
	QList<SettingsItem*> items;
	if (meta) {
		do {
			items += settingsItems.values(meta);
		} while (!!(meta = meta->superClass()));
	} else {
		items = settingsItems.values(0);
	}

	qSort(items.begin(), items.end(), [](SettingsItem *lhs, SettingsItem *rhs)->bool {
		int diff = lhs->priority() - rhs->priority();
		if (diff != 0)
			return diff > 0;
		else
			return QString::localeAwareCompare(lhs->title(), rhs->title()) > 0;
	});
	return items;
}

void SettingsManager::addSettings(const QMetaObject *meta, SettingsItem *item)
{
	settingsItems.insert(meta, item);
	item->destroyed.connect([item]() {
		auto itr = settingsItems.begin(), end = settingsItems.end();
		while (itr != end) {
			if (*itr == item)
				itr = settingsItems.erase(itr);
			else
				++itr;
		}
	});
}

} // Lime

