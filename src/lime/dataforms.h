/*
	This file is part of Lime.
	Copyright (C) 2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LIME_DATAFORMS_H
#define LIME_DATAFORMS_H

#include "signal.h"

#include <QDateTime>
#include <QWidget>

class QDialogButtonBox;
class QValidator;
class QFormLayout;
class QVBoxLayout;

#if ENABLE_UNITTESTS
class QLabel;
#endif

namespace Lime {

class DataFormLayout;

/** The DataField class is the base of all fields of \ref DataForm "data forms".
 *
 * A data field is a base part of data forms. It represents a widget for
 * displaying and usually editing value(). The value can be changed at any time
 * by calling setValue(). Whenever the value has been changed, either by setValue()
 * or as a result of a user action, onValueChanged signal is emitted.
 *
 * Each field can have a \ref label() "label" that is usually shown at the right side
 * of the field widget. setLabel() changes the label. The label is allowed to be an empty
 * string.
 *
 * A data field may have \ref name() "name" for identification purposes, call setName() to
 * change it.
 */
class DataField
{
#ifndef Q_MOC_RUN
	/**  Holds whether the field is enabled.
	 *
	 * Unlike an enabled field, a disabled field ignores keyboard, mouse and touch events.
	 * (i.e. the user will not be able to edit the \ref value() "value" of the field).
	 *
	 * Also, most fields are displayed differently when they are disabled. For instance,
	 * StringField might have a gray background when it is disabled.
	 *
	 * By default, the property is true.
	 */
	Q_PROPERTY(bool enabled READ isEnabled WRITE setEnabled)
	/** Holds true if the field is currently visible, otherwise holds false.

	 * If the property is false, the field (including the \ref label() "label")
	 * is completely invisible for the user.
	 *
	 * By default, the property is true.
	 */
	Q_PROPERTY(bool visible READ isVisible WRITE setVisible)
#endif
public:
	virtual ~DataField();
	/** Returns the name of the field.
	 *  \see setName()
	 */
	virtual QString name() = 0;
	/** Changes the name of the field to be \a name.
	 * \see name()
	 */
	virtual void setName(const QString &name) = 0;
	/** Return the label of the field
	 * \see setLabel()
	 */
	virtual QString label() = 0;
	/** Sets the label of the field to be \a label.
	 *
	 * The \a label can be an empty string.
	 * \see label()
	 */
	virtual void setLabel(const QString &label) = 0;
	/** Returns the current value of the field.
	 *
	 * The value has different type depending on the type of the field.
	 * See documentation of classes that inherits the class for more
	 * information about the type of a value that is returned by the method.
	 *
	 * \see setValue()
	 */
	virtual QVariant value() = 0;
	/** Sets the value of the field to be \a value.
	 *
	 * The value has different type depending on the type of the field.
	 * See documentation of classes that inherits the class for more
	 * information about the type of a value that can be set by the method.
	 *
	 * \see value()
	 */
	virtual void setValue(const QVariant &value) = 0;
	virtual bool isVisible() = 0;
	virtual void setVisible(bool visible) = 0;
	virtual bool isEnabled() = 0;
	virtual void setEnabled(bool enabled) = 0;
	/** The signal is emitted whenever the \ref value() "value" of the field is changed.
	 *
	 * \see setValue(), value()
	 */
	Signal<void()> onValueChanged;
protected:
	DataField();
#if ENABLE_UNITTESTS
private:
	friend class DataFormsTest;
	virtual QWidget *widget() = 0;
	virtual QLabel *labelWidget() = 0;
#endif
};

/** A base class of data fields the data of which can be validated.
 *
 * The class allows to place any arbitrary constraints on the value which
 * may be entered by user. setValidator() sets a validator that is used
 * to check whether the value() is invalid or accepted.
 */
class ValidatedField : public DataField
{
public:
	/** Sets the input \a validator for the value() of the field.
	 *
	 * If \a validator is null, removes the current input validator.
	 *
	 * The initial setting is to have no input validator.
	 *
	 * \see validator()
	 */
	virtual void setValidator(QValidator *validator) = 0;
	/** Returns validator of the field. Or null, if the field
	 * does not have a validator.
	 *
	 * \see setValidator()
	 */
	virtual const QValidator *validator() const = 0;

	/** \property setMandatory isMandatory
	 *
	 * \brief Holds whether the field is mandatory.
	 *
	 * If at least one mandatory field of a data form is not \ref isComplete() "complete",
	 * the data form is not complete as well.
	 *
	 * \see setMandatory()
	 */
	virtual bool isMandatory() const = 0;
	virtual void setMandatory(bool mandatory) = 0;
	/** Returns true if the field is complete, if it is incomplete, returns false.
	 *
	 * If a field has a validator, it is complete when validate() method of the validator
	 * returns QValidator::Acceptable, if the field does not have a validator, it is
	 * complete when value() is not empty().
	 *
	 * In all other cases, the field is incomplete.
	 *
	 * \see DataForm::isComplete()
	 */
	virtual bool isComplete() const = 0;
};

/** The field provides the user with a list of options.
 *
 * The field provides a means of presenting a list of text options to the user. The user
 * can choose one of the option or, if the field is \ref isEditable() "editable", enter
 * a new text value.
 *
 * value() method returns the currently chosen (or entered) value. The type of value()
 * is QVariant::String. If you will try to \ref setValue() "set value" that has another type,
 * no current option will be set.
 *
 * If a line of text that passed into setValue() method is one of the options, the option
 * will be chosen. If there are no such option and the field is \ref isEditable() "editable",
 * the currently entered text will be replaced with the line. If there are no such option
 * and the field is not editable, no current option will be set, however, when the option will
 * be added later the option will be chosen. For example, the code is perfectly correct,
 * even though the current option is set before it was added via addOption():
 * \code
 * 		auto stringChooser = dataForm->addStringChooser(tr("Chooser"));
 * 		stringChooser->setData("Second");
 * 		stringChooser->addOption("First");
 * 		stringChooser->addOption("Second");
 * \endcode
 *
 * Every option can has user data that is usually used to identify the option. To retrieve
 * user data of the current option, use currentUserData().
 */
class StringChooserField : public ValidatedField
{
#ifndef Q_MOC_RUN
	/**  Holds whether the field is editable.
	 *
	 * If the field is editable, the user will be able to enter a new value in
	 * addition to provided \ref options() "options".
	 *
	 * By default, that property is false.
	 */
	Q_PROPERTY(bool editable READ isEditable WRITE setEditable)
#endif
public:
	/** Adds a new \a option that has \a userData.
	 *
	 * \see removeOption(), options()
	 */
	virtual void addOption(const QString &option, const QVariant &userData = QVariant()) = 0;
	/** Removes an \a option.
	 *
	 * \see addOption(), options()
	 */
	virtual void removeOption(const QString &option) = 0;
	/** Adds a list of \a options.
	 *
	 * Lengths of \a options and \a userData must be same.
	 *
	 * \see addOption(), options()
	 */
	virtual void addOptions(const QStringList &options, const QVariantList &userData = QVariantList());
	/** Returns a list of current options.
	 */
	virtual QStringList options() const = 0;
	virtual void setEditable(bool editable) = 0;
	virtual bool isEditable() const = 0;
	/** Returns the user data of the current options.
	 *
	 * \see addOption(), addOptions()
	 */
	virtual QVariant currentUserData() = 0;
};

/** The field allows the user to edit a single line of plain text.
 *
 * value() method returns the currently entered text. The type of value()
 * is QVariant::String. If you will try to \ref setValue() "set value" that has another type,
 * the entered text will be clear.
 */
class StringField : public ValidatedField
{
#ifndef Q_MOC_RUN
	/** Holds whether the field is in password mode.
	 *
	 * If a field is in a password mode, asterisks will be displayed instead of the
	 * characters that were actually entered.
	 *
	 * By default, that property is false.
	 */
	Q_PROPERTY(bool passwordMode READ isPasswordMode WRITE setPasswordMode)
#endif
public:
	virtual void setPasswordMode(bool passwordMode) = 0;
	virtual bool isPasswordMode() const = 0;
};

/** The field displays multiple lines of plain text, allows the user to edit them,
 * to add a new line and remove an old one.
 *
 * value() method returns QStringList of the lines. If you will try to \ref setValue() "set value"
 * that has a type that is not QVariant::StringList, the all currently entered lines will be removed.
 */
class StringListField : public ValidatedField
{
};

/** The field allows the user to edit a plain text that consists of more than one line.
 *
 * value() method returns the currently entered text. The type of value()
 * is QVariant::String. If you will try to \ref setValue() "set value" that has another type,
 * the entered text will be clear.
 */
class MultiStringField : public DataField
{
public:
};

/** The field provides the user with two options.
 *
 * The field usually implemented as a check box. If the check box is checked,
 * value() returns true, otherwise it returns false. setValue() accepts only QVariant::Bool,
 * if a variant has another type, the check box will be unchecked.
 */
class BoolField : public DataField
{
public:
};

/** The field allows the user to enter an integer number.
 *
 * value() returns a variant with type QVariant::Int. setValue() ignores all values of other
 * types.
 *
 */
class IntField : public DataField
{
#ifndef Q_MOC_RUN
	/** Holds minimum value of the field.
	 *
	 * The default minimum is 0.
	 */
	Q_PROPERTY(int minimum READ minimum WRITE setMinimum)
	/** Holds maximum value of the field.
	 *
	 * The default maximum is 99.
	 */
	Q_PROPERTY(int maximum READ maximum WRITE setMaximum)
#endif
public:
	/** Convenience function to set the minimum, and maximum values with a single function call.
	 * \code
	 * 		setRange(minimum, maximum);
	 * \endcode
	 * 	is equivalent to:
	 * \code
	 * 	 setMinimum(minimum);
	 * 	 setMaximum(maximum);
	 * \endcode
	 */
	void setRange(int minimum, int maximum);
	virtual int minimum() const = 0;
	virtual int maximum() const = 0;
	virtual void setMaximum(int maximum) = 0;
	virtual void setMinimum(int minimum) = 0;
};

/** The field allows the user to enter a floating-point number.
 *
 * value() returns a variant with type QVariant::Double. setValue() ignores all values of other
 * types.
 */
class DoubleField : public DataField
{
#ifndef Q_MOC_RUN
	/** Holds minimum value of the field.
	 *
	 * The default minimum is 0.
	 */
	Q_PROPERTY(double minimum READ minimum WRITE setMinimum)
	/** Holds maximum value of the field.
	 *
	 * The default maximum is 99.99.
	 */
	Q_PROPERTY(double maximum READ maximum WRITE setMaximum)
#endif
public:
	/** Convenience function to set the minimum, and maximum values with a single function call.
	 * \code
	 * 		setRange(minimum, maximum);
	 * \endcode
	 * 	is equivalent to:
	 * \code
	 * 	 setMinimum(minimum);
	 * 	 setMaximum(maximum);
	 * \endcode
	 */
	void setRange(double minimum, double maximum);
	virtual double minimum() const = 0;
	virtual double maximum() const = 0;
	virtual void setMaximum(double maximum) = 0;
	virtual void setMinimum(double minimum) = 0;
};

/** A base class for fields that provides means to show and edit dates and times.
 */
class AbstractDateTimeField : public DataField
{
#ifndef Q_MOC_RUN
	/** This property holds the format used to display the time/date of the date time edit.
	 *
	 * This format is the same as the one used described in QDateTime::toString() and QDateTime::fromString()
	 *
	 * Example format strings (assuming that the date is 2nd of July 1969):
	 * <table>
	 *     <tr>
	 *          <th>Format</th>
	 *          <th>Result</th>
	 *     </tr>
	 *     <tr>
	 * 	        <td>dd.MM.yyyy</td>
	 *          <td>02.07.1969</td>
	 *     </tr>
	 *     <tr>
	 * 	        <td>MMM d yy</td>
	 *          <td>Jul 2 69</td>
	 *     </tr>
	 *     <tr>
	 * 	        <td>MMMM d yy</td>
	 *          <td>July 2 69</td>
	 *     </tr>
	 * </table>
	 *
	 * Note that if you specify a two digit year, it will be interpreted to be in the century in which
	 * the date time edit was initialized. The default century is the 21 (2000-2099).
	 *
	 * If you specify an invalid format the format will not be set.
	 */
	Q_PROPERTY(double displayFormat READ displayFormat WRITE displayFormat)
#endif
public:
	virtual void setDisplayFormat(const QString &format) = 0;
	virtual QString displayFormat() const= 0;
};

/** The field allows the user to edit a date.
 *
 * value() method returns a variant with type QVariant::Date. setValue() ignores all values
 * of other types.
 */
class DateField : public AbstractDateTimeField
{
#ifndef Q_MOC_RUN
	/** Holds minimum value of the field.
	 *
	 * By default, this property contains a date that refers to September 14, 1752.
	 */
	Q_PROPERTY(double minimum READ minimum WRITE setMinimum)
	/** Holds maximum value of the field.
	 *
	 * By default, this property contains a date that refers to December 31, 7999.
	 */
	Q_PROPERTY(double maximum READ maximum WRITE setMaximum)
#endif
public:
	/** Convenience function to set the minimum, and maximum values with a single function call.
	 * \code
	 * 		setRange(minimum, maximum);
	 * \endcode
	 * 	is equivalent to:
	 * \code
	 * 	 setMinimum(minimum);
	 * 	 setMaximum(maximum);
	 * \endcode
	 */
	void setRange(const QDate &minimum, const QDate &maximum);
	virtual QDate minimum() const = 0;
	virtual QDate maximum() const = 0;
	virtual void setMaximum(const QDate &maximum) = 0;
	virtual void setMinimum(const QDate &minimum) = 0;
};

/** The field allows the user to edit dates and times.
 *
 * value() method returns a variant with type QVariant::DateTime. setValue() ignores all values
 * of other types.
 */
class DateTimeField : public AbstractDateTimeField
{
#ifndef Q_MOC_RUN
	/** Holds minimum value of the field.
	 *
	 * By default, this property contains a date that refers to September 14, 1752
	 * and a time of 00:00:00 and 0 milliseconds.
	 */
	Q_PROPERTY(QDateTime minimum READ minimum WRITE setMinimum)
	/** Holds maximum value of the field.
	 *
	 * By default, this property contains a date that refers to 31 December, 7999
	 * and a time of 23:59:59 and 999 milliseconds.
	 */
	Q_PROPERTY(QDateTime maximum READ maximum WRITE setMaximum)
#endif
public:
	/** Convenience function to set the minimum, and maximum values with a single function call.
	 * \code
	 * 		setRange(minimum, maximum);
	 * \endcode
	 * 	is equivalent to:
	 * \code
	 * 	 setMinimum(minimum);
	 * 	 setMaximum(maximum);
	 * \endcode
	 */
	void setRange(const QDateTime &minimum, const QDateTime &maximum);
	virtual QDateTime minimum() const = 0;
	virtual QDateTime maximum() const = 0;
	virtual void setMaximum(const QDateTime &maximum) = 0;
	virtual void setMinimum(const QDateTime &minimum) = 0;
};

/** The field displays a image and allows the user to choose another one from their
 * collection.
 *
 * value() returns a path to the currently chosen image, or an empty string, if no valid image
 * has been chosen. The type of \ref value() "value" is QVariant::String. setValue() expects
 * a path to an image. If the value parameter passed into setValue() has a type other
 * that QVariant::String or if the value is not a valid path to a valid image,
 * the current image will be cleared and the \ref defaultImage "default image" will be shown instead.
 */
class PixmapChooserField : public DataField
{
#ifndef Q_MOC_RUN
	/** Holds the default image that is shown when value() is not
	 * a string or it is a string but not a valid path to an actual
	 * image.
	 */
	Q_PROPERTY(QPixmap defaultImage READ defaultImage WRITE setDefaultImage)
	/** Holds the maximum size of the displayed image.
	 *
	 * If the user has chosen an image that is more than the size, the image
	 * will be scaled to the size.
	 */
	Q_PROPERTY(QSize maximumSize READ maximumSize WRITE setMaximumSize)
#endif
public:
	/** Returns currently choosen image or the \ref defaultImage "default image"
	 * if no valid image has been chosen.
	 */
	virtual QPixmap pixmap() const = 0;
	virtual void setDefaultImage(const QPixmap &pixmap) = 0;
	virtual QPixmap defaultImage() const = 0;
	virtual void setMaximumSize(const QSize &size) = 0;
	virtual QSize maximumSize() const = 0;
};

/** The DataForm class provides a form to edit and display an uncomplicated structure of data.
 *
 * A data form comprised of several fields. Each field usually consists of a
 * \ref DataField::label() "label" and an editor for the \ref Data::Field::value() "value" of
 * the field. The class supports a wide range of fields:
 * -# \ref StringField allows the user to edit a single line of plain text.
 * -# \ref StringChooser provides the user with a list of options.
 * -# \ref StringListField displays multiple lines of plain text, allows the user to edit them,
 * to add a new line and remove an old one.
 * -# \ref MultiStringField allows the user to edit a plain text that consists of more than one
 * line.
 * -# \ref BoolField provides two options to the user.
 * -# \ref IntField allows the user to enter an integer number.
 * -# \ref DoubleField allows the user to enter a floating point value.
 * -# \ref DateField provides a means to choose a date.
 * -# \ref DataTimeField provides a means to choose a date and time.
 * -# \ref PixmapChooser displays an image and allows the user to choose another one from their
 * collection.
 *
 * The appearance and behavior of fields may change depending on the platform to be more complaint
 * with its look&feel. For instance, on mobile phones, the fields will be more compact than on PC
 * and will be organized into one column.
 *
 * If every \ref ValidatedField::mandatory() "mandatory" field is \ref ValidatedField::complete()
 * "complete", the data form is complete as well, otherwise it is incomplete. Use isComplete() to
 * retrieve the current state.
 *
 * A data form can be used as a dialog. addDialogButton() methods add a dialog button usually at the
 * bottom of the form. Buttons that have AcceptRole, YesRole or ApplyRole roles are disabled
 * if the form is \ref isComplete() "incomplete".
 */
class DataForm : public QWidget
{
	Q_OBJECT
public:
	enum ButtonRole {
		// keep this in sync with QMessageBox::ButtonRole
		InvalidRole = -1,
		AcceptRole,
		RejectRole,
		DestructiveRole,
		ActionRole,
		HelpRole,
		YesRole,
		NoRole,
		ResetRole,
		ApplyRole,

		NRoles
	};
	Q_DECLARE_FLAGS(ButtonRoles, ButtonRole);
	enum StandardButton {
		// keep this in sync with QMessageBox::ButtonRole
		NoButton           = 0x00000000,
		Ok                 = 0x00000400,
		Save               = 0x00000800,
		SaveAll            = 0x00001000,
		Open               = 0x00002000,
		Yes                = 0x00004000,
		YesToAll           = 0x00008000,
		No                 = 0x00010000,
		NoToAll            = 0x00020000,
		Abort              = 0x00040000,
		Retry              = 0x00080000,
		Ignore             = 0x00100000,
		Close              = 0x00200000,
		Cancel             = 0x00400000,
		Discard            = 0x00800000,
		Help               = 0x01000000,
		Apply              = 0x02000000,
		Reset              = 0x04000000,
		RestoreDefaults    = 0x08000000,
	};
	Q_DECLARE_FLAGS(StandardButtons, StandardButton);

	enum Flags
	{
		ReadOnly
	};

	DataForm(int flags = 0, QWidget *parent = 0);
	int flags();
	/** Creates a new field of type StringChooserField, initializing its \a value, \a label and \a name.
	 * Returns pointer to the field.
	 */
	StringChooserField *addStringChooser(const QString &value, const QString &label = QString(), const QString &name = QString());
	/** Creates a new field of type StringField, initializing its \a value, \a label and \a name.
	 * Returns pointer to the field.
	 */
	StringField *addString(const QString &value, const QString &label = QString(), const QString &name = QString());
	/** Creates a new field of type StringListField, initializing its \a value, \a label and \a name.
	 * Returns pointer to the field.
	 */
	StringListField *addStringList(const QStringList &value, const QString &label = QString(), const QString &name = QString());
	/** Creates a new field of type MultiStringField, initializing its \a value, \a label and \a name.
	 * Returns pointer to the field.
	 */
	MultiStringField *addMultiString(const QString &value, const QString &label = QString(), const QString &name = QString());
	/** Creates a new field of type BoolField, initializing its \a value, \a label and \a name.
	 * Returns pointer to the field.
	 */
	BoolField *addBool(bool value, const QString &label = QString(), const QString &name = QString());
	/** Creates a new field of type IntField, initializing its \a value, \a label and \a name.
	 * Returns pointer to the field.
	 */
	IntField *addInt(int value, const QString &label = QString(), const QString &name = QString());
	/** Creates a new field of type DoubleField, initializing its \a value, \a label and \a name.
	 * Returns pointer to the field.
	 */
	DoubleField *addDouble(double value, const QString &label = QString(), const QString &name = QString());
	/** Creates a new field of type DateField, initializing its \a value, \a label and \a name.
	 * Returns pointer to the field.
	 */
	DateField *addDate(const QDate &value, const QString &label = QString(), const QString &name = QString());
	/** Creates a new field of type DateTimeField, initializing its \a value, \a label and \a name.
	 * Returns pointer to the field.
	 */
	DateTimeField *addDateTime(const QDateTime &value, const QString &label = QString(), const QString &name = QString());
	/** Creates a new field of type PixmapChooserField, initializing its \a value, \a label and \a name.
	 * Returns pointer to the field.
	 */
	PixmapChooserField *addPixmap(const QString &value, const QString &label = QString(), const QString &name = QString());
	/** Creates a new dialog button with text \a text and role \role.
	 *
	 * clickHandler is the functor that is called whenever the button is clicked by the user.
	 */
	void addDialogButton(const QString &text, ButtonRole role, const function<void()> &clickHandler);
	/** Creates a new standard \a button if it is valid to do so. If the \a button is not valid the \a button
	 * is not added.
	 *
	 * clickHandler is the functor that is called whenever the button is clicked by the user.
	 */
	void addDialogButton(StandardButton button, const function<void()> &clickHandler);
	/** Returns true, it the data form is complete.
	 *
	 * The data form is complete when every its mandatory field is \ref ValidatedField::complete()
	 * "complete"
	 *
	 * \se completeChanged()
	 */
	bool isComplete() const;
signals:
	/** The signal is emitted whenever the state of completion of the data form is changed.
	 *
	 * \a complete is true, if the data form has become complete, it is false, if the data form
	 * has become incomplete.
	 *
	 * \see isComplete()
	 */
	void completeChanged(bool complete);
private:
	friend class DataWidgetBase;
	void ensureButtonBox();
	void incIncompleteWidgetsCount();
	void decIncompleteWidgetsCount();
private:
	DataFormLayout *m_layout;
	QVBoxLayout *m_mainLayout;
	QDialogButtonBox *m_buttonBox;
	int m_flags;
    int m_incompleteWidgetsCount;
};

} // Lime

#endif // LIME_DATAFORMS_H
