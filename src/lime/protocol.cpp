/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "protocol.h"
#include "account.h"
#include "config.h"
#include <algorithm>

namespace Lime
{

static QList<Protocol*> protocols;

Protocol::Protocol(const QString &id, int flags) :
	m_flags(flags), m_id(id)
{
	// TODO: move it somewhere else
	protocols.push_back(this);
}

Protocol::~Protocol()
{
	foreach (auto acc, m_accounts)
		acc->disable();
	protocols.removeOne(this);
}

template <typename Res, typename Container>
static Res findById(const Container &container, const QString &id)
{
	auto end = container.constEnd();
	auto itr = std::find_if(container.constBegin(), end, [=](Res curr) {
		return curr->id() == id;
	});
	if (itr == end)
		return 0;
	else
		return *itr;
}

Account *Protocol::account(const QString &id) const
{
	return findById<Account*>(m_accounts, id);
}

Protocol *Protocol::get(const QString &id)
{
	return findById<Protocol*>(protocols, id);
}

QList<Protocol*> Protocol::all()
{
	return protocols;
}

Config Protocol::config(const QString &group)
{
	return Config(id(), group);
}

void Protocol::addAccount(Account *account)
{
	auto id = account->id();
	m_accounts << account;
	if (account->config().value("isEnabled", true))
		account->enable();
	lconnect(account, SIGNAL(destroyed()), this, [=] {
		m_accounts.removeOne(account);
		Account::onDestroyed(account);
	});
	Account::onCreated(account);
	emit accountAdded(account);
}

} // Lime

