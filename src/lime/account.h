/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_ACCOUNT_H
#define LIME_ACCOUNT_H

#include "actionobject.h"
#include "status.h"
#include <QVariantMap>
#include <QHash>

namespace Lime {

class Protocol;
class Contact;
class Conference;
class Config;

namespace Private {
	template <typename FN>
	struct OnCreatedConnectionPolicy;
	template <typename FN>
	struct OnEnabledConnectionPolicy;
	template <typename FN>
	struct OnContactCreatedConnectionPolicy;
	template <typename FN>
	struct OnConferenceCreatedConnectionPolicy;
}

class Account : public QObject, public ActionContainer
{
	Q_OBJECT
public:
	enum AccountFlags
	{
		/* After an account has been connected, its contact list should be
		 * checked, because some contacts that were previously in contact list
		 * may have been removed while the account was offline. If the flag is
		 * set the contacts will be automatically removed.
		 * Notice that the feature relies on protocol implementations to call
		 * updateStatus() and Contact::updateInList() correctly. The right sequence
		 * of a connection to a server is:
		 *    1. Add 'Connecting' flag to the account status.
		 *    2. Synchronize the local contact list with a remote one. Call
		 *       Contact::updateInList() for every contact that are in the list,
		 *       even if it was already here.
		 *    3. Remove 'Connecting' flag from the account status.
		 */
		AutoRemoveContacts  = 0x00000001, // TODO: damn... how do I name the flag?
		AccountDefaultFlags = AutoRemoveContacts
	};
	Account(const QString &id, Protocol *protocol, int flags = AccountDefaultFlags);
    virtual ~Account();
	QString id() const { return m_id; }
	QString name() const { return m_localName.isEmpty() ? m_id : m_localName; }
	Config config(const QString &group = QString());
	Protocol *protocol() const { return m_protocol; }
	bool isEnabled() const { return m_isEnabled; }
	int flags() const { return m_flags; }
	QString avatar() const { return m_avatar; }
	Status  status() const { return m_status; }
	QString statusText() const { return m_statusText; }
	virtual QString statusIconName(Status status = Status::Invalid);
	virtual QString statusName(Status status = Status::Invalid);
	QIcon statusIcon(Status status = Status::Invalid);
	void setName(const QString &name);
	void setStatus(Status status, const QString &statusText = QString());
	void setStatusText(const QString &statusText);
	// Extended statuses like icq x-status, jabber mood, activity, etc.
	QVariantMap extendedInfo(const QString &name) const { return m_extendedInfo.value(name); }
	void setExtendedInfo(const QString &name, const QVariantMap &info);
	QMap<QString, QVariantMap> allExtendedInfo() { return m_extendedInfo; }
	bool enable();
	bool disable();
	/* Returns a contact with \a id, if the contact does not exist
	 * and \a create is true, tries to create it,
	 *
	 * Notice that even if \a create is true, the method may return
	 * false in case id is not valid or some protocol internal error
	 * has occurred.
	 */
	Contact *contact(const QString &id, bool create = false);
	/* Returns all contacts of the account.
	 */
	QList<Contact*> contacts() const { return m_contacts.values(); }
	/* Returns all contacts with type T.
	 */
	template <typename T>
	QList<T*> contacts() const;
	/* Returns a conference with \a id, if it exists, otherwise, return null.
	 */
	Conference *conference(const QString &id);
	/* Return all conferences oth the account.
	 */
	QList<Conference*> conferences() { return m_conferences.values(); }
	/* Store \a avatar with \a hash to the cache, returns path to the stored avatar.
	 * The avatar path may be later retrieved using getAvatar().
	 * \see getAvatar()
	 */
	QString storeAvatar(const QByteArray &avatar, const QString &hash);
	/* Returns path to an avatar with \a hash previously stored in the cache.
	 * \see storeAvatar()
	 */
	QString getAvatar(const QString &hash);
	/* Returns all contact list groups.
	 */
	QSet<QString> groups() const { return m_contactListGroups; }
	/* Creates a new empty \a group.
	 */
	void createEmptyGroup(const QString &group);
	/* Removes the \a group and all contacts that are in the group and are not in any other group.
	 */
	void removeGroup(const QString &group);
	/* Returns a list of all accounts.
	 */
	static QList<Account*> all();
	template <typename T>
	static void addStatusAction(const QString &name, Status status);
public: // static signals
	static Signal<void(Account* account, QString newName, QString oldName)> onLocalNameChanged;
	static Signal<void(Account*), Private::OnCreatedConnectionPolicy> onCreated;
	static Signal<void(Account* account), Private::OnEnabledConnectionPolicy> onEnabled;
	static Signal<void(Account* account)> onDisabled;
	static Signal<void(Account*)> onDestroyed;
	static Signal<void(Account* account, Contact *contact),
			   	   	   Private::OnContactCreatedConnectionPolicy> onContactCreated;
	static Signal<void(Account* account, Contact *contact)> onContactRemoved;
	static Signal<void(Account* account, QString path)> onAvatarChanged;
	static Signal<void(Account* account, Status newStatus, Status oldStatus)> onStatusChanged;
	static Signal<void(Account* account, QString text)> onStatusTextChanged;
	static Signal<void(Account* account, QString name, QVariantMap info)> onExtendedInfoChanged;
	static Signal<void(Account* account, QString group)> onGroupAdded;
	static Signal<void(Account* account, QString group)> onGroupRemoved;
	static Signal<void(Account* account, Conference *conference),
					   Private::OnConferenceCreatedConnectionPolicy> onConferenceCreated;
	static Signal<void(Account* account, Conference *conference)> onConferenceRemoved;
signals:
	void localNameChanged(const QString &newName, const QString &oldName);
	void enabled();
	void disabled();
	void contactCreated(Lime::Contact *contact) const;
	void contactRemoved(Lime::Contact *contact) const;
	void avatarChanged(const QString &path);
	void statusChanged(Lime::Status newStatus, Lime::Status oldStatus);
	void statusTextChanged(const QString &text);
	void extendedInfoChanged(const QString &name, const QVariantMap &info);
	void groupAdded(const QString &group);
	void groupRemoved(const QString &group);
	void conferenceCreated(Lime::Conference *conference) const;
	void conferenceRemoved(Lime::Conference *conference) const;
protected:
	/* Protocol must call the method whenever the account's avatar has
	 * been changed.
	 */
	void updateAvatar(const QString &avatar);
	/* Protocol must call the method when the account's status has been
	 * changed.
	 */
	void updateStatus(Status status, const QString &text = QString());
	/* Protocol must call the method whenever a new contact has been created
	 * even if the core requested the contact itself using createContact().
	 */
	void addContact(Contact *contact);
	/* Protocol must call the method whenever a new conference has been created.
	 */
	void addConference(Conference *conference);
	/* Protocol must call the method whenever an extended \a information with \a
	 * name has been received.
	 */
	void updateExtendedInfo(const QString &name, const QVariantMap &information);
	/* Protocol must call the method whenever a new \a group has been created in
	 * the remove contact list.
	 * The method must be called before adding a contact to the \a group.
	 * \returns Returns false, if the \a group already existed, otherwise, returns
	 * true.
	 */
	bool handleNewGroup(const QString &group);
	/* Protocol must call the method whenever a group has been removed from the remote
	 * contact list.
	 * The method should be called after removing all contacts from the \a group.
	 * \note The current behavior does not enforce the rule, but it may change
	 * in the future.
	 * \returns Returns true, if the \a group has been successfully removed, otherwise
	 * returns false.
	 */
	bool handleGroupRemoval(const QString &group);
protected:
	/* The protocol implementation must initialize all account's data in
	 * the method, including its contact list.
	 *
	 * Returns true if the account has been successfully enabled.
	 */
	virtual bool enableImpl() = 0;
	/* The protocol implementation must free all account's resources in
	 * the method, including its contact list.
	 *
	 * Returns true if the account has been successfully disabled.
	 */
	virtual bool disableImpl() = 0;
	/* The method must create a new contact with \a id and return pointer
	 * to the contact. Notice that the method must call addContact().
	 */
	virtual Contact *createContact(const QString &id) = 0;
	/* The method must send a new \a status of the account to the server.
	 */
	virtual void setStatusImpl(Status status, const QString &statusText) = 0;
	virtual void storeContactList(Config &cfg);
	virtual void loadContactList(const Config &cfg);
	virtual bool setExtendedInfoImpl(const QString &name, const QVariantMap &info);
	/* The protocol implementation must create a new empty \a group.
	 */
	virtual void createEmptyGroupImpl(const QString &group) = 0;
	/* The protocol implementation must remove the \a group and all contacts that
	 * are in the group and are not in any other group.
	 */
	virtual void removeGroupImpl(const QString &group) = 0;
	/* The method is called when groups of the \a contact are changed from \a oldGroups
	 * to \a newGroups.
	 */
	virtual void handleGroupUpdate(Contact *contact, const QStringList &newGroups, const QStringList &oldGroups);
private:
	friend class Contact;
	friend class Conference;
	void removeContact(Contact *contact);
	void removeConference(Conference *conference);
	virtual QObject* toQObject() { return this; }
	static void addStatusAction(QMetaObject *meta, const QString &name, Status status);
private:
	bool m_isEnabled;
	int m_flags;
	Protocol *m_protocol;
	QString m_id;
	QString m_localName;
	QHash<QString, Contact*> m_contacts;
	QMap<QString, QVariantMap> m_extendedInfo;
	Status  m_status;
	QString m_avatar;
	QString m_statusText;
	QSet<QString> m_contactListGroups;
	QMap<QString, Conference*> m_conferences;
};

class TagAccount : public Account
{
	Q_OBJECT
public:
	TagAccount(const QString &id, Protocol *protocol, int flags = AccountDefaultFlags);
protected:
	virtual void createEmptyGroupImpl(const QString &group);
	virtual void removeGroupImpl(const QString &group);
	virtual void storeContactList(Config &cfg);
	virtual void loadContactList(const Config &cfg);
	void storeEmptyGroups(Config &cfg);
	void loadEmptyGroups(const Config &cfg);
	virtual void handleGroupUpdate(Contact *contact, const QStringList &newGroups, const QStringList &oldGroups);
private:
	friend class Contact;
	QMap<QString, int> m_tagCount;
	QStringList m_emptyGroups;
};

class ContactListGroupActionContainer : public QObject, public ActionContainer
{
	Q_OBJECT
public:
	ContactListGroupActionContainer(const QString &name) :
		m_name(name)
	{}
	QString name() const { return m_name; }
	virtual QObject *toQObject() { return this; }
private:
	QString m_name;
};

namespace Private {
	template <typename FN>
	struct OnCreatedConnectionPolicy
	{
		static void call(const FN &fn)
		{
			foreach (auto account, Account::all())
				fn(account);
		}
	};

	template <typename FN>
	struct OnEnabledConnectionPolicy
	{
		static void call(const FN &fn)
		{
			foreach (auto account, Account::all()) {
				if (account->isEnabled())
					fn(account);
			}
		}
	};

	template <typename FN>
	struct OnContactCreatedConnectionPolicy
	{
		static void call(const FN &fn)
		{
			foreach (auto account, Account::all()) {
				foreach (auto contact, account->contacts())
					fn(account, contact);
			}
		}
	};

	template <typename FN>
	struct OnConferenceCreatedConnectionPolicy
	{
		static void call(const FN &fn)
		{
			foreach (auto account, Account::all()) {
				foreach (auto conf, account->conferences())
					fn(account, conf);
			}
		}
	};
}

template <typename T>
QList<T*> Account::contacts() const
{
	QList<T*> res;
	foreach (auto contact, m_contacts) {
		auto casted = qobject_cast<T*>(contact);
		if (casted)
			res << casted;
	}
	return res;
}

template <typename T>
void Account::addStatusAction(const QString &name, Status status)
{
	addStatusAction(const_cast<QMetaObject*>(&T::staticMetaObject), name, status);
}

} // namespace Lime

Q_DECLARE_METATYPE(Lime::Account*)

#endif // LIME_ACCOUNT_H
