/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "buddy.h"
#include "contact.h"
#include "icon.h"
#include "account.h"
#include "protocol.h"
#include "notification.h"
#include "tooltip.h"
#include "iconmanager/iconmanager.h"
#include <QTextDocument>
#include <QLatin1Literal>

namespace Lime
{

Signal<void(ExtendedInfoDescriptions &descriptions)> Buddy::supportedExtendedInfoHook;
Signal<void(Buddy* buddy, QString path)> Buddy::onAvatarChanged;
Signal<void(Buddy* buddy, Status newStatus, Status oldStatus)> Buddy::onStatusChanged;
Signal<void(Buddy* buddy, QString text)> Buddy::onStatusTextChanged;
Signal<void(Buddy* buddy, QString name, QVariantMap info)> Buddy::onExtendedInfoChanged;
Signal<void(Buddy* buddy, int newPriority, int oldPriority)> Buddy::onPriorityChanged;

Buddy::Buddy(const QString &id, Account *account, int flags) :
	ChatUnit(id, account, flags),
	m_priority(0),
	m_parentUnit(0),
	m_status(Status::Offline)
{
	toolTipHook += [=](ToolTip *tip) {
		if (tip->generateLayout()) {
			tip->addHtml("<table><tr><td>", ToolTip::Top + 100000);
			QString ava = avatar();
			if (ava.isEmpty())
				ava = ":/icons/64x64/lime.png";
			QString text = QLatin1Literal("</td><td><img width=\"64\" src=\"")
					% Qt::escape(ava)
					% QLatin1Literal("\"/></td></tr></table>");
			tip->addHtml(text, 50);
		}
		tip->addHtml("<font size=-1>", ToolTip::Middle);
		tip->addHtml("</font>", 100);

		auto statusText = this->statusText();
		if (statusText.isEmpty())
			statusText = statusName();
		tip->addField(statusText, QString(), this->statusIconName(),
					  ToolTip::IconBeforeTitle, ToolTip::Top + 800);

		auto client = m_extendedInfo.find("client");
		if (client != m_extendedInfo.end()) {
			QString title = m_status & Online ?
					tr("Possible client") :
					tr("Last client");
			QString desc = client->value("description").toString();
			QIcon icon = client->value("icon").value<QIcon>();
			tip->addField(title, desc, icon, ToolTip::IconBeforeDescription, ToolTip::Bottom - 2000);
		}
	};
}

Buddy::~Buddy()
{

}

QString Buddy::statusIconName()
{
	return account()->statusIconName(m_status);
}

QString Buddy::statusName()
{
	return account()->statusName(m_status);
}

QIcon Buddy::statusIcon()
{
	return Icon(statusIconName());
}

void Buddy::updateAvatar(const QString &avatar)
{
	if (m_avatar == avatar)
		return;
	m_avatar = avatar;
	emit avatarChanged(avatar);
	onAvatarChanged(this, avatar);
}

void Buddy::updateStatus(Status status, const QString &text)
{
	bool changed = false;

	if (m_statusText != text) {
		changed = true;
		m_statusText = text;
		emit statusTextChanged(text);
		onStatusTextChanged(this, text);
	}

	auto old = m_status;
	if (status != m_status) {
		changed = true;
		m_status = status;
		emit statusChanged(status, old);
		onStatusChanged(this, status, old);
	}

	if (changed) {
		if (auto contact = qobject_cast<Contact*>(this)) {
			if (contact->isInList())
				NotificationManager::instance()->handleStatusChange(contact, status, old, text);
		}
	}
}

void Buddy::updatePriority(int priority)
{
	if (m_priority == priority)
		return;
	auto old = m_priority;
	m_priority = priority;
	emit priorityChanged(priority, old);
	onPriorityChanged(this, priority, old);
}

void Buddy::setExtendedInfo(const QString &name, const QVariantMap &info)
{
	if (info.isEmpty())
		m_extendedInfo.remove(name);
	else
		m_extendedInfo.insert(name, info);
	emit extendedInfoChanged(name, info);
	onExtendedInfoChanged(this, name, info);
}

Contact *Buddy::parentContact() const
{
	return qobject_cast<Contact*>(m_parentUnit);
}

void Buddy::checkExtendedInfo()
{
	auto end = m_extendedInfo.end();
	for (auto itr = m_extendedInfo.begin(); itr != end; ) {
		if (!itr->value("keepOffline", true).toBool()) {
			auto key = itr.key();
			itr = m_extendedInfo.erase(itr);
			emit extendedInfoChanged(key, QVariantMap());
		} else {
			++itr;
		}
	}
}

} // Lime

