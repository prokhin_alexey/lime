/*
 This file is part of Lime.
 Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "notificationfilter.h"
#include <QCheckBox>
#include <QGridLayout>
#include <QMetaEnum>
#include <QPushButton>
#include "config.h"

namespace Lime
{

QString notificationDescription(int notif)
{
	static QStringList descriptions = QStringList()
			<< QT_TR_NOOP("You have received a new message")
			<< QT_TR_NOOP("You have sent a new message")
			<< QT_TR_NOOP("A message has been blocked")
			<< QT_TR_NOOP("You have received a new conference message")
			<< QT_TR_NOOP("You have sent a new conference message")
			<< QT_TR_NOOP("A new participant joined a conference")
			<< QT_TR_NOOP("A participant left a conference")
			<< QT_TR_NOOP("One of your buddies has gone online")
			<< QT_TR_NOOP("One of your buddies has gone offline")
			<< QT_TR_NOOP("One of your buddies has changed status")
			<< QT_TR_NOOP("Someone is typing you a message")
			<< QT_TR_NOOP("One of your buddies has his/her birthday")
			<< QT_TR_NOOP("A system notification")
			<< QT_TR_NOOP("The program has been started");

	return descriptions.at(notif);
}

QString notificationTypeName(int type)
{
	static QStringList names;
	if (names.isEmpty()) {
		const QMetaObject &meta = Notification::staticMetaObject;
		QMetaEnum e = meta.enumerator(meta.indexOfEnumerator("Type"));
		for (int i = 0; i <= Notification::LastType; ++i)
			names << e.key(i);
	}
	return names.value(type);
}

NotificationFilter::NotificationFilter(const std::initializer_list<Notification::Type> &filtered)
{
	m_filtered.resize(Notification::LastType + 1);
	for (auto type : filtered)
		m_filtered.setBit(type);
}

NotificationFilter NotificationFilter::loadFromConfig(const Config &cfg, const QString &group, const NotificationFilter &defaultValues)
{
	NotificationFilter filter = defaultValues;
	cfg.beginGroup(group);
	for (int i = 0; i <= Notification::LastType; ++i)
		filter.setFilter(i, !cfg.value(notificationTypeName(i), !defaultValues.isFiltered(i)));
	cfg.endGroup();
	return filter;
}

void NotificationFilter::saveToConfig(Config &cfg, const QString &group, const NotificationFilter &values)
{
	cfg.beginGroup(group);
	for (int i = 0; i <= Notification::LastType; ++i)
		cfg.setValue(notificationTypeName(i), !values.isFiltered(i));
	cfg.endGroup();
}

NotificationFilterWidget::NotificationFilterWidget(const QString &title, const NotificationFilter &enabledTypes, QWidget *parent) :
	QGroupBox(title, parent)
{
	m_layout = new QGridLayout(this);
	int row = 0;
	for (int type = 0; type <= Notification::LastType; ++type) {
		if (enabledTypes.isFiltered(type))
			continue;
		auto widget = new QCheckBox(notificationDescription(type), this);
		widget->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
		m_widgets.append({ Notification::Type(type), widget });
		m_layout->addWidget(widget, row++, 0);
		connect(widget, SIGNAL(toggled(bool)), SIGNAL(valuesChanged()));
	}
}

void NotificationFilterWidget::addTestButtonActions(const function<QWidget*(Notification::Type)> &createButtonFunc)
{
	int row = 0;
	for (auto &w : m_widgets) {
		auto button = createButtonFunc(w.type);
		m_layout->addWidget(button, row++, 1);
	}
}

void NotificationFilterWidget::setValues(const NotificationFilter &values)
{
	for (auto &w : m_widgets)
		w.widget->setChecked(!values.isFiltered(w.type));
}

NotificationFilter NotificationFilterWidget::values() const
{
	NotificationFilter filter;
	for (auto &w : m_widgets)
		filter.setFilter(w.type, !w.widget->isChecked());
	return filter;
}

QDataStream &operator<<(QDataStream &out, const NotificationFilter &filter)
{
	out << filter.m_filtered;
	return out;
}

QDataStream &operator>>(QDataStream &in, NotificationFilter &filter)
{
	in >> filter.m_filtered;
	return in;
}

struct Init
{
	Init()
	{
		 qRegisterMetaTypeStreamOperators<NotificationFilter>("NotificationFilter");
	}
};
static Init init;

} // Lime
