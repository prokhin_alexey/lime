/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "chatunit.h"
#include "account.h"
#include "contact.h"
#include "chat/chatwidget.h"
#include "notification.h"
#include "tooltip.h"
#include "chatmanager.h"

namespace Lime
{

Signal<void(ChatUnit* unit, QString  newName, QString oldName)> ChatUnit::onNameChanged;
Signal<void(ChatUnit* unit, QString newTitle, QString oldTitle)> ChatUnit::onTitleChanged;
Signal<void(ChatUnit* unit, Message message)> ChatUnit::onMessageReceived;
Signal<void(ChatUnit* unit, Message message)> ChatUnit::onNewUnreadMessage;
Signal<void(ChatUnit* unit)> ChatUnit::onUnreadMessagesCleared;
Signal<void(ChatUnit* unit)> ChatUnit::onChatOpened;
Signal<void(ChatUnit* unit)> ChatUnit::onChatActivated;
Signal<void(ChatUnit* unit)> ChatUnit::onChatDeactivated;
Signal<void(ChatUnit* unit)> ChatUnit::onChatClosed;
Signal<void(ChatUnit* unit, ChatUnit::ChatState state)> ChatUnit::onChatStateChanged;
Signal<void(ChatUnit* unit, int id, ChatUnit::MessageReceipt success)> ChatUnit::onMessageReceipt;

ChatUnit::ChatUnit(const QString &id, Account *account, int flags) :
	QObject(account),
	m_state(StateGone),
	m_flags(flags),
	m_account(account),
	m_id(id),
	m_isActive(false),
	m_isOpened(false)
{
	Q_ASSERT(account);

	toolTipHook += [=](ToolTip *tip) {
		auto id = this->id();
		auto name = this->name();
		QString str = QString("<b>%1</b>").arg(Qt::escape(name));
		if ((this->flags() & ShowIdInToolTip) && !id.isEmpty() && id != name)
			str += QLatin1String(" &lt;") + Qt::escape(id) + QLatin1String("&gt;");
		tip->addHtml(str, ToolTip::Top + 1000);
	};
}

ChatUnit::~ChatUnit()
{
}

void ChatUnit::sendMessage(const Message &message)
{
	if (sendMessageImpl(message) == MessageSent) {
		ChatManager::instance()->appendMessage(message);
		NotificationManager::instance()->handleNewMessage(message);
	}
}

void ChatUnit::activateChat()
{
	ChatManager::instance()->activateChat(this);
}

void ChatUnit::clearUnreadMessages()
{
	if (!m_unread.isEmpty()) {
		m_unread.clear();
		emit unreadMessagesCleared();
		onUnreadMessagesCleared(this);
	}
}

ChatUnit *ChatUnit::getHistoryUnit() const
{
	auto unit = const_cast<ChatUnit*>(this);
	forever {
		if (Buddy *buddy = qobject_cast<Buddy*>(unit)) {
			auto parent = buddy->parentContact();
			if (!parent)
				return buddy;
			unit = parent;
		} else {
			return unit;
		}
	}
	return 0;
}

QTextDocument *ChatUnit::document() const
{
	return ChatManager::instance()->document(const_cast<ChatUnit*>(this));
}

void ChatUnit::updateName(const QString &name)
{
	if (m_name == name)
		return;
	auto oldName = this->name();
	m_name = name;
	auto newName = this->name();
	if (newName != oldName) {
		emit nameChanged(newName, oldName);
		onNameChanged(this, newName, oldName);
	}
	if (m_title.isEmpty()) {
		emit titleChanged(newName, oldName);
		onTitleChanged(this, newName, oldName);
	}
}

void ChatUnit::updateTitle(const QString &title)
{
	if (m_title == title)
		return;
	auto oldTitle = this->title();
	m_title = title;
	auto newTitle = this->title();
	if (oldTitle != newTitle) {
		emit titleChanged(newTitle, oldTitle);
		onTitleChanged(this, newTitle, oldTitle);
	}
}

void ChatUnit::handleIncomingMessage(const Message &message)
{
	ChatManager::instance()->appendMessage(message);
	NotificationManager::instance()->handleNewMessage(message);
	emit messageReceived(message);
	onMessageReceived(this, message);
	if (!m_isActive) {
		m_unread << message;
		emit newUnreadMessage(message);
		onNewUnreadMessage(this, message);
	}
}

void ChatUnit::handleChatState(ChatState state)
{
	if (state == m_state)
		return;

	NotificationManager::instance()->handleChatState(this, state);
	m_state = state;
	emit chatStateChanged(state);
	onChatStateChanged(this, state);
}

void ChatUnit::handleMessageReceipt(int messageId, MessageReceipt success)
{
	emit messageReceipt(messageId, success);
	onMessageReceipt(this, messageId, success);
}

void ChatUnit::sendChatStateImpl(ChatState state)
{
}

}

