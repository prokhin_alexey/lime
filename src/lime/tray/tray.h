/*
	 This file is part of Lime.
	 Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIME_TRAY_H_
#define LIME_TRAY_H_

#include <lime/extention.h>
#include <lime/actionobject.h>
#include <lime/status.h>
#include <QSystemTrayIcon>

namespace Lime
{

class ChatUnit;
class Account;

class Tray : public QObject, public ActionContainer, public Extension, public SignalGuard
{
	Q_OBJECT
public:
	virtual ~Tray();
	static Tray *instance() { static Tray *tray = new Tray; return tray; }
	virtual QObject *toQObject() { return this; }
protected:
	virtual bool enableImpl();
	virtual bool disableImpl();
private:
	Tray();
	void checkMainIcon();
	void checkAlternativeIcon();
	void updateIcon();
private slots:
	void reloadSettings();
private:
	QSystemTrayIcon *m_tray;
	QMap<ChatUnit*, int> m_unreadedCount;
	QList<ChatUnit*> m_unreadedUnits;
	QMap<Account*, Status> m_statuses;
	Account *m_mainAccount;
	QString m_mainAccountProtoId;
	QString m_mainAccountId;
	QIcon m_newMessageIcon;
	QIcon m_onlineIcon;
	QIcon m_offlineIcon;
	QIcon m_main;
	QIcon m_alternative;
	QTimer *m_blinkTimer;
	bool m_showAlternative;
	bool m_clickShowsAllUnreaded;
	bool m_showMailIcon;
};

} // Lime
#endif // LIME_TRAY_H_
