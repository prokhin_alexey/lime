/*
	 This file is part of Lime.
	 Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TRAYSETTINGS_H
#define TRAYSETTINGS_H

#include <QWidget>
#include <lime/settings.h>

namespace Ui {
	class TraySettingsClass;
}

namespace Lime {

class TraySettings : public SettingsWidget
{
	Q_OBJECT
public:
	TraySettings(QWidget *parent = 0);
	~TraySettings();
protected:
	virtual void loadImpl();
	virtual void saveImpl();
private:
	Ui::TraySettingsClass *ui;
};

} // Lime

#endif // TRAYSETTINGS_H
