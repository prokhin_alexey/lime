/*
	 This file is part of Lime.
	 Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "traysettings.h"
#include "ui_traysettings.h"
#include <lime/accountmodel.h>
#include <lime/account.h>
#include <lime/protocol.h>

namespace Lime {

TraySettings::TraySettings(QWidget *parent) :
	SettingsWidget(parent), ui(new Ui::TraySettingsClass)
{
	auto central = new QWidget(this);
	setWidget(central);
	ui->setupUi(central);

	ui->showAccountComboBox->setModel(new AccountModel(AccountModel::ShowOnlyEnabledAccounts, this));
	bind("blink", ui->blinkBox, true);
	bind("showNewMail", ui->showNewMailBox, true);
	bind("clickShowsAllUnreaded", ui->clickShowsAllUnreadedBox, true);
	bind("showMainAccountStatus", ui->showAccountCheckBox, false);
}

TraySettings::~TraySettings()
{
	delete ui;
}

void TraySettings::loadImpl()
{
	Config cfg("", "tray");
	loadBinds(cfg);

	if (cfg.hasChildGroup("mainAccount")) {
		cfg.beginGroup("mainAccount");
		auto proto = Protocol::get(cfg.value("protocol", QString()));
		if (proto) {
			auto comboBox = ui->showAccountComboBox;
			auto acc = proto->account(cfg.value("account", QString()));
			int index = comboBox->findData(qVariantFromValue(acc), AccountModel::AccountRole);
			if (index != -1)
				comboBox->setCurrentIndex(index);
		}
	}
}

void TraySettings::saveImpl()
{
	Config cfg("", "tray");
	saveBinds(cfg);

	auto comboBox = ui->showAccountComboBox;
	auto acc = comboBox->itemData(comboBox->currentIndex(), AccountModel::AccountRole).value<Account*>();
	cfg.beginGroup("mainAccount");
	cfg.setValue("protocol", acc->protocol()->id());
	cfg.setValue("account", acc->id());
	cfg.endGroup();
}

} // Lime
