/*
	 This file is part of Lime.
	 Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tray.h"
#include <QApplication>
#include <QAction>
#include <QTimer>
#include <lime/contactlist/contactlist.h>
#include <lime/account.h>
#include <lime/chatunit.h>
#include <lime/icon.h>
#include <lime/config.h>
#include <lime/protocol.h>
#include "traysettings.h"

namespace Lime
{

Tray::Tray() :
	m_tray(0), m_mainAccount(0), m_blinkTimer(0), m_showAlternative(false)
{
}

Tray::~Tray()
{
}

bool Tray::enableImpl()
{
	if (!QSystemTrayIcon::isSystemTrayAvailable()) {
		qDebug() << "No System Tray Available. Tray icon not loaded.";
		return false;
	}

	m_newMessageIcon = Icon("mail-message-new");
	m_onlineIcon = QIcon(":/icons/24x24/lime.png");
	m_offlineIcon = QIcon(":/icons/24x24/lime-dark.png");

	m_tray = new QSystemTrayIcon(this);
	m_tray->setIcon(m_onlineIcon);
	m_tray->show();
	m_tray->setContextMenu(menu(Action::ContextMenuAction, m_tray));
	qApp->setQuitOnLastWindowClosed(false);

	lconnect(m_tray, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this,
		[=](QSystemTrayIcon::ActivationReason reason)
	{
		if (reason == QSystemTrayIcon::Trigger) {
			if (m_unreadedUnits.isEmpty()) {
				auto cl = ContactList::instance();
				if (cl->isActiveWindow()) {
					cl->close();
				} else {
					cl->show();
					cl->activateWindow();
				}
			} else {
				if (m_clickShowsAllUnreaded) {
					auto units = m_unreadedUnits;
					m_unreadedUnits.clear();
					foreach (auto unit, units)
						unit->activateChat();
				} else {
					// It's more efficient to leave the first unit in the list, because
					// removeOne() from onNewUnreadMessage handler will not have to
					// iterate over the every element of the list.
					m_unreadedUnits.first()->activateChat();
				}
			}
		}
	});

	Account::onEnabled.connect(this, [=](Account *acc) {
		auto act = new Action(acc->statusIcon(), acc->id(), this);
		act->setType(Action::ContextMenuAction);
		act->setPriority(Action::HighestPriority);
		lconnect(act, SIGNAL(actionCreated(QAction*,ActionContainer*)), acc, [=](QAction *action) {
			auto menu = acc->menu(Action::ContextMenuAction, act);
			action->setMenu(menu);
		});
		lconnect(acc, SIGNAL(disabled()), act, [=] {
			act->deleteLater();
			m_statuses.remove(acc);
		});
		lconnect(acc, SIGNAL(statusChanged(Lime::Status,Lime::Status)), act, [=](Lime::Status status) {
			act->setIcon(acc->statusIcon(), 0);
			m_statuses[acc] = status;
			checkMainIcon();
		});
		ActionContainer::addAction<Tray>(act);
		m_statuses[acc] = acc->status();
		if (m_mainAccountId == acc->id() && m_mainAccountProtoId == acc->protocol()->id())
			m_mainAccount = acc;
		checkMainIcon();
	});

	ChatUnit::onNewUnreadMessage.connect(this, [=](ChatUnit *unit, const Message &msg) {
		Q_UNUSED(msg);
		auto itr = m_unreadedCount.find(unit);
		if (itr == m_unreadedCount.end()) {
			Q_ASSERT(!m_unreadedUnits.contains(unit));
			m_unreadedUnits.push_back(unit);
			m_unreadedCount.insert(unit, 1);
		} else {
			*itr += 1;
		}
		checkAlternativeIcon();
	});
	ChatUnit::onUnreadMessagesCleared.connect(this, [=](ChatUnit *unit) {
		m_unreadedCount.remove(unit);
		m_unreadedUnits.removeOne(unit);
		checkAlternativeIcon();
	});

	SettingsManager::addGlobalSettings(Icon("dialog-information-tray"), tr("Tray"), [=] {
		auto settings = new TraySettings;
		connect(settings, SIGNAL(saved()), this, SLOT(reloadSettings()));
		return settings;
	});

	reloadSettings();

	return true;
}

bool Tray::disableImpl()
{
	return false;
}

void Tray::checkMainIcon()
{
	if (m_mainAccount) {
		m_main = m_mainAccount->statusIcon();
	} else if (m_statuses.isEmpty()) {
		m_main = m_onlineIcon;
	} else if (m_statuses.count() == 1) {
		m_main = m_statuses.begin().key()->statusIcon();
	} else {
		bool isOffline = true;
		foreach (auto &status, m_statuses) {
			if (status & Online) {
				isOffline = false;
				break;
			}
		}
		m_main = isOffline ? m_offlineIcon : m_onlineIcon;
	}
	if (!m_showAlternative)
		updateIcon();
}

void Tray::checkAlternativeIcon()
{
	if (m_showMailIcon && !m_unreadedCount.isEmpty()) {
		m_alternative = m_newMessageIcon;
		if (!m_blinkTimer) {
			m_showAlternative = true;
			updateIcon();
		} else if (!m_blinkTimer->isActive()) {
			m_blinkTimer->start();
			updateIcon();
		}
	} else {
		m_alternative = QIcon();
		m_showAlternative = false;
		if (m_blinkTimer)
			m_blinkTimer->stop();
		updateIcon();
	}
}

void Tray::updateIcon()
{
	m_tray->setIcon(m_showAlternative ? m_alternative : m_main);
}

void Tray::reloadSettings()
{
	Config cfg("", "tray");

	if (cfg.value("blink", true)) {
		m_blinkTimer = new QTimer(this);
		m_blinkTimer->setInterval(cfg.value("blinkInterval", 1000));
		lconnect(m_blinkTimer, SIGNAL(timeout()), this, [=] {
			m_showAlternative = !m_showAlternative;
			updateIcon();
		});
	} else {
		delete m_blinkTimer;
		m_blinkTimer = 0;
	}

	m_clickShowsAllUnreaded = cfg.value("clickShowsAllUnreaded", true);
	m_showMailIcon = cfg.value("showNewMail", true);

	m_mainAccount = 0;
	if (cfg.value("showMainAccountStatus", false)) {
		cfg.beginGroup("mainAccount");
		m_mainAccountProtoId = cfg.value("protocol", QString());
		m_mainAccountId = cfg.value("account", QString());
		auto proto = Protocol::get(m_mainAccountProtoId);
		if (proto)
			m_mainAccount = proto->account(m_mainAccountId);
		cfg.endGroup();
	}

	checkMainIcon();
	checkAlternativeIcon();
}

} // Lime
