/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_SIGNAL_H
#define LIME_SIGNAL_H

#include "global.h"
#include "utils.h"
#include <limits>

namespace Lime {

int methodIndex(QObject *meta, const char *method);

template<typename FN>
struct NoOnConnectionPolicy
{
	static void call(const FN &) { }
};

template<typename Signature,
		 template<typename FN> class OnConnectionPolicy = NoOnConnectionPolicy>
class Signal;

namespace Private {
	struct QtMethod
	{
		Pointer<QObject> receiver;
		int index;
	};
	struct SignalDataBase : public QSharedData
	{
		virtual void disconnect(int id) = 0;
	};
	template<typename FN>
	struct SignalData : public SignalDataBase
	{
		void disconnect(int id) { methods.remove(id); }
		QMap<int, FN> methods;
		mutable QList<QtMethod> qtMethods;
	};
}

class SignalGuard
{
public:
	~SignalGuard()
	{
		auto itr = m_connections.constBegin();
		for (auto end = m_connections.constEnd(); itr != end; ++itr) {
			foreach (int conn, *itr)
				itr.key()->disconnect(conn);
		}
	}

	template<typename Signal>
	void guardConnection(const Signal &signal, int conn)
	{
		m_connections[signal->d.data()] << conn;
	}
private:
	QMap<Private::SignalDataBase*, QList<int> > m_connections;
};

template<typename Res, typename... T,
		 template<typename FN> class OnConnectionPolicy>
class Signal<Res(T...), OnConnectionPolicy>
{
public:
	typedef Res Signature(T...);
	typedef function<Res(T...)> FN;
	Signal<Res(T...), OnConnectionPolicy>() : d(new Data)
	{
	}

	int connect(const FN &method)
	{
		int id = getId();
		d->methods.insert(id, method);
		OnConnectionPolicy<FN>::call(method);
		return id;
	}

	int connect(SignalGuard *guard, const FN &method)
	{
		int id = connect(method);
		guard->guardConnection(this, id);
		return id;
	}

	bool disconnect(int id)
	{
		return d->methods.remove(id);
	}

	void connect(QObject *receiver, const char *slot)
	{
		Q_ASSERT(slot);
		int index = methodIndex(receiver, slot);
		if (index == -1)
			return;

		Private::QtMethod qtMethod = { receiver, index };
		d->qtMethods << qtMethod;
	}

	void disconnect(QObject *receiver, const char *method)
	{
		Q_ASSERT(receiver);
		int index = -1;
		if (method) {
			index = methodIndex(receiver, method);
			if (index == -1)
				return;
		}

		auto itr = d->qtMethods.begin();
		while (itr != d->qtMethods.end()) {
			if ((receiver == 0 || itr->receiver == receiver) &&
				(method  == 0 || itr->index == index))
			{
				itr = d->qtMethods.erase(itr);
			} else {
				++itr;
			}
		}
	}

	void operator()(T... args) const
	{
		foreach (auto slot, d->methods)
			slot(args...);

		auto itr = d->qtMethods.begin();
		while (itr != d->qtMethods.end()) {
			if (!itr->receiver) {
				itr = d->qtMethods.erase(itr);
				continue;
			}

			void *_a[] = { (void*)0, (void*)&args... };
			itr->receiver->qt_metacall(QMetaObject::InvokeMetaMethod, itr->index, _a);
			++itr;
		}
	}

	Signal<Res(T...), OnConnectionPolicy> &operator+=(const FN &method)
	{
		connect(method);
		return *this;
	}
protected:
	static int getId() { static int id = std::numeric_limits<int>().max(); return id--; }
private:
	friend class SignalGuard;
	typedef Private::SignalData<FN> Data;
	QExplicitlySharedDataPointer<Data> d;
};

} // Lime

#endif // LIME_SIGNAL_H
