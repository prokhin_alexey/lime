/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "extention.h"
#include "iconmanager/iconmanager.h"
#include "account.h"
#include "protocols/null/nullprotocol.h"
#include "protocols/jabber/jabberprotocol.h"
#include "contactlist/contactlist.h"
#include "chat/chatmanager.h"
#include "settingswindow/settingswindow.h"
#include "history/history.h"
#include "popup/popupmanager.h"
#include "sound/soundmanager.h"
#include "notification.h"
#include "emoticons/emoticonsmanager.h"
#include "tray/tray.h"
#include "icon.h"
#include <QApplication>

namespace Lime
{

Extension::Extension() :
	m_isEnabled(false)
{

}

bool Extension::enable()
{
	if (m_isEnabled)
		return true;
	if (enableImpl()) {
		m_isEnabled = true;
		return true;
	}
	return false;
}

bool Extension::disable()
{
	if (!m_isEnabled)
		return true;
	if (disableImpl()) {
		m_isEnabled = false;
		return true;
	}
	return false;
}

ExtensionManager *ExtensionManager::instance()
{
	static ExtensionManager *manager = new ExtensionManager;
	return manager;
}

void ExtensionManager::initExtensions()
{
	m_state = Inititializing;
	// TODO: Easiest implementation I can think of just to make
	// everything work until I implement the extension manager.
	m_extensions
		<< IconManager::instance()
		<< new ChatManagerImpl
		//<< new NullProtocol
		<< new JabberProtocol
		<< History::instance()
		<< PopupManager::instance()
		<< SoundManager::instance()
		<< NotificationManager::instance()
		<< EmoticonsManager::instance()
		<< Tray::instance();

	auto cl = ContactList::instance();

	foreach (auto ext, m_extensions)
		ext->enable();
	SettingsWindow::enable();

	cl->enable();
	cl->show();

	auto act = new Action(Icon("application-exit"), tr("Quit"), this);
	act->setType(Action::ContextMenuAction);
	act->setPriority(Action::LowestPriority - 500);
	connect(act, SIGNAL(triggered(ActionContainer*,bool)), qApp, SLOT(quit()));
	ActionContainer::addAction<ContactList>(act);
	ActionContainer::addAction<Tray>(act);

	m_state = Working;
}

ExtensionManager::ExtensionManager() :
	m_state(Unittesting)
{
	connect(qApp, SIGNAL(aboutToQuit()), this, SLOT(deleteLater()));
}

ExtensionManager::~ExtensionManager()
{
	m_state = Quiting;
	ContactList::instance()->disable();

	foreach (auto acc, Account::all()) {
		acc->disable();
		delete acc;
	}

	SettingsWindow::disable();
	QListIterator<Extension*> itr(m_extensions);
	itr.toBack();
	while (itr.hasPrevious()) {
		auto ext = itr.previous();
		ext->disable();
		delete ext->toQObject();
	}
}

}

