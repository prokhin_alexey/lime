/*
    This file is part of Lime.
    Copyright (C) 2010  Aleksey Sidorov <sauron@citadelspb.com>
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "config.h"
#include <QSharedPointer>
#include <QStringList>
#include <QDebug>
#include <QFileInfo>
#include <QSize>
#include <QRect>
#include <QPoint>
#include "systeminfo.h"
#include "k8json.h"
#include "utils.h"

namespace Lime
{

struct Level
{
	Level(const QString &name_, QVariantMap *map_) :
		type(Map), name(name_), map(map_)
	{}
	Level(const QString &name_, QVariantList *list_) :
		type(List), name(name_), list(list_)
	{}
	enum Type
	{
		List,
		Map
	};
	Type type;
	QString name;
	union
	{
		QVariantList *list;
		QVariantMap *map;
	};
};

class ConfigPrivate : public QSharedData
{
public:
	QVariantMap *getGroupMap(const QStringList &groups);
	QVariantMap *getGroupMap(const QStringList &groups) const;
	mutable QList<Level> levels;
	QVariant data;
	QString filePath;
	bool changed;
};

static inline QStringList getGroupList(const QString &fullName)
{
	return fullName.split('/', QString::SkipEmptyParts);
}

static inline QStringList splitArgs(const QString &s, int idx)
{
	int l = s.length();
	Q_ASSERT(l > 0);
	Q_ASSERT(s.at(idx) == QLatin1Char('('));
	Q_ASSERT(s.at(l - 1) == QLatin1Char(')'));

	QStringList result;
	QString item;

	for (++idx; idx < l; ++idx) {
		QChar c = s.at(idx);
		if (c == QLatin1Char(')')) {
			Q_ASSERT(idx == l - 1);
			result.append(item);
		} else if (c == QLatin1Char(' ')) {
			result.append(item);
			item.clear();
		} else {
			item.append(c);
		}
	}

	return result;
}

static QVariant variantFromString(const QString &s)
{
	if (s.startsWith(QLatin1Char('@'))) {
		if (s.endsWith(")")) {
			if (s.startsWith(QLatin1String("@ByteArray("))) {
				return QVariant(QByteArray::fromBase64(s.toLatin1().mid(11, s.size() - 12)));
			} else if (s.startsWith(QLatin1String("@Variant("))) {
				QByteArray a(QByteArray::fromBase64(s.toLatin1().mid(9, s.size() - 10)));
				QDataStream stream(&a, QIODevice::ReadOnly);
				stream.setVersion(QDataStream::Qt_4_5);
				QVariant result;
				stream >> result;
				return result;
			} else if (s.startsWith(QLatin1String("@Rect("))) {
				QStringList args = splitArgs(s, 5);
				if (args.size() == 4)
					return QVariant(QRect(args[0].toInt(), args[1].toInt(), args[2].toInt(), args[3].toInt()));
			} else if (s.startsWith(QLatin1String("@Size("))) {
				QStringList args = splitArgs(s, 5);
				if (args.size() == 2)
					return QVariant(QSize(args[0].toInt(), args[1].toInt()));
			} else if (s.startsWith(QLatin1String("@Point("))) {
				QStringList args = splitArgs(s, 6);
				if (args.size() == 2)
					return QVariant(QPoint(args[0].toInt(), args[1].toInt()));
			} else if (s == QLatin1String("@Invalid()")) {
				return QVariant();
			}

		}
		if (s.startsWith(QLatin1String("@@")))
			return QVariant(s.mid(1));
	}
	return QVariant(s);
}

static bool variantGeneratorExt(void *udata, QString &err, QByteArray &result, const QVariant &val, int indent)
{
	Q_UNUSED(udata);
	Q_UNUSED(err);
	Q_UNUSED(indent);
	result += '\"';
	switch (val.type()) {
		case QVariant::ByteArray: {
			QByteArray a = val.toByteArray();
			result += "@ByteArray(";
			result += a.toBase64();
			result += ')';
			break;
		}
		case QVariant::KeySequence: {
			QString tmp = val.toString();
			if (tmp.startsWith(QLatin1Char('@')))
				tmp.prepend(QLatin1Char('@'));
			result += tmp.toUtf8();
			break;
		}
		case QVariant::Rect: {
			QRect r = qvariant_cast<QRect>(val);
			result += "@Rect(";
			result += QByteArray::number(r.x());
			result += ' ';
			result += QByteArray::number(r.y());
			result += ' ';
			result += QByteArray::number(r.width());
			result += ' ';
			result += QByteArray::number(r.height());
			result += ')';
			break;
		}
		case QVariant::Size: {
			QSize s = qvariant_cast<QSize>(val);
			result += "@Size(";
			result += QByteArray::number(s.width());
			result += ' ';
			result += QByteArray::number(s.height());
			result += ')';
			break;
		}
		case QVariant::Point: {
			QPoint p = qvariant_cast<QPoint>(val);
			result += "@Point(";
			result += QByteArray::number(p.x());
			result += ' ';
			result += QByteArray::number(p.y());
			result += ')';
			break;
		}

		default: {
			QByteArray a;
			{
				QDataStream s(&a, QIODevice::WriteOnly);
				s.setVersion(QDataStream::Qt_4_5);
				s << val;
			}

			result += "@Variant(";
			result += a.toBase64();
			result += ')';
			break;
		}
	}
	result += '\"';
	return true;
}

static void validateVariant(QVariant *var)
{
	switch (var->type()) {
	case QVariant::String: {
		var->setValue(variantFromString(var->toString()));
		break;
	}
	case QVariant::Map: {
		QVariantMap *map = reinterpret_cast<QVariantMap*>(var->data());
		QVariantMap::iterator it = map->begin();
		for (; it != map->end(); it++) {
			QVariant &value = it.value();
			if (value.type() == QVariant::Map || value.type() == QVariant::List
				|| value.type() == QVariant::String) {
				validateVariant(&value);
			}
		}
		break;
	}
	case QVariant::List: {
		QVariantList *list = reinterpret_cast<QVariantList*>(var->data());
		for (int i = 0; i < list->size(); i++) {
			QVariant &value = (*list)[i];
			if (value.type() == QVariant::Map || value.type() == QVariant::List
				|| value.type() == QVariant::String) {
				validateVariant(&value);
			}
		}
		break;
	}
	default:
		break;
	}
}

QVariantMap *ConfigPrivate::getGroupMap(const QStringList &groups)
{
	auto &level = levels.last();
	Q_ASSERT(level.type == Level::Map);
	auto map = level.map;
	foreach (const auto &group, groups) {
		auto itr = map->find(group);
		if (itr == map->end()) {
			itr = map->insert(group, QVariantMap());
			changed = true;
		} else if (itr->type() != QVariant::Map) {// Found value has the wrong type, fix it
			*itr = QVariantMap();
			changed = true;
		}
		map = reinterpret_cast<QVariantMap*>(itr->data());
	}
	return map;
}

QVariantMap *ConfigPrivate::getGroupMap(const QStringList &groups) const
{
	static QVariantMap emptyMap;
	auto &level = levels.last();
	Q_ASSERT(level.type == Level::Map);
	auto map = level.map;
	foreach (const auto &group, groups) {
		auto itr = map->find(group);
		if (itr == map->end() || itr->type() != QVariant::Map)
			return &emptyMap;
		map = reinterpret_cast<QVariantMap*>(itr->data());
	}
	return map;
}

Config::Config(const QString &fileName, const QString &rootGroup) :
	d(new ConfigPrivate)
{
	d->filePath = fileName.isEmpty() ? QLatin1String("profile") : fileName;
	if (!QFileInfo(d->filePath).isAbsolute())
		d->filePath = SystemInfo::getDir(SystemInfo::ConfigDir).filePath(d->filePath);

	QFile file(d->filePath);
	QByteArray data;
	if (file.open(QFile::ReadOnly))
		data = file.readAll();

	// We can't share data with other configs, because
	// they may remove a group or an array and the config's
	// levels will point to garbage.
	int len = data.length();
	K8JSON::parseRecord(d->data, reinterpret_cast<const uchar*>(data.data()), &len);
	if (d->data.type() == QVariant::Map) {
		validateVariant(&d->data);
		d->changed = false;
	} else {
		d->data = QVariantMap();
		d->changed = true;
	}

	// and, finally, open requested rootGroup
	d->levels.append(Level(QString(), reinterpret_cast<QVariantMap*>(d->data.data())));
	if (!rootGroup.isEmpty()) {
		auto groups = getGroupList(rootGroup);
		auto map = d->getGroupMap(groups);
		auto &level = d->levels.last();
		level.name = groups.last();
		level.map = map;
	}
}

Config::~Config()
{
	if (d->changed)
		save();
}

Config::Config(const Config &cfg) :
	d(cfg.d)
{
}

Config &Config::operator=(const Config &cfg)
{
	d = cfg.d;
	return *this;
}

void Config::save()
{
	QFileInfo info(d->filePath);
	if (!info.exists()) {
		auto dir = info.dir();
		if (!dir.exists())
			dir.mkpath(dir.absolutePath());
	}
	QFile file(d->filePath);
	if (!file.open(QFile::WriteOnly)) {
		qWarning() << "Cannot open" << d->filePath << "to save config:" << file.errorString();
		return;
	}

	QString err;
	QByteArray res;
	K8JSON::generateExCB(0, &variantGeneratorExt, err, res, d->data);
	file.write(res);
	file.close();
}

QString Config::currentName() const
{
	return d->levels.last().name;
}

bool Config::remove(const QString &fullName)
{
	auto groups = getGroupList(fullName);
	Q_ASSERT(!groups.isEmpty() && "Cannot remove a value with an empty name");
	auto name = groups.takeLast();
	Q_ASSERT(!name.isEmpty() && "Cannot remove a value with an empty name");
	auto map = d->getGroupMap(groups);
	return map->remove(name) > 0;
}

void Config::clear()
{
	auto &level = d->levels.last();
	if (level.type == Level::Map) {
		*level.map = QVariantMap();
	} else {
		Q_ASSERT(level.type == Level::List);
		*level.list = QVariantList();
	}
}

void Config::beginGroup(const QString &group)
{
	auto groups = getGroupList(group);
	auto map = d->getGroupMap(groups);
	d->levels.append(Level(groups.last(), map));
}

void Config::endGroup()
{
	Q_ASSERT(d->levels.size() > 1 && d->levels.last().type == Level::Map);
	d->levels.pop_back();
}

void Config::beginGroup(const QString &group) const
{
	auto groups = getGroupList(group);
	auto map = d->getGroupMap(groups);
	d->levels.append(Level(groups.last(), map));
}

void Config::endGroup() const
{
	Q_ASSERT(d->levels.size() > 1 && d->levels.last().type == Level::Map);
	d->levels.pop_back();
}

static QStringList getKeys(const Level &level, function<bool(const QVariant&)> cond)
{
	QStringList keys;
	if (level.type != Level::Map)
		return keys;
	auto itr = level.map->begin();
	auto end = level.map->end();
	for (; itr != end; ++itr) {
		if (cond(*itr))
			keys << itr.key();
	}
	return keys;
}

QStringList	Config::childGroups() const
{
	return getKeys(d->levels.last(), [](const QVariant &v)->bool {
		return v.type() == QVariant::Map;
	});
}

QStringList	Config::childKeys() const
{
	return getKeys(d->levels.last(), [](const QVariant &v)->bool {
		return v.type() != QVariant::Map;
	});
}

bool Config::hasChildKey(const QString &name) const
{
	auto level = d->levels.last();
	if (level.type != Level::Map)
		return false;
	auto itr = level.map->find(name);
	return itr != level.map->end() && itr->type() != QVariant::Map;
}

bool Config::hasChildGroup(const QString &name) const
{
	auto level = d->levels.last();
	if (level.type != Level::Map)
		return false;
	auto itr = level.map->find(name);
	return itr != level.map->end() && itr->type() == QVariant::Map;
}

void Config::beginArray(const QString &array)
{
	auto groups = getGroupList(array);
	auto name = groups.takeFirst();
	Q_ASSERT(!name.isEmpty() && "Cannot open an array with empty name");
	auto map = d->getGroupMap(groups);
	// Get list with the name, if it does not exist, create it
	auto itr = map->find(name);
	if (itr == map->end()) {
		itr = map->insert(name, QVariantList());
		d->changed = true;
	} else if (itr->type() != QVariant::List) { // Found value has the wrong type, fix it
		*itr = QVariantList();
		d->changed = true;
	}
	auto list = reinterpret_cast<QVariantList*>(itr->data());
	// Add new level with type List
	d->levels.append(Level(name, list));
}

void Config::endArray()
{
	auto &level = d->levels.last();
	// If level type is list, just remove the current level;
	// otherwise, setArrayIndex has been called and two
	// levels must be removed.
	if (level.type == Level::List) {
		Q_ASSERT(d->levels.size() > 1);
		d->levels.pop_back();
	} else {
		Q_ASSERT(d->levels.size() > 2);
		d->levels.pop_back();
		Q_ASSERT(d->levels.last().type == Level::List);
		d->levels.pop_back();
	}
}

int Config::arraySize()
{
	auto &level = d->levels.last();
	if (level.type == Level::List)
		return level.list->size();
	return 0;
}

void Config::setArrayIndex(int index)
{
	auto &level = d->levels.last();
	Q_ASSERT(level.type == Level::List);
	auto &list = *level.list;
	QVariant *map = 0;
	if (list.size() <= index) {
		// The list does not contain the index, so create it.
		for (int n = list.size(); index >= n; --index)
			list.append(QVariantMap());
		map = &(list.last());
		d->changed = true;
	} else {
		// The list has the index, we just need to check type, then.
		map = &list[index];
		if (map->type() != QVariant::Map) {
			*map = QVariantMap();
			d->changed = true;
		}
	}
	// Well.. we got our map, cast it to the right type and add level
	Level newLevel(QString::number(index), reinterpret_cast<QVariantMap*>(map->data()));
	d->levels.append(newLevel);
}

void Config::handleArray(const function<void()> &handler)
{
	auto &level = d->levels.last();
	Q_ASSERT(level.type == Level::List);
	auto list = level.list;
	for (int i = 0, n = list->size(); i < n; ++i) {
		setArrayIndex(i);
		handler();
	}
}

void Config::handleGroups(const function<void()> &handler) const
{
	auto &level = d->levels.last();
	Q_ASSERT(level.type == Level::Map);
	auto itr = level.map->begin();
	auto end = level.map->end();
	for (; itr != end; ++itr) {
		if (itr.value().type() == QVariant::Map) {
			beginGroup(itr.key());
			handler();
			endGroup();
		}
	}
}

void Config::handleValues(const function<void(const QString &key, const QVariant &value)> &handler)
{
	auto &level = d->levels.last();
	Q_ASSERT(level.type == Level::Map);
	auto itr = level.map->begin();
	auto end = level.map->end();
	for (; itr != end; ++itr) {
		if (itr.value().type() != QVariant::Map)
			handler(itr.key(), itr.value());
	}
}

void Config::handleValuesAndGroup(const function<void(const QString &key, const QVariant &value)> &handler) const
{
	auto &level = d->levels.last();
	Q_ASSERT(level.type == Level::Map);
	auto itr = level.map->begin();
	auto end = level.map->end();
	for (; itr != end; ++itr)
		handler(itr.key(), itr.value());
}

void Config::setValue(const QString &fullName, const QVariant &value)
{
	auto groups = getGroupList(fullName);
	Q_ASSERT(!groups.isEmpty() && "Cannot set a value with an empty name");
	auto name = groups.takeLast();
	Q_ASSERT(!name.isEmpty() && "Cannot set a value with an empty name");
	auto map = d->getGroupMap(groups);
	(*map)[name] = value;
	d->changed = true;
}

QVariant Config::value(const QString &fullName, const QVariant &def) const
{
	auto groups = getGroupList(fullName);
	Q_ASSERT(!groups.isEmpty() && "Cannot get a value with an empty name");
	auto name = groups.takeLast();
	Q_ASSERT(!name.isEmpty() && "Cannot get a value with an empty name");
	auto map = d->getGroupMap(groups);
	return map->value(name, def);
}

} // Lime


