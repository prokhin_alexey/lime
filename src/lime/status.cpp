/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "status.h"

namespace Lime
{

QString defaultStatusIconName(Status status, const QString &protocol)
{
	QString name = QLatin1String("user-");

	if (status == Status::Offline)
		name += "offline";
	else if (status & Status::Connecting)
		name += "network-connect";
	else if (status == Status::NA)
		name += "away-extended";
	else if (status == Status::DND)
		name += "busy";
	else if (status == Status::Away)
		name += "away";
	else if (status == Status::FreeChat)
		name += "online-chat";
	else if (status == Status::Invisible)
		name += "invisible";
	else if (status & Status::Online)
		name += "online";
	else
		name += "unkwnonstatus";

	if (!protocol.isEmpty()) {
		name += QLatin1Char('-');
		name += protocol;
	}
	return name;
}

QString defaultStatusName(Status status)
{
	if (status == Status::Offline)
		return QT_TRANSLATE_NOOP("Status", "Offline");
	else if (status == Status::Connecting)
		return QT_TRANSLATE_NOOP("Status", "Connecting");
	else if (status == Status::NA)
		return QT_TRANSLATE_NOOP("Status", "Not available");
	else if (status == Status::DND)
		return QT_TRANSLATE_NOOP("Status", "Busy");
	else if (status == Status::Away)
		return QT_TRANSLATE_NOOP("Status", "Away");
	else if (status == Status::FreeChat)
		return QT_TRANSLATE_NOOP("Status", "Free for Chat");
	else if (status == Status::Invisible)
		return QT_TRANSLATE_NOOP("Status", "Invisible");
	else if (status & Status::Online)
		return QT_TRANSLATE_NOOP("Status", "Online");
	else
		return QT_TRANSLATE_NOOP("Status", "Unknown");
}

} // Lime
