/*
 This file is part of Lime.
 Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIME_NOTIFICATIONFILTER_H_
#define LIME_NOTIFICATIONFILTER_H_

#include "notification.h"
#include "config.h"
#include <QBitArray>
#include <QGroupBox>

class QCheckBox;
class QGridLayout;

namespace Lime
{

class NotificationFilter
{
public:
	NotificationFilter() { m_filtered.resize(Notification::LastType + 1); }
	NotificationFilter(const std::initializer_list<Notification::Type> &filtered);
	virtual ~NotificationFilter() {}
	bool isFiltered(int notif) const { return m_filtered.at(notif); }
	void setFilter(int notif, bool enableFilter = true) { m_filtered.setBit(notif, enableFilter); }
	void clear() { m_filtered.fill(false); }
	void enableAll() { m_filtered.fill(true); }
	bool operator&(Notification::Type notif) const { return !isFiltered(notif); }
	static NotificationFilter loadFromConfig(const Config &cfg, const QString &group, const NotificationFilter &defaultValues);
	static void saveToConfig(Config &cfg, const QString &group, const NotificationFilter &values);
private:
	friend QDataStream &operator<<(QDataStream &out, const NotificationFilter &filter);
	friend QDataStream &operator>>(QDataStream &in, NotificationFilter &filter);
private:
	QBitArray m_filtered;
};

class NotificationFilterWidget : public QGroupBox
{
	Q_OBJECT
	Q_PROPERTY(NotificationFilter values READ values WRITE setValues NOTIFY valuesChanged)
public:
	NotificationFilterWidget(const QString &title, const NotificationFilter &enabledTypes = NotificationFilter(),
							 QWidget *parent = 0);
	void setValues(const NotificationFilter &values);
	NotificationFilter values() const;
	void addTestButtonActions(const function<QWidget*(Notification::Type)> &createButtonFunc);
signals:
	void valuesChanged();
private:
	struct Widget
	{
		Notification::Type type;
		QCheckBox *widget;
	};
	QList<Widget> m_widgets;
	QGridLayout *m_layout;
};

QDataStream &operator<<(QDataStream &out, const NotificationFilter &filter);
QDataStream &operator>>(QDataStream &in, NotificationFilter &filter);

} // Lime

#endif // LIME_NOTIFICATIONFILTER_H_
