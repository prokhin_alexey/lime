/*****************************************************************************
    System Info

    Copyright (c) 2007-2008 by Remko Tronçon
                  2008-2011 by Nigmatullin Ruslan <euroelessar@gmail.com>
                  2011 by Nicolay Izoderov <nico-izo@yandex.ru>
                  2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SYSTEMINFO_H
#define SYSTEMINFO_H

#include <QDir>

namespace Lime
{
	
struct SystemInfoPrivate;

class SystemInfo
{
public:
	enum Type
	{
		WinCE       = 'c',
		Win32       = 'w',
		Linux       = 'l',
		MacOSX      = 'm',
		Symbian     = 's',
		Unix        = 'u'
	};
	enum DirType { ConfigDir, HistoryDir, ShareDir, SystemConfigDir, SystemShareDir };
	static QString getFullName();
	static QString getName();
	static QString getVersion();
	static QString getTimezone();
	static int getTimezoneOffset();
	static QDir getDir(DirType type);
	static QString getPath(DirType type);
	static quint32 getSystemVersionID();
	static quint8 getSystemTypeID();
	static QString systemID2String(quint8 type, quint32 id);
private:
	SystemInfo();
	virtual ~SystemInfo();
};
}

#endif // SYSTEMINFO_H
