/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LIME_PROTOCOL_H
#define LIME_PROTOCOL_H

#include "global.h"
#include "extention.h"
#include "signal.h"

namespace Lime
{

class Account;
class Config;

class Protocol : public QObject, public Extension
{
	Q_OBJECT
public:
	enum ProtocolFlags
	{
		ProtocolDefaultFlags = 0
	};
	Protocol(const QString &id, int flags = ProtocolDefaultFlags);
	virtual ~Protocol();
	QString id() const { return m_id; }
	QString name() const { return m_name.isEmpty() ? m_id : m_name; }
	QList<Account*> accounts() const { return m_accounts; }
	Account *account(const QString &id) const;
	int flags() { return m_flags; }
	QObject *toQObject() { return this; }
	Config config(const QString &group);
	static Protocol *get(const QString &id);
	static QList<Protocol*> all();
signals:
	void accountAdded(Lime::Account *account);
protected:
	/* A protocol implementation must call the method whenever
	 * a new account has been created.
	 */
	void addAccount(Account *account);
private:
	int m_flags;
	QString m_id;
	QString m_name;
	QList<Account*> m_accounts;
};

} // Lime

#endif // LIME_PROTOCOL_H
