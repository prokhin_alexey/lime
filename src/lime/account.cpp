/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "account.h"
#include "contact.h"
#include "conference.h"
#include "config.h"
#include "protocol.h"
#include "icon.h"
#include "systeminfo.h"
#include <QApplication>
#include <QAction>

namespace Lime
{

/*	A few words about API for groups/tags:

	Most of the protocols provides means to logically organize your contact list —
	they support groups. However, the groups may be stored in a remote copy of
	the list differently. Some protocol servers keep a tree of the contact list,
	the first level of the tree is the groups, the second is the contacts. On the other
	hand, remote contact list of other protocols comprised of the contacts only, each
	contact entry has a field that contains groups the contact is in — the more
	appropriate name for them is "tags". Later on, I will say about first type of protocols
	that they support groups, and about second type that they support tags.

	Although API for the two types of storage can be very similar, it slightly differ
	in details. The protocols supporting groups may contain an empty group, a contact
	usually can be only in one group. The other protocols type has its peculiarity
	as well: a contact may have several tags, or even no tags at all.

	To fully support all protocols, an internet messenger core must know about the
	distinction and provide API that will satisfy needs of every protocol implementation.
	On the other hand, the API must be easy to use. How do we achieve that?

	First of all, the most essential part of the group API is Contact::groups() method
	that returns a list of all groups a contact is currently in. When the groups
	of a contact has been changed, a protocol implementation must call Contact::updateGroups().
	Contact::setGroups() is to be used to request a contact to be moved into another groups.
	Note that when the contact is about to be put in a non-existent group, the core will
	automatically request creation of the new group.

	Secondly, there must be API to create and remove a group. Account::addEmptyGroup()
	and Account::removeGroup() was added exactly for that. Account::handleNewGroup() and
	Account::handleGroupRemoval() methods should be called by a protocol implementation
	to inform the core that a group has been created or removed accordingly.

	However, since there cannot possibly be a tag that is not set for at least one contact,
	API described in the previous paragraph does not make any sense for tags whatsoever.
	Here we came to a third important thing about the group API — methods of an account
	to retrieve a list of groups, to add a new group and to remove an old one may be easily
	emulated for protocols that supports tags. TagAccount does precisely that. It correctly
	emits groupAdded() when a new tag has been set to a contact, it emits groupRemoved()
	when a tag has been removed from all contacts. It even supports empty groups, although it
	stores them into a local cache. Bottom line, if a protocol supports tags, its implementation
	have only to inherit TagAccount and call Contact::updateGroups() whenever tags of a contact
	has changed, everything else the core will take care of itself.

	If a contact has the SupportsMultiGroups flag, it means it may be in several groups, otherwise,
	it can be only in one group at a time. In the case, an attempt to move a contact into several
	groups will result in undefined behavior.

	Passing an empty list to Contact::setGroups() will result in different behavior, depending
	whether the contact has the SupportDefaultGroup or not. If the flag is not set, the contact
	will be removed from the contact list, otherwise, the contact will be placed into a special
	'Default' group (protocols that support tags should implement the feature by stripping
	all tags from the contact).
*/

Signal<void(Account* account, QString newName, QString oldName)> Account::onLocalNameChanged;
Signal<void(Account*), Private::OnCreatedConnectionPolicy> Account::onCreated;
Signal<void(Account*), Private::OnEnabledConnectionPolicy> Account::onEnabled;
Signal<void(Account*)> Account::onDisabled;
Signal<void(Account*)> Account::onDestroyed;
Signal<void(Account* account, Contact *contact),
			Private::OnContactCreatedConnectionPolicy> Account::onContactCreated;
Signal<void(Account* account, Contact *contact)> Account::onContactRemoved;
Signal<void(Account* account, QString path)> Account::onAvatarChanged;
Signal<void(Account* account, Status newStatus, Status oldStatus)> Account::onStatusChanged;
Signal<void(Account* account, QString text)> Account::onStatusTextChanged;
Signal<void(Account* account, QString name, QVariantMap info)> Account::onExtendedInfoChanged;
Signal<void(Account* account, QString group)> Account::onGroupAdded;
Signal<void(Account* account, QString group)> Account::onGroupRemoved;
Signal<void(Account* account, Conference *conference),
			Private::OnConferenceCreatedConnectionPolicy> Account::onConferenceCreated;
Signal<void(Account* account, Conference *conference)> Account::onConferenceRemoved;

Account::Account(const QString &id, Protocol *protocol, int flags) :
	QObject(protocol),
	m_isEnabled(false),
	m_flags(flags),
	m_protocol(protocol),
	m_id(id),
	m_status(Status::Offline)
{
	Q_ASSERT(protocol);
}

Account::~Account()
{
	auto contacts = m_contacts;
	m_contacts.clear();
	qDeleteAll(contacts);

	auto conferences = m_conferences;
	m_conferences.clear();
	qDeleteAll(conferences);
}

QString Account::statusIconName(Status status)
{
	auto st = status == -1 ? m_status : status;
	return defaultStatusIconName(st, protocol()->name());
}

QString Account::statusName(Status status)
{
	return defaultStatusName(status == -1 ? m_status : status);
}

QIcon Account::statusIcon(Status status)
{
	return Icon(statusIconName(status));
}

Config Account::config(const QString &group)
{
	return Config(id() + QLatin1Char('/') + QLatin1String("account"), group);
}

void Account::setName(const QString &name)
{
	auto old = m_localName;
	m_localName = name;
	emit localNameChanged(name, old);
	onLocalNameChanged(this, name, old);
}

void Account::setStatus(Status status, const QString &statusText)
{
	if (status != m_status)
		setStatusImpl(status, statusText);
}

void Account::setStatusText(const QString &statusText)
{
	if (statusText != m_statusText)
		setStatusImpl(m_status, statusText);
}

void Account::setExtendedInfo(const QString &name, const QVariantMap &info)
{
	if (!setExtendedInfoImpl(name, info))
		updateExtendedInfo(name, info);
}

void Account::updateAvatar(const QString &avatar)
{
	m_avatar = avatar;
	emit avatarChanged(avatar);
	onAvatarChanged(this, avatar);
}

void Account::updateStatus(Status status, const QString &statusText)
{
	if (status != m_status) {
		if (m_flags & AutoRemoveContacts) {
			bool wasConnecting = m_status & Connecting;
			bool isConnecting = status & Connecting;

			if (!wasConnecting && isConnecting) {
				foreach (auto contact, m_contacts) {
					if (contact->isInList())
						contact->m_isInList = Contact::MaybeInList;
				}
			} else if (wasConnecting && !isConnecting) {
				foreach (auto contact, m_contacts) {
					if (contact->m_isInList == Contact::MaybeInList)
						contact->updateInList(false);
				}
			}
		}

		if ((m_status & Online) && !(status & Online)) {
			auto end = m_extendedInfo.end();
			for (auto itr = m_extendedInfo.begin(); itr != end; ) {
				if (!itr->value("keepOffline", true).toBool()) {
					auto key = itr.key();
					itr = m_extendedInfo.erase(itr);
					emit extendedInfoChanged(key, QVariantMap());
				} else {
					++itr;
				}
			}

			foreach (auto contact, m_contacts)
				contact->checkExtendedInfo();
		}

		auto old = m_status;
		m_status = status;
		emit statusChanged(status, old);
		onStatusChanged(this, status, old);
	}

	if (m_statusText != statusText) {
		m_statusText = statusText;
		emit statusTextChanged(statusText);
		onStatusTextChanged(this, statusText);
	}
}

bool Account::enable()
{
	if (m_isEnabled)
		return true;
	if (!enableImpl())
		return false;

	m_isEnabled = true;
	if (ExtensionManager::state() == ExtensionManager::Working)
		config().setValue("isEnabled", true);
	emit enabled();
	onEnabled(this);

	auto cfg = config("contactListCache");
	int ver = cfg.value("version", 0);
	if (ver == 0)
		return true;
	if (ver != 1) {
		qWarning() << "Contact list cache is of unsupported version";
		return true;
	}
	loadContactList(cfg);

	return true;
}

bool Account::disable()
{
	if (!m_isEnabled)
		return true;

	auto cfg = config("contactListCache");
	cfg.clear();
	cfg.setValue("version", 1);
	storeContactList(cfg);

	setStatus(Status::Offline);
	if (disableImpl()) {
		m_isEnabled = false;
		if (ExtensionManager::state() == ExtensionManager::Working)
			config().setValue("isEnabled", false);
		emit disabled();
		onDisabled(this);
		return true;
	}
	return false;
}

Contact *Account::contact(const QString &id, bool create)
{
	auto contact = m_contacts.value(id);
	if (!contact && create) {
		contact = createContact(id);
		// The protocol already must have called addContact(),
		// so we don't have to bother to call it ourselves.
	}
	return contact;
}

void Account::addContact(Contact *contact)
{
	auto id = contact->id();
	Q_ASSERT(!m_contacts.contains(id));
	m_contacts.insert(id, contact);
	emit contactCreated(contact);
	onContactCreated(this, contact);
}

void Account::removeContact(Contact *contact)
{
	auto id = contact->id();
	if (!m_contacts.contains(id))
		return;
	m_contacts.remove(id);
	emit contactRemoved(contact);
	onContactRemoved(this, contact);
}

Conference *Account::conference(const QString &id)
{
	return m_conferences.value(id);
}

void Account::addConference(Conference *conference)
{
	auto id = conference->id();
	Q_ASSERT(!m_conferences.contains(id));
	m_conferences.insert(id, conference);
	emit conferenceCreated(conference);
	onConferenceCreated(this, conference);
}

void Account::removeConference(Conference *conference)
{
	auto id = conference->id();
	if (!m_conferences.contains(id))
		return;
	m_conferences.remove(id);
	emit conferenceRemoved(conference);
	onConferenceRemoved(this, conference);
}

void Account::updateExtendedInfo(const QString &name, const QVariantMap &info)
{
	if (info.isEmpty())
		m_extendedInfo.remove(name);
	else
		m_extendedInfo.insert(name, info);
	emit extendedInfoChanged(name, info);
	onExtendedInfoChanged(this, name, info);
}

bool Account::handleNewGroup(const QString &group)
{
	int oldSize = m_contactListGroups.count();
	m_contactListGroups.insert(group);
	if (oldSize != m_contactListGroups.count()) {
		emit groupAdded(group);
		emit onGroupAdded(this, group);
		return true;
	}
	return false;
}

bool Account::handleGroupRemoval(const QString &group)
{
	if (m_contactListGroups.remove(group)) {
		emit groupRemoved(group);
		emit onGroupRemoved(this, group);
		return true;
	}
	return false;
}

void Account::storeContactList(Config &cfg)
{
	foreach (auto contact, m_contacts) {
		if (!contact->isInList())
			return;
		cfg.beginGroup(contact->id());
		cfg.setValue("name", contact->name());
		cfg.setValue("groups", contact->groups());
		cfg.setValue("avatar", contact->avatar());
		cfg.beginGroup("extendedInfo");
		auto extInfos = contact->allExtendedInfo();
		auto itr = extInfos.constBegin();
		for (auto end = extInfos.constEnd(); itr != end; ++itr) {
			if (itr->value("keepOffline", true).toBool())
				cfg.setValue(itr.key(), itr.value());
		}
		cfg.endGroup();
		cfg.endGroup();
	}
}

void Account::loadContactList(const Config &cfg)
{
	cfg.handleGroups([&] {
		auto contact = createContact(cfg.currentName());
		if (!contact)
			return;
		contact->updateName(cfg.value("name", QString()));
		contact->updateGroups(cfg.value("groups", QStringList()));
		contact->updateAvatar(cfg.value("avatar", QString()));
		cfg.beginGroup("extendedInfo");
		cfg.handleValuesAndGroup([=] (const QString &key, const QVariant &value) {
			contact->setExtendedInfo(key, value.toMap());
		});
		cfg.endGroup();
		contact->updateInList(true);
	});
}

void Account::handleGroupUpdate(Contact *contact, const QStringList &newGroups, const QStringList &oldGroups)
{
	// If the contact is about to be put in a non-existent group,
	// request creation of the group.
	foreach (auto &group, newGroups.toSet() - oldGroups.toSet()) {
		if (!m_contactListGroups.contains(group))
			createEmptyGroupImpl(group);
	}
}

bool Account::setExtendedInfoImpl(const QString &name, const QVariantMap &info)
{
	return false;
}

QString Account::storeAvatar(const QByteArray &avatar, const QString &hash)
{
	auto avatarDir = QString("%1/avatars/%2")
			.arg(SystemInfo::getPath(SystemInfo::ConfigDir))
			.arg(protocol()->id());
	QDir dir(avatarDir);
	if (!dir.exists())
		dir.mkpath(dir.absolutePath());
	auto avatarPath = dir.absoluteFilePath(hash);
	QFile file(avatarPath);
	if (file.open(QIODevice::WriteOnly)) {
		file.write(avatar);
		file.close();
		return avatarPath;
	}
	qWarning() << "Could not open" << avatarPath << ": " << file.errorString();
	return QString();
}

QString Account::getAvatar(const QString &hash)
{
	auto avatarPath = QString("%1/avatars/%2/%3")
			.arg(SystemInfo::getPath(SystemInfo::ConfigDir))
			.arg(protocol()->id())
			.arg(hash);
	QFileInfo info(avatarPath);
	return info.exists() ? avatarPath : QString();
}

void Account::createEmptyGroup(const QString &group)
{
	if (!m_contactListGroups.contains(group))
		createEmptyGroupImpl(group);
}

void Account::removeGroup(const QString &group)
{
	if (m_contactListGroups.contains(group))
		removeGroupImpl(group);
}

QList<Account*> Account::all()
{
	QList<Account*> res;
	foreach (auto proto, Protocol::all())
		res << proto->accounts();
	return res;
}

void Account::addStatusAction(QMetaObject *meta, const QString &name, Status status)
{
	auto action = new Action(name);
	action->setPriority(Action::NormalPriority - status);
	lconnect(action, SIGNAL(actionCreated(QAction*,ActionContainer*)), qApp,
		[status](QAction *action, ActionContainer *container)
	{
		auto acc = sender_cast<Account*>(container->toQObject());
		action->setIcon(acc->statusIcon(status));
	});
	lconnect(action, SIGNAL(triggered(ActionContainer*,bool)), qApp,
		[status](ActionContainer *container)
	{
		auto acc = sender_cast<Account*>(container->toQObject());
		acc->setStatus(status);
	});
	addAction(meta, action);
}

TagAccount::TagAccount(const QString &id, Protocol *protocol, int flags) :
	Account(id, protocol, flags)
{
}


void TagAccount::createEmptyGroupImpl(const QString &group)
{
	// Since we know for sure that the account does not support empty groups,
	// we will create them locally.
	if (handleNewGroup(group)) {
		Q_ASSERT(!m_tagCount.contains(group));
		m_emptyGroups.push_back(group);
	}
	return;
}

void TagAccount::removeGroupImpl(const QString &group)
{
	if (!m_emptyGroups.removeOne(group)) {
		m_tagCount.remove(group);
	} else {
		foreach (auto contact, contacts()) {
			auto groups = contact->groups();
			if (groups.removeOne(group)) {
				if (groups.isEmpty())
					contact->setInList(false);
				else
					contact->setGroups(groups);
			}
		}
	}
	handleGroupRemoval(group);
}

void TagAccount::handleGroupUpdate(Contact *contact, const QStringList &newGroups, const QStringList &oldGroups)
{
	auto newSet = newGroups.toSet();
	auto oldSet = oldGroups.toSet();

	foreach (auto &group, newSet - oldSet) {
		QMap<QString, int>::iterator groupItr = m_tagCount.find(group);
		if (groupItr == m_tagCount.end()) {
			handleNewGroup(group);
			m_emptyGroups.removeOne(group);
			m_tagCount.insert(group, 1);
		} else {
			*groupItr += 1;
		}
	}

	foreach (auto &group, oldSet - newSet) {
		auto groupItr = m_tagCount.find(group);
		Q_ASSERT(groupItr != m_tagCount.end());
		*groupItr -= 1;
		if (*groupItr == 0) {
			Q_ASSERT(!m_emptyGroups.contains(group));
			if (flags() & AutoRemoveContacts && status() & Connecting)
				handleGroupRemoval(group);
			else if (groups().contains(group))
				m_emptyGroups.push_back(group);
			m_tagCount.erase(groupItr);
		}
	}
}

void TagAccount::storeContactList(Config &cfg)
{
	storeEmptyGroups(cfg);
	Account::storeContactList(cfg);
}

void TagAccount::loadContactList(const Config &cfg)
{
	loadEmptyGroups(cfg);
	Account::loadContactList(cfg);
}

void TagAccount::storeEmptyGroups(Config &cfg)
{
	cfg.setValue("emptyGroups", m_emptyGroups);
}

void TagAccount::loadEmptyGroups(const Config &cfg)
{
	foreach (auto &group, cfg.value("emptyGroups", QStringList()))
		createEmptyGroup(group);
}

} // namespace Lime
