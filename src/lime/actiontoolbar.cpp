/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "actiontoolbar.h"
#include <QAction>
#include <QToolButton>

namespace Lime {

ActionToolBar::ActionToolBar(const QString &title, ActionContainer *container, Action::Type type, QWidget *parent) :
	QToolBar(title, parent), m_type(type)
{
	setContainer(container);
}

ActionToolBar::ActionToolBar(ActionContainer *container, Action::Type type, QWidget *parent) :
	QToolBar(parent), m_type(type)
{
	setContainer(container);
}

ActionToolBar::~ActionToolBar()
{
}

void ActionToolBar::setContainer(ActionContainer *container)
{
	m_container = container;
	qDeleteAll(actions());
	clear();
	if (!container)
		return;

	auto fixQAction = [this](QAction *qAction) {
		auto button = qobject_cast<QToolButton*>(this->widgetForAction(qAction));
		if (!button)
			return;
		if (qAction->menu())
			button->setPopupMode(QToolButton::InstantPopup);
		if (m_buttonSize.isValid()) {
			button->setMaximumSize(m_buttonSize);
			button->setMinimumSize(m_buttonSize);
		}
	};

	foreach (auto action, container->actions(m_type)) {
		auto qAction = action->createAction(m_container, this);
		addAction(qAction);
		fixQAction(qAction);
	}

	container->actionAdded.connect([container, this, fixQAction](Action *action) {
		if (!(action->type() & m_type))
			return;
		auto qAction = action->createAction(container, this);
		int priority = action->priority();
		foreach (auto act, this->actions()) {
			if (priority > act->property("actionPriority").toInt()) {
				this->insertAction(act, qAction);
				fixQAction(qAction);
				return;
			}
		}
		this->addAction(qAction);
		fixQAction(qAction);
	});
}

void ActionToolBar::setType(Action::Type type)
{
	if (m_type == type)
		return;
	m_type = type;
	setContainer(m_container);
}

void ActionToolBar::setButtonSize(const QSize &size)
{
	m_buttonSize = size;
	foreach (auto act, this->actions()) {
		auto button = qobject_cast<QToolButton*>(this->widgetForAction(act));
		if (!button)
			continue;
		button->setMaximumSize(size);
		button->setMinimumSize(size);
	}
}

} // Lime
