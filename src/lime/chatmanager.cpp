/*
	 This file is part of Lime.
	 Copyright (C) 2011-2012 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "chatmanager.h"
#include "chat/chatmanager.h"

namespace Lime
{

ChatManager *ChatManager::self = 0;

ChatManager::ChatManager() :
	m_activeUnit(0)
{
	Q_ASSERT(!self);
	self = this;
}

ChatManager::~ChatManager()
{
	self = 0;
}

ChatManager *ChatManager::instance()
{
	return self;
}

void ChatManager::setActive(ChatUnit *unit, bool activate)
{
	if (activate && m_activeUnit == unit)
		return;
	if (m_activeUnit && (activate || m_activeUnit == unit)) {
		Q_ASSERT(unit->m_isActive);
		unit->m_isActive = false;
		emit unit->chatDeactivated();
		unit->onChatDeactivated(unit);
		m_activeUnit = 0;
	}
	if (activate) {
		m_activeUnit = unit;
		if (unit) {
			Q_ASSERT(!unit->m_isActive);
			unit->m_isActive = true;
			emit unit->chatActivated();
			unit->onChatActivated(unit);
			unit->clearUnreadMessages();
		}
	}
}

void ChatManager::handleChatOpening(ChatUnit *unit)
{
	Q_ASSERT(!unit->m_isOpened);
	unit->m_isOpened = true;
	emit unit->chatOpened();
	unit->onChatOpened(unit);
}

void ChatManager::ChatManager::handleChatClosing(ChatUnit *unit)
{
	Q_ASSERT(unit->m_isOpened);
	unit->m_isOpened = false;
	emit unit->chatClosed();
	unit->onChatClosed(unit);
}

void ChatManager::sendChatState(ChatUnit *unit, ChatUnit::ChatState state)
{
	Q_ASSERT(unit->flags() & ChatUnit::SupportsChatStates);
	unit->sendChatStateImpl(state);
}

} // Lime
