/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "contact.h"
#include "account.h"
#include "tooltip.h"
#include <lime/contactlist/contactlist.h>

namespace Lime
{

Signal<void(Contact* /*contact*/, QStringList /*newGroups*/, QStringList /*oldGroups*/)> Contact::onGroupsUpdated;
Signal<void(Contact* /*contact*/, bool /*inList*/)> Contact::onInListChanged;
Signal<void(Contact* /*contact*/)> Contact::onAddedToContactList;
Signal<void(Contact* /*contact*/)> Contact::onRemovedFromContactList;

Contact::Contact(const QString &id, Account *account, int flags) :
	Buddy(id, account, flags), m_isInList(NotInList)
{
	if (this->flags() & ShowAccountInToolTip) {
		toolTipHook += [=](ToolTip *tip) {
			auto id = this->account()->id();
			tip->addField(tr("Account"), id, QString(), ToolTip::Top + 900);
		};
	}
}

Contact::~Contact()
{
	//setInList(false);
	account()->removeContact(this);
}

void Contact::setName(const QString &name)
{
	Q_ASSERT(flags() & NameMayBeChanged   );
	setNameImpl(name);
}

void Contact::setGroup(const QString &group)
{
	setGroupsImpl(QStringList() << group);
}

void Contact::setGroups(const QStringList &groups)
{
	Q_ASSERT((flags() & SupportsMultiGroups) || groups.count() <= 1);
	if (!groups.isEmpty() || flags() & SupportDefaultGroup)
		setGroupsImpl(groups);
	else
		setInList(false);
}

void Contact::setInList(bool inList)
{
	if (isInList() != inList)
		setInListImpl(inList);
}

void Contact::updateGroups(const QStringList &groups)
{
	Q_ASSERT((flags() & SupportsMultiGroups) || groups.count() <= 1);
	if (groups == m_groups)
		return;
	auto old = m_groups;
	account()->handleGroupUpdate(this, groups, old);
	m_groups = groups;
	emit groupsUpdated(groups, old);
	onGroupsUpdated(this, groups, old);
}

void Contact::updateInList(bool inList)
{
	bool wasInList = isInList();
	m_isInList = inList ? IsInList : NotInList;
	if (wasInList == inList)
		return;

	if (inList) {
#if ENABLE_UNITTESTS
		if (ExtensionManager::state() != ExtensionManager::Unittesting)
#endif
			ContactList::addContact(this);
		emit addedToContactList();
		onAddedToContactList(this);
	} else {
#if ENABLE_UNITTESTS
		if (ExtensionManager::state() != ExtensionManager::Unittesting)
#endif
			ContactList::removeContact(this);
		emit removedFromContactList();
		onRemovedFromContactList(this);
	}
	emit inListChanged(inList);
	onInListChanged(this, inList);
}

}

