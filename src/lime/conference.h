/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_CONFERENCE_H
#define LIME_CONFERENCE_H

#include "buddy.h"

namespace Lime {

class Conference;

class ConferenceParticipant : public Buddy
{
	Q_OBJECT
public:
	enum ConferenceParticipantFlags
	{
		ConferenceParticipantDefaultFlags = BuddyDefaultFlags
	};
    ConferenceParticipant(const QString &id, Conference *conference,
						  int flags = ConferenceParticipantDefaultFlags);
    virtual ~ConferenceParticipant();
	int role() const { return m_role; }
	Conference *conference() const { return m_conference; };
public: // static signals
	static Signal<void(ConferenceParticipant *participant, int role)> onRoleChanged;
signals:
	void roleChanged(int role);
protected:
	void updateRole(int role);
private:
	int m_role;
	Conference *m_conference;
};

class Conference : public ChatUnit
{
	Q_OBJECT
public:
	enum ConferenceFlags
	{
		ConferenceDefaultFlags = ChatUnitDefaultFlags
	};
	enum LeaveReason
	{
		User,
		NetworkError,
		Kicked,
		Banned
	};
	Conference(const QString &id, Account *account, int flags = ConferenceDefaultFlags);
	virtual ~Conference();
	QList<ConferenceParticipant*> participants() const { return m_participants; }
	ConferenceParticipant *participant(const QString &name) const;
	ConferenceParticipant *user() const { return m_user; }
	bool isJoined() { return m_isJoined; }
	void join();
	void leave();
public: // static signals
	static Signal<void(Conference *conference)> onJoined;
	static Signal<void(Conference *conference)> onLeft;
	static Signal<void(Conference *conference, ConferenceParticipant *participant)> onParticipantJoined;
	static Signal<void(Conference *conference, ConferenceParticipant *participant)> onParticipantLeft;
	static Signal<void(Conference *conference, ConferenceParticipant *user)> onUserChanged;
signals:
	void joined();
	void left();
	void participantJoined(Lime::ConferenceParticipant *participant);
	void participantLeft(Lime::ConferenceParticipant *participant);
	void userChanged(Lime::ConferenceParticipant *user);
protected:
	virtual void joinImpl() = 0;
	virtual void leaveImpl() = 0;
protected:
	void addParticipant(ConferenceParticipant *participant, const QString &role = QString());
	void removeParticipant(ConferenceParticipant *participant,
						   LeaveReason reason = User,
						   const QString &reasonDesc = QString());
	void handleJoin(ConferenceParticipant *user);
	void handleLeave(LeaveReason reason = User, const QString &reasonDesc = QString());
	void addServiceMessage(const QString &message);
private:
	QList<ConferenceParticipant*> m_participants;
	ConferenceParticipant *m_user;
	bool m_isJoined;
};

}

#endif // LIME_CONFERENCE_H
