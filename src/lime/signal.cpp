/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "signal.h"

namespace Lime
{

int methodIndex(QObject *receiver, const char *method)
{
	const QMetaObject *meta = receiver->metaObject();
	const char type = method[0];
	QByteArray tmp = QMetaObject::normalizedSignature(method + 1);
	auto name = tmp.constData();

	int index = -1;
	switch (type) {
	case '0':
		return meta->indexOfMethod(name);
		break;
	case '1':
		index = meta->indexOfSlot(name);
		break;
	case '2':
		index = meta->indexOfSignal(name);
		break;
	default:
		break;
	}
	if (index == -1)
		qWarning() << "Class" << meta->className() << "does not have method" << name;
	return index;
}

} // Lime

