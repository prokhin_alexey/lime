/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_UTILS_H
#define LIME_UTILS_H

#include <QObject>
#include <functional>
#include <QDir>
#include <QWeakPointer>

namespace Lime {

QList<QDir> themeDirs(const QString &type);
QStringList themes(const QString &type);
QString theme(const QString &type, const QString &theme);
void centerizeWidget(QWidget *widget);

template<typename T>
class Pointer : public QWeakPointer<T>
{
public:
	inline Pointer() : QWeakPointer<T>() {}
	inline Pointer(const QWeakPointer<T> &other) : QWeakPointer<T>(other) {}
	inline Pointer(const QSharedPointer<T> &other) : QWeakPointer<T>(other) {}
	template <class X>
	inline Pointer(X *obj) : QWeakPointer<T>(obj) {}
	inline T &operator*() const { return *QWeakPointer<T>::data(); }
	inline T *operator->() const { return QWeakPointer<T>::data(); }
	inline operator T*() const { return QWeakPointer<T>::data(); }
	inline operator bool() const { return QWeakPointer<T>::data() != 0; }
};

}

#endif // LIME_UTILS_H
