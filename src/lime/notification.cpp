/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "notification.h"
#include "popup/popupmanager.h"
#include "sound/soundmanager.h"
#include "contactlist/contactlist.h"
#include "chatunit.h"
#include "buddy.h"
#include "conference.h"

namespace Lime {

Notification::~Notification()
{
	ignore();
	qDeleteAll(m_actions);
}

void Notification::addAction(NotificationAction *action)
{
	if (m_actions.contains(action))
		return;

	m_actions << action;
	action->destroyed.connect([=] {
		m_actions.removeOne(action);
	});

	if (action->type() == NotificationAction::Accept)
		action->triggered.connect(std::bind(&Notification::acceptImpl, this));
	else
		action->triggered.connect(std::bind(&Notification::rejectImpl, this));

}

void Notification::accept()
{
	acceptImpl();

	foreach (auto action, m_actions) {
		if (action->type() == NotificationAction::Accept)
			action->triggered();
	}
}

void Notification::reject()
{
	rejectImpl();

	foreach (auto action, m_actions) {
		if (action->type() == NotificationAction::Reject)
			action->triggered();
	}
}

void Notification::ignore()
{
	emit ignored();
}

void Notification::acceptImpl()
{
	emit accepted();
}

void Notification::rejectImpl()
{
	emit rejected();
}

void NotificationManager::handleNewMessage(const Message &message)
{
	if (ExtensionManager::state() != ExtensionManager::Working)
		return;

	auto type = message.type();
	if ((type & Message::ServiceMessage) || (type & Message::HistoryMessage))
		return;

	bool isIncoming = message.direction() == Message::Incoming;
	bool fromConference = qobject_cast<Conference*>(message.unit()) != 0;
	Notification::Type notifType;
	if (!fromConference) {
		notifType = isIncoming ?
						Notification::IncomingMessage :
						Notification::OutgoingMessage;
	} else {
		notifType = isIncoming ?
						Notification::IncomingConferenceMessage :
						Notification::OutgoingConferenceMessage;
	}

	SoundManager::instance()->play(notifType);

	if (isIncoming) {
		auto unit = message.unit()->getHistoryUnit();
		Notification *notif = new Notification(notifType,
											   tr("Message from %1").arg(message.senderName()),
											   message.text(),
											   message.unit());
		connect(notif, SIGNAL(accepted()), unit, SLOT(activateChat()));
		connect(notif, SIGNAL(rejected()), unit, SLOT(clearUnireadMessages()));
		PopupManager::instance()->showPopup(notif);
	}
}

void NotificationManager::handleStatusChange(Buddy *buddy, Status newStatus,
											 Status oldStatus, const QString &statusText)
{
	if (ExtensionManager::state() != ExtensionManager::Working)
		return;

	bool isOnline = newStatus & Online;
	bool wasOnline = oldStatus & Online;

	Notification::Type type;
	QString title;
	if (isOnline && !wasOnline) {
		type = Notification::UserWentOnline;
		title = tr("%1 has gone online");
	} else if (!isOnline && wasOnline) {
		type = Notification::UserWentOffline;
		title = tr("%1 has gone offline");
	} else {
		type = Notification::UserChangedStatus;
		title = tr("%1 has changed status");
	}
	title = title.arg(buddy->title());

	Notification *notif = new Notification(type, title, statusText, buddy);
	notif->setImage(QPixmap(buddy->avatar()));
	connect(notif, SIGNAL(accepted()), buddy, SLOT(activateChat()));
	PopupManager::instance()->showPopup(notif);
	SoundManager::instance()->play(type);
}

void NotificationManager::handleChatState(ChatUnit *unit, ChatUnit::ChatState state)
{
	if (ExtensionManager::state() != ExtensionManager::Working)
		return;

	if (state != ChatUnit::StateComposing)
		return;

	Notification *notif = new Notification(Notification::UserTyping, tr("%1 is typing").arg(unit->title()), QString(), unit);
	if (auto buddy = qobject_cast<Buddy*>(unit))
		notif->setImage(QPixmap(buddy->avatar()));
	connect(notif, SIGNAL(accepted()), unit, SLOT(activateChat()));
	PopupManager::instance()->showPopup(notif);
	SoundManager::instance()->play(Notification::UserTyping);
}

bool NotificationManager::enableImpl()
{
	SoundManager::instance()->play(Notification::AppStartup);
	ChatUnit::onNewUnreadMessage.connect(this, [](ChatUnit* unit, const Message &message) {
		Q_UNUSED(message);
		auto contact = qobject_cast<Contact*>(unit);
		if (contact)
			ContactList::instance()->enableBlinking(contact, Notification::IncomingMessage, true);
	});
	ChatUnit::onUnreadMessagesCleared.connect(this, [](ChatUnit* unit) {
		auto contact = qobject_cast<Contact*>(unit);
		if (contact)
			ContactList::instance()->enableBlinking(contact, Notification::IncomingMessage, false);
	});
	return true;
}

NotificationManager::NotificationManager()
{
}

} // Lime
