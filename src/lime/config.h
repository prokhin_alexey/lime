/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_CONFIG_H
#define LIME_CONFIG_H

#include <QVariant>
#include <QSharedDataPointer>
#include <type_traits>
#include "global.h"

namespace Lime {

class Config;
class ConfigPrivate;

namespace Private
{
	template <typename T, bool isEnum>
	class VariantCastHelper;

	template <typename T>
	class VariantCastHelper<T, true>
	{
	public:
		static QVariant toVariant(const T &t)
		{
			return QVariant::fromValue(int(t));

		}
		static T fromVariant(const QVariant &v)
		{
			return static_cast<T>(v.toInt());

		}
	};

	template <typename T>
	class VariantCastHelper<T, false>
	{
	public:
		static QVariant toVariant(const T &t)
		{
			return QVariant::fromValue(t);

		}
		static T fromVariant(const QVariant &v)
		{
			return v.value<T>();

		}
	};

	template <typename T>
	QVariant toVariant(const T &t)
	{
		return VariantCastHelper<T, std::is_enum<T>::value>::toVariant(t);
	}

	template <typename T>
	T fromVariant(const QVariant &v)
	{
		return VariantCastHelper<T, std::is_enum<T>::value>::fromVariant(v);
	}
}

class Config
{
public:
	Config(const QString &fileName = QString(), const QString &rootGroup = QString());
	virtual ~Config();
	Config(const Config &cfg);
	Config &operator=(const Config &cfg);
	void save();
	QString currentName() const; // name of the current group or array
	bool remove(const QString &name);
	void clear(); // clear current group or an array

	void beginGroup(const QString &group);
	void endGroup();
	void beginGroup(const QString &group) const;
	void endGroup() const;
	QStringList	childGroups() const;
	QStringList	childKeys() const;
	bool hasChildKey(const QString &name) const;
	bool hasChildGroup(const QString &name) const;

	void beginArray(const QString &array);
	void endArray();
	int arraySize();
	void setArrayIndex(int index);

	void handleArray(const function<void()> &handler);
	void handleGroups(const function<void()> &handler) const;
	void handleValues(const function<void(const QString &key, const QVariant &value)> &handler);
	void handleValuesAndGroup(const function<void(const QString &key, const QVariant &value)> &handler) const;

	void setValue(const QString &name, const QVariant &value);
	QVariant value(const QString &name, const QVariant &def = QVariant()) const;
	void setValue(const QString &name, const char *value);
	QString value(const QString &name, const char *def) const;
	void setValue(const QString &name, const QLatin1String &value);
	QString value(const QString &name, const QLatin1String &def) const;
	template <typename T>
	void setValue(const QString &name, const T &value);
	template <typename T>
	T value(const QString &name, const T &def = T()) const;
private:
	QSharedDataPointer<ConfigPrivate> d;
};

inline void Config::setValue(const QString &name, const char *value)
{
	setValue(name, QVariant(value));
}

inline QString Config::value(const QString &name, const char *def) const
{
	auto val = value(name);
	return val.isValid() ? val.toString() : QString(def);
}

inline void Config::setValue(const QString &name, const QLatin1String &value)
{
	setValue(name, QString(value));
}

inline QString Config::value(const QString &name, const QLatin1String &def) const
{
	return value(name, QString(def));
}

template <typename T>
void Config::setValue(const QString &name, const T &value)
{
	setValue(name, Private::toVariant<T>(value));
}

template <typename T>
T Config::value(const QString &name, const T &def) const
{
	auto val = value(name);
	return val.isValid() ? Private::fromVariant<T>(val) : def;
}

} // Lime


#endif // LIME_CONFIG_H
