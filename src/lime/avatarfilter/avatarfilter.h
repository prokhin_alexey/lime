#ifndef AVATARFILTER_H
#define AVATARFILTER_H
#include <QString>
#include <QScopedPointer>
#include <QGraphicsEffect>
#include <QIcon>


namespace Lime
{

class AvatarFilterPrivate;
class AvatarFilter
{
	Q_DECLARE_PRIVATE(AvatarFilter)
public:
	AvatarFilter(const QSize &defaultSize/*, Qt::AspectRatioMode mode = Qt::IgnoreAspectRatio*/);
	~AvatarFilter();
	bool draw(QPainter *painter, int x, int y,
			  const QString &path,const QIcon &overlayIcon) const;
	static QIcon icon(const QString &path,const QIcon &overlayIcon = QIcon());
private:
	QScopedPointer<AvatarFilterPrivate> d_ptr;
};

}
#endif // AVATARFILTER_H
