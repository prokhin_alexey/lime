/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "emoticonsmanager.h"
#include <lime/config.h>
#include <lime/utils.h>
#include "sources/kopeteemosource.h"
#include <QTextDocument>

namespace Lime {

void EmoticonsManager::setPack(const QString &pack)
{
	if (m_currentPack == pack)
		return;

	m_currentPack = pack;
	foreach (auto source, m_sources) {
		auto emoticons = source->getPack(pack);
		if (!emoticons.isEmpty()) {
			m_emoticons.clear();
			foreach (auto &emo, emoticons) {
				EmoticonEx emoEx;
				emoEx.icon = emo.icon;
				foreach (auto &token, emo.tokens) {
					auto lower = token.toLower();
					emoEx.tokens << token;
					emoEx.lower << lower;
					emoEx.escaped << Qt::escape(lower);
				}
				m_emoticons << emoEx;
			}

			break;
		}
	}
	emit currentPackChanged();
}

static bool compareEmo(const QString &text, const QString &s, int pos)
{
	int l = s.length();
	if (pos + l > text.length())
		return false;

	for (int i = pos, j = 0; j < l; ++i, ++j) {
		if (text[i].toLower() != s[j])
			return false;
	}

	return true;
}

QList<EmoToken> EmoticonsManager::parse(const QString &text, bool isHtml)
{
	QList<EmoToken> result;
	QString current;

	bool inTag = false;
	QChar c; // current char
	for (int pos = 0, len = text.length(); pos < len; ++pos) {
		c = text.at(pos);

		if (!isHtml || !inTag) {
			if (isHtml && c == '<') {
				inTag = true;
			} else {
				foreach (auto &emo, m_emoticons) {
					foreach (auto &tok, isHtml ? emo.escaped : emo.lower) {
						if (compareEmo(text, tok, pos)) {
							EmoToken emoToken;
							emoToken.text = current;
							emoToken.type = EmoToken::Text;
							result << emoToken;
							current.clear();

							emoToken.icon = emo.icon;
							emoToken.text = tok;
							emoToken.type = EmoToken::Image;
							result << emoToken;
							pos += tok.length();

							goto continue_;
						}
					}
				}
			}

		} else if (c == '>') {
			inTag = false;
		}

		current += c;

	continue_:
		qt_noop();
	}

	EmoToken emoToken;
	emoToken.text = current;
	emoToken.type = EmoToken::Text;
	result << emoToken;

	return result;
}

QList<Emoticon> EmoticonsManager::emoticons()
{
	QList<Emoticon> emoticons;
	foreach (auto &emo, m_emoticons)
		emoticons << emo;
	return emoticons;
}

QList<Emoticon> EmoticonsManager::emoticons(const QString &pack)
{
	if (pack.isEmpty())
		return emoticons();

	foreach (auto source, m_sources) {
		auto emoticons = source->getPack(pack);
		if (!emoticons.isEmpty())
			return emoticons;
	}

	return QList<Emoticon>();
}

bool EmoticonsManager::enableImpl()
{
	m_sources << new KopeteEmoSource;
	auto pack = Config("", "emoticons").value("currentPack", QString());
	if (pack.isEmpty())
		pack = theme("emoticons", "default");
	setPack(pack);
	return true;
}

bool EmoticonsManager::disableImpl()
{
	m_emoticons.clear();
	qDeleteAll(m_sources);
	return true;
}

} // Lime

