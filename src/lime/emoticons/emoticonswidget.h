/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EMOTICONSWIDGET_H_
#define EMOTICONSWIDGET_H_

#include <lime/global.h>
#include <QWidget>

class QGridLayout;
class QLabel;

namespace Lime
{

class EmoticonsWidget : public QWidget
{
	Q_OBJECT
public:
	EmoticonsWidget(QWidget *parent = 0);
	virtual ~EmoticonsWidget();
signals:
	void emoticonClicked(const QString &icon, const QString &token);
	void emoticonsReloaded();
public slots:
	void ensureGeometry();
protected:
	void hideEvent(QHideEvent *ev);
	void showEvent(QShowEvent *ev);
	bool eventFilter(QObject *obj, QEvent *ev);
private:
	void loadEmoticons();
	void startMovie();
private:
	QGridLayout *m_layout;
	QList<QLabel*> m_emoticonWidgets;
};

} /* namespace Lime */
#endif /* EMOTICONSWIDGET_H_ */
