/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_EMOTICONSMANAGER_H
#define LIME_EMOTICONSMANAGER_H

#include <lime/extention.h>
#include <QStringList>

namespace Lime {

struct Emoticon
{
	QString icon;
	QStringList tokens;
};
class EmoticonSource;

struct EmoToken
{
	enum Type
	{
		Image,
		Text
	};
	Type type;
	QString text;
	QString icon;
};

class EmoticonsManager : public QObject, public Extension
{
	Q_OBJECT
public:
	static EmoticonsManager *instance() { static auto manager = new EmoticonsManager; return manager; }
	void setPack(const QString &pack);
	QList<EmoToken> parse(const QString &text, bool isHtml = false);
	QList<Emoticon> emoticons();
	QList<Emoticon> emoticons(const QString &pack);
signals:
	void currentPackChanged();
protected:
	virtual QObject* toQObject() { return this; }
	virtual bool enableImpl();
	virtual bool disableImpl();
private:
	struct EmoticonEx : public Emoticon
	{
		QStringList lower;
		QStringList escaped;
	};
	QString m_currentPack;
	QList<EmoticonEx> m_emoticons;
	QList<EmoticonSource*> m_sources;
};

}

#endif // LIME_EMOTICONSMANAGER_H
