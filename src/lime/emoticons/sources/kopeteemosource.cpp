/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "kopeteemosource.h"
#include <lime/utils.h>
#include <QDir>
#include <QFile>
#include <QDomDocument>

namespace Lime {

QStringList KopeteEmoSource::getAllPacks()
{
	QStringList packs;
	auto paths = themeDirs("emoticons");
	foreach (auto &dir, paths) {
		if (QFileInfo(dir.filePath("emoticons.xml")).exists())
			packs << dir.canonicalPath();
	}
	return packs;
}

static bool checkExtensions(QString &icon, const QStringList &extensions)
{
	foreach (auto &ext, extensions) {
		auto path = icon + ext;
		if (QFileInfo(path).exists()) {
			icon = path;
			return true;
		}
	}
	return false;
}

QList<Emoticon> KopeteEmoSource::getPack(const QString &path)
{
	QList<Emoticon> emoticons;
	QFile file(path + "/emoticons.xml");
	if (!file.open(QIODevice::ReadOnly))
		return emoticons;

	QDomDocument document;
	if (!document.setContent(&file)) {
		qWarning() << "KopeteEmoSource: invalid emoticons.xml";
		return emoticons;
	}

	auto docElem = document.documentElement();
	auto node = docElem.firstChild();
	while (!node.isNull()) {
		auto emoElem = node.toElement();
		node = node.nextSibling();
		if (emoElem.isNull() || emoElem.tagName() != "emoticon")
			continue;

		Emoticon emoticon;
		auto iconName = emoElem.attribute("file");
		auto icon = path + "/" + iconName;

		if (!checkExtensions(icon, QStringList() << ".png" << ".jpg" << ".gif")) {
			qWarning() << "KopeteEmoSource: icon with name" << iconName << "was not found";
			continue;
		}
		emoticon.icon = icon;

		auto tokenNode = emoElem.firstChild();
		while (!tokenNode.isNull()) {
			auto tokenElem = tokenNode.toElement();
			tokenNode = tokenNode.nextSibling();
			if (tokenElem.isNull() || tokenElem.tagName() != "string")
				continue;

			emoticon.tokens << tokenElem.text();
		}

		if (!emoticon.tokens.isEmpty())
			emoticons << emoticon;
	}

	return emoticons;
}

} // Lime

