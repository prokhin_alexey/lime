/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "emoticonswidget.h"
#include "emoticonsmanager.h"
#include <QGridLayout>
#include <QLabel>
#include <QMovie>
#include <QMouseEvent>
#include <cmath>

namespace Lime
{

EmoticonsWidget::EmoticonsWidget(QWidget *parent) :
	QWidget(parent)
{
	m_layout = new QGridLayout(this);
	loadEmoticons();

	lconnect(EmoticonsManager::instance(), SIGNAL(currentPackChanged()), this, [=]() {
		 qDeleteAll(m_emoticonWidgets);
		 m_emoticonWidgets.clear();
		 loadEmoticons();
	});
}

EmoticonsWidget::~EmoticonsWidget()
{
}

void EmoticonsWidget::ensureGeometry()
{
	startMovie();
	adjustSize();
}

void EmoticonsWidget::hideEvent(QHideEvent *ev)
{
	foreach (auto label, m_emoticonWidgets)
		label->movie()->stop();
	QWidget::hideEvent(ev);
}

void EmoticonsWidget::showEvent(QShowEvent *ev)
{
	startMovie();
	QWidget::showEvent(ev);
}

bool EmoticonsWidget::eventFilter(QObject *obj, QEvent *ev)
{
	if (ev->type() == QEvent::MouseButtonRelease) {
		auto event = static_cast<QMouseEvent*>(ev);
		if (event->button() == Qt::LeftButton) {
			auto emo = obj->property("emoticon").toString();
			if (!emo.isEmpty())
				emit emoticonClicked(emo, obj->property("emotoken").toString());
			return true;
		}
	}

	return QWidget::eventFilter(obj, ev);
}

void EmoticonsWidget::loadEmoticons()
{
	auto emoticons = EmoticonsManager::instance()->emoticons();
	int countInColumn = qMin((int)std::floor(std::sqrt((float)emoticons.count())), 10);

	int currentRow = 0, currentColumn = 0;
	foreach (auto &emo, emoticons) {
		auto label = new QLabel(this);
		auto movie = new QMovie(emo.icon, QByteArray(), label);
		label->setMovie(movie);
		label->installEventFilter(this);
		label->setProperty("emoticon", emo.icon);
		label->setProperty("emotoken", emo.tokens.first());
		label->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
		m_emoticonWidgets << label;

		m_layout->addWidget(label, currentRow, currentColumn++);
		if (currentColumn == countInColumn) {
			++currentRow;
			currentColumn = 0;
		}
	}
	emit emoticonsReloaded();
}

void EmoticonsWidget::startMovie()
{
	foreach (auto label, m_emoticonWidgets)
		label->movie()->start();
}

} /* namespace Lime */
