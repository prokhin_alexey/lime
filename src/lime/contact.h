/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_CONTACT_H
#define LIME_CONTACT_H

#include "buddy.h"

namespace Lime {

// Everything that may be added to contact list
class Contact : public Buddy
{
	Q_OBJECT
public:
	enum ContactFlags
	{
		SupportsMultiGroups  = 0x00010000, // The contact may have sevaral groups (like jabber contact)
		NameMayBeChanged     = 0x00020000, // The contact's name may be changed by a user
		ShowAccountInToolTip = 0x00040000,
		GroupsMayBeChanged   = 0x00100000, // The contact's group(s) may be changed by a user
		SupportDefaultGroup  = 0x00200000,
		ContactDefaultFlags  = BuddyDefaultFlags | ShowAccountInToolTip
	};
	Contact(const QString &id, Account *account, int flags = ContactDefaultFlags);
    virtual ~Contact();
	QString group() const { return m_groups.first(); }
	QStringList groups() const { return m_groups; }
	bool isInList() const { return m_isInList != NotInList; }
	void setName(const QString &name);
	void setGroup(const QString &group);
	void setGroups(const QStringList &groups);
	void setInList(bool inList);
	void addToContactList() { setInList(true); }
	void removeFromContactList() { setInList(false); }
public:
	static Signal<void(Contact* /*contact*/, QStringList /*newGroups*/, QStringList /*oldGroups*/)> onGroupsUpdated;
	static Signal<void(Contact* /*contact*/, bool /*inList*/)> onInListChanged;
	static Signal<void(Contact* /*contact*/)> onAddedToContactList;
	static Signal<void(Contact* /*contact*/)> onRemovedFromContactList;
signals:
	void groupsUpdated(const QStringList &newGroups, const QStringList &oldGroups);
	void inListChanged(bool inList);
	void addedToContactList();
	void removedFromContactList();
protected:
	void updateGroups(const QStringList &groups);
	void updateInList(bool inList);
protected:
	virtual void setNameImpl(const QString &name) = 0;
	virtual void setGroupsImpl(const QStringList &groups) = 0;
	virtual void setInListImpl(bool inList) = 0;
private:
	friend class Account;
	enum InList
	{
		NotInList,
		IsInList,
		// We may not know whether the contact is in the contact list or not.
		// A comment of Account::AutoRemoveContacts describes such case.
		// Although, while we are not sure about it, treat the contact as if
		// it were in the contact list.
		MaybeInList
	};
	QStringList m_groups;
	InList m_isInList;
};

}

Q_DECLARE_METATYPE(Lime::Contact*)

#endif // LIME_CONTACT_H
