/*
	 This file is part of Lime.
	 Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "extendedstatusdialog.h"
#include "utils.h"
#include <QTreeWidget>
#include <QFormLayout>
#include <QLineEdit>
#include <QTextEdit>
#include <QDialogButtonBox>
#include <QPushButton>

namespace Lime
{

ExtendedStatusDialog::ExtendedStatusDialog(int flags, QWidget *parent) :
	QWidget(parent), m_titleEdit(0), m_descriptionEdit(0)
{
	resize(300, 350);
	centerizeWidget(this);

	auto layout = new QFormLayout(this);
	auto buttons = new QDialogButtonBox(QDialogButtonBox::Apply | QDialogButtonBox::Cancel, Qt::Horizontal, this);
	m_statusTree = new QTreeWidget(this);
	m_statusTree->setUniformRowHeights(true);
	m_statusTree->setIconSize(QSize(32, 32));
	m_statusTree->setHeaderHidden(true);

	QSizePolicy policy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	policy.setVerticalStretch(3);
	m_statusTree->setSizePolicy(policy);

	if (flags & HasTitle) {
		m_titleEdit = new QLineEdit(this);
		layout->addRow(m_titleEdit);
	}
	if (flags & HasDescrition) {
		m_descriptionEdit = new QTextEdit(this);
		policy.setVerticalStretch(1);
		m_descriptionEdit->setSizePolicy(policy);
		layout->addRow(m_descriptionEdit);
	}
	layout->addRow(m_statusTree);
	layout->addRow(buttons);

	lconnect(m_statusTree, SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)), this,
		[=] (QTreeWidgetItem *current)
	{
		bool isEnabled = current != m_emptyItem;
		if (m_titleEdit)
			m_titleEdit->setEnabled(isEnabled);
		if (m_descriptionEdit)
			m_descriptionEdit->setEnabled(isEnabled);
	});

	lconnect(buttons->button(QDialogButtonBox::Apply), SIGNAL(clicked(bool)), this, [=] {
		emit accepted();
		close();
	});
	lconnect(buttons, SIGNAL(rejected()), this, [=] {
		emit rejected();
		close();
	});
}

ExtendedStatusDialog::~ExtendedStatusDialog()
{
}

auto ExtendedStatusDialog::beginGroup(const QIcon &icon, const QString &title, const QVariant &userData) -> Item
{
	auto groupItem = createItem(icon, title, userData);
	m_groups.push(groupItem);
	return groupItem;
}

void ExtendedStatusDialog::endGroup()
{
	Q_ASSERT(!m_groups.isEmpty());
	m_groups.pop();
}

auto ExtendedStatusDialog::addStatus(const QIcon &icon, const QString &title, const QVariant &userData) -> Item
{
	return createItem(icon, title, userData);
}

void ExtendedStatusDialog::finish(const QIcon &emptyIcon, const QString &emptyTitle)
{
	Q_ASSERT(m_groups.isEmpty());
	m_statusTree->sortByColumn(0, Qt::AscendingOrder);
	m_emptyItem = new QTreeWidgetItem;
	m_emptyItem->setIcon(0, emptyIcon);
	m_emptyItem->setText(0, emptyTitle);
	m_statusTree->insertTopLevelItem(0, m_emptyItem);
}

void ExtendedStatusDialog::setTitle(const QString &title)
{
	Q_ASSERT(m_titleEdit);
	m_titleEdit->setText(title);
}

void ExtendedStatusDialog::setDescription(const QString &description)
{
	Q_ASSERT(m_descriptionEdit);
	m_descriptionEdit->setPlainText(description);
}

void ExtendedStatusDialog::setCurrentItem(Item item)
{
	m_statusTree->setCurrentItem(item ? item : m_emptyItem);
}

QString ExtendedStatusDialog::title()
{
	Q_ASSERT(m_titleEdit);
	return m_titleEdit->text();
}

QString ExtendedStatusDialog::description()
{
	Q_ASSERT(m_descriptionEdit);
	return m_descriptionEdit->toPlainText();
}

QVariant ExtendedStatusDialog::currentUserData()
{
	auto current = m_statusTree->currentItem();
	return current ? current->data(0, Qt::UserRole) : QVariant();
}

QTreeWidgetItem *ExtendedStatusDialog::createItem(const QIcon &icon, const QString &title, const QVariant &userData)
{
	QTreeWidgetItem *item = 0;
	if (!m_groups.isEmpty())
		item = new QTreeWidgetItem(m_groups.last());
	else
		item = new QTreeWidgetItem(m_statusTree);

	item->setIcon(0, icon);
	item->setText(0, title);
	item->setData(0, Qt::UserRole, userData);
	return item;
}

} // Lime
