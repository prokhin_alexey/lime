/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "configtest.h"
#include <QDir>
#include <QDate>

namespace Lime {

const char *cfgName = "unittest.json";

void ConfigTest::basicConfigApi()
{
	enum Enum
	{
		No = 0,
		Yes = 1
	};

	auto tmpDir = QDir::temp();
	tmpDir.remove(cfgName);
	auto cfgPath = tmpDir.filePath(cfgName);
	{
		Config cfg(cfgPath);
		cfg.setValue("str", "val");
		cfg.setValue("int", 1);
		cfg.setValue("bool", true);
		cfg.beginGroup("group");
		cfg.setValue("date", QDate(1989, 7, 24));
		cfg.endGroup();
		cfg.beginArray("array");
		cfg.setArrayIndex(5);
		cfg.setValue("enum", Enum(Yes));
		cfg.endArray();
	}
	{
		Config cfg(cfgPath);
		QCOMPARE(cfg.value("str", ""), QString("val"));
		QCOMPARE(cfg.value("int", 0), 1);
		QCOMPARE(cfg.value("bool", false), true);
		cfg.beginGroup("group");
		QCOMPARE(cfg.value("date", QDate()), QDate(1989, 7, 24));
		cfg.endGroup();
		cfg.beginArray("array");
		cfg.setArrayIndex(5);
		QCOMPARE(cfg.value("enum", Enum(No)), Yes);
		cfg.endArray();
		QCOMPARE(cfg.value("str", ""), QString("val"));
		QCOMPARE(cfg.value("empty", ""), QString(""));
		QCOMPARE(cfg.childGroups(), QStringList() << "group");
		QCOMPARE(cfg.childKeys(), QStringList() << "array" << "bool" << "int" << "str");
		QVERIFY(!cfg.hasChildKey("nonexistent"));
		QVERIFY(!cfg.hasChildGroup("nonexistent"));
	}
	tmpDir.remove(cfgName);
}

void ConfigTest::lamdaConfigApi()
{
	auto tmpDir = QDir::temp();
	tmpDir.remove(cfgName);
	auto cfgPath = tmpDir.filePath(cfgName);
	{
		Config cfg(cfgPath);
		cfg.beginGroup("group");
		cfg.endGroup();
		cfg.beginGroup("group1");
		cfg.endGroup();
		cfg.beginGroup("group2");
		cfg.endGroup();

		cfg.beginArray("array");
		cfg.setArrayIndex(0);
		cfg.setValue("str", "lambda");
		cfg.setValue("str1", "Api");
		cfg.endArray();
	}
	{
		QStringList groups;
		Config cfg(cfgPath);
		cfg.handleGroups([&](){
			groups << cfg.currentName();
		});
		QCOMPARE(groups, QStringList() << "group" << "group1" << "group2");

		QString lambdaApi;
		cfg.beginArray("array");
		cfg.handleArray([&]() {
			cfg.handleValues([&](QString, QVariant value) {
				lambdaApi += value.toString();
			});
		});
		cfg.endArray();
		QCOMPARE(lambdaApi, QString("lambdaApi"));
	}
	tmpDir.remove(cfgName);
}

void ConfigTest::removeAndClear()
{
	auto tmpDir = QDir::temp();
	tmpDir.remove(cfgName);
	auto cfgPath = tmpDir.filePath(cfgName);
	Config cfg(cfgPath);
	{
		cfg.beginGroup("group");
		cfg.setValue("str", "I'm suicidal value");
		cfg.clear();
		QCOMPARE(cfg.value("str", QString()), QString());
		cfg.endGroup();
	}
	{
		cfg.beginGroup("group");
		cfg.setValue("value", 3.14);
		cfg.remove("value");
		QCOMPARE(cfg.value("value", 9.8), 9.8);
		cfg.endGroup();
		cfg.beginArray("array");
		cfg.endArray();
		cfg.remove("group");
		cfg.remove("array");
		QCOMPARE(cfg.hasChildGroup("group"), false);
		QCOMPARE(cfg.hasChildGroup("array"), false);
	}
}

UNITTEST(ConfigTest);

} // Lime
