/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_SIGNALTEST_H
#define LIME_SIGNALTEST_H

#include "testutils.h"
#include <lime/signal.h>

namespace Lime {

class SignalTest : public QObject, public SignalGuard
{
	Q_OBJECT
public slots:
	void slot1(int i, const QString &str);
	void slot2();
	void slot3();
private slots:
	void general();
	void chaining();
	void guard();
};

} // Lime

#endif // LIME_SIGNALTEST_H
