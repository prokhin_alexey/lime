/*
	This file is part of Lime.
	Copyright (C) 2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LIME_TST_DATAFORMS_H
#define LIME_TST_DATAFORMS_H

#include <lime/dataforms.h>
#include "testutils.h"

namespace Lime {

class DataFormsTest : public QObject
{
	Q_OBJECT
private slots:
	void testStringChooserField();
	void testStringField();
	void testStringListField();
	void testMultiStringField();
	void testBoolField();
	void testIntField();
	void testDoubleField();
	void testDateField();
	void testDateTimeField();
	void testPixmapChooserField();
	void testDataFormComplition();
	void testDialogButtons();
private:
	static QString getLabelHelper(DataField *field);
	void checkMainApi(DataForm *dataForm,
					  function<DataField *(const QString &label, const QString &name)> getField,
					  function<QString(DataField *field)> getLabel = &getLabelHelper);
};

} // Lime

#endif // LIME_TST_DATAFORMS_H
