/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LIME_TESTUTILS_H
#define LIME_TESTUTILS_H

#include <lime/global.h>
#include <lime/signal.h>
#include <QTest>
#include <QSignalSpy>

namespace Lime {

template<typename Signature>
class StaticSignalSpy;

typedef QList<QList<QVariant> > SpyBase;

namespace Private {

template <int N, typename... T>
struct Appender;

template <int N, typename First, typename... Others>
struct Appender<N, First, Others...>
{
	static void append(QList<QVariant> &array, const First &first, Others... values)
	{
		array.append(qVariantFromValue(first));
		Appender<N-1, Others...>::append(array, values...);
	}
};

template <>
struct Appender<0>
{
	static void append(QList<QVariant> &)
	{
	}
};

} // Private

template<typename T, typename... Args>
class StaticSignalSpy<void(T *, Args...)>: protected SignalGuard, public SpyBase
{
public:
	StaticSignalSpy(T *sender, Signal<void(T *, Args...)> signal) :
		m_sender(sender), m_signal(signal)
	{
		connectSignal();
	}
	StaticSignalSpy(const StaticSignalSpy<void(T *, Args...)> &other) :
		m_sender(other.m_sender), m_signal(other.m_signal)
	{
		connectSignal();
	}
	StaticSignalSpy &operator=(const StaticSignalSpy<void(T *, Args...)> &other)
	{
		m_signal.disconnect(m_connId);
		m_sender = other.m_sender;
		m_signal = other.m_signal;
		connectSignal();
		return *this;
	}
	~StaticSignalSpy()
	{
		m_signal.disconnect(m_connId);
	}
private:
	void connectSignal()
	{
		m_connId = m_signal.connect([=](T *sender, Args... args) {
			if (sender == m_sender) {
				QList<QVariant> values;
				Private::Appender<sizeof...(Args), Args...>::append(values, args...);
				*this << values;
			}
		});
	}
private:
	T *m_sender;
	Signal<void(T *, Args...)> m_signal;
	int m_connId;
};

template<typename T, typename Signature>
StaticSignalSpy<Signature> createStaticSignalSpy(T *sender, Signal<Signature> signal)
{
	return StaticSignalSpy<Signature>(sender, signal);
}

template<typename... Args>
class StaticSignalSpy<void(Args...)>: protected SignalGuard, public SpyBase
{
public:
	StaticSignalSpy(Signal<void(Args...)> signal) :
		m_signal(signal)
	{
		connectSignal();
	}
	StaticSignalSpy(const StaticSignalSpy<void(Args...)> &other) :
		m_signal(other.m_signal)
	{
		connectSignal();
	}
	StaticSignalSpy &operator=(const StaticSignalSpy<void(Args...)> &other)
	{
		m_signal.disconnect(m_connId);
		m_signal = other.m_signal;
		connectSignal();
		return *this;
	}
	~StaticSignalSpy()
	{
		m_signal.disconnect(m_connId);
	}
private:
	void connectSignal()
	{
		m_connId = m_signal.connect([=](Args... args) {
			QList<QVariant> values;
			Private::Appender<sizeof...(Args), Args...>::append(values, args...);
			*this << values;
		});
	}
private:
	Signal<void(Args...)> m_signal;
	int m_connId;
};

template<typename Signature>
StaticSignalSpy<Signature> createStaticSignalSpy(Signal<Signature> signal)
{
	return StaticSignalSpy<Signature>(signal);
}

template<typename T, typename Signature>
class SignalChecker
{
public:
	SignalChecker<T, Signature>(T *sender, const char *signal, Signal<Signature> staticSignal) :
		m_spy(new QSignalSpy(sender, signal)),
		m_staticSpy(new StaticSignalSpy<Signature>(sender, staticSignal))
	{
	}
	~SignalChecker<T, Signature>()
	{
		delete m_spy;
	}
	void check(const function<void(SpyBase &spy)> &checker)
	{
		checker(*m_spy);
		checker(*m_staticSpy);
	}
	bool isEmpty() const
	{
		return m_spy->isEmpty() && m_staticSpy->isEmpty();
	}
	int count() const
	{
		return m_spy->count() == m_staticSpy->count() ? m_spy->count() : -1;
	}
	void removeFirst()
	{
		QVERIFY(count() >= 1);
		m_spy->removeFirst();
		m_staticSpy->removeFirst();
	}
private:
	QSignalSpy *m_spy;
	StaticSignalSpy<Signature> *m_staticSpy;
};

template<typename T, typename Signature>
SignalChecker<T, Signature> createSignalChecker(T *sender, const char *signal, Signal<Signature> staticSignal)
{
	return SignalChecker<T, Signature>(sender, signal, staticSignal);
}

void addUnittest(QObject *test);
bool runUnittests();

#define UNITTEST(name) \
struct Init##name \
{ \
	Init##name() \
	{ \
		addUnittest(new name); \
	} \
}; \
static Init##name init##name;

}

#endif // LIME_TESTUTILS_H
