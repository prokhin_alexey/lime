/*
    This file is part of Lime.
    Copyright (C) 2011-2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "testutils.h"

namespace Lime {


static QList<QObject*> &tests()
{
	static QList<QObject*> tests;
	return tests;
}

void addUnittest(QObject *test)
{
	tests() << test;
}

bool runUnittests()
{
	bool isFailed = false;
	foreach (auto test, tests())
		isFailed = QTest::qExec(test) != 0 || isFailed;
	qDeleteAll(tests());
	tests().clear();
	return !isFailed;
}

} // Lime

