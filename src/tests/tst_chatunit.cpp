/*
	 This file is part of Lime.
	 Copyright (C) 2012 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tst_chatunit.h"

namespace Lime
{

void ChatUnitTest::initTestCase()
{
	qRegisterMetaType<Message>("Lime::Message");
	qRegisterMetaType<ChatUnit::ChatState>("Lime::ChatUnit::ChatState");
	m_proto = new NullProtocol;
	m_account = new NullAccount("null");
	m_chatManager = new NullChatManager;
}

void ChatUnitTest::cleanupTestCase()
{
	delete m_account;
	delete m_proto;
	delete m_chatManager;
}

void ChatUnitTest::testName()
{
	NullContact contact("contact", m_account);
	auto sigChecker = createSignalChecker(&contact,
			SIGNAL(nameChanged(QString,QString)), ChatUnit::onNameChanged);

	auto testName = [&](QString name, QString newValue, QString oldValue) {
		contact.setName(name);
		QCOMPARE(contact.name(), newValue);

		sigChecker.check([&](SpyBase &spy) {
			QCOMPARE(spy.count(), 1);
			QList<QVariant> arguments = spy.takeFirst();
			QCOMPARE(arguments.count(), 2);
			QCOMPARE(arguments.at(0).toString(), newValue);
			QCOMPARE(arguments.at(1).toString(), oldValue);
		});

		contact.setName(name);
		QCOMPARE(contact.name(), newValue);
		QVERIFY(sigChecker.isEmpty());
	};

	QCOMPARE(contact.name(), contact.id());
	testName("alex", "alex", contact.id());
	testName("al", "al", "alex");
	testName("", contact.id(), "al");
}

void ChatUnitTest::testTitle()
{
	NullContact contact("contact", m_account);
	auto sigChecker = createSignalChecker(&contact,
			SIGNAL(titleChanged(QString,QString)), ChatUnit::onTitleChanged);

	auto testTitleIsNotChanged = [&](QString title, QString newValue) {
		contact.setTitle(title);
		QCOMPARE(contact.title(), newValue);
		QVERIFY(sigChecker.isEmpty());
	};
	auto testTitleChangedSignalEmission = [&](QString newValue, QString oldValue) {
		sigChecker.check([&](SpyBase &spy) {
			QCOMPARE(spy.count(), 1);
			QList<QVariant> arguments = spy.takeFirst();
			QCOMPARE(arguments.count(), 2);
			QCOMPARE(arguments.at(0).toString(), newValue);
			QCOMPARE(arguments.at(1).toString(), oldValue);
		});
	};
	auto testTitle = [&](QString title, QString newValue, QString oldValue) {
		contact.setTitle(title);
		QCOMPARE(contact.title(), newValue);
		testTitleChangedSignalEmission(newValue, oldValue);
		testTitleIsNotChanged(title, newValue);
	};

	QCOMPARE(contact.title(), contact.id());
	testTitle("alex", "alex", contact.id());
	testTitle("al", "al", "alex");
	testTitle("", contact.id(), "al");

	contact.setName("alex");
	QCOMPARE(contact.title(), contact.name());
	testTitleChangedSignalEmission(contact.name(), contact.id());
	contact.setName("alex");
	QCOMPARE(contact.title(), contact.name());
	QVERIFY(sigChecker.isEmpty());
	contact.setName("al");
	testTitleChangedSignalEmission("al", "alex");

	testTitleIsNotChanged("", contact.name());
	testTitle("alex", "alex", contact.name());

	testTitle("", contact.name(), "alex");
	testTitleIsNotChanged(contact.name(), contact.name());
	contact.setName("_alex");
	QVERIFY(sigChecker.isEmpty());
}

template <typename T>
static void wasSignalEmitted(T &signalChecker)
{
	QCOMPARE(signalChecker.count(), 1);
	signalChecker.removeFirst();
}

void ChatUnitTest::testChatApi()
{
	NullContact contact("contact", m_account);
	auto chatOpenedChecker      = createSignalChecker(&contact, SIGNAL(chatOpened()),      ChatUnit::onChatOpened);
	auto chatClosedChecker      = createSignalChecker(&contact, SIGNAL(chatClosed()),      ChatUnit::onChatClosed);
	auto chatActivatedChecker   = createSignalChecker(&contact, SIGNAL(chatActivated()),   ChatUnit::onChatActivated);
	auto chatDeactivatedChecker = createSignalChecker(&contact, SIGNAL(chatDeactivated()), ChatUnit::onChatDeactivated);

	QVERIFY(!contact.isChatActive());
	QVERIFY(!contact.isChatOpened());

	m_chatManager->openChat(&contact);
	wasSignalEmitted(chatOpenedChecker);
	QVERIFY(contact.isChatOpened());
	QVERIFY(!contact.isChatActive());

	contact.activateChat();
	wasSignalEmitted(chatActivatedChecker);
	QVERIFY(contact.isChatOpened());
	QVERIFY(contact.isChatActive());

	contact.activateChat();
	QVERIFY(chatActivatedChecker.isEmpty());
	QVERIFY(contact.isChatOpened());
	QVERIFY(contact.isChatActive());

	m_chatManager->deactivateChat(&contact);
	wasSignalEmitted(chatDeactivatedChecker);
	QVERIFY(contact.isChatOpened());
	QVERIFY(!contact.isChatActive());

	m_chatManager->deactivateChat(&contact);
	QVERIFY(chatDeactivatedChecker.isEmpty());
	QVERIFY(contact.isChatOpened());
	QVERIFY(!contact.isChatActive());

	m_chatManager->closeChat(&contact);
	wasSignalEmitted(chatClosedChecker);
	QVERIFY(!contact.isChatActive());
	QVERIFY(!contact.isChatOpened());

	QVERIFY(chatOpenedChecker.isEmpty());
	QVERIFY(chatClosedChecker.isEmpty());
	QVERIFY(chatActivatedChecker.isEmpty());
	QVERIFY(chatDeactivatedChecker.isEmpty());
}

void ChatUnitTest::testUnreadMessages()
{
	NullContact contact("contact", m_account);
	NullContact::receiver = &contact;

	auto newUnreadMessageChecker = createSignalChecker(&contact,
			SIGNAL(newUnreadMessage(Lime::Message)), ChatUnit::onNewUnreadMessage);
	auto unreadMessagesClearedChecker = createSignalChecker(&contact,
			SIGNAL(unreadMessagesCleared()), ChatUnit::onUnreadMessagesCleared);

	int msgCount = 0;
	auto check = [&](const QString &text) {
		QVERIFY(contact.unreadMessages().count() == msgCount);
		contact.sendMessage(Message(text, Message::Outgoing));
		++msgCount;

		QCOMPARE(contact.unreadMessages().count(), msgCount);
		QCOMPARE(contact.unreadMessages().at(msgCount-1).text(), text);
		newUnreadMessageChecker.check([&](SpyBase &spy) {
			QCOMPARE(spy.count(), 1);
			QList<QVariant> arguments = spy.takeFirst();
			QCOMPARE(arguments.count(), 1);
			QCOMPARE(arguments.at(0).value<Message>().text(), text);
		});
		QVERIFY(unreadMessagesClearedChecker.isEmpty());
	};

	for (int i = 0; i < 10; ++i)
		check(QString("Message %1").arg(i));

	contact.clearUnreadMessages();
	QVERIFY(contact.unreadMessages().isEmpty());
	wasSignalEmitted(unreadMessagesClearedChecker);

	msgCount = 0;
	check("msg");
	contact.activateChat();
	QVERIFY(contact.unreadMessages().isEmpty());
	wasSignalEmitted(unreadMessagesClearedChecker);

	QVERIFY(newUnreadMessageChecker.isEmpty());
}

void ChatUnitTest::testIncomingMessages()
{
	QList<Message> incomingMessages;
	SignalGuard guard;
	m_chatManager->newMessage.connect(&guard, [&](const Message &msg) {
		if (msg.direction() == Message::Incoming) {
			incomingMessages << msg;
		}
	});

	NullContact contact("contact", m_account);
	NullContact::receiver = &contact;

	auto messageReceivedChecker = createSignalChecker(&contact,
			SIGNAL(messageReceived(Lime::Message)), ChatUnit::onMessageReceived);

	auto check = [&](const QString &text) {
		contact.sendMessage(Message(text, Message::Outgoing));
		QCOMPARE(incomingMessages.count(), 1);
		QCOMPARE(incomingMessages.takeFirst().text(), text);

		messageReceivedChecker.check([&](SpyBase &spy) {
			QCOMPARE(spy.count(), 1);
			QList<QVariant> arguments = spy.takeFirst();
			QCOMPARE(arguments.count(), 1);
			QCOMPARE(arguments.at(0).value<Message>().text(), text);
		});
	};

	for (int i = 0; i < 10; ++i)
		check(QString("Message %1").arg(i));
}

void ChatUnitTest::testChatState()
{
	NullContact contact("contact", m_account);
	NullContact::receiver = &contact;

	auto chatStateChangedChecker = createSignalChecker(&contact,
			SIGNAL(chatStateChanged(Lime::ChatUnit::ChatState)),
			ChatUnit::onChatStateChanged);

	QCOMPARE(contact.chatState(), ChatUnit::StateGone);

	auto check = [&](ChatUnit::ChatState state) {
		m_chatManager->sendChatState(&contact, state);
		QCOMPARE(contact.chatState(), state);
		chatStateChangedChecker.check([&](SpyBase &spy) {
			QCOMPARE(spy.count(), 1);
			QList<QVariant> arguments = spy.takeFirst();
			QCOMPARE(arguments.count(), 1);
			QCOMPARE(arguments.at(0).value<ChatUnit::ChatState>(), state);
		});
	};

	check(ChatUnit::StateActive);
	check(ChatUnit::StateInActive);
	check(ChatUnit::StateComposing);
	check(ChatUnit::StatePaused);
	check(ChatUnit::StateGone);
}

UNITTEST(ChatUnitTest);

} // Lime
