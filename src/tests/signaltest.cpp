/*
    This file is part of Lime.
    Copyright (C) 2011  Alexey Prokhin <alexey.prokhin@yandex.ru>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "signaltest.h"

namespace Lime
{


static int callFlags = 0;

void SignalTest::slot1(int i, const QString &str)
{
	if (callFlags == 0x01) {
		QVERIFY(i == 4);
		QVERIFY(str == "Hello");
		callFlags |= 0x02;
	} else {
		QVERIFY(!"It is not supposed to be called");
	}
}

void SignalTest::slot2()
{
	QVERIFY(callFlags == 0x07);
	callFlags |= 0x08;
}

void SignalTest::slot3()
{
	QVERIFY(callFlags == 0x0f);
	callFlags |= 0x10;
}

void SignalTest::general()
{
	callFlags = 0;
	auto res = new SignalTest;
	Signal<void(int, QString)> sig;

	sig.connect(res, SLOT(slot1(int,QString)));
	auto connId = sig.connect([this](int i, QString str) {
		if (callFlags == 0) {
			QVERIFY(i == 4);
			QVERIFY(str == "Hello");
			callFlags |= 0x01;
		} else if (callFlags == 0x03) {
			QVERIFY(i == 7);
			QVERIFY(str == "slot1");
			callFlags |= 0x04;
		} else {
			QVERIFY(!"It is not supposed to be called");
		}
	});
	sig(4, "Hello");
	delete res;
	sig(7, "slot1");
	QVERIFY(sig.disconnect(connId));
	QVERIFY(!sig.disconnect(connId));
	sig(2, "It must call nothing");

	res = new SignalTest;
	sig.connect(res, SLOT(slot2()));
	sig(0, "");
	sig.disconnect(res, 0);
	sig(0, "");

	sig.connect(res, SLOT(slot3()));
	sig(0, "");
	sig.disconnect(res, SLOT(slot3()));
	sig(0, "");

	QVERIFY(callFlags == 0x1f);
	delete res;
}

void SignalTest::chaining()
{
	callFlags = 0;
	Signal<void()> sig1;
	Signal<void()> sig2;
	sig1.connect(sig2);
	sig1 += [this]() {
		callFlags |= 0x01;
	};
	sig2.connect([this]() {
		callFlags |= 0x02;
	});
	sig1();
	QVERIFY(callFlags == 0x03);
}

void SignalTest::guard()
{
	int count = 0;
	auto guard = new SignalTest;
	Signal<void()> sig;
	sig.connect(guard, [&] { ++count; });
	sig();
	delete guard;
	sig();
	QVERIFY(count == 1);
}

UNITTEST(SignalTest);

} // Lime

