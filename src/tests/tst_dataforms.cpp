/*
	This file is part of Lime.
	Copyright (C) 2012  Alexey Prokhin <alexey.prokhin@yandex.ru>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "tst_dataforms.h"
#include <QTest>
#include <QComboBox>
#include <QLabel>
#include <QLineEdit>
#include <QListView>
#include <QPushButton>
#include <QTextEdit>
#include <QCheckBox>
#include <QSpinBox>
#include <QDateEdit>
#include <QValidator>
#include <QSignalSpy>
#include <QDialogButtonBox>

namespace QTest {

bool qCompare(const QString &t1, const char *t2, const char *actual,
			  const char *expected, const char *file, int line)
{
	return qCompare<QString>(t1, QString(t2), actual, expected, file, line);
}

} // QTest

namespace Lime {

#define CHECK_VALUE_HELPER_EX(Type, WidgetMethod, val, widgetVal) \
	QVERIFY(field->value().type() == QVariant:: Type); \
	QCOMPARE(field->value().to##Type (), val); \
	QCOMPARE(widget-> WidgetMethod, widgetVal); \

#define CHECK_VALUE_HELPER(Type, WidgetMethod, val) \
	CHECK_VALUE_HELPER_EX(Type, WidgetMethod, val, val)

#define CHECK_NEW_VALUE_HELPER_EX(Type, WidgetMethod, newVal, widgetVal) \
	CHECK_VALUE_HELPER_EX(Type, WidgetMethod, newVal, widgetVal) \
	QCOMPARE(spy.count(), 1); \
	spy.removeFirst();

#define CHECK_NEW_VALUE_HELPER(Type, WidgetMethod, newVal) \
	CHECK_NEW_VALUE_HELPER_EX(Type, WidgetMethod, newVal, newVal)

class TestValidator : public QValidator
{
public:
	TestValidator(int len) :
		m_len(len)
	{}
	State validate(QString &str, int &) const
	{
		if (str.length() < m_len)
			return Intermediate;
		else if (str.length() == m_len)
			return Acceptable;
		return Invalid;
	}
private:
	int m_len;
};

QString DataFormsTest::getLabelHelper(DataField *field)
{
	Q_ASSERT(field->labelWidget());
	auto str = field->labelWidget()->text();
	str.resize(str.length() - 1);
	return str;
}

void DataFormsTest::checkMainApi(DataForm *dataForm,
		function<DataField *(const QString &label, const QString &name)> getField,
		function<QString(DataField *field)> getLabel)
{
	DataField *field = getField(QString(), QString());
	auto widget = field->widget();

	QVERIFY(field->name().isNull());
	QVERIFY(widget->objectName().isNull());
	QVERIFY(field->label().isNull());
	QVERIFY(!field->labelWidget());

	field->setName("fieldName");
	QCOMPARE(field->name(), "fieldName");
	QCOMPARE(widget->objectName(), "fieldName");
	QVERIFY(!field->labelWidget());

	field->setLabel("fieldLabel");
	QCOMPARE(field->label(), "fieldLabel");
	QCOMPARE(getLabel(field), "fieldLabel");

	field = getField("fieldLabel", "fieldName");
	QCOMPARE(field->name(), "fieldName");
	QCOMPARE(widget->objectName(), "fieldName");
	QCOMPARE(field->label(), "fieldLabel");
	QCOMPARE(getLabel(field), "fieldLabel");

	QVERIFY(field->isEnabled());
	field->setEnabled(false);
	QVERIFY(!field->isEnabled());
	field->setEnabled(true);
	QVERIFY(field->isEnabled());

	QVERIFY(field->isVisible());
	field->setVisible(false);
	QVERIFY(!field->isVisible());
	field->setVisible(true);
	QVERIFY(field->isVisible());
}

void DataFormsTest::testStringChooserField()
{
	DataForm dataForm;
	checkMainApi(&dataForm, [&](const QString &label, const QString &name) {
		return dataForm.addStringChooser(QString(), label, name);
	});

	auto field = dataForm.addStringChooser("second");
	auto widget = qobject_cast<QComboBox*>(field->widget());
	auto spy = createStaticSignalSpy(field->onValueChanged);

#define CHECK_NEW_VALUE(newVal) \
	CHECK_NEW_VALUE_HELPER(String, currentText(), newVal)

	QVERIFY(widget);
	CHECK_VALUE_HELPER(String, currentText(), QString());
	QCOMPARE(field->isComplete(), false);

	QVERIFY(spy.isEmpty());
	QVERIFY(!field->isEditable());
	QVERIFY(!widget->isEditable());
	field->setEditable(true);
	QVERIFY(field->isEditable());
	QVERIFY(widget->isEditable());
	CHECK_NEW_VALUE("second");
	QCOMPARE(field->isComplete(), true);

	field->setValue("new_text");
	CHECK_NEW_VALUE("new_text");

	widget->lineEdit()->setText("second");
	CHECK_NEW_VALUE("second");

	field->setEditable(false);
	CHECK_NEW_VALUE(QString());
	QCOMPARE(field->isComplete(), false);

	field->setEditable(true);
	CHECK_NEW_VALUE("second");
	QCOMPARE(field->isComplete(), true);

	field->setEditable(false);
	CHECK_NEW_VALUE(QString());
	QCOMPARE(field->isComplete(), false);

	auto options = QStringList() << "first" << "second";
	field->addOptions(options, QVariantList() << 1 << 2);
	QCOMPARE(field->options(), options);
	for (int i = 0, n = options.length(); i < n; ++i)
		QCOMPARE(widget->itemText(i), options.at(i));

	CHECK_NEW_VALUE("second");
	QCOMPARE(field->currentUserData().toInt(), 2);
	field->setEditable(true);
	QVERIFY(spy.isEmpty());

	field->setValue("first");
	CHECK_NEW_VALUE("first");
	QCOMPARE(field->currentUserData().toInt(), 1);

	field->setEditable(false);
	QVERIFY(spy.isEmpty());

	field->setValue("second");
	CHECK_NEW_VALUE("second");
	QCOMPARE(field->currentUserData().toInt(), 2);

	field->setValue("third");
	field->setEditable(true);
	CHECK_NEW_VALUE("third");

	field->addOption("third", 3);
	QCOMPARE(field->currentUserData().toInt(), 3);
	QCOMPARE(widget->itemText(2), "third");
	QCOMPARE(field->options().at(2), "third");

	field->removeOption("second");
	QVERIFY(spy.isEmpty());
	QCOMPARE(field->options().length(), 2);
	QCOMPARE(widget->count(), 2);

	field->removeOption("third");
	CHECK_NEW_VALUE("first");
	QCOMPARE(field->options().length(), 1);
	QCOMPARE(widget->count(), 1);

	field->setValue(1);
	CHECK_NEW_VALUE(QString());

	TestValidator validator(5);
	QCOMPARE(field->isComplete(), false);
	field->setValidator(&validator);

	field->setValue("first");
	QCOMPARE(field->isComplete(), true);

	field->addOptions(QStringList() << "inv" << "invalid");

	field->setValue("inv");
	QCOMPARE(field->isComplete(), false);

	field->setValue("invalid");
	QCOMPARE(field->isComplete(), false);

	field->setValidator(0);
	QCOMPARE(field->isComplete(), true);
	field->setValidator(&validator);
	QCOMPARE(field->isComplete(), false);

	field->setEditable(true);
	QTest::keyPress(widget, Qt::Key_Backspace);
	QTest::keyPress(widget, Qt::Key_Backspace);
	QCOMPARE(field->isComplete(), true);

	QTest::keyPress(widget, Qt::Key_Backspace);
	QCOMPARE(field->isComplete(), false);

	QTest::keyPress(widget, Qt::Key_L);
	QTest::keyPress(widget, Qt::Key_I);
	QCOMPARE(field->isComplete(), true);
	QCOMPARE(field->value().toString(), "inval");

	field->setValidator(0);
	field->setEditable(false);
	field->setValidator(&validator);
	QCOMPARE(field->validator(), &validator);
	QVERIFY(widget->validator() == NULL);
	field->setEditable(true);
	QCOMPARE(widget->validator(), &validator);

#undef CHECK_NEW_VALUE
}

void DataFormsTest::testStringField()
{
	DataForm dataForm;
	checkMainApi(&dataForm, [&](const QString &label, const QString &name) {
		return dataForm.addString(QString(), label, name);
	});

	auto field = dataForm.addString("text");
	auto widget = qobject_cast<QLineEdit*>(field->widget());
	auto spy = createStaticSignalSpy(field->onValueChanged);

	QVERIFY(widget);
	CHECK_VALUE_HELPER(String, text(), "text");
	QCOMPARE(field->isComplete(), true);

	QVERIFY(!field->isPasswordMode());
	QCOMPARE(widget->echoMode(), QLineEdit::Normal);

	field->setPasswordMode(true);
	QVERIFY(field->isPasswordMode());
	QCOMPARE(widget->echoMode(), QLineEdit::Password);

	field->setPasswordMode(false);
	QVERIFY(!field->isPasswordMode());
	QCOMPARE(widget->echoMode(), QLineEdit::Normal);

#define CHECK_NEW_VALUE(newVal) \
	CHECK_NEW_VALUE_HELPER(String, text(), newVal)

	field->setValue("new_text");
	CHECK_NEW_VALUE("new_text");
	QCOMPARE(field->isComplete(), true);

	field->setValue(1);
	CHECK_NEW_VALUE(QString());
	QCOMPARE(field->isComplete(), false);

	widget->setText("text");
	CHECK_NEW_VALUE("text");
	QCOMPARE(field->isComplete(), true);

	TestValidator validator(4);
	field->setValidator(&validator);
	QCOMPARE(field->isComplete(), true);

	widget->setText("123");
	CHECK_NEW_VALUE("123");
	QCOMPARE(field->isComplete(), false);

	field->setValidator(0);
	QCOMPARE(field->isComplete(), true);
	field->setValidator(&validator);
	QCOMPARE(field->isComplete(), false);

	QTest::keyPress(widget, Qt::Key_4);
	QTest::keyPress(widget, Qt::Key_5);
	CHECK_NEW_VALUE("1234");
	QCOMPARE(field->isComplete(), true);

#undef CHECK_NEW_VALUE

}

void DataFormsTest::testStringListField()
{
	DataForm dataForm;
	checkMainApi(&dataForm, [&](const QString &label, const QString &name) {
		return dataForm.addStringList(QStringList(), label, name);
	});

	auto field = dataForm.addStringList(QStringList());
	auto widget = field->widget();
	auto spy = createStaticSignalSpy(field->onValueChanged);

	auto view = widget->findChild<QListView*>("listView");
	auto model = view->model();
	auto addButton = widget->findChild<QPushButton*>("addButton");
	auto removeButton = widget->findChild<QPushButton*>("removeButton");

	QVERIFY(view);
	QVERIFY(addButton);
	QVERIFY(removeButton);

	QCOMPARE(field->value().type(), QVariant::StringList);
	QVERIFY(field->value().toStringList().isEmpty());
	QCOMPARE(field->isComplete(), false);

#define CHECK_NEW_VALUE(newVal) \
	QVERIFY(field->value().type() == QVariant::StringList); \
	QCOMPARE(field->value().toStringList(), newVal); \
	for (int i = 0, n = newVal.count(); i < n; ++i) \
		QCOMPARE(model->data(model->index(i, 0)).toString(), newVal.at(i)); \
	QCOMPARE(spy.count(), 1); \
	spy.removeFirst();

	auto list = QStringList() << "first" << "second";
	field->setValue(list);
	CHECK_NEW_VALUE(list);
	QCOMPARE(field->isComplete(), true);

	QTest::mouseClick(addButton, Qt::LeftButton);
	list << QString();
	CHECK_NEW_VALUE(list);

	model->setData(model->index(2, 0), "third");
	list[2] = "third";
	CHECK_NEW_VALUE(list);

	view->setCurrentIndex(model->index(1, 0));
	QTest::mouseClick(removeButton, Qt::LeftButton);
	list.removeAt(1);
	CHECK_NEW_VALUE(list);

	list.removeAt(1);
	field->setValue(list);
	CHECK_NEW_VALUE(list);

	field->setValue(1);
	CHECK_NEW_VALUE(QStringList());
	QCOMPARE(field->isComplete(), false);

	TestValidator validator(3);
	field->setValidator(&validator);
	QCOMPARE(field->isComplete(), false);

	field->setValue(QStringList() << "val");
	QCOMPARE(field->isComplete(), true);

	field->setValue(QStringList() << "val" << "long one");
	QCOMPARE(field->isComplete(), false);

	field->setValidator(0);
	QCOMPARE(field->isComplete(), true);
	field->setValidator(&validator);
	QCOMPARE(field->isComplete(), false);

	model->removeRow(1);
	QCOMPARE(field->isComplete(), true);

	model->removeRow(0);
	QCOMPARE(field->isComplete(), false);

#undef CHECK_NEW_VALUE
}

void DataFormsTest::testMultiStringField()
{
	DataForm dataForm;
	checkMainApi(&dataForm, [&](const QString &label, const QString &name) {
		return dataForm.addMultiString(QString(), label, name);
	});

	auto field = dataForm.addMultiString("text");
	auto widget = qobject_cast<QTextEdit*>(field->widget());
	auto spy = createStaticSignalSpy(field->onValueChanged);

	QVERIFY(widget);
	CHECK_VALUE_HELPER(String, toPlainText(), "text");

#define CHECK_NEW_VALUE(newVal) \
	CHECK_NEW_VALUE_HELPER(String, toPlainText(), newVal)

	field->setValue("new\ntext");
	CHECK_NEW_VALUE("new\ntext");

	field->setValue(1);
	CHECK_NEW_VALUE(QString());

	widget->setText("text");
	CHECK_NEW_VALUE("text");

#undef CHECK_NEW_VALUE
}

void DataFormsTest::testBoolField()
{
	DataForm dataForm;
	checkMainApi(&dataForm,
		[&](const QString &label, const QString &name)
		{
			return dataForm.addBool(false, label, name);
		},
		[&](DataField *field)
		{
			return qobject_cast<QCheckBox*>(field->widget())->text();
		});

	auto field = dataForm.addBool(false);
	auto widget = qobject_cast<QCheckBox*>(field->widget());
	auto spy = createStaticSignalSpy(field->onValueChanged);

	QVERIFY(widget);
	CHECK_VALUE_HELPER(Bool, isChecked(), false);

#define CHECK_NEW_VALUE(newVal) \
	CHECK_NEW_VALUE_HELPER(Bool, isChecked(), newVal)

	field->setValue(true);
	CHECK_NEW_VALUE(true);

	field->setValue(1);
	CHECK_NEW_VALUE(false);

	field->setValue(true);
	CHECK_NEW_VALUE(true);

	field->setValue(false);
	CHECK_NEW_VALUE(false);

#undef CHECK_NEW_VALUE
}

void DataFormsTest::testIntField()
{
	DataForm dataForm;
	checkMainApi(&dataForm, [&](const QString &label, const QString &name) {
		return dataForm.addInt(0, label, name);
	});

	auto field = dataForm.addInt(0);
	auto widget = qobject_cast<QSpinBox*>(field->widget());
	auto spy = createStaticSignalSpy(field->onValueChanged);

	QVERIFY(widget);
	CHECK_VALUE_HELPER(Int, value(), 0);
	QCOMPARE(field->minimum(), 0);
	QCOMPARE(widget->minimum(), 0);
	QCOMPARE(field->maximum(), 99);
	QCOMPARE(widget->maximum(), 99);

#define CHECK_NEW_VALUE(newVal) \
	CHECK_NEW_VALUE_HELPER(Int, value(), newVal)

	field->setValue(5);
	CHECK_NEW_VALUE(5);

	field->setValue(QString());
	CHECK_VALUE_HELPER(Int, value(), 5);

	field->setValue(2);
	CHECK_NEW_VALUE(2);

	field->setMinimum(3);
	CHECK_NEW_VALUE(3);
	QCOMPARE(field->minimum(), 3);
	QCOMPARE(widget->minimum(), 3);

	field->setMaximum(2);
	CHECK_NEW_VALUE(2);
	QCOMPARE(field->maximum(), 2);
	QCOMPARE(widget->maximum(), 2);

	field->setRange(1, 10);
	QCOMPARE(field->minimum(), 1);
	QCOMPARE(widget->minimum(), 1);
	QCOMPARE(field->maximum(), 10);
	QCOMPARE(widget->maximum(), 10);
	QVERIFY(spy.isEmpty());

#undef CHECK_NEW_VALUE
}

void DataFormsTest::testDoubleField()
{
	DataForm dataForm;
	checkMainApi(&dataForm, [&](const QString &label, const QString &name) {
		return dataForm.addDouble(0.0, label, name);
	});

	auto field = dataForm.addDouble(0.0);
	auto widget = qobject_cast<QDoubleSpinBox*>(field->widget());
	auto spy = createStaticSignalSpy(field->onValueChanged);

	QVERIFY(widget);
	CHECK_VALUE_HELPER(Double, value(), 0.0);
	QCOMPARE(field->minimum(), 0.0);
	QCOMPARE(widget->minimum(), 0.0);
	QCOMPARE(field->maximum(), 99.99);
	QCOMPARE(widget->maximum(), 99.99);

#define CHECK_NEW_VALUE(newVal) \
	CHECK_NEW_VALUE_HELPER(Double, value(), newVal)

	field->setValue(5.3);
	CHECK_NEW_VALUE(5.3);

	field->setValue(QString());
	CHECK_VALUE_HELPER(Double, value(), 5.3);

	field->setValue(2.4);
	CHECK_NEW_VALUE(2.4);

	field->setMinimum(3.5);
	CHECK_NEW_VALUE(3.5);
	QCOMPARE(field->minimum(), 3.5);
	QCOMPARE(widget->minimum(), 3.5);

	field->setMaximum(2.1);
	CHECK_NEW_VALUE(2.1);
	QCOMPARE(field->maximum(), 2.1);
	QCOMPARE(widget->maximum(), 2.1);

	field->setRange(1.0, 9.9);
	QCOMPARE(field->minimum(), 1.0);
	QCOMPARE(widget->minimum(), 1.0);
	QCOMPARE(field->maximum(), 9.9);
	QCOMPARE(widget->maximum(), 9.9);
	QVERIFY(spy.isEmpty());

#undef CHECK_NEW_VALUE
}

void DataFormsTest::testDateField()
{
	DataForm dataForm;
	checkMainApi(&dataForm, [&](const QString &label, const QString &name) {
		return dataForm.addDate(QDate(), label, name);
	});

	QDate mostImportantDayEver(1989, 7, 24);
	auto field = dataForm.addDate(mostImportantDayEver);
	auto widget = qobject_cast<QDateEdit*>(field->widget());
	auto spy = createStaticSignalSpy(field->onValueChanged);

	QVERIFY(widget);
	CHECK_VALUE_HELPER(Date, date(), mostImportantDayEver);
	QCOMPARE(field->minimum(), QDate(1752, 9, 14));
	QCOMPARE(widget->minimumDate(), QDate(1752, 9, 14));
	QCOMPARE(field->maximum(), QDate(7999, 12, 31));
	QCOMPARE(widget->maximumDate(), QDate(7999, 12, 31));

#define CHECK_NEW_VALUE(newVal) \
	CHECK_NEW_VALUE_HELPER(Date, date(), newVal)

	QDate date(2012, 4, 1);
	field->setValue(date);
	CHECK_NEW_VALUE(date);

	field->setValue(QString());
	CHECK_VALUE_HELPER(Date, date(), date);

	date = date.addDays(2);
	field->setValue(date);
	CHECK_NEW_VALUE(date);

	date = date.addDays(2);
	field->setMinimum(date);
	CHECK_NEW_VALUE(date);
	QCOMPARE(field->minimum(), date);
	QCOMPARE(widget->minimumDate(), date);

	date = date.addDays(-4);
	field->setMaximum(date);
	CHECK_NEW_VALUE(date);
	QCOMPARE(field->maximum(), date);
	QCOMPARE(widget->maximumDate(), date);

	auto min = date.addDays(-2);
	auto max = date.addDays(2);
	field->setRange(min, max);
	QCOMPARE(field->minimum(), min);
	QCOMPARE(widget->minimumDate(), min);
	QCOMPARE(field->maximum(), max);
	QCOMPARE(widget->maximumDate(), max);
	QVERIFY(spy.isEmpty());

#undef CHECK_NEW_VALUE
}

void DataFormsTest::testDateTimeField()
{
	DataForm dataForm;
	checkMainApi(&dataForm, [&](const QString &label, const QString &name) {
		return dataForm.addDateTime(QDateTime(), label, name);
	});

	QDateTime mostImportantDayEver(QDate(1989, 7, 24));
	auto field = dataForm.addDateTime(mostImportantDayEver);
	auto widget = qobject_cast<QDateTimeEdit*>(field->widget());
	auto spy = createStaticSignalSpy(field->onValueChanged);

	QVERIFY(widget);
	CHECK_VALUE_HELPER(DateTime, dateTime(), mostImportantDayEver);

	QDateTime defMin(QDate(1752, 9, 14), QTime(0, 0));
	QCOMPARE(field->minimum(), defMin);
	QCOMPARE(widget->minimumDateTime(), defMin);

	QDateTime defMax(QDate(7999, 12, 31), QTime(23, 59, 59, 999));
	QCOMPARE(field->maximum(), defMax);
	QCOMPARE(widget->maximumDateTime(), defMax);

#define CHECK_NEW_VALUE(newVal) \
	CHECK_NEW_VALUE_HELPER(DateTime, dateTime(), newVal)

	QDateTime date(QDate(2012, 4, 1), QTime(19, 58));
	field->setValue(date);
	CHECK_NEW_VALUE(date);

	field->setValue(QString());
	CHECK_VALUE_HELPER(DateTime, dateTime(), date);

	date = date.addDays(2);
	field->setValue(date);
	CHECK_NEW_VALUE(date);

	date = date.addDays(2);
	field->setMinimum(date);
	CHECK_NEW_VALUE(date);
	QCOMPARE(field->minimum(), date);
	QCOMPARE(widget->minimumDateTime(), date);

	date = date.addDays(-4);
	field->setMaximum(date);
	CHECK_NEW_VALUE(date);
	QCOMPARE(field->maximum(), date);
	QCOMPARE(widget->maximumDateTime(), date);

	auto min = date.addDays(-2);
	auto max = date.addDays(2);
	field->setRange(min, max);
	QCOMPARE(field->minimum(), min);
	QCOMPARE(widget->minimumDateTime(), min);
	QCOMPARE(field->maximum(), max);
	QCOMPARE(widget->maximumDateTime(), max);
	QVERIFY(spy.isEmpty());

#undef CHECK_NEW_VALUE
}

void DataFormsTest::testPixmapChooserField()
{
	DataForm dataForm;
	checkMainApi(&dataForm, [&](const QString &label, const QString &name) {
		return dataForm.addPixmap(QString(), label, name);
	});

	QString pixmap1Path(":/pictures/bullet-send.png");
	QString pixmap2Path(":/pictures/bullet-received.png");
	QString largePixmapPath(":/pictures/wizard.png");

	QPixmap pixmap1(pixmap1Path);
	QPixmap pixmap2(pixmap2Path);
	QPixmap largePixmap(largePixmapPath);

	auto field = dataForm.addPixmap(pixmap1Path);
	auto mainWidget = field->widget();
	auto spy = createStaticSignalSpy(field->onValueChanged);

	QVERIFY(mainWidget);
	auto widget = mainWidget->findChild<QLabel*>("pixmapLabel");
	auto setButton = mainWidget->findChild<QPushButton*>("setButton");
	auto removeButton = mainWidget->findChild<QPushButton*>("removeButton");

	QVERIFY(widget);
	QVERIFY(setButton);
	QVERIFY(removeButton);

	CHECK_VALUE_HELPER_EX(String, pixmap()->cacheKey(), pixmap1Path, pixmap1.cacheKey());

#define CHECK_NEW_VALUE(newPath, newVal) \
	CHECK_NEW_VALUE_HELPER_EX(String, pixmap()->cacheKey(), newPath, newVal.cacheKey());

	field->setValue(pixmap2Path);
	CHECK_NEW_VALUE(pixmap2Path, pixmap2);

	field->setValue(pixmap2Path);
	QVERIFY(spy.isEmpty());

	field->setDefaultImage(pixmap1);
	QVERIFY(spy.isEmpty());
	QCOMPARE(field->defaultImage().cacheKey(), pixmap1.cacheKey());
	QTest::mouseClick(removeButton, Qt::LeftButton);
	CHECK_NEW_VALUE(QString(), pixmap1);

	field->setDefaultImage(pixmap2);
	CHECK_NEW_VALUE(QString(), pixmap2);

	field->setValue(largePixmapPath);
	CHECK_NEW_VALUE(largePixmapPath, largePixmap);
	QCOMPARE(largePixmap.size(), field->pixmap().size());
	field->setMaximumSize(QSize(32, 32));
	QCOMPARE(field->maximumSize(), QSize(32, 32));

	QSize shrunkSize = field->pixmap().size();
	QCOMPARE(shrunkSize.height(), 32);
	QVERIFY(shrunkSize.width() < 32);

#undef CHECK_NEW_VALUE
}

void DataFormsTest::testDataFormComplition()
{
	DataForm dataForm;
	auto stringChooser = dataForm.addStringChooser("second");
	auto string = dataForm.addString(QString());
	auto list = dataForm.addStringList(QStringList() << "value");
	QSignalSpy spy(&dataForm, SIGNAL(completeChanged(bool)));
	TestValidator validator(5);

	QCOMPARE(dataForm.isComplete(), true);

#define CHECK_VALUE(val) \
	QCOMPARE(dataForm.isComplete(), val); \
	QVERIFY(spy.isEmpty());

#define CHECK_NEW_VALUE(val) \
	QCOMPARE(dataForm.isComplete(), val); \
	QVERIFY(!spy.isEmpty() && !spy.first().isEmpty()); \
	QCOMPARE(spy.takeFirst().first().toBool(), val); \
	QVERIFY(spy.isEmpty());

	stringChooser->setMandatory(true);
	CHECK_NEW_VALUE(false);

	stringChooser->addOption("first");
	CHECK_NEW_VALUE(true);

	stringChooser->addOption("second");
	CHECK_VALUE(true);

	stringChooser->setValidator(&validator);
	CHECK_NEW_VALUE(false);

	stringChooser->removeOption("second");
	CHECK_NEW_VALUE(true);

	stringChooser->setValidator(0);
	CHECK_VALUE(true);

	stringChooser->addOption("second");
	stringChooser->setValue("second");
	CHECK_VALUE(true);


	string->setMandatory(true);
	CHECK_NEW_VALUE(false);

	string->setValue("text");
	CHECK_NEW_VALUE(true);

	string->setValidator(&validator);
	CHECK_NEW_VALUE(false);

	string->setValue("value");
	CHECK_NEW_VALUE(true);

	string->setValue("other");
	CHECK_VALUE(true);

	string->setValidator(0);
	CHECK_VALUE(true);

	stringChooser->setMandatory(false);
	CHECK_VALUE(true);


	list->setMandatory(true);
	CHECK_VALUE(true);

	list->setValue(QStringList());
	CHECK_NEW_VALUE(false);

	list->setValue(QStringList() << "text");
	CHECK_NEW_VALUE(true);

	list->setValidator(&validator);
	CHECK_NEW_VALUE(false);

	list->setValue(QStringList() << "value");
	CHECK_NEW_VALUE(true);

	list->setValidator(0);
	CHECK_VALUE(true);

	stringChooser->setMandatory(true);
	CHECK_VALUE(true);

	list->setValue(QStringList());
	CHECK_NEW_VALUE(false);

	list->setMandatory(false);
	CHECK_NEW_VALUE(true);

#undef CHECK_NEW_VALUE
}

void DataFormsTest::testDialogButtons()
{
	DataForm dataForm;
	int flags = 0;
	QVERIFY(dataForm.findChild<QDialogButtonBox*>() == NULL);

	dataForm.addDialogButton(DataForm::Ok, [&] {
		QCOMPARE(flags, 0);
		flags |= 0x01;
	});
	auto buttonBox = dataForm.findChild<QDialogButtonBox*>();
	QVERIFY(buttonBox);
	QCOMPARE(buttonBox->buttons().count(), 1);

	dataForm.addDialogButton("no", DataForm::RejectRole, [&] {
		QCOMPARE(flags, 0x01);
		flags |= 0x02;
	});
	auto noButton = buttonBox->buttons().at(1);
	QCOMPARE(noButton->text(), "no");
	QCOMPARE(buttonBox->buttons().count(), 2);

	QTest::mouseClick(buttonBox->button(QDialogButtonBox::Ok), Qt::LeftButton);
	QTest::mouseClick(noButton, Qt::LeftButton);
	QCOMPARE(flags, 0x03);
}

UNITTEST(DataFormsTest);

} // Lime
