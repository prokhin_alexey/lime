/*
	 This file is part of Lime.
	 Copyright (C) 2012 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIME_TST_CHATUNIT_H_
#define LIME_TST_CHATUNIT_H_

#include "testutils.h"
#include <protocols/null/nullcontact.h>
#include <protocols/null/nullaccount.h>
#include <protocols/null/nullprotocol.h>
#include <QTextDocument>
#include <lime/chatmanager.h>

namespace Lime
{

class NullChatManager : public ChatManager
{
public:
	void activateChat(ChatUnit *unit)
	{
		if (!unit->isChatOpened())
			openChat(unit);
		setActive(unit, true);
	}
	void deactivateChat(ChatUnit *unit)
	{
		setActive(unit, false);
	}
	void openChat(ChatUnit *unit)
	{
		handleChatOpening(unit);
	}
	void closeChat(ChatUnit *unit)
	{
		deactivateChat(unit);
		handleChatClosing(unit);
	}
	void appendMessage(const Message &message)
	{
		newMessage(message);
	}
	void sendChatState(ChatUnit *unit, ChatUnit::ChatState state)
	{
		ChatManager::sendChatState(unit, state);
	}
	QTextDocument *document(ChatUnit *unit) { return &doc; }
	QObject *toQObject() { return 0; }
	Signal<void(Message)> newMessage;
protected:
	bool enableImpl() { return false; }
	bool disableImpl() { return false; }
private:
	QTextDocument doc;
};

class ChatUnitTest : public QObject, public SignalGuard
{
	Q_OBJECT
private slots:
	void initTestCase();
	void cleanupTestCase();
	void testName();
	void testTitle();
	void testChatApi();
	void testUnreadMessages();
	void testIncomingMessages();
	void testChatState();
private:
	NullProtocol *m_proto;
	NullAccount *m_account;
	NullChatManager *m_chatManager;
};

} // Lime

#endif // LIME_TST_CHATUNIT_H_
