/*
	 This file is part of Lime.
	 Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIME_CONTACTMODELTEST_H_
#define LIME_CONTACTMODELTEST_H_

#include <lime/contactlist/model/contactlistmodel.h>
#include <protocols/null/nullcontact.h>
#include <protocols/null/nullaccount.h>
#include <protocols/null/nullprotocol.h>
#include <protocols/null/nullconference.h>
#include "testutils.h"

namespace Lime
{

class ContactModelTest : public QObject
{
	Q_OBJECT
public:
	typedef QScopedPointer<NullProtocol> ProtocolPtr;
	typedef QScopedPointer<NullAccount> AccountPtr;
	typedef QScopedPointer<ContactListModel> ModelPtr;
	ContactModelTest();
	virtual ~ContactModelTest();
private slots:
	void plainModel();
	void groupModel();
	void accountModel();
	void groupAccountModel();
	void plainModelStatusTest();
	void groupModelStatusTest();
	void accountModelStatusTest();
	void groupAccountModelStatusTest();
	void plainModelConferenceTest();
	void groupModelConferenceTest();
	void accountModelConferenceTest();
	void groupAccountModelConferenceTest();
private:
	void statusTest(int defFlags, std::function<QModelIndex(QAbstractItemModel*)> getParent);
	void conferenceTest(int defFlags, std::function<QModelIndex(QAbstractItemModel*)> getParent);
};

} // Lime

#endif // LIME_CONTACTMODELTEST_H_
