/*
	 This file is part of Lime.
	 Copyright (C) 2011 Alexey Prokhin <alexey.prokhin@yandex.ru>

	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "contactmodeltest.h"

namespace Lime
{

#define ROW(row) \
	model->index(row, 0, QModelIndex())

#define ROW2(row,parent) \
	model->index(row, 0, parent)

#define ROW3(row) \
	model->index(row, 0, getParent(model.data()))

#define CHECK(row,value) \
	QCOMPARE(model->data(row).toString(), QString(value));

#define CHECK_LEN(len) \
	QCOMPARE(model->rowCount(), len)

#define CHECK_LEN2(row, len) \
	QCOMPARE(model->rowCount(row), len)

#define CHECK_LEN3(len) \
	QCOMPARE(model->rowCount(getParent(model.data())), len)

ContactModelTest::ContactModelTest()
{
}

ContactModelTest::~ContactModelTest()
{
}

void ContactModelTest::plainModel()
{
	ProtocolPtr proto(new NullProtocol);
	AccountPtr acc(new NullAccount("test"));
	ModelPtr model(new ContactListModel);
	model->setFlags(0);

	auto contact1 = acc->createContactImpl("1");
	model->addContact(contact1);
	contact1->addToContactList();

	auto contact2 = acc->createContactImpl("2");
	model->addContact(contact2);
	contact2->addToContactList();

	auto contact3 = acc->createContactImpl("3");
	model->addContact(contact3);
	contact3->addToContactList();

	CHECK(ROW(0), "1");
	CHECK(ROW(1), "2");
	CHECK(ROW(2), "3");

	contact1->setName("Alexey");
	CHECK(ROW(0), "2");
	CHECK(ROW(1), "3");
	CHECK(ROW(2), "Alexey");

	contact2->setName("Igor");
	contact3->setName("Andrey");
	CHECK(ROW(0), "Alexey");
	CHECK(ROW(1), "Andrey");
	CHECK(ROW(2), "Igor");

	model->removeContact(contact3);
	CHECK(ROW(0), "Alexey");
	CHECK(ROW(1), "Igor");
}

void ContactModelTest::groupModel()
{
	ProtocolPtr proto(new NullProtocol);
	AccountPtr acc(new NullAccount("test"));
	ModelPtr model(new ContactListModel);
	model->setFlags(ShowGroups);

	auto contact1 = acc->createContactImpl("1");
	contact1->setGroups(QStringList() << "group1" << "group2");
	model->addContact(contact1);
	contact1->addToContactList();

	auto contact2 = acc->createContactImpl("2");
	contact2->setGroups(QStringList() << "group2" << "group3");
	model->addContact(contact2);
	contact2->addToContactList();

	auto contact3 = acc->createContactImpl("3");
	contact3->setGroups(QStringList() << "group1" << "group3");
	model->addContact(contact3);
	contact3->addToContactList();

	auto group = ROW(0);
	CHECK(group, "group1");
	CHECK(ROW2(0, group), "1");
	CHECK(ROW2(1, group), "3");

	group = ROW(1);
	CHECK(group, "group2");
	CHECK(ROW2(0, group), "1");
	CHECK(ROW2(1, group), "2");

	group = ROW(2);
	CHECK(group, "group3");
	CHECK(ROW2(0, group), "2");
	CHECK(ROW2(1, group), "3");

	contact1->setGroup("group2");
	contact2->setGroup("group1");
	contact3->setGroup("group2");

	group = ROW(0);
	CHECK(group, "group1");
	CHECK(ROW2(0, group), "2");

	group = ROW(1);
	CHECK(group, "group2");
	CHECK(ROW2(0, group), "1");
	CHECK(ROW2(1, group), "3");

	contact1->setName("Andrey");
	contact3->setName("Alexey");
	CHECK(ROW2(0, group), "Alexey");
	CHECK(ROW2(1, group), "Andrey");

	model->removeContact(contact3);
	CHECK(ROW2(0, group), "Andrey");

	contact1->setGroups(QStringList());
	CHECK_LEN(2);
	CHECK(ROW(0), "Andrey");
	CHECK(ROW(1), "group1");
}

void ContactModelTest::accountModel()
{
	ProtocolPtr proto(new NullProtocol);
	AccountPtr acc1(new NullAccount("account1"));
	AccountPtr acc2(new NullAccount("account2"));
	ModelPtr model(new ContactListModel);
	model->setFlags(ShowAccounts);

	auto contact1 = acc1->createContactImpl("1");
	model->addContact(contact1);
	contact1->addToContactList();

	auto contact2 = acc1->createContactImpl("2");
	model->addContact(contact2);
	contact2->addToContactList();

	auto contact3 = acc2->createContactImpl("3");
	model->addContact(contact3);
	contact3->addToContactList();

	auto acc = ROW(1);
	CHECK(acc, "account2");
	CHECK(ROW2(0, acc), "3");

	acc = ROW(0);
	CHECK(acc, "account1");
	CHECK(ROW2(0, acc), "1");
	CHECK(ROW2(1, acc), "2");

	contact1->setName("Andrey");
	contact2->setName("Alexey");
	CHECK(ROW2(0, acc), "Alexey");
	CHECK(ROW2(1, acc), "Andrey");

	model->removeContact(contact2);
	CHECK(ROW2(0, acc), "Andrey");
}

void ContactModelTest::groupAccountModel()
{
	ProtocolPtr proto(new NullProtocol);
	AccountPtr acc1(new NullAccount("account1"));
	AccountPtr acc2(new NullAccount("account2"));
	ModelPtr model(new ContactListModel);
	model->setFlags(ShowAccounts | ShowGroups);

	auto contact1 = acc1->createContactImpl("1");
	contact1->setGroups(QStringList() << "group1" << "group2");
	model->addContact(contact1);
	contact1->addToContactList();

	auto contact2 = acc1->createContactImpl("2");
	contact2->setGroup("group1");
	model->addContact(contact2);
	contact2->addToContactList();

	auto contact3 = acc2->createContactImpl("3");
	contact3->setGroup("group1");
	model->addContact(contact3);
	contact3->addToContactList();

	auto acc = ROW(0);
	CHECK(acc, "account1");

	auto group = ROW2(0, acc);
	CHECK(group, "group1");
	CHECK(ROW2(0, group), "1");
	CHECK(ROW2(1, group), "2");

	group = ROW2(1, acc);
	CHECK(group, "group2");
	CHECK(ROW2(0, group), "1");

	contact2->setGroup("group2");
	CHECK(ROW2(1, group), "2");

	contact2->setGroups(QStringList());
	CHECK_LEN2(acc, 3);
	CHECK(ROW2(0, acc), "2");
	CHECK(ROW2(1, acc), "group1");
	CHECK(ROW2(2, acc), "group2");

	acc = ROW(1);
	CHECK(acc, "account2");
	group = ROW2(0, acc);
	CHECK(ROW2(0, group), "3");

	AccountPtr acc3(new NullAccount("account3"));
	auto contact4 = acc3->createContactImpl("4");
	auto contact5 = acc3->createContactImpl("5");
	auto contact6 = acc3->createContactImpl("6");
	model->addContact(contact5);
	model->addContact(contact4);
	model->addContact(contact6);
	contact5->addToContactList();
	contact6->addToContactList();
	contact4->addToContactList();

	acc = ROW(2);
	CHECK(acc, "account3");
	CHECK(ROW2(0, acc), "4");
	CHECK(ROW2(1, acc), "5");
	CHECK(ROW2(2, acc), "6");

	model->removeContact(contact5);
	CHECK(ROW2(0, acc), "4");
	CHECK(ROW2(1, acc), "6");
}

void ContactModelTest::plainModelStatusTest()
{
	statusTest(0, [](QAbstractItemModel *model) {
		return QModelIndex();
	});
}

void ContactModelTest::groupModelStatusTest()
{
	statusTest(ShowGroups, [](QAbstractItemModel *model) {
		return QModelIndex();
	});
}

void ContactModelTest::accountModelStatusTest()
{
	statusTest(ShowAccounts, [](QAbstractItemModel *model) {
		return ROW(0);
	});
}

void ContactModelTest::groupAccountModelStatusTest()
{
	statusTest(ShowGroups | ShowAccounts, [](QAbstractItemModel *model) {
		return ROW(0);
	});
}

void ContactModelTest::statusTest(int defFlags, std::function<QModelIndex(QAbstractItemModel*)> getParent)
{
	ProtocolPtr proto(new NullProtocol);
	AccountPtr acc(new NullAccount("account1"));
	ModelPtr model(new ContactListModel);
	model->setFlags(defFlags);

	auto contact1 = acc->createContactImpl("1");
	contact1->setStatus(Offline);
	model->addContact(contact1);
	contact1->addToContactList();

	auto contact2 = acc->createContactImpl("2");
	contact2->setStatus(Offline);
	model->addContact(contact2);
	contact2->addToContactList();

	auto contact3 = acc->createContactImpl("3");
	contact3->setStatus(Offline);
	model->addContact(contact3);
	contact3->addToContactList();

	auto contact4 = acc->createContactImpl("4");
	contact4->setStatus(Away);
	model->addContact(contact4);
	contact4->addToContactList();

	CHECK_LEN3(1);
	CHECK(ROW3(0), "4");

	contact4->setStatus(Offline);
	CHECK_LEN3(0);

	contact2->setStatus(Online);
	contact1->setStatus(DND);
	contact3->setStatus(Away);
	CHECK_LEN3(3);
	CHECK(ROW3(0), "1");
	CHECK(ROW3(1), "2");
	CHECK(ROW3(2), "3");

	model->setFlags(defFlags | SortByStatus);
	CHECK(ROW3(0), "2");
	CHECK(ROW3(1), "3");
	CHECK(ROW3(2), "1");

	model->setFlags(defFlags | ShowInactive);
	CHECK_LEN3(4);
	CHECK(ROW3(3), "4");

	contact2->setStatus(Offline);
	CHECK(ROW3(1), "2");
	model->setFlags(defFlags | ShowInactive | SortByStatus);
	CHECK(ROW3(0), "3");
	CHECK(ROW3(1), "1");
	CHECK(ROW3(2), "2");
	CHECK(ROW3(3), "4");

	model->setFlags(defFlags | ShowInactive | ShowSeparators);
	CHECK_LEN3(6);
	CHECK(ROW3(0), tr("Online"));
	CHECK(ROW3(1), "1");
	CHECK(ROW3(2), "3");
	CHECK(ROW3(3), tr("Offline"));
	CHECK(ROW3(4), "2");
	CHECK(ROW3(5), "4");

	model->setFlags(defFlags | ShowInactive | ShowSeparators | SortByStatus);
	CHECK(ROW3(1), "3");
	CHECK(ROW3(2), "1");

	model->setFlags(defFlags | ShowSeparators | SortByStatus);
	CHECK_LEN3(3);

	contact1->setStatus(Offline);
	contact3->setStatus(Offline);
	CHECK_LEN3(0);

	// Check checkContactsVisiblity()
	contact2->setStatus(Online);
	contact3->setStatus(Online);
	contact1->setStatus(Offline);
	contact4->setStatus(Offline);
	CHECK_LEN3(3);
	model->m_flags = model->m_flags | ShowInactive;
	model->checkContactsVisiblity();
	CHECK_LEN3(6);
	CHECK(ROW3(0), tr("Online"));
	CHECK(ROW3(1), "2");
	CHECK(ROW3(2), "3");
	CHECK(ROW3(3), tr("Offline"));
	CHECK(ROW3(4), "1");
	CHECK(ROW3(5), "4");
}

void ContactModelTest::plainModelConferenceTest()
{
	conferenceTest(ShowConferences, [](QAbstractItemModel *model) {
		return QModelIndex();
	});

	ProtocolPtr proto(new NullProtocol);
	AccountPtr acc(new NullAccount("account1"));
	acc->setStatus(Online);
	ModelPtr model(new ContactListModel);
	model->setFlags(ShowConferences | ShowInactive | SortByStatus);

	auto conf1 = acc->createConference("1");
	auto conf2 = acc->createConference("2");

	auto contact1 = acc->createContactImpl("3");
	contact1->setStatus(Offline);
	model->addContact(contact1);
	contact1->addToContactList();

	CHECK_LEN(3);
	CHECK(ROW(0), "1");
	CHECK(ROW(1), "2");
	CHECK(ROW(2), "3");

	contact1->setStatus(Online);
	CHECK(ROW(0), "3");
	CHECK(ROW(1), "1");
	CHECK(ROW(2), "2");

	model->setFlags(ShowConferences);
	conf1->join();
	CHECK_LEN(3);
	CHECK(ROW(0), "1");
	CHECK(ROW(1), "2");
	CHECK(ROW(2), "3");

	model->setFlags(ShowConferences | SortByStatus);
	CHECK(ROW(0), "1");
	CHECK(ROW(1), "3");
	CHECK(ROW(2), "2")

	model->setFlags(0);
	CHECK_LEN(1);
	CHECK(ROW(0), "3");

	model->setFlags(ShowConferences);
	acc->setStatus(Offline);
	CHECK_LEN(1);
	CHECK(ROW(0), "3");

	model->setFlags(ShowConferences | ShowInactive);
	CHECK_LEN(3);
	CHECK(ROW(0), "1");
	CHECK(ROW(1), "2");
	CHECK(ROW(2), "3");

	model->setFlags(ShowConferences);
	acc->setStatus(DND);
	CHECK_LEN(3);
}

void ContactModelTest::groupModelConferenceTest()
{
	conferenceTest(ShowConferences | ShowGroups, [](QAbstractItemModel *model) {
		return ROW(0);
	});
}

void ContactModelTest::accountModelConferenceTest()
{
	conferenceTest(ShowConferences | ShowAccounts, [](QAbstractItemModel *model) {
		return ROW(0);
	});
}

void ContactModelTest::groupAccountModelConferenceTest()
{
	conferenceTest(ShowConferences | ShowGroups | ShowAccounts, [](QAbstractItemModel *model) {
		return ROW2(0, ROW(0));
	});
}

void ContactModelTest::conferenceTest(int defFlags, std::function<QModelIndex(QAbstractItemModel*)> getParent)
{
	ProtocolPtr proto(new NullProtocol);
	AccountPtr acc(new NullAccount("account1"));
	ModelPtr model(new ContactListModel);
	acc->setStatus(Online);
	model->setFlags(defFlags | ShowInactive | SortByStatus);

	auto conf1 = acc->createConference("1");
	auto conf2 = acc->createConference("2");
	auto conf3 = acc->createConference("3");

	CHECK_LEN3(3);
	CHECK(ROW3(0), "1");
	CHECK(ROW3(1), "2");
	CHECK(ROW3(2), "3");

	conf2->join();
	CHECK(ROW3(0), "2");
	CHECK(ROW3(1), "1");

	model->setFlags(defFlags ^ ShowConferences);
	CHECK_LEN3((defFlags & ShowGroups) && (defFlags & ShowAccounts) ? 1 : 0);
}

UNITTEST(ContactModelTest);

} // Lime
