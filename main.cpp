#include <QApplication>
#include <lime/utils.h>
#include <lime/extention.h>
#include <lime/icon.h>
#include <QDate>
#include <QDir>
#if ENABLE_UNITTESTS
#include "tests/testutils.h"
#endif

using namespace Lime;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

#if ENABLE_UNITTESTS
	if (!runUnittests())
		return 1;
#endif

	ExtensionManager::instance()->initExtensions();
	a.setWindowIcon(Icon("lime"));

    return a.exec();
}
